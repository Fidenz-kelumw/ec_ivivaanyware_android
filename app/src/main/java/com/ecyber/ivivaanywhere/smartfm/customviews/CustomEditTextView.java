package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by ChathuraHettiarachchi on 4/8/16.
 */
public class CustomEditTextView extends EditText{

    private Context context;

    public CustomEditTextView(Context context) {
        super(context);
        this.context=context;
    }

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init(attrs);
    }

    public CustomEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        //if (attrs!=null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditTextView);
            String fontName = "MelbourneRegularBasic.otf";//a.getString(R.styleable.CustomEditTextView_edtFontName);
            if (fontName!=null) {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        //}
    }
}
