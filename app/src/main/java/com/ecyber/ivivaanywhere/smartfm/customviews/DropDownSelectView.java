package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.SingleListAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.FilterDataField;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetObjectForQrResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectForQRSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.InfoSync;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Lakmal on 5/2/2016.
 */
public class DropDownSelectView implements View.OnClickListener, GetObjectForQRSync.GetObjectForQRCallback, SingleListAdapter.SingleItemClickListener, InfoSync.InfoCallback {

    private Activity mActivity;
    private DropDownSelectTextListener mDownSelectTextListener;
    private InfoSync infoSync;
    private AccountPermission mAccountPermission;
    private ObjectInfoModel mObjectInfo;
    private SingleListAdapter mAdapter;

    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    private List<OptionListModel> mOptionList;
    private List<DataLookUp> mDataLookUps;
    private Dialog mDialogList;

    private View mDialogView;
    private View mView;

    private CustomButton mBtnSelect;
    private ProgressBar mProgressBar;
    private TextView mHeaderTitle;
    private CustomTextView mTvMessage;
    private CustomTextView mBtnSelectText;
    private ImageButton mTvQR;
    private RecyclerView mRvSingleList;
    private CustomEditTextView mSearchView;
    private CompoundBarcodeView qr;
    private RelativeLayout mRelCamContainer;
    private RelativeLayout mRelSearchContainer;
    private ImageButton mQRBack, btnQR;
    private CustomTextView mTvDropdownTitle;
    private RelativeLayout layQrBtn;

    private String lookUpService = "";
    private String mTitle;
    private String mObjectType;
    private String mObjectKey;
    private String mFieldId;
    private int mPosition;
    private boolean mIsSearch;
    private boolean mHideQR = false;
    private boolean isOnlyMembers = false;

    public DropDownSelectView(Activity activity, DropDownSelectTextListener selectTextListener, boolean isSearch, boolean hideQR, @ColorInt int color) {
        mActivity = activity;
        mDownSelectTextListener = selectTextListener;
        mIsSearch = isSearch;
        mColoredSnackBar = new ColoredSnackbar(mActivity);
        mNetwork = new NetworkCheck();
        mHideQR = hideQR;
        initView(color);
    }

    private void initView(@ColorInt int color) {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_select_dialog, null);

        mBtnSelect = (CustomButton) mView.findViewById(R.id.btn_dropdown_select);
        mBtnSelectText = (CustomTextView) mView.findViewById(R.id.btn_dropdown_select_text);
        mTvDropdownTitle = (CustomTextView) mView.findViewById(R.id.tv_dropdown_title);
        layQrBtn = (RelativeLayout) mView.findViewById(R.id.layQrBtn);
        btnQR = (ImageButton) mView.findViewById(R.id.btnQr);

        mBtnSelect.setOnClickListener(this);
        mTvDropdownTitle.setTextColor(color);
    }

    private void initDialog(boolean isOnlyQR) {
        mDialogList = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(mActivity.getApplicationContext());
        mDialogView = inflater.inflate(R.layout.dialog_list, null);

        ImageButton btnClose = (ImageButton) mDialogView.findViewById(R.id.img_btn_close);
        mRvSingleList = (RecyclerView) mDialogView.findViewById(R.id.rv_single_list);
        mSearchView = (CustomEditTextView) mDialogView.findViewById(R.id.et_searchView);
        mHeaderTitle = (TextView) mDialogView.findViewById(R.id.tv_dialog_header_title);
        mProgressBar = (ProgressBar) mDialogView.findViewById(R.id.progressBar);
        mTvMessage = (CustomTextView) mDialogView.findViewById(R.id.tv_message);
        mTvQR = (ImageButton) mDialogView.findViewById(R.id.btnQr);
        mQRBack = (ImageButton) mDialogView.findViewById(R.id.btn_qr_back);
        mRelCamContainer = (RelativeLayout) mDialogView.findViewById(R.id.QR_fragment_cam);
        mRelSearchContainer = (RelativeLayout) mDialogView.findViewById(R.id.rel_lay_search_container);
        qr = (CompoundBarcodeView) mDialogView.findViewById(R.id.zxing_barcode_scanner);
        qr.decodeContinuous(callback);
        mHeaderTitle.setText(mTitle);

        if(mHideQR) {
            mQRBack.setVisibility(View.GONE);
            mTvQR.setVisibility(View.GONE);
        }

        if (!isOnlyQR) {
            if (mIsSearch) {
                mRelSearchContainer.setVisibility(View.VISIBLE);
            } else {
                mRelSearchContainer.setVisibility(View.GONE);
            }

            if (mIsSearch) {
                if (mObjectInfo != null) {
                    if (mObjectInfo.getQrcode() != null && mObjectInfo.getQrcode().equals("1"))
                        mTvQR.setVisibility(View.VISIBLE);
                    else
                        mTvQR.setVisibility(View.GONE);
                }
            } else {
                mTvQR.setVisibility(View.VISIBLE);
            }

            mRvSingleList.setLayoutManager(new LinearLayoutManager(mActivity.getApplicationContext()));
            mRvSingleList.setHasFixedSize(true);

            mSearchView.addTextChangedListener(mTextWatcher);
        } else {
            mRelSearchContainer.setVisibility(View.GONE);
            mTvQR.setVisibility(View.GONE);
            mQRBack.setVisibility(View.GONE);
            mTvMessage.setVisibility(View.GONE);
            mRvSingleList.setVisibility(View.GONE);
            mTvQR.setVisibility(View.GONE);
            mRelCamContainer.setVisibility(View.VISIBLE);

            qr.resume();
        }

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogList.dismiss();
                try {
                    qr.pause();
                } catch (Exception e) {

                }
            }
        });

        mTvQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsSearch) {
                    mRelSearchContainer.setVisibility(View.GONE);
                }
                mTvMessage.setVisibility(View.GONE);
                mTvQR.setVisibility(View.INVISIBLE);
                mQRBack.setVisibility(View.VISIBLE);
                mRvSingleList.setVisibility(View.GONE);
                mRelCamContainer.setVisibility(View.VISIBLE);
                try {
                    qr.resume();
                } catch (Exception e) {

                }
            }
        });

        mQRBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsSearch) {
                    mRelSearchContainer.setVisibility(View.VISIBLE);
                    if (mAdapter.getItemCount() == 0)
                        mTvMessage.setVisibility(View.VISIBLE);
                }

                mTvQR.setVisibility(View.VISIBLE);
                mQRBack.setVisibility(View.INVISIBLE);
                mRvSingleList.setVisibility(View.VISIBLE);
                mRelCamContainer.setVisibility(View.GONE);
                try {
                    qr.pause();
                } catch (Exception e) {

                }
            }
        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 57;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;


        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mDialogList.setContentView(mDialogView);
        mDialogList.show();
        mDialogList.setCanceledOnTouchOutside(false);
        mDialogList.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialogList.getWindow().setLayout((int) x, (int) y);
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            try {
                qr.pause();
            } catch (Exception e) {

            }
            if (result.getText() != null && result.getBarcodeFormat().toString().equals("QR_CODE")) {
                getObjectForQr(result.getText());
            } else {
                Toast.makeText(mActivity, "QR code not supported.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };

    private void getObjectForQr(String data) {
        boolean state = NetworkCheck.IsAvailableNetwork(mActivity);
        Log.e("SMartFM", data.toString());
        Style style = new Style.Builder()
                .setTextColor(R.color.pure_white)
                .setBackgroundColor(R.color.update)
                .build();
        Crouton.makeText(mActivity, "Processing QR code", style, mRelCamContainer).show();
        if (state) {
            AccountPermission accountPermission = new AccountPermission();
            new GetObjectForQRSync(this).getObjectForQR(
                    accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), data, lookUpService, SmartConstants.OS);
        }
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mTvMessage.setVisibility(View.GONE);
            mTvMessage.setText("");
            if (mDataLookUps != null) {
                List<DataLookUp> filteredData = new FilterDataField().filterLookupData(Collections.unmodifiableList(mDataLookUps), s.toString().trim());
                if (filteredData.size() > 0) {
                    mAdapter.clearDataSet();
                    for (DataLookUp dataLookUp : filteredData) {
                        mAdapter.addItem(dataLookUp);
                    }

                } else {
                    mAdapter.clearDataSet();
                    mTvMessage.setText("No Result for \"" + s + "\"");
                    mTvMessage.setVisibility(View.VISIBLE);
                }
            } else {
                mTvMessage.setText("Could not fetch data.");
                mTvMessage.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public View getSelectDialogObjectInfo(ObjectInfoModel info, int position, String title) {
        mPosition = position;
        mObjectInfo = info;
        setDefaultValueToTag(mPosition);
        mTitle = title;
        lookUpService = info.getLookupservice();

        mBtnSelectText.setText("Select");

        if (info.getDisplaytext() != null && !info.getDisplaytext().trim().equals("")) {
            mBtnSelectText.setText(info.getDisplaytext());
        } else {
            List<ValueModel> valueModels = info.getValueModels();
            if (valueModels != null && valueModels.size() > 0) {
                if (valueModels.get(0).getValue() != null && valueModels.get(0).getValue().length() > 0) {
                    List<OptionListModel> optionListModels = info.getOptionListModels();
                    boolean isDataFound = false;
                    for (OptionListModel model : optionListModels) {
                        if (model.getValue().equals(valueModels.get(0).getValue())) {
                            isDataFound = true;
                            mBtnSelectText.setText(model.getDisplayText());
                            break;
                        }
                    }
                    if (!isDataFound)
                        mBtnSelectText.setText(valueModels.get(0).getValue());
                } else {
                    if (mObjectInfo.getNoDataValue() != null && !mObjectInfo.getNoDataValue().isEmpty())
                        mBtnSelectText.setText(mObjectInfo.getNoDataValue());
                    else
                        mBtnSelectText.setText("Select");
                }
            } else {
                if (mObjectInfo.getNoDataValue() != null && !mObjectInfo.getNoDataValue().isEmpty())
                    mBtnSelectText.setText(mObjectInfo.getNoDataValue());
                else
                    mBtnSelectText.setText("Select");
            }
        }

        if (mIsSearch) {
            if (info.getQrcode() != null && info.getQrcode().equals("1") && info.getEditable().equals("1"))
                layQrBtn.setVisibility(View.VISIBLE);
            else
                layQrBtn.setVisibility(View.GONE);
        } else {
            layQrBtn.setVisibility(View.VISIBLE);
        }

        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mIsSearch) {
                    generateQRView(true);
                }
            }
        });

        mTvDropdownTitle.setText(info.getFieldname());

        mFieldId = info.getFieldid();
        setEnableButton(info.getEditable().equals("1") ? true : false);

        mOptionList = info.getOptionListModels();

        return mView;
    }

    public View getSearchAndSelectDialog(String objectType, String objectKey, ObjectInfoModel info, int position, String title) {
        mPosition = position;
        mObjectType = objectType;
        mObjectKey = objectKey;
        mAccountPermission = new AccountPermission();
        infoSync = new InfoSync(mActivity, DropDownSelectView.this);
        mObjectInfo = info;
        lookUpService = info.getLookupservice();
        mTvDropdownTitle.setText(info.getFieldname());
        mBtnSelectText.setText(info.getDisplaytext());
        mTitle = title;

        if (info.getDisplaytext() != null) {
            if (info.getDisplaytext().length() <= 0) {
                List<ValueModel> valueModels = info.getValueModels();
                if (valueModels != null && valueModels.size() > 0) {
                    if (valueModels.get(0).getValue() != null && valueModels.get(0).getValue().length() > 0) {
                        List<OptionListModel> optionListModels = info.getOptionListModels();
                        boolean isDataFound = false;
                        for (OptionListModel model : optionListModels) {
                            if (model.getValue().equals(valueModels.get(0).getValue())) {
                                isDataFound = true;
                                mBtnSelectText.setText(model.getDisplayText());
                                break;
                            }
                        }
                        if (!isDataFound)
                            mBtnSelectText.setText(valueModels.get(0).getValue());
                    } else
                        mBtnSelectText.setText("Select");
                } else {
                    mBtnSelectText.setText("Select");
                }
            } else {
                mBtnSelectText.setText(info.getDisplaytext());
            }
        } else {
            mBtnSelectText.setText("Select");
        }

        if (mIsSearch) {
            if (info.getQrcode() != null && info.getQrcode().equals("1") && info.getEditable().equals("1"))
                layQrBtn.setVisibility(View.VISIBLE);
            else
                layQrBtn.setVisibility(View.GONE);
        } else {
            layQrBtn.setVisibility(View.VISIBLE);
        }

        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateQRView(true);
            }
        });

        boolean isEditable = info.getEditable().equals("1") ? true : false;
        setEnableButton(isEditable);

        return mView;
    }

    public View getUsersBox(String objectType, String objectKey, boolean isQrVisible) {
        mObjectType = objectType;
        mObjectKey = objectKey;
        mAccountPermission = new AccountPermission();
        infoSync = new InfoSync(mActivity, DropDownSelectView.this);
        mTvDropdownTitle.setText("Member");
        mTvDropdownTitle.setTextColor(mActivity.getResources().getColor(R.color.pure_black));
        mBtnSelectText.setText("Select Member");
        mTitle = "Select Member";

        if (isQrVisible)
            layQrBtn.setVisibility(View.VISIBLE);
        else
            layQrBtn.setVisibility(View.GONE);

        isOnlyMembers = true;
        setEnableButton(true);

        return mView;
    }

    private void setEnableButton(boolean isEditable) {
        mBtnSelect.setEnabled(isEditable);

        if (!isEditable) {
            Drawable img = mActivity.getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_24dp);
            mBtnSelect.setCompoundDrawables(null, null, img, null);
            mBtnSelect.setEnabled(false);
        }

    }

    private void setDefaultValueToTag(int position) {
        mBtnSelect.setTag(position + "");
    }

    private String getFilterFieldsData() {
        JSONArray fieldData = new JSONArray();
        List<ObjectInfoModel> selectedObjectInfo = SmartConstants.selectedObjectInfo;
        for (ObjectInfoModel objectInfo : selectedObjectInfo) {
            JSONObject field = new JSONObject();
            try {
                field.put("FieldID", objectInfo.getFieldid());

                List<ValueModel> valueModels = objectInfo.getValueModels();
                field.put("Value", (valueModels != null && valueModels.size() > 0) ? valueModels.get(0).getValue() : "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fieldData.put(field);
        }

        return fieldData.toString();
    }

    private void generateQRView(boolean isOnlyQr) {
        initDialog(isOnlyQr);
    }

    @Override
    public void onClick(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            initDialog(false);
            if (isOnlyMembers) {
                String dataValues = getFilterFieldsData();

                mProgressBar.setVisibility(View.VISIBLE);
                infoSync.getLookUpData(mAccountPermission.getAccountInfo().get(0),
                        mAccountPermission.getAccountInfo().get(1),
                        mAccountPermission.getAccountInfo().get(2),
                        mObjectType, "GetUsers", mObjectKey, dataValues);
                return;
            }

            if (mIsSearch) {
                String dataValues = getFilterFieldsData();

                mProgressBar.setVisibility(View.VISIBLE);
                infoSync.getLookUpData(mAccountPermission.getAccountInfo().get(0),
                        mAccountPermission.getAccountInfo().get(1),
                        mAccountPermission.getAccountInfo().get(2),
                        mObjectType, mObjectInfo.getLookupservice(), mObjectKey, dataValues);

            } else {
                addOptionListToDataLookUpList();
            }
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }

    }

    private void addOptionListToDataLookUpList() {
        mDataLookUps = new ArrayList<>();
        ArrayList<DataLookUp> mGo = new ArrayList<>();
        if (mObjectInfo.getNoDataValue() != null && !mObjectInfo.getNoDataValue().isEmpty()) {
            DataLookUp data = new DataLookUp();
            data.mDisplayText = mObjectInfo.getNoDataValue();
            data.mValue = "";
            mDataLookUps.add(data);
            mGo.add(data);
        }

        for (OptionListModel optionList : mOptionList) {
            DataLookUp data = new DataLookUp();
            data.mDisplayText = optionList.getDisplayText();
            data.mValue = optionList.getValue();
            mDataLookUps.add(data);
            mGo.add(data);
        }

        mAdapter = new SingleListAdapter(mActivity.getApplicationContext(), mGo, this);
        mRvSingleList.setAdapter(mAdapter);
    }

    private void addDataLookUpList(List<DataLookUp> dataResponse) {
        mDataLookUps = new ArrayList<>();
        ArrayList<DataLookUp> mGo = new ArrayList<>();

        for (DataLookUp data : dataResponse) {
            mDataLookUps.add(data);
            mGo.add(data);
        }

        mAdapter = new SingleListAdapter(mActivity.getApplicationContext(), mGo, this);
        mRvSingleList.setAdapter(mAdapter);
    }

    private void setDisplayValue(String displayText) {
        mBtnSelectText.setText(displayText);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    @Override
    public void onClickSingleItem(DataLookUp item) {
        mBtnSelectText.setText(item.getDisplayText());
        mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, item.getDisplayText(), item.getValue());
        mDialogList.dismiss();
    }

    @Override
    public void getObjectForQRSuccess(final GetObjectForQrResponse response) {
        if (!response.mMessage.isEmpty()) {
            Crouton.cancelAllCroutons();
            Crouton.makeText(mActivity, "QR not supported", Style.ALERT, mRelCamContainer).show();
            qr.resume();
            return;
        }

        System.out.println(response.toString());

        boolean state = mNetwork.IsAvailableNetwork(mActivity);
        mDialogList.dismiss();

        if (state) {
            if (!mIsSearch) {
                qrCodeWithTypeDSWithOptionList(response);
            } else {
                if (mObjectInfo.getLookupservice() != null && !mObjectInfo.getLookupservice().equals("")) {
                    qrCodeWithLookupService(response);
                } else {
                    qrCodeWithNoLookupService(response);
                }
            }
        } else {
            Toast.makeText(mActivity, SmartConstants.NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
        }
    }

    private void qrCodeWithTypeDSWithOptionList(GetObjectForQrResponse response) {
        //region QR validation removed
        //        if(mObjectInfo.getOptionListModels() != null && mObjectInfo.getOptionListModels().size()>0){
//            OptionListModel model = null;
//            for (OptionListModel modelx : mObjectInfo.getOptionListModels()) {
//                if(modelx.getValue().equals(response.mObjectKey)){
//                    model = modelx;
//                    break;
//                }
//            }
//
//            if(model != null){
//                String value = response.mObjectKey;
//                String displayText = model.getDisplayText();
//
//                mBtnSelect.setText(displayText);
//                mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, displayText, value);
//                mDialogList.dismiss();
//
//                Log.e("SmartFM", displayText + "|" + value);
//            } else {
//                Toast.makeText(mActivity, "Invalid data found", Toast.LENGTH_SHORT).show();
//            }
//        }
        //endregion
        String value = response.mObjectKey;
        String displayText = response.mObjectID;

        mBtnSelectText.setText(displayText);
        mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, displayText, value);
        mDialogList.dismiss();

        Log.e("SmartFM", displayText + "|" + value);
    }

    private void qrCodeWithNoLookupService(GetObjectForQrResponse response) {
        //region QR varification removed ;(
        //        if (mObjectInfo.getOptionListModels() != null && mObjectInfo.getOptionListModels().size() > 0) {
//            for (OptionListModel m : mObjectInfo.getOptionListModels()) {
//                if (m.getValue().equals(response.mObjectID)) {
//                    String value = response.mObjectKey;
//                    String displayText = m.getDisplayText();
//
//                    mBtnSelect.setText(displayText);
//                    mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, displayText, value);
//
//                    Log.e("SmartFM", displayText + "|" + value);
//                    break;
//                }
//            }
//            mDialogList.dismiss();
//        } else {
//            mDialogList.dismiss();
//            Toast.makeText(mActivity, "Data not found", Toast.LENGTH_SHORT).show();
//        }
        //endregion

        String value = response.mObjectKey;
        String displayText = response.mObjectID;

        mBtnSelectText.setText(displayText);
        mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, displayText, value);

        Log.e("SmartFM", displayText + "|" + value);
    }

    private void qrCodeWithLookupService(final GetObjectForQrResponse response) {
        //region lookup service verification removed :(
        //        new LookupServiceSync(mActivity, new LookupServiceSync.LookupServiceCallback() {
//            @Override
//            public void onLookupServiceSuccess(List<OptionListModel> optionListModels) {
//                if (optionListModels != null && optionListModels.size() > 0) {
//                    for (OptionListModel m : optionListModels) {
//                        if (m.getValue().equals(response.mObjectID)) {
//
//                            break;
//                        }
//                    }
//                } else {
//                    Toast.makeText(mActivity, "Data not found", Toast.LENGTH_SHORT).show();
//                    mDialogList.dismiss();
//                }
//            }
//
//            @Override
//            public void onLookupServiceFailed(String error) {
//                Toast.makeText(mActivity, "Data not found", Toast.LENGTH_SHORT).show();
//                mDialogList.dismiss();
//            }
//        }).getLookupServiceData(mAccountPermission.getAccountInfo().get(0),
//                mObjectInfo.getLookupservice(),
//                mAccountPermission.getAccountInfo().get(2),
//                mAccountPermission.getAccountInfo().get(1));
        //endregion

        String value = response.mObjectKey;
        String displayText = response.mObjectID;

        mBtnSelectText.setText(displayText);
        mDownSelectTextListener.onDropDownSelectFocusOut(mPosition, displayText, value);
        mDialogList.dismiss();

        Log.e("SmartFM", displayText + "|" + value);
    }

    @Override
    public void getObjectForQRError(String message) {
        if (qr != null) {
            Crouton.cancelAllCroutons();
            Crouton.makeText(mActivity, "QR not supported", Style.ALERT, mRelCamContainer).show();
            try {
                qr.resume();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onInfoCallSuccess(InfoObject infoObject) {

    }

    @Override
    public void onInfoCallFail(String error) {
        mTvMessage.setText(error);
        mTvMessage.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onGetWOCategoriesSuccess(List<DataLookUp> dataResponse) {
        addDataLookUpList(dataResponse);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onGetUpdateObjectSuccess(DataUpdateStatus updateStatus, String objectKey, JSONArray jsonfieldData) {

    }

    @Override
    public void onGetUpdateObjectError(String error) {

    }

    @Override
    public void onExecuteActionSuccess(DataUpdateStatus response) {

    }

    @Override
    public void onExecuteActionError(DataUpdateStatus response) {

    }

    public interface DropDownSelectTextListener {
        void onDropDownSelectFocusOut(int position, String displayText, String value);
    }
}
