package com.ecyber.ivivaanywhere.smartfm.customviews;

/**
 * Created by Lakmal on 5/13/2016.
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class SignatureView extends View {
    private Path mPath;
    private Paint mPaint;
    private Bitmap mBitmap;
    private Bitmap mOriginal;
    private Canvas mCanvas;
    private int bgColor;
    private boolean mHasOriginal, isSigViewChanged;

    private float curX, curY;
    private boolean isDragged = false;

    private static final int TOUCH_TOLERANCE = 4;
    private static final int STROKE_WIDTH = 10;

    public SignatureView(Context context) {
        super(context);
        init();
    }
    public SignatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public SignatureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setFocusable(true);
        bgColor = Color.WHITE;
        mPath = new Path();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(bgColor ^ 0x00FFFFFF);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE_WIDTH);
        mHasOriginal = false;
    }

    public void initWithBitmap(Bitmap original) {
        setFocusable(true);
        bgColor = Color.WHITE;
        mPath = new Path();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(bgColor ^ 0x00FFFFFF);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE_WIDTH);
        mHasOriginal = true;
        mOriginal = original;
    }

    /**
     * Set the color of the signature.
     * @param color the hex representation of the desired color, most likely an instance of Color.*
     */
    public void setSigColor(int color) {
        mPaint.setColor(color);
    }

    /**
     * Set the color of the signature. For simpler option just us setSigColor(int color).
     * @param a alpha value
     * @param r red value
     * @param g green value
     * @param b blue value\
     */
    public void setSigColor(int a, int r, int g, int b) {
        mPaint.setARGB(a, r, g, b);
    }

    /**
     * Clear the signature from the view.
     */
    public void clearSignature() {
        isSigViewChanged = false;
        if (mCanvas != null) {
            mCanvas.drawColor(bgColor);

            if(mHasOriginal) {
                //newCanvas.drawBitmap(Bitmap.createScaledBitmap(mOriginal, newCanvas.getWidth(), newCanvas.getHeight(), false), 0, 0, null);

                Rect src = new Rect(0,0,mOriginal.getWidth(), mOriginal.getHeight());
                Rect dest = new Rect(0,0, mCanvas.getWidth(), mCanvas.getHeight());

//                if(mOriginal.getWidth() > mOriginal.getHeight()) {
//                    float ratio = mOriginal.getHeight() / mOriginal.getWidth();
//                    dest = new Rect(0,0,mCanvas.getWidth(), (int)(mCanvas.getWidth() * ratio));
//                } else {
//                    float ratio = mOriginal.getWidth() / mOriginal.getHeight();
//                    dest = new Rect(0,0, (int)(mCanvas.getHeight() * ratio), mCanvas.getHeight());
//                }

                mCanvas.drawBitmap(mOriginal, src, dest, null);

                //mCanvas.drawBitmap(mOriginal, 0, 0, null);
                //mCanvas.scale((float)mOriginal.getWidth(), (float)mOriginal.getHeight());
            }

            mPath.reset();
            invalidate();
        }
    }

    public boolean isSignatureViewChanged() {
        return isSigViewChanged;
    }
    /**
     * Clear the signature from the view.
     *
     * @deprecated use {@link clearSignature()} instead (no need to shorten).
     */
    @Deprecated
    public void clearSig() {
        clearSignature();
    }

    /**
     * Get the bitmap backing the view.
     */
    public Bitmap getImage() {
        return this.mBitmap;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        int bitW = mBitmap != null ? mBitmap.getWidth() : 0;
        int bitH = mBitmap != null ? mBitmap.getWidth() : 0;

        // If the width and height of the bitmap are bigger than the
        // new defined size, then keep the excess bitmap and return
        // (Part of the backing bitmap will be clipped off, but it
        // will still exist)
        if (bitW >= w && bitH >= h) {
            return;
        }

        if (bitW < w) bitW = w;
        if (bitH < h) bitH = h;

        // create a new bitmap and canvas for the new size
        Bitmap newBitmap = Bitmap.createBitmap(bitW, bitH, Bitmap.Config.ARGB_8888);
        Canvas newCanvas = new Canvas();
        newCanvas.setBitmap(newBitmap);

        if (mBitmap != null) {	// already have a bitmap
            newCanvas.drawBitmap(mBitmap, 0, 0, null);	// redraw it onto the new bitmap
        } else {				// no path yet
            newCanvas.drawColor(bgColor);

            if(mHasOriginal) {
                //newCanvas.drawBitmap(Bitmap.createScaledBitmap(mOriginal, newCanvas.getWidth(), newCanvas.getHeight(), false), 0, 0, null);

                Rect src = new Rect(0,0,mOriginal.getWidth(), mOriginal.getHeight());
                Rect dest = new Rect(0,0, newCanvas.getWidth(), newCanvas.getHeight());

//                if(mOriginal.getWidth() > mOriginal.getHeight()) {
//                    float ratio = (float)mOriginal.getHeight() / (float)mOriginal.getWidth();
//                    dest = new Rect(0,0,newCanvas.getWidth(), (int)(newCanvas.getWidth() * ratio));
//                } else {
//                    float ratio = (float)mOriginal.getWidth() / (float)mOriginal.getHeight();
//                    dest = new Rect(0,0, (int)(newCanvas.getHeight() * ratio), newCanvas.getHeight());
//                }

                newCanvas.drawBitmap(mOriginal, src, dest, null);

                //newCanvas.drawBitmap(mOriginal, 0, 0, null);
                //newCanvas.scale((float)mOriginal.getWidth(), (float)mOriginal.getHeight());
            }

        }
        // Replace the old bitmap and canvas with the new one
        mBitmap = newBitmap;
        mCanvas = newCanvas;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDown(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                break;
            case MotionEvent.ACTION_UP:
                touchUp();
                break;
        }
        invalidate();

        isSigViewChanged = true;
        return true;
    }


    /**----------------------------------------------------------
     * Private methods
     **---------------------------------------------------------*/

    private void touchDown(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        curX = x;
        curY = y;
        isDragged = false;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - curX);
        float dy = Math.abs(y - curY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(curX, curY, (x + curX)/2, (y + curY)/2);
            curX = x;
            curY = y;
            isDragged = true;
        }
    }

    private void touchUp() {
        if (isDragged) {
            mPath.lineTo(curX, curY);
        } else {
            mPath.lineTo(curX+2, curY+2);
        }
        mCanvas.drawPath(mPath, mPaint);
        mPath.reset();
    }
}