package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "Actions")
public class Actions extends Model {

    @Column(name = "InfoId")
    public long InfoId;

    @Column(name = "ActionID")
    public String mActionID;

    @Column(name = "ActionName")
    public String mActionName;

    @Column(name = "Enabled")
    public String mEnabled;

    @Column(name = "Confirmation")
    public String mConfirmation;

    @Column(name = "Cargo")
    public String mCargo;

    public Actions() {
        super();
    }

    public Actions(long infoId, String mActionID, String mActionName, String mEnabled, String mConfirmation, String mCargo) {
        InfoId = infoId;
        this.mActionID = mActionID;
        this.mActionName = mActionName;
        this.mEnabled = mEnabled;
        this.mConfirmation = mConfirmation;
        this.mCargo = mCargo;
    }

    public List<Actions> getAll() {
        return new Select().from(Actions.class).execute();
    }

    public void clearTable() {
        new Delete().from(Actions.class).execute();
    }

    public String getActionID() {
        return mActionID;
    }

    public void setActionID(String actionID) {
        mActionID = actionID;
    }

    public String getActionName() {
        return mActionName;
    }

    public void setActionName(String actionName) {
        mActionName = actionName;
    }

    public String getEnabled() {
        return mEnabled;
    }

    public void setEnabled(String enabled) {
        mEnabled = enabled;
    }

    public String getConfirmation() {
        return mConfirmation;
    }

    public void setConfirmation(String confirmation) {
        mConfirmation = confirmation;
    }

    public String getCargo() {
        return mCargo;
    }

    public void setCargo(String cargo) {
        mCargo = cargo;
    }

    public long getInfoId() {
        return InfoId;
    }

    public void setInfoId(long infoId) {
        InfoId = infoId;
    }
}
