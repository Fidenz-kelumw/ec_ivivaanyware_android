package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectAction;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectDeafultFilter;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectOptionList;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SubObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetSubObjects;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SubObjectsSync {
    private Context context;
    private SubObjectsCallback callback;
    private String mTabName;

    public SubObjectsSync(Context context, SubObjectsCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void getSubObjects(String url, final String objectKey, String objectType, String userKey, String tabName, String apikey, String filter, final boolean isNeedToREsetFilters) {
        this.mTabName = tabName;

        ServiceGenerator.CreateService(GetSubObjects.class, url).getSubObjects(objectKey, objectType, userKey, tabName, filter, apikey).enqueue(new Callback<SubObjectResponse>() {
            @Override
            public void onResponse(Call<SubObjectResponse> call, Response<SubObjectResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (isNeedToREsetFilters) {
                            new SubObject().clearTable();
                        } else {
                            new Delete().from(SubObject.class).execute();
                            new Delete().from(SubObjectAction.class).execute();
                        }

                        if (response.body().getSubObjects().size() != 0) {
                            List<SubObject> subObjects = response.body().getSubObjects();

                            DataStoreAsync dataStoreAsync = new DataStoreAsync();
                            dataStoreAsync.dataSet = subObjects;
                            dataStoreAsync.objectKey = objectKey;
                            dataStoreAsync.execute();
//
                        } else {
                            callback.onGetSubObjectsFail("No items found");
                        }

                        if (isNeedToREsetFilters)
                            storeFiltersInDb(response.body().getFilter(), objectKey);
                    }
                } else {
                    try {
                        String message = response.errorBody().string();
                        Log.d("SmartFm - ErrorLog", message);
                        ErrorLog.saveErrorLog("Get SubObject", message);
                        callback.onGetSubObjectsFail(SmartConstants.NO_MESSAGE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onGetSubObjectsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubObjectResponse> call, Throwable t) {
                callback.onGetSubObjectsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
            }
        });
    }

    public void performSubObjectAction(String url, String objectKey, String objectType, String userKey, String action, String apikey, SubObject subObject, SubObjectAction subObjectAction) {
        ServiceGenerator.CreateService(GetSubObjects.class, url).performSubObjectAction(objectKey, objectType, userKey, action, apikey)
                .enqueue(new Callback<SuccessResponse>() {
                    @Override
                    public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                        if (response != null && response.isSuccessful()) {
                            if (response.body().getSuccess() != null && response.body().getSuccess().endsWith("1"))
                                callback.onPerformSubObjectActionSuccess((response.body().getMessage() != null && !response.body().getMessage().isEmpty()) ? response.body().getMessage() : "Action successful",
                                        subObject, subObjectAction);
                            else
                                callback.onPerformSubObjectActionFail((response.body().getMessage() != null && !response.body().getMessage().isEmpty()) ? response.body().getMessage() : "Action failed");
                        } else {
                            callback.onPerformSubObjectActionFail("Action failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessResponse> call, Throwable t) {
                        callback.onPerformSubObjectActionFail("Action failed");
                    }
                });
    }

    public boolean isDataAvailable(String ObjectKeyFromSelection) {
        List<SubObject> all = new SubObject().getSelectedObjects(ObjectKeyFromSelection);

        try {
            SubObjectResponse.Filter filter = new SubObjectResponse.Filter();
            SubObjectDeafultFilter deafultFilter = new SubObjectDeafultFilter().getSubObjectDeafultFilter(ObjectKeyFromSelection);
            List<SubObjectOptionList> optionLists = new SubObjectOptionList().getSubObjectOptionLists(ObjectKeyFromSelection);

            filter.setNoDataText(deafultFilter.getNoDataText());
            filter.setDeafultFilter(deafultFilter);
            filter.setOptionLists(optionLists);

            callback.onSubObjectFiltersFound(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (all.size() == 0) {
            return false;
        } else {
            callback.onGetSubObjectsFoundItems(all);
            return true;
        }
    }

    public void storeFiltersInDb(SubObjectResponse.Filter filter, String objectKey) {
       if(filter  != null ) {
           if (filter.getDeafultFilter() != null) {
               filter.getDeafultFilter().setObjectKey(objectKey);
               filter.getDeafultFilter().setNoDataText(filter.getNoDataText());
               System.out.println(filter.getDeafultFilter().save());
           } else {
               if (filter.getOptionLists() != null)
                   new SubObjectDeafultFilter(null, filter.getNoDataText(), objectKey).save();
           }

           if (filter.getOptionLists() != null) {
               for (SubObjectOptionList optionList : filter.getOptionLists()) {
                   optionList.setObjectKey(objectKey);
                   System.out.println(optionList.save());
               }
           }
       }
        callback.onSubObjectFiltersFound(filter);
    }

    private class DataStoreAsync extends AsyncTask<Void, Void, Void> {

        public List<SubObject> dataSet;
        public String objectKey;

        @Override
        protected Void doInBackground(Void... voids) {
            for (SubObject subObject : dataSet) {

                subObject.setObjectKeyFromSelection(objectKey);

                if(subObject.getAction()!= null) {
                    for (int i = 0; i < subObject.getAction().size(); i++) {
                        SubObjectAction action = subObject.getAction().get(i);
                        action.setObjectKey(subObject.getObjectKey());
                        action.save();
                    }
                }
                subObject.save();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            callback.onGetSubObjectsSuccess(dataSet);
            new TimeCap().updateTableTime(mTabName);
        }
    }

    public interface SubObjectsCallback {
        void onGetSubObjectsSuccess(List<SubObject> subObjects);

        void onGetSubObjectsFail(String error);

        void onGetSubObjectsFoundItems(List<SubObject> subObjects);

        void onPerformSubObjectActionSuccess(String message, SubObject subObject, SubObjectAction subObjectAction);

        void onPerformSubObjectActionFail(String message);

        void onSubObjectFiltersFound(SubObjectResponse.Filter filter);
    }
}
