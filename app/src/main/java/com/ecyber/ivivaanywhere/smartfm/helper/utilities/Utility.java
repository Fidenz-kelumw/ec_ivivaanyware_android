package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.models.DomainData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.activeandroid.Cache.getContext;

/**
 * Created by Lakmal on 6/7/2016.
 */
public class Utility {

    private static final String TAG = "Utility";

    public Utility() {
    }

    public static String buildUrl(String remainder) {
        String url = new AccountPermission().getAccount();
        String ssl = DomainData.getSsl();

        String protocol;

        if (ssl.equals(null) || ssl.equals("0")) {
            protocol = "http://";
        } else {
            protocol = "https://";
        }

        return protocol + url + remainder;
    }

    public static String getDateTimeDifferent(String createdAt) {
        TReXTimeFormattor tReXTimeFormattor = new TReXTimeFormattor();

        SimpleDateFormat simpleDateFormatMain = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        simpleDateFormatMain.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));


        Date sdate = null;
        String DateTocovDate = "";
        String agoText = "";

        try {
            sdate = simpleDateFormatMain.parse(createdAt);
            DateTocovDate = simpleDateFormat.format(sdate);
            sdate = simpleDateFormat.parse(DateTocovDate);

            agoText = tReXTimeFormattor.printDifference(sdate);

        } catch (ParseException e) {
            Log.d("Error message : ", e.toString());
        }

        return agoText;
    }

    public static String getDateNowISOFormat() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String nowAsISO = df.format(new Date());
        return nowAsISO;
    }

    public static String makeGMTtoCurrentTimeZone(String inputDate) {
        String ret = "";
        SimpleDateFormat gmtFrmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        gmtFrmt.setTimeZone(TimeZone.getDefault());

        //SimpleDateFormat defFrmt = new SimpleDateFormat("yyyy-MM-dd h:mm:ss a");
        SimpleDateFormat defFrmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        defFrmt.setTimeZone(TimeZone.getDefault());

        Date parsedDate = null;

        try {
            parsedDate = gmtFrmt.parse(inputDate);
            ret = defFrmt.format(parsedDate).toString();

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e) {
            return "";
        }

        return ret;
    }

    public static String convertDate(String inputDate){
        String ret = "";
        SimpleDateFormat gmtFrmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat defFrmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date parsedDate = null;

        try {
            parsedDate = gmtFrmt.parse(inputDate);
            ret = defFrmt.format(parsedDate).toString();

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e) {
            return "";
        }

        return ret;
    }

    public static boolean IsEmpty(String value) {
        if (value == null) {
            return true;
        } else {
            if (value.trim().isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image) {
        Bitmap retBitmap = null;
        try {

            int height = image.getHeight();
            int width = image.getWidth();
            Log.d(TAG, "Real Size -> Height(" + height + ")|Weight(" + width + ")");
            boolean shouldResized = false;
            boolean isH = false;
            DecimalFormat decFormat = new DecimalFormat("0.0");
            double shorterSide = height;
            double longSide = width;

            if (width <= height) {
                shorterSide = width;
                longSide = height;
                isH = false;
            } else {
                shorterSide = height;
                longSide = width;
                isH = true;
            }

            double ratio = Double.parseDouble(decFormat.format((longSide / shorterSide)));

            int minSize = 700;//default
            int longSize = 1800;

            String resizedImage = new SmartSharedPreferences(getContext()).getResizedImage();
            Log.d(TAG, "TYPE-" + resizedImage);
            switch (resizedImage) {
                case SmartConstants.SMALL:
                    minSize = 400;
                    if (shorterSide > minSize) {
                        shouldResized = true;
                    } else {
                        shouldResized = false;
                    }
                    break;
                case SmartConstants.MEDIUM:
                    minSize = 800;
                    if (shorterSide > minSize) {
                        shouldResized = true;
                    } else {
                        shouldResized = false;
                    }
                    break;
                case SmartConstants.LARGE:
                    minSize = 1200;
                    if (shorterSide > minSize) {
                        shouldResized = true;
                    } else {
                        shouldResized = false;
                    }
                    break;
                case SmartConstants.ORIGINAL:
                    //minSize = 1200;
                    //if (shorterSide > minSize) {
                    //    shouldResized = true;
                    //} else {
                    shouldResized = false;
                    //}
                    break;

            }
            int r_width = 1024;
            int r_height = 768;

            if (shouldResized) {
                longSize = (int) Math.round(minSize * ratio);
                if (isH) {
                    r_height = minSize;
                    r_width = longSize;
                } else {
                    r_height = longSize;
                    r_width = minSize;
                }
            } else {
                r_width = width;
                r_height = height;
            }

            Log.d(TAG, "Resized -> Height(" + r_height + ")|Weight(" + r_width + ")");
            retBitmap = Bitmap.createScaledBitmap(image, r_width, r_height, false);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            retBitmap = null;
        }
        return retBitmap;
    }

    public static String createNewPath() {
        String path = "";
        path = Environment.getExternalStorageDirectory().getAbsolutePath();
        path += "/attached_image.jpg";

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return Uri.fromFile(mediaFile).getPath();
    }

    public static File createdTempImageFile(Bitmap bitmap, String tempPath) {
        try {
            File file = new File(tempPath);
            FileOutputStream fOut;

            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();

            return  new File(tempPath);
        } catch (Exception e) {
            return  null;
        }
    }

    public static File compressFile(Bitmap bitmap, String path) {
        File file = new File(path);
        long length = file.length();
        try {


            long mega_bytes = (length / 1024) / 1024;
            int reduce = 100;
            if (mega_bytes > 1) {
                Long v = Math.round(100 / (mega_bytes - (mega_bytes * (0.3))));
                reduce = Integer.valueOf(v.intValue());
            }

            Bitmap out = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), false);
            FileOutputStream fOut_stream;

            fOut_stream = new FileOutputStream(file);
            out.compress(Bitmap.CompressFormat.JPEG, reduce, fOut_stream);
            fOut_stream.flush();
            fOut_stream.close();
            bitmap.recycle();
            out.recycle();

            return new File(path);
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap rotateImage(Bitmap bitmap, String _filePath, Context context) {

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(_filePath);
        } catch (IOException e) {
            Toast.makeText(context, "File not found.", Toast.LENGTH_LONG).show();
            return null;
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        int angle = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                angle = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                angle = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                angle = 270;
                break;
            case ExifInterface.ORIENTATION_NORMAL:
                angle = 0;
            default:
                break;
        }

        if (angle != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix,
                    true);
        } else {
            return bitmap;
        }

    }
}
