package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ObjectURLsAdapter extends RecyclerView.Adapter<ObjectURLsAdapter.Holder>{

    private Context context;
    private List<ObjectURL> items;

    public ObjectURLsAdapter(Context context, List<ObjectURL> subObjects) {
        this.context = context;
        this.items = subObjects;
    }

    public void addItem(ObjectURL objects) {
        items.add(objects);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_url_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ObjectURL object = items.get(position);
        holder.name.setText(object.getURLName());

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat formatUTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            holder.description.setText(format.format(formatUTC.parse(object.getUpdatedAt())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return items !=null ? items.size():0;
    }

    public class Holder extends RecyclerView.ViewHolder{
        CustomTextView name, description;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tv_filter_name);
            description = (CustomTextView) itemView.findViewById(R.id.tv_filter_description);
        }
    }

}
