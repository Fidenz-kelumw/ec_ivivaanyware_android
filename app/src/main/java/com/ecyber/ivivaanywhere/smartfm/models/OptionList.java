package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 4/29/2016.
 */
@Table(name = "OptionList")
public class OptionList extends Model{
    /*"OptionList": [
    {
        "DisplayText": "Corrective",
            "Value": "1"
    },
    {
        "DisplayText": "Planned",
            "Value": "2"
    }
    ]*/

    @Column(name = "ObjectInfoId")
    @SerializedName("ObjectInfoId")
    @Expose
    public long mObjectInfoId;

    @Column(name = "DisplayText")
    @SerializedName("DisplayText")
    @Expose
    public String mDisplayText;

    @Column(name = "Value")
    @SerializedName("Value")
    @Expose
    public String mValue;

    public OptionList() {
        super();
    }

    public OptionList(long objectInfoId, String displayText, String value) {
        mObjectInfoId = objectInfoId;
        mDisplayText = displayText;
        mValue = value;
    }

    public long getObjectInfoId() {
        return mObjectInfoId;
    }

    public void setObjectInfoId(long objectInfoId) {
        mObjectInfoId = objectInfoId;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getDisplayText() {

        return mDisplayText;
    }

    public void setDisplayText(String displayText) {
        mDisplayText = displayText;
    }

}
