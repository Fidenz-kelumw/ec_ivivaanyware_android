package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SketchActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.AddAttachAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.AddAttachment;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InputModesModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.popup.AudioPlaybackDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.AudioRecordDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.SignatureDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.MultiPartUploadSync;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by lakmal on 7/21/16.
 */
public class AddAttachmentView implements AddAttachAdapter.AddAttachAdapterCallback, SignatureDialog.SignatureCallback, MultiPartUploadSync.AttachmentUploadCallback, AudioRecordDialog.AudioRecordCallback, AddAttachAdapter.InfoAttachmentCallback {
    private static final String TAG = "AddAttachmentView";
    private Activity mActivity;
    private Context mContext;
    private View mView;
    private String mFieldId;
    private int mPosition;
    private RecyclerView mRvAttach;
    private AddAttachAdapter adapter;
    private AddAttachmentCallback mDelegate;
    private String mBaseUrl;
    private String mApiKey;

    private LinearLayout mAttachmentTypeContainer;
    private LinearLayout mTypePhoto;
    private LinearLayout mTypeAudio;
    private LinearLayout mTypeSig;
    private LinearLayout mTypeVideo;
    private LinearLayout mTypeFile;
    private ImageButton mBtnPhoto;
    private ImageButton mBtnFile;
    private ImageButton mBtnSig;
    private ImageButton mBtnVideo;
    private ImageButton mBtnAudio;

    private LinearLayout mLinBtnContainer;
    private String mUserId;
    private ObjectInfoModel mObjectInfo;
    private Fragment mFragment;
    private AddAttachmentView mViewobject;
    private CustomTextView mTvMessage;
    private CustomTextView mTitle;

    private int selectedPosition = -1;
    private boolean isCreateObject = false;
    private boolean isDisableDelete;
    private boolean isFromSketch;

    public AddAttachmentView(Context context, Activity activity, Fragment fragment, AddAttachmentCallback delegate, String url, String userId, String apikey, boolean isDisableDelete) {
        this.mContext = context;
        this.mActivity = activity;
        this.mFragment = fragment;
        this.mDelegate = delegate;
        this.mBaseUrl = url;
        this.mUserId = userId;
        this.mApiKey = apikey;
        this.isDisableDelete = isDisableDelete;
        this.isFromSketch = false;
        initView();
    }

    public void setCreateObject() {
        isCreateObject = true;
    }

    private void initView() {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_add_attach_view, null);
        CustomTextView titleUnderLine = (CustomTextView) mView.findViewById(R.id.tv_just_title);

//        mLinBtnContainer = (LinearLayout) mView.findViewById(R.id.lin_btn_container);
//        mImgBtnCamera = (ImageButton) mView.findViewById(R.id.imageButton_camera);
//        mImgBtnGallery = (ImageButton) mView.findViewById(R.id.imageButton_gallery);
//        mImgBtnSignature = (ImageButton) mView.findViewById(R.id.imageButton_signature);

        mAttachmentTypeContainer = (LinearLayout) mView.findViewById(R.id.lin_lay_att_type_cont);
        mTypePhoto = (LinearLayout) mView.findViewById(R.id.lin_lay_att_photo);
        mTypeAudio = (LinearLayout) mView.findViewById(R.id.lin_lay_att_audio);
        mTypeVideo = (LinearLayout) mView.findViewById(R.id.lin_lay_att_video);
        mTypeSig = (LinearLayout) mView.findViewById(R.id.lin_lay_att_sig);
        mTypeFile = (LinearLayout) mView.findViewById(R.id.lin_lay_att_file);

        mBtnPhoto = (ImageButton) mView.findViewById(R.id.img_btn_photo);
        mBtnVideo = (ImageButton) mView.findViewById(R.id.img_btn_video);
        mBtnAudio = (ImageButton) mView.findViewById(R.id.img_btn_audio);
        mBtnSig = (ImageButton) mView.findViewById(R.id.img_btn_sig);
        mBtnFile = (ImageButton) mView.findViewById(R.id.img_btn_file);

        mTvMessage = (CustomTextView) mView.findViewById(R.id.tv_message);
        mTitle = (CustomTextView) mView.findViewById(R.id.tv_attach_title);
        mRvAttach = (RecyclerView) mView.findViewById(R.id.rv_attach_horiz_list);

        mRvAttach.setHasFixedSize(true);
        mRvAttach.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mRvAttach.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        adapter = new AddAttachAdapter(mContext, mActivity, this, this, mBaseUrl, isDisableDelete);
        mRvAttach.setAdapter(adapter);

        setTitleUnderLine(titleUnderLine);
    }

    public View getAddAttachView(ObjectInfoModel info, int position) {
        mObjectInfo = info;
        mTitle.setText(mObjectInfo.getFieldname());
        boolean isEditable = info.getEditable().equals("1") ? true : false;
        mFieldId = info.getFieldid();
        mPosition = position;
        String defVal = "";
        setDefaultImage(isEditable);
        setEditable(isEditable);

        mView.post(new Runnable() {
            @Override
            public void run() {
                int itemWidth = mAttachmentTypeContainer.getWidth() / 5;

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(itemWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.height = itemWidth;

                mTypePhoto.setLayoutParams(lp);
                mTypeAudio.setLayoutParams(lp);
                mTypeVideo.setLayoutParams(lp);
                mTypeSig.setLayoutParams(lp);
                mTypeFile.setLayoutParams(lp);

                mTypePhoto.setVisibility(View.GONE);
                mTypeAudio.setVisibility(View.GONE);
                mTypeVideo.setVisibility(View.GONE);
                mTypeSig.setVisibility(View.GONE);
                mTypeFile.setVisibility(View.GONE);

                if (isCreateObject) {
                    float size = (float) itemWidth / 1.5f;

                    RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams((int) size, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    rlp.height = (int) size;
                    rlp.addRule(RelativeLayout.CENTER_IN_PARENT);

                    mBtnPhoto.setLayoutParams(rlp);
                    mBtnAudio.setLayoutParams(rlp);
                    mBtnSig.setLayoutParams(rlp);
                    mBtnFile.setLayoutParams(rlp);
                    mBtnVideo.setLayoutParams(rlp);
                }

                String[] inputModes = InputModesModel.getInputModes(mObjectInfo.getFieldid());

                if (inputModes != null) {
                    for (String mode : inputModes) {

                        if (mode.equals("PHOTO")) {
                            mTypePhoto.setVisibility(View.VISIBLE);
                        } else if (mode.equals("AUDIO")) {
                            mTypeAudio.setVisibility(View.VISIBLE);
                        } else if (mode.equals("VIDEO")) {
                            mTypeVideo.setVisibility(View.VISIBLE);
                        } else if (mode.equals("SIG")) {
                            mTypeSig.setVisibility(View.VISIBLE);
                        } else if (mode.equals("FILE")) {
                            mTypeFile.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    mTypePhoto.setVisibility(View.VISIBLE);
                    mTypeAudio.setVisibility(View.VISIBLE);
                    mTypeVideo.setVisibility(View.VISIBLE);
                    mTypeSig.setVisibility(View.VISIBLE);
                    mTypeFile.setVisibility(View.VISIBLE);
                }
            }
        });

        registerCallbacks();

        return mView;
    }

    public void setInstance(AddAttachmentView view) {
        mViewobject = view;
    }

    private void setDefaultImage(boolean isEditable) {
        String downloadFolder = mObjectInfo.getDownloadfolder();
        List<ValueModel> valueModels = mObjectInfo.getValueModels();

        //mLinBtnContainer.setVisibility(isEditable ? View.VISIBLE : View.GONE);
        if (valueModels != null) {
            if (valueModels.size() > 0) {
                for (ValueModel valueModel : valueModels) {

                    switch (valueModel.getATTType()) {

                        case "AUD": {
                            String url = "";
                            String checkSsl = DomainData.getSsl();
                            if (checkSsl != null && !checkSsl.equals("")) {
                                if (checkSsl.equals("1")) {
                                    url = "https://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                } else {
                                    url = "http://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                }
                            }

                            adapter.addItem(new AddAttachment("name", "id", url, valueModel.getATTName(), isEditable, true, false, url, false, SmartConstants.ATTACHMENT_AUD, null));
                            break;
                        }

                        case "VID": {
                            String url = "";
                            String checkSsl = DomainData.getSsl();
                            if (checkSsl != null && !checkSsl.equals("")) {
                                if (checkSsl.equals("1")) {
                                    url = "https://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                } else {
                                    url = "http://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                }
                            }

                            adapter.addItem(new AddAttachment("name", "id", url, valueModel.getATTName(), isEditable, true, false, url, false, SmartConstants.ATTACHMENT_VID, null));
                            break;
                        }

                        case "IMG": {
                            String url = "";
                            String checkSsl = DomainData.getSsl();
                            if (checkSsl != null && !checkSsl.equals("")) {
                                if (checkSsl.equals("1")) {
                                    url = "https://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                } else {
                                    url = "http://" + mBaseUrl + downloadFolder + "/" + valueModel.getATTName();
                                }
                            }

                            adapter.addItem(new AddAttachment("name", "id", url, valueModel.getATTName(), isEditable, true, false, url, false, SmartConstants.ATTACHMENT_IMG, null));
                            break;
                        }
                    }
                }
            } else {
                mTvMessage.setVisibility(View.VISIBLE);
            }

        } else {
            mTvMessage.setVisibility(View.VISIBLE);
        }


    }

    private void setEditable(boolean isEditable) {
        mBtnPhoto.setEnabled(isEditable);
        mBtnVideo.setEnabled(isEditable);
        mBtnAudio.setEnabled(isEditable);
        mBtnSig.setEnabled(isEditable);
        mBtnFile.setEnabled(isEditable);
    }

    private void setTitleUnderLine(CustomTextView titleUnderLine) {
        String strTitle = new String("Attachments:");
        SpannableString title = new SpannableString(strTitle);
        title.setSpan(new UnderlineSpan(), 0, strTitle.length(), 0);
        titleUnderLine.setText(title);
    }

    public void addAttachment(String path, Uri uri, int type, boolean isFromSketch) {
        this.isFromSketch = isFromSketch;
        switch (type) {
            case SmartConstants.ATTACHMENT_AUD: {
                addAudio(path);
                break;
            }
            case SmartConstants.ATTACHMENT_VID: {
                addVideo(uri);
                break;
            }
            case SmartConstants.ATTACHMENT_IMG: {
                addImage(path, uri);
                break;
            }
        }
    }

    public void addImage(String path, Uri filePath) {
        Bitmap bitmap = null;

        try {
            if (Utility.IsEmpty(path)) {
                bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), filePath);
            } else {
                bitmap = BitmapFactory.decodeFile(path);
            }

            bitmap = new Utility().getResizedBitmap(bitmap);
            String tempPath = Utility.IsEmpty(path) ? Utility.createNewPath() : path;

            if (Utility.IsEmpty(path))
                Utility.createdTempImageFile(bitmap, tempPath);

            if (!Utility.IsEmpty(tempPath))
                bitmap = Utility.rotateImage(bitmap, tempPath, mActivity);

            File fileToSend = Utility.compressFile(bitmap, tempPath);
            if (fileToSend == null) {
                showMsgFailedImageSelect();
                return;
            }

            String guId = makeGUID(SmartConstants.ATTACHMENT_IMG);
            adapter.insert(new AddAttachment("Test", "100", tempPath, guId, true, false, true, "", true, SmartConstants.ATTACHMENT_IMG, filePath), 0);
            mRvAttach.scrollToPosition(0);
            mDelegate.onUploadingStatus(+1);

            new MultiPartUploadSync().uploadFile(this, mBaseUrl, mApiKey, mObjectInfo.getUploadfolder(), fileToSend, guId);

            /*bitmap.recycle();*/// TODO: 20/10/2016 check is this recycler
        } catch (Exception e) {
            showMsgFailedImageSelect();
        }
    }

    public void addAudio(String filePath) {
        String realPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + filePath;
        File uploadFile = new File(realPath);

        String guId = makeGUID(SmartConstants.ATTACHMENT_AUD);
        adapter.insert(new AddAttachment("Test", "100", realPath, guId, true, false, true, "", true, SmartConstants.ATTACHMENT_AUD, null), 0);
        mRvAttach.scrollToPosition(0);
        mDelegate.onUploadingStatus(+1);

        new MultiPartUploadSync().uploadFile(this, mBaseUrl, mApiKey, mObjectInfo.getUploadfolder(), uploadFile, guId);
    }

    public void addVideo(Uri filePath) {

        if (filePath != null) {
            String path = getRealPathFromURI(filePath);

            String guId = makeGUID(SmartConstants.ATTACHMENT_VID);
            adapter.insert(new AddAttachment("Test", "100", path, guId, true, false, true, "", true, SmartConstants.ATTACHMENT_VID, filePath), 0);
            mRvAttach.scrollToPosition(0);
            mDelegate.onUploadingStatus(+1);

            File uploadFile = new File(path);

            new MultiPartUploadSync().uploadFile(this, mBaseUrl, mApiKey, mObjectInfo.getUploadfolder(), uploadFile, guId);
        }

    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = mActivity.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void showMsgFailedImageSelect() {
        Toast.makeText(mContext, "Failed image select.Please try again", Toast.LENGTH_SHORT).show();
    }

    private void onClickGallery(View v) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mDelegate.onCapturePath(null, mViewobject);
        if (mFragment != null) {
            mFragment.startActivityForResult(i, SmartConstants.RESULT_IMAGE_GALLERY);
        } else {
            mActivity.startActivityForResult(i, SmartConstants.RESULT_IMAGE_GALLERY);
        }
    }

    private void onClickSignature(View v) {
        new SignatureDialog(mActivity, this).show();
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(SmartConstants.FILE_SAVE_LOCATION, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + makeGUID(SmartConstants.ATTACHMENT_IMG));

        return mediaFile;
    }

    public String makeGUID(int type) {
        String guid = UUID.randomUUID().toString();
        guid = new AccountPermission().getAccountInfo().get(2) + mActivity.getString(R.string.underscore) + guid + mActivity.getString(R.string.underscore) + new Date().getTime();

        String extension = "";

        switch (type) {
            case SmartConstants.ATTACHMENT_AUD: {
                extension = ".mp4";
                break;
            }
            case SmartConstants.ATTACHMENT_IMG: {
                extension = ".jpg";
                break;
            }
            case SmartConstants.ATTACHMENT_VID: {
                extension = ".mov";
                break;
            }
        }

        return guid + extension;
    }

    private void registerCallbacks() {

        mBtnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final int requestId = 1;
                    Uri fileUri = Uri.fromFile(getOutputMediaFile());

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, fileUri);
                    mDelegate.onCapturePath(fileUri.getPath(), mViewobject);

                    Bundle bundle = new Bundle();
                    bundle.putString("imgPath", fileUri.getPath());
                    cameraIntent.putExtras(bundle);

                    if (mFragment == null) {
                        mActivity.startActivityForResult(cameraIntent, SmartConstants.RESULT_IMAGE_CAMERA);
                    } else {
                        mFragment.startActivityForResult(cameraIntent, SmartConstants.RESULT_IMAGE_CAMERA);
                    }

                } catch (IllegalStateException e) {
                    Log.e(TAG, "onClick: " + e.getStackTrace().toString());
                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e.getStackTrace().toString());
                }
            }
        });

        mBtnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                mDelegate.onCapturePath(null, mViewobject);
                if (mFragment != null) {
                    mFragment.startActivityForResult(i, SmartConstants.RESULT_IMAGE_GALLERY);
                } else {
                    mActivity.startActivityForResult(i, SmartConstants.RESULT_IMAGE_GALLERY);
                }
            }
        });

        mBtnAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAudioRecordDialog();
            }
        });

        mBtnSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignatureDialog();
            }
        });

        mBtnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                //File mediaFile =  new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + makeGUIDForType(".mp4"));
                //takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
                if (takeVideoIntent.resolveActivity(mActivity.getPackageManager()) != null) {

                    if (mFragment == null) {
                        mActivity.startActivityForResult(takeVideoIntent, SmartConstants.RESULT_VIDEO_CAPTURE);
                    } else {
                        mFragment.startActivityForResult(takeVideoIntent, SmartConstants.RESULT_VIDEO_CAPTURE);
                    }
                    mDelegate.onCapturePath(null, mViewobject);
                }
            }
        });
    }

    private void openSignatureDialog() {
        new SignatureDialog(mActivity, this).show();
    }

    private void openAudioRecordDialog() {
        new AudioRecordDialog(mActivity, mActivity, this).show();
    }

    @Override
    public void onSaveSignature(String mPicturePath) {
        addAttachment(mPicturePath, null, SmartConstants.ATTACHMENT_IMG, false);
    }

    @Override
    public void attachmentSuccess(String fileName, File file) {

        //remove duplicate
        try {
            if (isFromSketch && selectedPosition != -1) {
                this.isFromSketch = false;
                adapter.remove(selectedPosition + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTvMessage.setVisibility(View.GONE);
        List<AddAttachment> list = adapter.getArrayList();
        mTvMessage.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        int i = 0;

        for (AddAttachment attachment : list) {
            if (attachment.mGUID.equals(fileName)) {
                mDelegate.addFileToValue(fileName, mPosition);
                String downloadFolder = mObjectInfo.getDownloadfolder();

                String url = "";
                String checkSsl = DomainData.getSsl();
                if (checkSsl != null && !checkSsl.equals("")) {
                    if (checkSsl.equals("1")) {
                        url = "https://" + mBaseUrl + downloadFolder + "/" + fileName;
                    } else {
                        url = "http://" + mBaseUrl + downloadFolder + "/" + fileName;
                    }
                }

                adapter.updateItem(i, url);
                mDelegate.onUploadingStatus(-1);
                return;
            }
            i++;
        }
    }

    @Override
    public void attachmentFailed(String fileName, File file) {

        Toast.makeText(mContext, "Attachment upload failed.", Toast.LENGTH_SHORT).show();
        List<AddAttachment> list = adapter.getArrayList();
        mTvMessage.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        int i = 0;
        for (AddAttachment attachment : list) {
            if (attachment.mGUID.equals(fileName)) {
                adapter.remove(i);
                mDelegate.onUploadingStatus(-1);
                return;
            }
            i++;
        }
    }

    @Override
    public void onSaveAudio(String fileName) {
        addAudio(fileName);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = mActivity.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onItemRemove(int position, String guId) {
        mTvMessage.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        mDelegate.onAttachedItemRemove(mPosition, position, guId);
    }

    @Override
    public void onAttachmentClicked(AddAttachment attachment, int position) {

        switch (attachment.mAttachmentType) {
            case SmartConstants.ATTACHMENT_IMG: {
                this.selectedPosition = position;

                Intent intent = new Intent(mActivity, SketchActivity.class);
                intent.putExtra(SmartConstants.SKETCH_IMAGE, attachment.mPath);
                mDelegate.onCapturePath(null, mViewobject);

                if (mFragment == null) {
                    mActivity.startActivityForResult(intent, SmartConstants.RESULT_SKETCH);
                } else {
                    mFragment.startActivityForResult(intent, SmartConstants.RESULT_SKETCH);
                }

                mActivity.overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
                break;
            }
            case SmartConstants.ATTACHMENT_AUD: {
                new AudioPlaybackDialog(mActivity, mActivity, attachment.mFullUrl, true).show();
                break;
            }
            case SmartConstants.ATTACHMENT_VID: {

                if (attachment.actualFileURI != null) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(attachment.actualFileURI, "video/*");

                    System.out.println(attachment.actualFileURI);

                    if (mFragment == null) {
                        mActivity.startActivity(intent);
                    } else {
                        mFragment.startActivity(intent);
                    }
                } else {
                    Toast.makeText(mContext, "Video file not available", Toast.LENGTH_LONG).show();
                }

                break;
            }
        }

    }

    public interface AddAttachmentCallback {

        //void onAttachmentSend(String title, String base64String, ProgressDialog progressDialog);

        void onCapturePath(String path, AddAttachmentView view);

        void addFileToValue(String fileName, int position);

        void onAttachedItemRemove(int position, int removeItemPosition, String guiId);

        void onUploadingStatus(int count);
    }
}
