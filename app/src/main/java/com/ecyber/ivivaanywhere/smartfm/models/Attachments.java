package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InputModesModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/11/2016.
 */
@Table(name = "Attachments")
public class Attachments extends Model {

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "AddNewAttachment")
    @SerializedName("AddNewAttachment")
    @Expose
    public int mAddNewAttachment;

    @SerializedName("Attachments")
    @Expose
    public List<AttachmentMaster> mAttachmentMasterList;

    @SerializedName("AttachmentTypes")
    @Expose
    public List<AttachmentTypesMaster> mAttachmentTypes;

    @Column(name = "InputModes")
    @SerializedName("InputModes")
    @Expose
    public List<String> mInputModes;

    public Attachments() {
        super();
    }


    public List<AttachmentTypesMaster> getAttachmentTypes() {
        return mAttachmentTypes;
    }

    public List<String> getInputModes() {
        return mInputModes;
    }

    public void setInputModes(List<String> mInputModes) {
        this.mInputModes = mInputModes;
    }

    public void setAttachmentTypes(List<AttachmentTypesMaster> attachmentTypes) {
        mAttachmentTypes = attachmentTypes;

    }

    public Attachments(int addNewAttachment, String objectKey, List<String> inputModes) {
        mAddNewAttachment = addNewAttachment;
        mObjectKey = objectKey;
        mInputModes = inputModes;
    }

    public int getAddNewAttachment() {
        return mAddNewAttachment;
    }

    public void setAddNewAttachment(int addNewAttachment) {
        mAddNewAttachment = addNewAttachment;
    }

    public List<AttachmentMaster> getAttachmentMasterList() {
        return mAttachmentMasterList;
    }

    public void setAttachmentMasterList(List<AttachmentMaster> attachmentMasterList) {
        mAttachmentMasterList = attachmentMasterList;
    }

    public Attachments select(String objectKey) {
        return new Select().from(Attachments.class).where("ObjectKey =?",objectKey).executeSingle();
    }

    public static Attachments getAttachmentsForObject(String objectKey) {
        return new Select().from(Attachments.class).where("ObjectKey =?",objectKey).executeSingle();
    }

    public void clearTable() {
        new Delete().from(Attachments.class).execute();
        new Delete().from(AttachmentMaster.class).execute();
        new Delete().from(AttachmentTypesMaster.class).execute();
        new Delete().from(InputModesModel.class).execute();
    }
}
