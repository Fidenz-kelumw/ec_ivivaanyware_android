package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.graphics.Bitmap;
import android.util.Base64;

import com.google.gson.annotations.Expose;

import java.io.ByteArrayOutputStream;

/**
 * Created by Chathura Jayanath on 7/20/2015.
 */
public class BitMaptoBase64Converter {
    public BitMaptoBase64Converter() {
    }

    public String converterBase64(Bitmap bitmap,int percentage){
        String encoded = "";
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, percentage, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            bitmap.recycle();
            encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e) {

        }

        return encoded;
    }
}
