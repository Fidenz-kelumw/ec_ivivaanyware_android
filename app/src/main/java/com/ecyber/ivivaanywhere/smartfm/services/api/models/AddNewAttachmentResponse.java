package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/12/2016.
 */
public class AddNewAttachmentResponse {
    @SerializedName("Success")
    @Expose
    public int mSuccess;

    @SerializedName("Message")
    @Expose
    public String mMessage;

    @SerializedName("AttachmentKey")
    @Expose
    public int mAttachmentKey;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getAttachmentKey() {
        return mAttachmentKey;
    }

    public void setAttachmentKey(int attachmentKey) {
        mAttachmentKey = attachmentKey;
    }
}
