package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SketchActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.BitMaptoBase64Converter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;
import com.ecyber.ivivaanywhere.smartfm.models.InputMode;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Lakmal on 6/1/2016.
 */
public class AttachmentSendDialog implements AttachmentTypeDialog.AttachmentTypeListCallback, SignatureDialog.SignatureCallback, AudioRecordDialog.AudioRecordCallback {

    private static final String TAG = "AttachmentSendDialog";
    private ColoredSnackbar snackbar;

    private Activity mActivity;
    private AttachmentSendCallback mDelegate;
    private ImageView mAttachedImage;
    private ImageView mAttType;
    private Uri mAttachedVideoPath;
    private CustomEditTextView mTitleDialog;
    private CustomButton mBtnSaveFile;
    private CustomButton mBtnTypeSelect;
    private Bitmap mBitmapFile;
    private Uri mUri = null;
    private Fragment mFragment;
    private String mObjectKey;
    private Dialog mDialog;
    private RelativeLayout mAudioPlaybackContainer;
    private LinearLayout mAttachmentTypeContainer;
    private LinearLayout mTypePhoto;
    private LinearLayout mTypeAudio;
    private LinearLayout mTypeSig;
    private LinearLayout mTypeVideo;
    private LinearLayout mTypeFile;
    private ImageButton mBtnPhoto;
    private ImageButton mBtnFile;
    private ImageButton mBtnSig;
    private ImageButton mBtnVideo;
    private ImageButton mBtnAudio;
    private ImageButton mBtnPlayPause;
    private ImageButton mBtnStop;
    private MediaPlayer mPlayer = null;
    private Chronometer mPlaybackTime;
    private boolean mIsPlaying = false;
    private long mTimeWhenStopped = 0;
    private int mContinuePos = 0;

    // Attachment Details
    private String mCurrentAttachmentCode = null;
    private String mFileName = null;
    private String mFilePath = null;
    private boolean isAttachmentSet = false;

    //region Public Methods
    public AttachmentSendDialog(Activity activity, AttachmentSendCallback delegate, Fragment fragment, String objectKey) {
        mActivity = activity;
        mDelegate = delegate;
        mFragment = fragment;
        mObjectKey = objectKey;

        snackbar = new ColoredSnackbar(activity);
    }

    public Dialog show() {
        final Dialog dialogAttachment = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View viewDialog = i.inflate(R.layout.view_add_attachment, null);

        // mLayoutButtonHolder = (LinearLayout) viewDialog.findViewById(R.id.lay_lin_add_attachment_button_holder);
        mAttachedImage = (ImageView) viewDialog.findViewById(R.id.img_preview_att);
        mAttType = (ImageView) viewDialog.findViewById(R.id.img_type);

        mBtnSaveFile = (CustomButton) viewDialog.findViewById(R.id.btn_save_file);
        mBtnTypeSelect = (CustomButton) viewDialog.findViewById(R.id.btn_type_select);

        mAttachmentTypeContainer = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_type_cont);
        mAudioPlaybackContainer = (RelativeLayout) viewDialog.findViewById(R.id.rel_lay_audio_playback);

        mTypePhoto = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_photo);
        mTypeAudio = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_audio);
        mTypeVideo = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_video);
        mTypeSig   = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_sig);
        mTypeFile  = (LinearLayout) viewDialog.findViewById(R.id.lin_lay_att_file);

        mBtnPhoto = (ImageButton) viewDialog.findViewById(R.id.img_btn_photo);
        mBtnVideo = (ImageButton) viewDialog.findViewById(R.id.img_btn_video);
        mBtnAudio = (ImageButton) viewDialog.findViewById(R.id.img_btn_audio);
        mBtnSig   = (ImageButton) viewDialog.findViewById(R.id.img_btn_sig);
        mBtnFile  = (ImageButton) viewDialog.findViewById(R.id.img_btn_file);

        mBtnPlayPause  = (ImageButton) viewDialog.findViewById(R.id.btn_play_pause);
        mBtnStop  = (ImageButton) viewDialog.findViewById(R.id.btn_stop);
        mPlaybackTime = (Chronometer) viewDialog.findViewById(R.id.lbl_audio_time);

        mPlaybackTime.setText("00:00:00");

        mPlaybackTime.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h   = (int)(time /3600000);
                int m = (int)(time - h*3600000)/60000;
                int s= (int)(time - h*3600000- m*60000)/1000 ;
                String hh = h < 10 ? "0"+h: h+"";
                String mm = m < 10 ? "0"+m: m+"";
                String ss = s < 10 ? "0"+s: s+"";
                cArg.setText(hh+":"+mm+":"+ss);
            }
        });

        registerCallbacks();
        registerAudioPlayerCallbacks();

        viewDialog.post(new Runnable() {
            @Override
            public void run() {
                int itemWidth = mAttachmentTypeContainer.getWidth() / 5;

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(itemWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.height = itemWidth;

                mTypePhoto.setLayoutParams(lp);
                mTypeAudio.setLayoutParams(lp);
                mTypeVideo.setLayoutParams(lp);
                mTypeSig.setLayoutParams(lp);
                mTypeFile.setLayoutParams(lp);

                mTypePhoto.setVisibility(View.GONE);
                mTypeAudio.setVisibility(View.GONE);
                mTypeVideo.setVisibility(View.GONE);
                mTypeSig.setVisibility(View.GONE);
                mTypeFile.setVisibility(View.GONE);

                List<InputMode> inputModes = InputMode.getInputModesForObject(mObjectKey);

                if(inputModes != null) {
                    for(InputMode im : inputModes) {

                        String mode = im.getMode();

                        if(mode.equals("PHOTO")) {
                            mTypePhoto.setVisibility(View.VISIBLE);
                        } else if(mode.equals("AUDIO")) {
                            mTypeAudio.setVisibility(View.VISIBLE);
                        } else if(mode.equals("VIDEO")) {
                            mTypeVideo.setVisibility(View.VISIBLE);
                        } else if(mode.equals("SIG")) {
                            mTypeSig.setVisibility(View.VISIBLE);
                        } else if(mode.equals("FILE")) {
                            mTypeFile.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });

        List<AttachmentTypesMaster> attachmentTypes = new Select().from(AttachmentTypesMaster.class).execute();
        if (attachmentTypes != null) {
            if (attachmentTypes.size() != 0) {
                if (attachmentTypes.get(0).getTypeName() != null) {
                    mBtnTypeSelect.setText(attachmentTypes.get(0).getTypeName());
                }

            }
        }
        //mBtnSaveFile.setEnabled(false);
        isAttachmentSet = false;

        mTitleDialog = (CustomEditTextView) viewDialog.findViewById(R.id.editText_attachmnet_titile);
        dialogAttachment.setContentView(viewDialog);

        ImageButton dClose = (ImageButton) viewDialog.findViewById(R.id.imageButton_attachment_close);
        dClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAttachment.dismiss();
                mAttachedImage = null;
            }
        });

        mBtnTypeSelect.setOnClickListener(v -> showAttachmentTypeSelectDialog());

        mBtnSaveFile.setOnClickListener(view -> {

            if (isAttachmentSet) {
                String Title = mTitleDialog.getText().toString();
                mDialog.dismiss();

                snackbar.showSnackBar("Updating...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);

                new ConvertAsync(Title).execute("");
            } else {
                Toast.makeText(mActivity, "Please select an attachment to continue", Toast.LENGTH_LONG).show();
            }
        });

        dialogAttachment.show();
        dialogAttachment.setCanceledOnTouchOutside(false);
        dialogAttachment.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAttachment.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        return dialogAttachment;
    }


    public void setVideoToView(Uri videoPath) {

        mAttType.setImageResource(R.drawable.att_video);
        mAttType.setVisibility(View.VISIBLE);

        mCurrentAttachmentCode = "VID";
        //mBtnSaveFile.setEnabled(true);
        isAttachmentSet = true;

        if(videoPath != null) {
            mAttachedVideoPath = videoPath;
            mAttachmentTypeContainer.setVisibility(View.GONE);

            mFilePath = getRealPathFromURI(videoPath);
            //mFilePath = videoPath.getPath();

            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mFilePath, MediaStore.Images.Thumbnails.MINI_KIND);
            mAttachedImage.setImageBitmap(thumb);
            mAttachedImage.setVisibility(View.VISIBLE);

            mAttachedImage.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(mAttachedVideoPath, "video/*");
                mFragment.startActivity(intent);
            });
        }

    }

    public void imageSetToView(String imagePath, boolean isSignature, Uri uri) {
        mCurrentAttachmentCode = "IMG";

        mAttachedImage.setVisibility(View.VISIBLE);
        try {
            mFilePath = imagePath;
            if (Utility.IsEmpty(mFilePath)) {
                String tempPath = Utility.IsEmpty(mFilePath) ? Utility.createNewPath() : mFilePath;
                mBitmapFile = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), uri);
                if (Utility.createdTempImageFile(mBitmapFile, tempPath) != null)
                    mFilePath = tempPath;

                mBitmapFile = Bitmap.createBitmap(BitmapFactory.decodeFile(mFilePath));
            } else {
                mBitmapFile = Bitmap.createBitmap(BitmapFactory.decodeFile(mFilePath));
            }

//            mBitmapFile = new Utility().getResizedBitmap(mBitmapFile);
//            String tempPath = Utility.createNewPath();
//            Utility.createdTempImageFile(mBitmapFile, tempPath);
//            mFilePath = tempPath;

            if (!Utility.IsEmpty(mFilePath))
                mBitmapFile = Utility.rotateImage(mBitmapFile, mFilePath, mActivity.getBaseContext());

            Bitmap TempViewBitmap = Bitmap.createScaledBitmap(mBitmapFile, mBitmapFile.getWidth() / 2, mBitmapFile.getHeight() / 2, false);
            mAttachedImage.setImageBitmap(TempViewBitmap);
            //mBtnSaveFile.setEnabled(true);
            isAttachmentSet = true;

            if (isSignature) {
                //mBtnSignature.setVisibility(View.GONE);
            }
            //mLayoutButtonHolder.setVisibility(View.GONE);
            mAttachmentTypeContainer.setVisibility(View.GONE);

            mAttachedImage.setOnClickListener(view -> {
                Intent intent = new Intent(mActivity, SketchActivity.class);
                intent.putExtra(SmartConstants.SKETCH_IMAGE, mFilePath);
                mFragment.startActivityForResult(intent, SmartConstants.RESULT_SKETCH);
                mActivity.overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
            });

        } catch (Exception e) {
            Toast.makeText(mActivity, "Unable to load image", Toast.LENGTH_SHORT).show();
            //mBtnSaveFile.setEnabled(false);
            isAttachmentSet = false;
            mAttachmentTypeContainer.setVisibility(View.GONE);
            //mLayoutButtonHolder.setVisibility(View.GONE);
            return;
        }
    }

    private void setAudioToPlayer(String fileName) {

        mCurrentAttachmentCode = "AUD";

        mFileName = fileName;
        mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mFileName;
        mAttachmentTypeContainer.setVisibility(View.GONE);
        mAudioPlaybackContainer.setVisibility(View.VISIBLE);
    }
    //endregion

    //region Private Methods
    private void registerCallbacks() {

        mBtnPhoto.setOnClickListener(view -> {
            try {
                final int requestId = 1;
                Uri fileUri = Uri.fromFile(getOutputMediaFile());

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                mDelegate.onCapturePath(fileUri.getPath());

                Bundle bundle = new Bundle();
                bundle.putString("imgPath", fileUri.getPath());
                cameraIntent.putExtras(bundle);

                mFragment.startActivityForResult(cameraIntent, SmartConstants.RESULT_IMAGE_CAMERA);
            } catch (IllegalStateException e) {
                Log.e(TAG, "onClick: " + e.getStackTrace().toString());
            } catch (Exception e) {
                Log.e(TAG, "onClick: " + e.getStackTrace().toString());
            }
        });

        mBtnFile.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            mFragment.startActivityForResult(i, SmartConstants.RESULT_IMAGE_GALLERY);
        });

        mBtnAudio.setOnClickListener(view -> openAudioRecordDialog());

        mBtnSig.setOnClickListener(view -> openSignatureDialog());

        mBtnVideo.setOnClickListener(view -> {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            //File mediaFile =  new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + makeGUIDForType(".mp4"));
            //takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
            if (takeVideoIntent.resolveActivity(mActivity.getPackageManager()) != null) {
                mFragment.startActivityForResult(takeVideoIntent, SmartConstants.RESULT_VIDEO_CAPTURE);
            }
        });
    }

    void registerAudioPlayerCallbacks() {
        mBtnPlayPause.setOnClickListener(view -> {
            if(mIsPlaying) {
                pausePlayback();
            } else {
                startPlayback();
            }
        });

        mBtnStop.setOnClickListener(view -> {
            if(mIsPlaying) {
                stopPlayback();
            }
        });
    }


    //endregion

    //region Audio Player
    void startPlayback() {

        mPlaybackTime.setBase(SystemClock.elapsedRealtime() + mTimeWhenStopped);
        mPlaybackTime.start();

        if(mPlayer != null) {
            mPlayer.seekTo(mContinuePos);
            mPlayer.start();

            mBtnPlayPause.setImageResource(R.drawable.pause);
        } else {
            mIsPlaying = true;

            mBtnPlayPause.setImageResource(R.drawable.pause);

            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mFileName);

                mPlayer.setOnCompletionListener(mp -> {
                    stopPlayback();
                    mBtnPlayPause.setImageResource(R.drawable.play);
                });

                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {

            }
        }
    }

    void stopPlayback() {
        mPlayer.release();
        mPlayer = null;
        mTimeWhenStopped = 0;
        mPlaybackTime.setBase(SystemClock.elapsedRealtime());
        mPlaybackTime.stop();
        mPlaybackTime.setText("00:00:00");
        mBtnPlayPause.setImageResource(R.drawable.play);
        mIsPlaying = false;
    }

    void pausePlayback() {
        mPlayer.pause();
        mContinuePos = mPlayer.getCurrentPosition();
        mTimeWhenStopped = mPlaybackTime.getBase() - SystemClock.elapsedRealtime();
        mPlaybackTime.stop();
        mBtnPlayPause.setImageResource(R.drawable.play);
        mIsPlaying = false;
    }
    //endregion

    //region Utility
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = mActivity.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String converterImage() {
        BitMaptoBase64Converter conv = new BitMaptoBase64Converter();
        String base64String = "";
        Bitmap bitmap;

        try {
            if (Utility.IsEmpty(mFilePath)) {
                bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), mUri);
            } else {
                bitmap = BitmapFactory.decodeFile(mFilePath);
            }

            bitmap = new Utility().getResizedBitmap(bitmap);

            String tempPath = Utility.IsEmpty(mFilePath) ? Utility.createNewPath() : mFilePath;


            if (Utility.IsEmpty(mFilePath))
                Utility.createdTempImageFile(bitmap, tempPath);

            if (!Utility.IsEmpty(tempPath))
                bitmap = Utility.rotateImage(bitmap, tempPath, mActivity);

            File file = new File(tempPath);
            long length = file.length();// TODO: 20/10/2016 change this function for the recusive func
            long mega_bytes = (length / (1024 * 1024));
            int reduce = 80;

            if (mega_bytes > 1) {
                reduce = 50;
            }

            base64String = conv.converterBase64(bitmap, reduce);
            bitmap.recycle();
        } catch (Exception e) {

        }
        return base64String;
    }

    private void showMsgFailedImageSelect() {
        Toast.makeText(mActivity, "Failed image select.Please try again", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    private static File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(SmartConstants.FILE_SAVE_LOCATION, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public void setDialogInstance(Dialog thisDialog) {
        mDialog = thisDialog;
    }

    private void displayMsg() {
        Toast.makeText(mActivity, "Please select attachment type", Toast.LENGTH_SHORT).show();
    }

    public String makeGUIDForType(String ext) {
        String guid = UUID.randomUUID().toString();
        guid = new AccountPermission().getAccountInfo().get(2) + mActivity.getString(R.string.underscore) + guid + mActivity.getString(R.string.underscore) + new Date().getTime();

        return guid + ext;
    }

    public String makeGUID() {
        String guid = UUID.randomUUID().toString();
        guid = new AccountPermission().getAccountInfo().get(2) + mActivity.getString(R.string.underscore) + guid + mActivity.getString(R.string.underscore) + new Date().getTime();

        String extension = "";

        if(mCurrentAttachmentCode.equals("IMG")) {
            extension = ".jpg";
        } else if(mCurrentAttachmentCode.equals("AUD")) {
            extension = ".mp4";
        } else if(mCurrentAttachmentCode.equals("VID")) {
            extension = ".mov";
        }

        return guid + extension;
    }
    //endregion

    //region Dialog Methods
    private void openSignatureDialog() {
        new SignatureDialog(mActivity, this).show();
    }

    private void openAudioRecordDialog() {
        new AudioRecordDialog(mActivity ,mActivity, this).show();
    }

    private void showAttachmentTypeSelectDialog() {
        new AttachmentTypeDialog(mActivity, this).makeDialog();
    }
    //endregion

    //region Callbacks Implementations
    @Override
    public void onSaveAudio(String fileName) {
        setAudioToPlayer(fileName);
        //mBtnSaveFile.setEnabled(true);
        isAttachmentSet = true;
    }

    @Override
    public void onSaveSignature(String picturePath) {
        imageSetToView(picturePath, true, null);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =  mActivity.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onTypeItemClick(AttachmentTypesMaster type, Dialog dialogInstance) {
        mBtnTypeSelect.setText("" + type.getTypeName());
        mBtnTypeSelect.setTag("" + type.getSignature());
        dialogInstance.dismiss();
        //mBtnSignature.setVisibility(View.GONE);
        if (type.getSignature() == 1) {
            //mBtnSignature.setVisibility(View.VISIBLE);
            //mLayoutButtonHolder.setVisibility(View.GONE);
            mAttachmentTypeContainer.setVisibility(View.GONE);
            mAttachedImage.setImageResource(R.drawable.rounded_white_box);
            //mBtnSaveFile.setEnabled(false);
            isAttachmentSet = false;
        } else {
            mAttachedImage.setImageResource(R.drawable.rounded_white_box);
            //mBtnSaveFile.setEnabled(false);
            isAttachmentSet = false;
            //mLayoutButtonHolder.setVisibility(View.VISIBLE);
            mAttachedImage.setOnClickListener(null);
            mAttachedImage.setVisibility(View.INVISIBLE);
            mAttachmentTypeContainer.setVisibility(View.VISIBLE);
        }
    }

    //endregion

    //region Callback Interfaces
    public interface AttachmentSendCallback {

        void onAttachmentSend(String attachmentType, String title, String base64String, Bitmap bitmap);

        void onAttachmentSend(String attachmentCode, String attachmentType, String title, String filePatch, String fileName);

        void onCapturePath(String path);
    }
    //endregion

    //region Inner Classes
    private class ConvertAsync extends AsyncTask<String, Integer, String> {
        private ProgressDialog dialog;
        private String mTitle;

        public ConvertAsync(String title) {
            this.mTitle = title;
            this.dialog = new ProgressDialog(mActivity);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setIndeterminate(true);
        }

        protected void onPreExecute() {
            this.dialog.setMessage("Please wait...");
            //this.dialog.show();
        }


        //public void onAttachmentSend(String attachmentCode, String attachmentType, String title, String filePath, String fileName){

        @Override
        protected void onPostExecute(String base64String) {
            //super.onPostExecute(s);

            if(mCurrentAttachmentCode == "IMG") {
                mBitmapFile = new Utility().getResizedBitmap(mBitmapFile);
                String tempPath = Utility.createNewPath();
                Utility.createdTempImageFile(mBitmapFile, tempPath);
                mFilePath = tempPath;
            }

            mDelegate.onAttachmentSend(mCurrentAttachmentCode, mBtnTypeSelect.getText().toString(), mTitleDialog.getText().toString(), mFilePath, makeGUID());
            if (mBitmapFile != null) {
                mBitmapFile.recycle();
            }
            //dialog.dismiss();
        }

        @Override
        protected String doInBackground(String... strings) {
            String base64String = converterImage();
            return base64String;
        }
    }
    //endregion
}
