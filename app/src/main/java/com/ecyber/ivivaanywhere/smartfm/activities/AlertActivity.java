package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMApplication;

public class AlertActivity extends AppCompatActivity implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {

    AlertDialog mAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);

        setTitle("");

        SmartFMApplication.setAlertActivity(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.connectivity_alert_title));
        builder.setMessage(getResources().getString(R.string.connectivity_alert_body));

        // Add the buttons
        builder.setPositiveButton("OK", (dialog, id) -> {
            // User clicked OK button
            dialog.dismiss();
        });

        mAlert = builder.create();
        mAlert.setOnCancelListener(this);
        mAlert.setOnDismissListener(this);
        mAlert.setCanceledOnTouchOutside(false);
        mAlert.show();

        final Button positiveButton = mAlert.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout parent = (LinearLayout) positiveButton.getParent();
        parent.setGravity(Gravity.CENTER_HORIZONTAL);
        View leftSpacer = parent.getChildAt(1);
        leftSpacer.setVisibility(View.GONE);
    }

    public void closeAlert() {
        if(mAlert != null) {
            mAlert.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
