package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentMaster;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.models.InputMode;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectAttachments;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.joda.time.Interval;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/11/2016.
 */
public class GetObjectAttachmentsSync {
    private GetObjectAttachmentsCallBack mGetObjectAttachmentsCallBack;
    public Context mContext;

    public GetObjectAttachmentsSync(Context context) {
        mContext = context;
    }

    public GetObjectAttachmentsSync(GetObjectAttachmentsCallBack getObjectAttachmentsCallBack) {
        mGetObjectAttachmentsCallBack = getObjectAttachmentsCallBack;
    }

    public void GetObjectAttachments(String url, String apiKey, String userKey, String objType, final String objKey, String tabName) {
        ServiceGenerator.CreateService(GetObjectAttachments.class, url).getObjectAttachments(apiKey, userKey, objType, objKey, tabName)
                .enqueue(new Callback<Attachments>() {
                    @Override
                    public void onResponse(Call<Attachments> call, Response<Attachments> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                new Attachments().clearTable();
                                boolean isSaved = saveToDb(response.body(), objKey);
                                if (isSaved) {
                                    mGetObjectAttachmentsCallBack.OnGetObjectAttachmentsSuccess(response.body());
                                    new TimeCap().updateTableTime(tabName);
                                } else {
                                    mGetObjectAttachmentsCallBack.OnOnGetObjectAttachmentsError(SmartConstants.REFRESH_LIST);
                                }

                            } else {
                                mGetObjectAttachmentsCallBack.OnOnGetObjectAttachmentsError(SmartConstants.ITEM_NOT_FOUND);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Object Attachments", message);
                                mGetObjectAttachmentsCallBack.OnOnGetObjectAttachmentsError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetObjectAttachmentsCallBack.OnOnGetObjectAttachmentsError(SmartConstants.ITEM_NOT_FOUND);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Attachments> call, Throwable t) {
                        mGetObjectAttachmentsCallBack.OnOnGetObjectAttachmentsError(SmartConstants.REFRESH_LIST);
                    }
                });
    }

    public boolean getFromDb(String objectKey) {
        Attachments attachment = new Attachments().select(objectKey);
        List<AttachmentMaster> attachmentList = new AttachmentMaster().select(objectKey);
        List<AttachmentTypesMaster> attachmentTypesMasterList = new AttachmentTypesMaster().select(objectKey);

        if (attachmentList.size() == 0) {
            return false;
        } else {
            attachment = reCreateAttachment(attachmentList, attachment, attachmentTypesMasterList);
            mGetObjectAttachmentsCallBack.OnGetObjectAttachmentsSuccess(attachment);
            return true;
        }


    }

    private Attachments reCreateAttachment(List<AttachmentMaster> _attachmentList, Attachments _attachment, List<AttachmentTypesMaster> _attachmentTypesMasterList) {
        List<AttachmentMaster> newAttachmentMasterList = new ArrayList<>();
        List<AttachmentTypesMaster> newAttachmentTypesMasterList = new ArrayList<>();
        Attachments returnAttachment = new Attachments();

        for (AttachmentTypesMaster attachmentTypesMaster : _attachmentTypesMasterList) {
            newAttachmentTypesMasterList.add(attachmentTypesMaster);
        }
        for (AttachmentMaster attachmentMaster : _attachmentList) {
            AttachmentMaster resAttachmentMaster = setUserToAttachmentMaster(attachmentMaster);
            newAttachmentMasterList.add(resAttachmentMaster);
        }

        returnAttachment.setAddNewAttachment(_attachment.getAddNewAttachment());
        returnAttachment.setAttachmentMasterList(newAttachmentMasterList);
        returnAttachment.setAttachmentTypes(newAttachmentTypesMasterList);

        // Input Modes
        returnAttachment.setInputModes(_attachment.getInputModes());

        return returnAttachment;
    }

    /**
     * get attachment details from local database
     *
     * @param attachmentMaster
     * @return
     */
    private AttachmentMaster setUserToAttachmentMaster(AttachmentMaster attachmentMaster) {
        AttachmentMaster attachment = new AttachmentMaster();

        attachment.setAttachmentKey(attachmentMaster.getAttachmentKey());
        attachment.setAttachmentType(attachmentMaster.getAttachmentType());
        attachment.setAttachmentName(attachmentMaster.getAttachmentName());
        attachment.setFilePath(attachmentMaster.getFilePath());
        attachment.setUploadedAt(attachmentMaster.getUploadedAt());

        User user = new User(attachmentMaster.getUserKey(), attachmentMaster.getUserName(), attachmentMaster.getImagePath(), "");

        attachment.setUploadedUser(user);
        return attachment;
    }


    private boolean saveToDb(Attachments attachments, String objectKey) {
        // Attachments save to DB
        // this attachmentID is using another master tables(AttachmentTypesMaster , AttachmentMaster)
        Long attachmentID = new Attachments(attachments.getAddNewAttachment(), objectKey, attachments.getInputModes()).save();

        InputMode.setInputModesForObject(attachments.getInputModes(), objectKey);

        // Attachment types master data save to DB
        List<AttachmentTypesMaster> attachmentTypes = attachments.getAttachmentTypes();
        if (attachmentTypes != null) {
            new Delete().from(AttachmentTypesMaster.class).execute();
            for (AttachmentTypesMaster attachmentType : attachmentTypes) {
                new AttachmentTypesMaster(attachmentID, attachmentType.getTypeName(), attachmentType.getSignature(), objectKey).save();
            }
            // attachmentsMaster data save to DB
            List<AttachmentMaster> attachmentMasterList = attachments.getAttachmentMasterList();
            for (AttachmentMaster attachmentMaster : attachmentMasterList) {
                saveMessageToLocalDB(attachmentMaster, attachmentID, objectKey);
            }
            return true;
        } else {
            return false;
        }
    }

    private void saveMessageToLocalDB(AttachmentMaster attachments, long attachmentID, String objectKey) {

        new AttachmentMaster(objectKey, attachmentID,
                attachments.getAttachmentKey(),
                attachments.getAttachmentType(),
                attachments.getAttachmentName(),
                attachments.getFilePath(),
                attachments.getUploadedAt(),
                attachments.getUploadedUser().getUserKey(),
                attachments.getUploadedUser().getUserName(),
                attachments.getUploadedUser().getImagePath(),
                attachments.getAttachmentType()).save();
    }

    public void RemoveDiskCacheBeforeWeek() {
        new RemoveDiskCacheAsync().execute("");

    }

    public interface GetObjectAttachmentsCallBack {
        void OnGetObjectAttachmentsSuccess(Attachments attachments);

        void OnOnGetObjectAttachmentsError(String message);
    }

    private class RemoveDiskCacheAsync extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... args) {
            try {
                File[] fileList = StorageUtils.getOwnCacheDirectory(mContext, SmartConstants.CACHE_DIR_ATTACHMETNS).listFiles();
                for (File file : fileList) {
                    long modified = file.lastModified();//milliseconds
                    long current = Calendar.getInstance(Locale.getDefault()).getTime().getTime();//milliseconds
                    Interval interval = new Interval(modified, current);
                    if (interval.toDuration().getStandardSeconds() > 604800) {
                        File forceDel = new File(file.getPath());
                        if (forceDel.exists()) {
                            forceDel.delete();
                            Log.e(SmartConstants.CACHE_DIR_ATTACHMETNS, file.getPath());
                        }
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }
    }
}
