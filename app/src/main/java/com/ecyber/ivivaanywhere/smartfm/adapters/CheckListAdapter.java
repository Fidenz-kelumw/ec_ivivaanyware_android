package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class CheckListAdapter extends RecyclerView.Adapter<CheckListAdapter.Holder> {

    private Context mContext;
    private List<CheckList> mCheckList;
    private CheckListActionListener mCheckListActionListener;

    public CheckListAdapter(Context context, CheckListActionListener checkListActionListener, List<CheckList> checkList) {
        mContext = context;
        mCheckList = checkList;
        mCheckListActionListener = checkListActionListener;
    }

    public void addItem(CheckList checkList) {
        mCheckList.add(checkList);

//        Collections.sort(mCheckList, new Comparator<CheckList>() {
//            @Override
//            public int compare(CheckList r2, CheckList r1) {
//                Date one = null, two = null;
//                try {
//                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//                    one = format.parse(r1.getmDeadLine());
//                    two = format.parse(r2.getmDeadLine());
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//                return one.compareTo(two);
//            }
//        });

        Collections.sort(mCheckList, new Comparator<CheckList>() {
            @Override
            public int compare(CheckList r1, CheckList r2) {
                return r1.getmItemKey().compareTo(r2.getmItemKey());
            }
        });

        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(mContext.getApplicationContext()).inflate(R.layout.widget_checklist_item, parent, false);

        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        CheckList checkItem = mCheckList.get(position);

        holder.tvCheckTitle.setText(checkItem.getmItemName());
        holder.tvCheckItemKey.setText(checkItem.getmItemKey());
        holder.tvChecklistTime.setText(checkItem.getmDeadLine());
        holder.tvDescription.setText(checkItem.getmDescription());
        holder.tvChecklistTime.setText(getDeadLineFormat(checkItem));

        String status = checkItem.getmStatus();

        if (status.equals("0"))
            holder.mCheckBoxItem.setChecked(false);
        else
            holder.mCheckBoxItem.setChecked(true);

        // checkbox status
        try {
            if(checkItem != null) {
                if(checkItem.getmSetItemStatus() != null && !checkItem.getmSetItemStatus().equals("")) {
                    if (checkItem.getmSetItemStatus().equals("0"))
                        holder.mCheckBoxItem.setEnabled(false);
                    else
                        holder.mCheckBoxItem.setEnabled(true);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        // calendar icon state
        if(checkItem.getmSetItemDeadline() != null && !checkItem.getmSetItemDeadline().equals("")) {
            if (checkItem.getmSetItemDeadline().equals("0")) {
                // display but disable
                holder.imgBtnViewSetDate.setEnabled(false);
                holder.imgBtnViewSetDate.setVisibility(View.VISIBLE);
                holder.imgBtnViewSetDate.setBackgroundResource(R.drawable.ico_checklist_right_normal);  // state disable
            } else if (checkItem.getmSetItemDeadline().equals("1")) {
                // display and edit
                holder.imgBtnViewSetDate.setEnabled(true);
                holder.imgBtnViewSetDate.setVisibility(View.VISIBLE);
                if (checkItem.getmDeadLine() == null)
                    holder.imgBtnViewSetDate.setBackgroundResource(R.drawable.ico_checklist_right_normal);
                else
                    holder.imgBtnViewSetDate.setBackgroundResource(R.drawable.ico_checklist_right_green);
            } else if (checkItem.getmSetItemDeadline().equals("2")) {
                // hide
                holder.imgBtnViewSetDate.setEnabled(false);
                holder.imgBtnViewSetDate.setVisibility(View.INVISIBLE);
            } else {
                holder.imgBtnViewSetDate.setEnabled(false);
                holder.imgBtnViewSetDate.setVisibility(View.INVISIBLE);
            }
        }

        // info icon
        if(checkItem.getInfoCode() != null && !checkItem.getInfoCode().isEmpty()){
            holder.imageButtonInfo.setVisibility(View.VISIBLE);
        } else {
            holder.imageButtonInfo.setVisibility(View.GONE);
        }

        holder.imgBtnViewSetDate.setTag(position);
    }

    private String getDeadLineFormat(CheckList checkItem) {

        String DateShowLeft = "";
        if (checkItem.getmDeadLine() != null) {
            String year, month, day, hrs, min;
            year = (checkItem.getmDeadLine().toString()).substring(0, 4);
            month = (checkItem.getmDeadLine().toString()).substring(5, 7);
            day = (checkItem.getmDeadLine().toString()).substring(8, 10);
            hrs = (checkItem.getmDeadLine().toString()).substring(11, 13);
            min = (checkItem.getmDeadLine().toString()).substring(14, 16);

            String SDate = day + "/" + month + "/" + year + " " + hrs + ":" + min + ":00";

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");

            SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/M/dd HH:mm");

            Date sdate = new Date();
            try {
                sdate = simpleDateFormat.parse(SDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DateShowLeft = simpleDate.format(sdate);
        }

        return DateShowLeft;
    }

    @Override
    public int getItemCount() {
        return null != mCheckList ? mCheckList.size() : 0;
    }

    public void updateCheckDeadLine(String deadLineDate, int position) {
        mCheckList.get(position).setmDeadLine(deadLineDate);
        notifyItemChanged(position);
    }


    public void updateCheckDescription(String description, int position) {
        mCheckList.get(position).setmDescription(description);
        notifyItemChanged(position);
    }

    public void updateStatus(String status, int position) {
        mCheckList.get(position).setmStatus(status);
        notifyDataSetChanged();
    }

    public void clear() {
        mCheckList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CheckBox mCheckBoxItem;
        CustomTextView tvCheckTitle, tvCheckItemKey, tvChecklistTime, tvDescription;
        ImageButton imgBtnViewSetDate, imageButtonInfo;

        public Holder(View itemView) {
            super(itemView);
            mCheckBoxItem = (CheckBox) itemView.findViewById(R.id.checkBox_checklist);
            tvCheckTitle = (CustomTextView) itemView.findViewById(R.id.textView_checklist_title);
            tvCheckItemKey = (CustomTextView) itemView.findViewById(R.id.textView_checkitemkey);
            tvChecklistTime = (CustomTextView) itemView.findViewById(R.id.textView_checkliat_time);
            tvDescription = (CustomTextView) itemView.findViewById(R.id.textView_checklist_description);
            imgBtnViewSetDate = (ImageButton) itemView.findViewById(R.id.imageView_set_date);
            imageButtonInfo = (ImageButton) itemView.findViewById(R.id.imageView_info);
            mCheckBoxItem.setOnClickListener(this);
            imgBtnViewSetDate.setOnClickListener(this);
            tvDescription.setOnClickListener(this);
            imageButtonInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.checkBox_checklist:
                    mCheckListActionListener.OnCheckChanged((CompoundButton) v, mCheckList.get(this.getAdapterPosition()).getmItemKey(), getAdapterPosition());
                    break;
                case R.id.imageView_set_date:
                    mCheckListActionListener.OnClickDate(this.getAdapterPosition(), mCheckList.get(this.getAdapterPosition()));
                    break;
                case R.id.textView_checklist_description:
                    mCheckListActionListener.OnClickRemark(this.getAdapterPosition(), mCheckList.get(this.getAdapterPosition()));
                    break;
                case R.id.imageView_info:
                    mCheckListActionListener.OnClickInfo(this.getAdapterPosition(), mCheckList.get(this.getAdapterPosition()));
                    break;
            }
        }
    }

    public interface CheckListActionListener {

        void OnCheckChanged(CompoundButton checkButton, String itemKey, int position);

        void OnClickDate(int position, CheckList checkList);

        void OnClickRemark(int position, CheckList checkList);

        void OnClickInfo(int position, CheckList checkList);
    }
}
