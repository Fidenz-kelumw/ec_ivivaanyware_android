package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "ValueModel")
public class ValueModel extends Model{

    @Column(name = "fieldID")
    String fieldID;

    @Column(name = "valueType")
    ValueType valueType;

    @Column(name = "value")
    String value;

    @Column(name = "attName")
    String ATTName;

    @Column(name = "attType")
    String ATTType;

    public ValueModel() {
    }

    public ValueModel(String fieldID, ValueType valueType, String value, String ATTName, String ATTType) {
        this.fieldID = fieldID;
        this.valueType = valueType;
        this.value = value;
        this.ATTName = ATTName;
        this.ATTType = ATTType;
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getATTName() {
        return ATTName;
    }

    public void setATTName(String ATTName) {
        this.ATTName = ATTName;
    }

    public String getATTType() {
        return ATTType;
    }

    public void setATTType(String ATTType) {
        this.ATTType = ATTType;
    }

    public void clearTable(){
        new Delete().from(ValueModel.class).execute();
    }

    public String getValueSingle(String fieldID){

        List<ValueModel> model = new Select()
                .from(ValueModel.class)
                .where("fieldID = ?",fieldID)
                .execute();

        return model != null ? model.get(0).value : null;
    }

    public String[] getValuesStringArray(String fieldID){

        List<ValueModel> model = new Select()
                .from(ValueModel.class)
                .where("fieldID = ?",fieldID)
                .execute();

        String[] tempArray = new String[model.size()];
        for (int i = 0; i < model.size(); i++) {
            tempArray[i] = model.get(i).value;
        }

        return tempArray;
    }

    public List<ValueModel> getValuesObjectArray(String fieldID){
        return new Select()
                .from(ValueModel.class)
                .where("fieldID = ?",fieldID)
                .execute();
    }
}
