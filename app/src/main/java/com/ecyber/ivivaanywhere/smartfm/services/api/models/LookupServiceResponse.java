package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/15/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class LookupServiceResponse {
    @SerializedName("Data")
    List<OptionListModel> optionListModels;

    public LookupServiceResponse(List<OptionListModel> optionListModels) {
        this.optionListModels = optionListModels;
    }

    public List<OptionListModel> getOptionListModels() {
        return optionListModels;
    }

    public void setOptionListModels(List<OptionListModel> optionListModels) {
        this.optionListModels = optionListModels;
    }
}
