package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DipPixConverter;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

public class LabelView {

    private View view;

    private Context context;
    private CustomTextView title, lable;
    private RelativeLayout layLabelContainer;
    private ObjectInfoModel infoModel;
    private int position;

    public LabelView(Context context) {
        this.context = context;
    }

    public View getLabelView(ObjectInfoModel infoModel, int position) {
        this.infoModel = infoModel;
        this.position = position;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.view_label, null);

        this.initView();

        if (infoModel.getFieldname() == null | infoModel.getFieldname().equals("")) {
            this.title.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.layLabelContainer.getLayoutParams();

            int pixToChange = (int)new DipPixConverter().dipToPixels(context, 8.0f);
            lp.setMargins(0, pixToChange,0,0);
            this.layLabelContainer.setLayoutParams(lp);
        } else {
            this.title.setVisibility(View.VISIBLE);
        }

        this.title.setText((infoModel.getFieldname() != null) ? infoModel.getFieldname() : "");
        this.lable.setText("");

        if (infoModel.getValueModels() != null) {
            List<ValueModel> valueModels = infoModel.getValueModels();
            String val = valueModels.get(0).getValue();

            if (val != null){
                this.lable.setText(val);
            }
        }

        return view;
    }

    private void initView() {
        title = view.findViewById(R.id.tvTitle);
        lable = view.findViewById(R.id.tvLabel);
        layLabelContainer = view.findViewById(R.id.layLabelContainer);
    }
}
