package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.ObjectInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class CreateFields {

    @SerializedName("CreateFields")
    @Expose
    private List<ObjectInfo> mCreateFields;

    public List<ObjectInfo> getCreateFields() {
        return mCreateFields;
    }

    public void setCreateFields(List<ObjectInfo> createFields) {
        mCreateFields = createFields;
    }
}
