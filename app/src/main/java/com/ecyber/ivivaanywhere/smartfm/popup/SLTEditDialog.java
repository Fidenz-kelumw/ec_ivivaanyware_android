package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class SLTEditDialog {
    private Context context;
    private SLTCallback callback;
    private Dialog bDialog;

    public SLTEditDialog(Context context, SLTCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void show(String heading, String text, String isEditable) {
        bDialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_slt_edit, null);

        final CustomEditTextView edtRemark = (CustomEditTextView) view.findViewById(R.id.edt);
        TextView title = (TextView) view.findViewById(R.id.title);
        ImageButton btnCancel = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);
        CustomButton btnUpdate = (CustomButton) view.findViewById(R.id.btn_update);

        if (isEditable.equals("0")) {
            btnUpdate.setBackgroundResource(R.drawable.button_rounded_disable_info);
            btnUpdate.setEnabled(false);
            edtRemark.setEnabled(false);
        }

        edtRemark.setOnClickListener(v -> edtRemark.setCursorVisible(true));
        edtRemark.setText(text);

        btnCancel.setOnClickListener(v -> bDialog.dismiss());
        title.setText(heading);

        btnUpdate.setOnClickListener(v -> {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

            bDialog.dismiss();
            callback.onSLTTextUpdate(edtRemark.getText().toString());
        });

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 40;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);
        y = converter.dipToPixels(context, dpHeight);

        bDialog.setContentView(view);
        bDialog.show();
        bDialog.setCancelable(true);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout((int) x, (int) y);
    }

    public interface SLTCallback {
        void onSLTTextUpdate(String text);
    }
}
