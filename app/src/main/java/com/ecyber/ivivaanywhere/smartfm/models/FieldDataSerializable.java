package com.ecyber.ivivaanywhere.smartfm.models;

import java.io.Serializable;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class FieldDataSerializable implements Serializable {

    /*"FieldID": "Location",
            "Value": "8392",
            "DisplayText": "Building Two"*/
    public String FieldID;
    public String Value;
    public String DisplayText;
    public String[] SAARValues;
    public String Type;

    public FieldDataSerializable(String fieldID, String value, String displayText) {
        FieldID = fieldID;
        Value = value;
        DisplayText = displayText;
    }

    public FieldDataSerializable(String fieldID, String value, String displayText, String[] SAARValues, String type) {
        FieldID = fieldID;
        Value = value;
        DisplayText = displayText;
        this.SAARValues = SAARValues;
        Type = type;
    }
}
