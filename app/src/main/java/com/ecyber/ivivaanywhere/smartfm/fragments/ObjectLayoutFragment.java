package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.DroppedPin;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.popup.PinDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LayoutResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectLayoutSync;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

import static com.ecyber.ivivaanywhere.smartfm.R.id.map;

/**
 * Created by Lakmal on 5/24/2016.
 */
public class ObjectLayoutFragment extends Fragment implements View.OnClickListener, GetObjectLayoutSync.GetObjectLayoutCallBack, OnMapReadyCallback {
    private static final String TAG = "SmartFM";
    private float maxZoomLevel;

    private String mMapUrlPath = "";/*"http://fmdev.ivivacloud.com/LayoutUtil/tile/31/b7fa30b1-79f1-4173-bc30-084cd64c3fdc/%d/%d/%d";*/
    private String customName, customDescription;

    private TileOverlay mCustomTileLayout;
    MarkerOptions markerOptions;
    Marker marker = null;
    private ColoredSnackbar mColoredSnackBar;
    private String mObjectType;
    private String mObjectKey;
    private String mTabName;
    private AccountPermission mAccountPermission;
    private GetObjectLayoutSync mGetObjectLayoutSync;
    private NetworkCheck mNetwork;
    private String mLayoutPath;
    private GoogleMap mMap;
    List<LayoutResponse.Pins> mPinList;
    private double mOldX, mOldY;
    private String isEditable = null;
    private LinkedHashMap<String, Integer> mMarkers = new LinkedHashMap<>();
    private boolean isUpdateDropPin;
    private boolean isTabForceLoad;

    ProgressBar mProgressBar;
    CustomTextView mTvMessage;
    CustomButton mBtnUpdateDropPin;
    private Marker mUpdateMarker;
    View rootView;
    Activity mActivity;
    private RelativeLayout mRelMessage;
    private Context mContext;

    public ObjectLayoutFragment() {

    }

    @SuppressLint("ValidFragment")
    public ObjectLayoutFragment(Activity activity, ProgressBar progressBar) {
        mProgressBar = progressBar;
        mActivity = activity;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            FragmentManager fm = getActivity().getFragmentManager();
            Fragment fragment = (fm.findFragmentById(map));
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_gmaps, container, false);
            mBtnUpdateDropPin = (CustomButton) rootView.findViewById(R.id.btn_update_drop_pin);
            mTvMessage = (CustomTextView) rootView.findViewById(R.id.tv_message);
            mRelMessage = (RelativeLayout) rootView.findViewById(R.id.rel_msg);

            getMapFragment().getMapAsync(this);

            mBtnUpdateDropPin.setOnClickListener(this);
            setEnableBtnUpdateDropPin(isUpdateDropPin);

            ButterKnife.bind(rootView);
        } catch (InflateException e) {

        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mColoredSnackBar = new ColoredSnackbar(getActivity());
        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);
        mAccountPermission = new AccountPermission();
        mGetObjectLayoutSync = new GetObjectLayoutSync(this);
        mNetwork = new NetworkCheck();

        /*if (mMap != null)
            loadMap(mMap);

        getObjectLayoutLoad();*/
    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        Log.d(TAG, "sdk: " + Build.VERSION.SDK_INT);
        Log.d(TAG, "release: " + Build.VERSION.RELEASE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fm = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(map);
    }

    private void setEnableBtnUpdateDropPin(boolean isEnable) {
        if (isEnable) {
            mBtnUpdateDropPin.setBackgroundResource(R.drawable.button_rounded_red);
            mBtnUpdateDropPin.setClickable(true);
        } else {
            mBtnUpdateDropPin.setBackgroundResource(R.drawable.button_rounded_disable_info);
            mBtnUpdateDropPin.setClickable(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (isUpdateDropPin) {
            if (mUpdateMarker != null)
                invokeUpdateDropPin(mUpdateMarker, mOldX, mOldY);
        }
    }


    private void getObjectLayoutLoad() {
        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
        mProgressBar.setVisibility(View.VISIBLE);

        if (isTabForceLoad) {
            invokeObjectLayoutLoadSync();
        } else {
            if (dataDownloadOptimization.isDataAvailableInTable(ObjectLayout.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeObjectLayoutLoadSync();
                } else {
                    boolean isAvailable = mGetObjectLayoutSync.getLayoutFromDb(mObjectKey);
                    if (!isAvailable) {
                        invokeObjectLayoutLoadSync();
                    }
                }
            } else {
                invokeObjectLayoutLoadSync();
            }
        }
    }

    private void invokeObjectLayoutLoadSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mGetObjectLayoutSync.getObjectLayout(
                    mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NONE);
        mMap = map;
        loadMap(mMap);
        getObjectLayoutLoad();
    }

    private void loadMap(GoogleMap map) {
        reverseZoomOnMaxLevelReached(map);

        addMarkerClickListner(map);
        setMapMarkerDragListner(map);
        changeMapUISettings(map);

        TileProvider tileProvider = getTileProvider();
        mCustomTileLayout = map.addTileOverlay(new TileOverlayOptions().tileProvider(tileProvider));
    }

    private void setMapMarkerDragListner(final GoogleMap map) {

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker mark) {
                Log.d("System out", "onMarkerDragStart..." + mark.getPosition().latitude + "..." + mark.getPosition().longitude);

            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                Log.d("System out", "onMarkerDragEnd..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
                map.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.i("System out", "onMarkerDrag...");
                isUpdateDropPin = true;
                mUpdateMarker = marker;
                setEnableBtnUpdateDropPin(true);
            }
        });
    }

    private void updatingToOldDropPin(Marker newestLocation) {
        newestLocation.remove();
        MarkerOptions old = new MarkerOptions()
                .position(new LatLng(mOldY, mOldX))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_pin)) // set size according to image u get
                .draggable(isEditable.equals("1") ? true : false);
        mMap.addMarker(old);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mOldX, mOldY)));
    }

    @NonNull
    private TileProvider getTileProvider() {
        return new UrlTileProvider(256, 256) {
            @Override
            public synchronized URL getTileUrl(int x, int y, int zoom) {

                String s = String.format(Locale.US, mMapUrlPath, zoom, x, y);

                URL url = null;
                try {
                    url = new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
                System.out.println(url);
                return url;
            }

        };
    }

    private void changeMapUISettings(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NONE);

        map.getUiSettings().setCompassEnabled(false);   // remove compass view
        map.getUiSettings().setMapToolbarEnabled(false);   // remove toolbar
        map.getUiSettings().setRotateGesturesEnabled(false);   // remove rotate gesture from this
        map.getUiSettings().setMyLocationButtonEnabled(false);   // remove gps button
        map.getUiSettings().setTiltGesturesEnabled(false);   // remove two finger tlit feature
    }

    private void addMarkers(GoogleMap map, LayoutResponse.Pins pin, int position) {
        String objectKey = pin.getObjectKey();
        Marker mkr = map.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(pin.getY()), Double.parseDouble(pin.getX())))
                .icon(BitmapDescriptorFactory.fromResource(getIcon(pin.getColor()))) // set size according to image u get
                .draggable(false).zIndex(1.5f));

        mMarkers.put(mkr.getId(), position);
    }

    private void reverseZoomOnMaxLevelReached(final GoogleMap map) {
        map.setOnCameraChangeListener(cameraPosition -> {
            if (map.getCameraPosition().zoom > maxZoomLevel) {
                map.animateCamera(CameraUpdateFactory.zoomTo(maxZoomLevel - 1));
            }
        });
    }

    private void addMarkerClickListner(final GoogleMap map) {
        map.setOnMarkerClickListener(marker -> {
            if (mMarkers.get(marker.getId()) != null) {
                int position = 0;
                for (int i = 0; i < mPinList.size(); i++) {
                    if (mPinList.get(i).getY().equals("" + marker.getPosition().latitude) && mPinList.get(i).getX().equals("" + marker.getPosition().longitude)) {
                        position = i;
                    }
                }
                LayoutResponse.Pins pins = mPinList.get(position);
                new PinDialog(getActivity(), pins).showConfirmation(customName, customDescription);
            }
            System.out.println(marker.getId() + " pos:" + marker.getPosition().latitude + "," + marker.getPosition().longitude);
            return false;
        });

        map.setOnInfoWindowClickListener(marker -> {
            if (marker != null)
                marker.hideInfoWindow();
        });
    }

    public void setFadeIn(View v) {
        if (mCustomTileLayout == null) {
            return;
        }
        mCustomTileLayout.setFadeIn(((CheckBox) v).isChecked());
    }

    @Override
    public void OnGetObjectLayoutSuccess(LayoutResponse response) {
        loadMap(mMap);
        setDataToLayout(response);

        if (response != null && response.getLayout() != null && response.getLayout().getPinLabel() != null) {
            customDescription = response.getLayout().getPinLabel().getDescription();
            customName = response.getLayout().getPinLabel().getName();
        }
    }

    private void setDataToLayout(LayoutResponse response) {
        mPinList = null;
        AccountPermission accountPermission = new AccountPermission();
        String baseUrl = mAccountPermission.getAccountInfo().get(0);
        String checkSsl = DomainData.getSsl();
        if (checkSsl != null && !checkSsl.equals("")) {
            if (checkSsl.equals("1")) {
                mMapUrlPath = "https://" + baseUrl + response.getLayout().getmPath() + "/%d/%d/%d";
            } else {
                mMapUrlPath = "http://" + baseUrl + response.getLayout().getmPath() + "/%d/%d/%d";
            }
        }

        String layoutZoomLvl = response.getLayout().getmZoomLevels();

        mPinList = response.getPinList();

        maxZoomLevel = layoutZoomLvl == "" ? 5 : Integer.parseInt(layoutZoomLvl == null ? "5" : layoutZoomLvl);
        int i = 0;
        for (LayoutResponse.Pins pin : mPinList) {
            addMarkers(mMap, pin, i);
            i++;
        }

        DroppedPin droppedPin = response.getDroppedPin();
        if (droppedPin.getmEditable() != null) {
            if (droppedPin.getmEditable().equals("1")) {
                if (droppedPin.getmX() != null) {
                    mOldX = Double.parseDouble(droppedPin.getmX());
                    mOldY = Double.parseDouble(droppedPin.getmY());
                    isEditable = droppedPin.getmEditable();
                    addDropPin(mMap, Double.parseDouble(droppedPin.getmY()), Double.parseDouble(droppedPin.getmX()), droppedPin.getmEditable());
                }
            } else {
                mBtnUpdateDropPin.setVisibility(View.GONE);
                mRelMessage.setVisibility(View.GONE);
            }
        }

        mProgressBar.setVisibility(View.GONE);
    }

    private void addDropPin(GoogleMap map, double y, double x, String isEditable) {
        map.addMarker(new MarkerOptions()
                .position(new LatLng(y, x))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_pin/*resizeMapIcons("ic_drop_pin", 65, 100)*/)) // set size according to image u get
                .draggable(isEditable.equals("1") ? true : false));
    }

    @Override
    public void OnGetObjectLayoutError(String message) {
        mProgressBar.setVisibility(View.GONE);
        mCustomTileLayout.remove();
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    private void invokeUpdateDropPin(Marker newMarker, double oldX, double oldY) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        double x = newMarker.getPosition().longitude;
        double y = newMarker.getPosition().latitude;
        if (state) {
            mColoredSnackBar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            mGetObjectLayoutSync.updateDropPin(
                    mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName, x, y, newMarker, oldX, oldY);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void OnUpdateDropPinSuccess(String message, Marker newMarker) {
        mOldX = newMarker.getPosition().longitude;
        mOldY = newMarker.getPosition().latitude;
        mColoredSnackBar.dismissSnacBar();
        isUpdateDropPin = false;
        setEnableBtnUpdateDropPin(false);
        mBtnUpdateDropPin.setBackgroundResource(R.drawable.button_rounded_disable_info);
        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_LONG);
    }

    @Override
    public void OnUpdateDropPinError(String message, double oldX, double oldY, Marker newMarker) {
        updatingToOldDropPin(newMarker);
        isUpdateDropPin = true;
        setEnableBtnUpdateDropPin(false);
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(message == null ? SmartConstants.TRY_AGAIN_EXCEPTION : message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
    }

    private int getIcon(String color) {
        int pin = 0;
        switch (PinColor.valueOf(color.toUpperCase())) {
            case BLUE:
                pin = R.drawable.ic_pin_blue;
                break;
            case BROWN:
                pin = R.drawable.ic_pin_brown;
                break;
            case GRAY:
                pin = R.drawable.ic_pin_gray;
                break;
            case GREEN:
                pin = R.drawable.ic_pin_green;
                break;
            case PURPLE:
                pin = R.drawable.ic_pin_purple;
                break;
            case RED:
                pin = R.drawable.ic_pin_red;
                break;
            case YELLOW:
                pin = R.drawable.ic_pin_yellow;
                break;
            case ORANGE:
                pin = R.drawable.ic_pin_orange;
                break;
        }
        return pin;
    }

    enum PinColor {
        BLUE, BROWN, GRAY, GREEN, PURPLE, RED, YELLOW, ORANGE;
    }

    enum PinName {
        ic_pin_blue, ic_pin_brown, ic_pin_gray, ic_pin_green, ic_pin_purple, ic_pin_red, ic_pin_yellow
    }

}
