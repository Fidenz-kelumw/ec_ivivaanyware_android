package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;

import java.util.List;

/**
 * Created by Lakmal on 5/13/2016.
 */
public class AttachmentTypeAdapter extends RecyclerView.Adapter<AttachmentTypeAdapter.Holder> {

    private Context mContext;
    private List<AttachmentTypesMaster> mDataSet;
    private AttachmentTypeItemClickListener mSingleItemClickListener;

    public AttachmentTypeAdapter(Context context, List<AttachmentTypesMaster> dataSet, AttachmentTypeItemClickListener singleItemClickListener) {
        mContext = context;
        mDataSet = dataSet;
        mSingleItemClickListener = singleItemClickListener;
    }

    public void addItem(AttachmentTypesMaster item) {
        mDataSet.add(item);
        notifyDataSetChanged();
    }

    public void clearDataSet() {
        mDataSet.clear();
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.widget_single_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.tvItemValue.setText(mDataSet.get(position).getTypeName());
        holder.tvItemValue.setTag(mDataSet.get(position).getSignature());
    }

    @Override
    public int getItemCount() {
        return null != mDataSet ? mDataSet.size() : 0;
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView tvItemValue;

        public Holder(View itemView) {
            super(itemView);
            tvItemValue = (CustomTextView) itemView.findViewById(R.id.tv_single_item_value);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mSingleItemClickListener.onClickTypeItem(mDataSet.get(getAdapterPosition()));
        }
    }

    public interface AttachmentTypeItemClickListener {
        void onClickTypeItem(AttachmentTypesMaster type);
    }


}
