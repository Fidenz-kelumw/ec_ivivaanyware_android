package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/9/2016.
 */
@Table(name = "MassageStatus")
public class MessagesStatus extends Model{

    @Column(name = "Messages")
    @SerializedName("Messages")
    @Expose
    public List<Messages> mMessagesList;

    @Column(name = "AddNewMessage")
    @SerializedName("AddNewMessage")
    @Expose
    public int mAddNewMessage;

    @Column(name = "NewMessageAction")
    @SerializedName("NewMessageAction")
    @Expose
    public String mNewMessageAction;

    @SerializedName("MessageTypes")
    @Expose
    public List<MessageType> messageTypes;

    public List<MessageType> getMessageTypes() {
        return messageTypes;
    }

    public void setMessageTypes(List<MessageType> messageTypes) {
        this.messageTypes = messageTypes;
    }

    public List<Messages> getMessagesList() {
        return mMessagesList;
    }

    public void setMessagesList(List<Messages> messagesList) {
        mMessagesList = messagesList;
    }

    public int getAddNewMessage() {
        return mAddNewMessage;
    }

    public void setAddNewMessage(int addNewMessage) {
        mAddNewMessage = addNewMessage;
    }

    public String getNewMessageAction() {
        return mNewMessageAction;
    }

    public void setNewMessageAction(String newMessageAction) {
        mNewMessageAction = newMessageAction;
    }
}
