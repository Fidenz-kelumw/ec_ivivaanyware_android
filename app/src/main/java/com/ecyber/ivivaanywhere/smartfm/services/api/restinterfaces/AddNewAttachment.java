package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.AddNewAttachmentResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/12/2016.
 */
public interface AddNewAttachment {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "AddNewAttachment")
    Call<AddNewAttachmentResponse> addNewAttachments(@Field("apikey") String apiKey,
                                                     @Field("UserKey") String userKey,
                                                     @Field("ObjectType") String objectType,
                                                     @Field("ObjectKey") String objectKey,
                                                     @Field("TabName") String tabName,
                                                     @Field("AttachmentType") String attachmentType,
                                                     @Field("AttachmentName") String attachmentName,
                                                     @Field("FileName") String fileName,
                                                     @Field("ATTType")String ATTType);

}
