package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/22/2016.
 */
@Table(name="CheckList")
public class CheckList extends Model{

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "ItemKey")
    @SerializedName("ItemKey")
    @Expose
    private String mItemKey;

    @Column(name = "ItemName")
    @SerializedName("ItemName")
    @Expose
    private String mItemName;

    @Column(name = "Description")
    @SerializedName("Description")
    @Expose
    private String mDescription;

    @Column(name = "Deadline")
    @SerializedName("Deadline")
    @Expose
    private String mDeadLine;

    @Column(name = "Status")
    @SerializedName("Status")
    @Expose
    private String mStatus;

    @Column(name = "StatusEdit")
    @SerializedName("StatusEdit")
    @Expose
    public String mSetItemStatus;

    @Column(name = "CalendarEdit")
    @SerializedName("CalendarEdit")
    @Expose
    public String mSetItemDeadline;

    @Column(name = "DescriptionEdit")
    @SerializedName("DescriptionEdit")
    @Expose
    public String mSetItemDescription;

    @Column(name = "InfoCode")
    @SerializedName("InfoCode")
    @Expose
    public String infoCode;

    @Column(name = "InfoText")
    @Expose
    public String infoText;

    public CheckList() {
        super();
    }

    public CheckList(String mObjectKey, String mItemKey, String mItemName, String mDescription, String mDeadLine, String mStatus, String mSetItemStatus, String mSetItemDeadline, String mSetItemDescription, String infoCode, String infoText) {
        this.mObjectKey = mObjectKey;
        this.mItemKey = mItemKey;
        this.mItemName = mItemName;
        this.mDescription = mDescription;
        this.mDeadLine = mDeadLine;
        this.mStatus = mStatus;
        this.mSetItemStatus = mSetItemStatus;
        this.mSetItemDeadline = mSetItemDeadline;
        this.mSetItemDescription = mSetItemDescription;
        this.infoCode = infoCode;
        this.infoText = infoText;
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    public String getmItemKey() {
        return mItemKey;
    }

    public void setmItemKey(String mItemKey) {
        this.mItemKey = mItemKey;
    }

    public String getmItemName() {
        return mItemName;
    }

    public void setmItemName(String mItemName) {
        this.mItemName = mItemName;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmDeadLine() {
        return mDeadLine;
    }

    public void setmDeadLine(String mDeadLine) {
        this.mDeadLine = mDeadLine;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmSetItemStatus() {
        return mSetItemStatus;
    }

    public void setmSetItemStatus(String mSetItemStatus) {
        this.mSetItemStatus = mSetItemStatus;
    }

    public String getmSetItemDeadline() {
        return mSetItemDeadline;
    }

    public void setmSetItemDeadline(String mSetItemDeadline) {
        this.mSetItemDeadline = mSetItemDeadline;
    }

    public String getmSetItemDescription() {
        return mSetItemDescription;
    }

    public void setmSetItemDescription(String mSetItemDescription) {
        this.mSetItemDescription = mSetItemDescription;
    }

    public String getInfoCode() {
        return infoCode;
    }

    public void setInfoCode(String infoCode) {
        this.infoCode = infoCode;
    }

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    public void clearTable() {
        new Delete().from(CheckList.class).execute();
    }

    public List<CheckList> getAll(String objectKey){
        return new Select().from(CheckList.class).where("ObjectKey =?",objectKey).execute();
    }

    public CheckList getCheckItem(String itemKey){
        List<CheckList> temp = new Select().from(CheckList.class).where("ItemKey =?",itemKey).execute();
        if(temp != null && temp.size()>0)
            return temp.get(0);
        else
            return null;
    }

    public void updateDeadLine(String deadLine, String whereItemKey) {
        new Update(CheckList.class)
                .set("Deadline = ?", deadLine)
                .where("ItemKey = ?", whereItemKey)
                .execute();
    }

    public void updateStatus(String status, String whereItemKey) {
        new Update(CheckList.class)
                .set("Status= ?",status)
                .where("ItemKey = ?", whereItemKey)
                .execute();
    }

    public void updateDescription(String description, String checkItemKey) {
        new Update(CheckList.class)
                .set("Description= ?",description)
                .where("ItemKey = ?", checkItemKey)
                .execute();
    }

    public void updateCheckItemInfo(String text, String whereItemKey) {
        new Update(CheckList.class)
                .set("InfoText = ?", text)
                .where("ItemKey = ?", whereItemKey)
                .execute();
    }
}

