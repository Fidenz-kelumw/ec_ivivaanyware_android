package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.RegistrationResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.UpdateFirebaseResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ValidateRegistrationResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public interface RegistrationService {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "RegisterUser")
    Call<RegistrationResponce> getRegistrationData(@Field("DomainID") String DomainID,
                                                   @Field("UserID") String UserID,
                                                   @Field("RegistrationCode") String RegistrationCode,
                                                   @Field("apikey") String apikey,
                                                   @Field("FirebaseToken") String fireBaseToken);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "ValidateRegistration")
    Call<ValidateRegistrationResponse> getValidateRegistration(@Field("apikey") String apiKey,
                                                               @Field("UserKey") String userKey);

    @Headers("Content-Type: application/json")
    @POST("/v1/domaindata")
    Call<DomainData> getDomainData(@Body Map<String, String> requestRow);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "UpdateFirebaseToken")
    Call<UpdateFirebaseResponse> UpdateFirebaseToken(@Field("apikey") String apikey, @Field("UserKey") String userKey, @Field("FirebaseToken") String token);
}
