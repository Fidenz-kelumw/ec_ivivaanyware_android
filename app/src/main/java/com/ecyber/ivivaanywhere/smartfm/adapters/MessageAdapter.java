package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;

import java.util.List;

/**
 * Created by Lakmal on 5/9/2016.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.Holder> {
    private Context mContext;
    private List<Messages> mMessageList;
    private String mBaseUrl;
    private CImageLoader mCImageLoader;

    public MessageAdapter(Context context, List<Messages> messageList, String baseUrl/*, List<Roles> allRoles*/) {
        mContext = context;
        mMessageList = messageList;
        mBaseUrl = baseUrl;
        mCImageLoader = new CImageLoader(mContext);
    }

    public void addItem(Messages messages) {
        mMessageList.add(messages);
        notifyDataSetChanged();
    }

    public void clearDataSet() {
        mMessageList.clear();
        notifyDataSetChanged();
    }

    public List<Messages> getCurrentList() {
        return mMessageList;
    }

    public void notifyDataSet() {
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_message_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        Messages message = mMessageList.get(position);

        String createdAt = message.getCreatedAt();

        holder.tvMemberName.setText(message.mUserName);
        holder.tvCreatedAt.setText(String.valueOf(Utility.getDateTimeDifferent(createdAt)));
        holder.tvMessage.setText(message.getMessageText());

        String checkSsl = DomainData.getSsl();
        if(checkSsl!=null && !checkSsl.equals("")){
            if(checkSsl.equals("1")){
                mCImageLoader.DisplayImage("https://" + mBaseUrl + message.mImagePath, holder.imgMemberItemUserImage);
            }
            else {
                mCImageLoader.DisplayImage("http://" + mBaseUrl + message.mImagePath, holder.imgMemberItemUserImage);
            }
        }

        if (message.getMessageKey().equals("-1")) {
            holder.relLayMassagePending.setVisibility(View.VISIBLE);
        } else {
            holder.relLayMassagePending.setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return null != mMessageList ? mMessageList.size() : 0;
    }

    public void clear() {
        mMessageList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView tvMemberName, tvMessage, tvCreatedAt;
        ImageView imgMemberItemUserImage;
        RelativeLayout relLayMassagePending;

        public Holder(View itemView) {
            super(itemView);
            imgMemberItemUserImage = (ImageView) itemView.findViewById(R.id.image_view_member_item_user_image);
            tvMemberName = (CustomTextView) itemView.findViewById(R.id.tv_member_name);
            tvMessage = (CustomTextView) itemView.findViewById(R.id.tv_message);
            tvCreatedAt = (CustomTextView) itemView.findViewById(R.id.tv_created_at);
            relLayMassagePending = (RelativeLayout) itemView.findViewById(R.id.rel_lay_msg_pending);
        }
    }
}
