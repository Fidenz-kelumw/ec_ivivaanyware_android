package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CreateObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.RegistrationResponce;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 6/2/2016.
 */
public interface GetDefaultData {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetDefaultData")
    Call<DefaultResponse> getDefaultData(@Field("apikey") String apiKey,
                                         @Field("UserKey") String userKey);
}
