package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.MessageTypeAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.models.MessageType;

import java.util.List;

/**
 * Created by Choota on 3/8/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class MessageTypeSelectDialog {
    private Context context;
    private MessageTypeSelectCallback callback;

    private RecyclerView recycleMessagetypes;
    private ImageButton btnBack;
    private MessageTypeAdapter adapter;

    public MessageTypeSelectDialog(Context context, MessageTypeSelectCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void showTypeSelect(final List<MessageType> messageTypes) {

        this.adapter = new MessageTypeAdapter(context, messageTypes);

        final Dialog dialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_message_type, null);

        recycleMessagetypes = (RecyclerView) view.findViewById(R.id.recycleMessagetypes);
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);

        btnBack.setOnClickListener(view1 -> dialog.dismiss());

        recycleMessagetypes.setHasFixedSize(true);
        recycleMessagetypes.setLayoutManager(new LinearLayoutManager(context));
        recycleMessagetypes.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recycleMessagetypes.setAdapter(adapter);

        ItemClickSupport.addTo(recycleMessagetypes).setOnItemClickListener((recyclerView, position, v) -> {
            callback.onMessageTypeSelect(messageTypes.get(position).getTypeID());
            dialog.dismiss();
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout((int) x, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    public interface MessageTypeSelectCallback {
        void onMessageTypeSelect(String typeId);
    }
}
