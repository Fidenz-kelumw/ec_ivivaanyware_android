package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lakmal on 6/2/2016.
 */
public class FilterObjects {

    public FilterObjects() {
    }

    public List<Objects> filterObjects(List<Objects> objectsList , String query) {
        List<Objects> filteredList = new ArrayList<>();

        for (Objects objects : objectsList) {
            boolean result = StringLikes.like(objects.getObjectID().toString(), query);
            if (!result){
                if (Utility.IsEmpty(objects.getDescrition())) {
                    result = false;
                } else {
                    result = StringLikes.like(objects.getDescrition().toString(), query);
                }
            }

            if (result) {
                filteredList.add(objects);
            }
        }

        return filteredList;
    }

    public List<SubObject> filterSubObjects(List<SubObject> objectsList , String query) {
        List<SubObject> filteredList = new ArrayList<>();

        for (SubObject objects : objectsList) {
            boolean result = StringLikes.like(objects.getObjectID().toString(), query);
            if (!result){
                if (Utility.IsEmpty(objects.getDescription())) {
                    result = false;
                } else {
                    result = StringLikes.like(objects.getDescription().toString(), query);
                }
            }

            if (result) {
                filteredList.add(objects);
            }
        }

        return filteredList;
    }
}
