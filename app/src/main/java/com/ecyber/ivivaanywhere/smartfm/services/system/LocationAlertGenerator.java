package com.ecyber.ivivaanywhere.smartfm.services.system;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

/**
 * Created by Choota on 1/26/18.
 */

public class LocationAlertGenerator {

    private static Activity activity;
    private static LocationManager locationManager;

    public static boolean checkLocation(Activity activityToLocation) {

        activity = activityToLocation;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (!isLocationEnabled())
            showAlert();

        return isLocationEnabled();
    }

    private static boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private static void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle("Enable Location")
                .setMessage("Your \"Locations Settings\" is set to \"Off\".\nPlease enable \"Locations Settings\" and try again")
                .setPositiveButton("Location Settings", (paramDialogInterface, paramInt) -> {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(myIntent);
                })
                .setNegativeButton("Cancel", (paramDialogInterface, paramInt) -> {
                });
        dialog.show();
    }
}
