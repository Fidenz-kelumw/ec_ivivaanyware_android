package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by lakmal on 8/16/16.
 */
@Table(name = SmartConstants.OBJECT_LIST)
public class ObjectListCopy extends Model {

    @Column(name = "ObjectType")
    @Expose
    public String mObjectType;

    @Column(name = "ObjectKey")
    @Expose
    public String mObjectKey;

    @Column(name = "ObjectID")
    @Expose
    public String mObjectID;

    @Column(name = "Description")
    @Expose
    public String mDescription;

    @Column(name = "Stage")
    @Expose
    public String mStage;

    public ObjectListCopy() {

    }

    public ObjectListCopy(String objectType, String objectKey, String objectID, String description, String stage) {
        mObjectType = objectType;
        mObjectKey = objectKey;
        mObjectID = objectID;
        mDescription = description;
        mStage = stage;
    }


    public List<ObjectListCopy> select(/*String objectType*/) {
        List<ObjectListCopy> retList = new Select().from(ObjectListCopy.class)/*.where("ObjectType = ?",objectType)*/.execute();
        return retList;
    }
}
