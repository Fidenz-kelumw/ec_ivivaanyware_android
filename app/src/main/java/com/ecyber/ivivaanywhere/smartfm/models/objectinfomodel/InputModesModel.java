package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "InputModesModel")
public class InputModesModel extends Model {

    @Column(name = "fieldID")
    String fieldID;

    @Column(name = "mode")
    String mode;

    public InputModesModel() {
    }

    public InputModesModel(String fieldID, String mode) {
        this.fieldID = fieldID;
        this.mode = mode;
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void clearTable() {
        new Delete().from(InputModesModel.class).execute();
    }

    static private List<InputModesModel> filterModes(String fieldID) {
        return new Select()
                .from(InputModesModel.class)
                .where("fieldID = ?", fieldID)
                .execute();
    }

    static public String[] getInputModes(String fieldID) {
        List<InputModesModel> inputModesModels = filterModes(fieldID);
        if (inputModesModels != null && inputModesModels.size() > 0) {
            String[] data = new String[inputModesModels.size()];
            for (int i = 0; i < inputModesModels.size(); i++) {
                data[i] = inputModesModels.get(i).getMode();
            }
            return data;
        } else {
            return null;
        }
    }
}
