package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/15/2016.
 */
public class DroppedPin {

    @SerializedName("X")
    @Expose
    public String mX;

    @SerializedName("Y")
    @Expose
    public String mY;

    @SerializedName("Editable")
    @Expose
    public String mEditable;

    public DroppedPin() {
        super();
    }

    public DroppedPin(String mX, String mY, String mEditable) {
        this.mX = mX;
        this.mY = mY;
        this.mEditable = mEditable;
    }

    public String getmX() {
        return mX;
    }

    public void setmX(String mX) {
        this.mX = mX;
    }

    public String getmY() {
        return mY;
    }

    public void setmY(String mY) {
        this.mY = mY;
    }

    public String getmEditable() {
        return mEditable;
    }

    public void setmEditable(String mEditable) {
        this.mEditable = mEditable;
    }
}
