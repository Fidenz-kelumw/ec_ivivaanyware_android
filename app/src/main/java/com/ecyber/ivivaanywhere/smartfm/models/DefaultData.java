package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 4/29/2016.
 */
@Table(name = "DefaultData")
public class DefaultData extends Model{
    /*"DefaultData": {
        "DisplayText": "",
                "Value": "New"
    }*/

    @Column(name = "DisplayText")
    @SerializedName("DisplayText")
    @Expose
    public String mDisplayText;

    @Column(name = "Value")
    @SerializedName("Value")
    @Expose
    public String mValue;

    public DefaultData() {
        super();
    }

    public DefaultData(String displayText, String value) {
        mDisplayText = displayText;
        mValue = value;
    }

    public String getDisplayText() {
        return mDisplayText;
    }

    public void setDisplayText(String displayText) {
        mDisplayText = displayText;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }
}
