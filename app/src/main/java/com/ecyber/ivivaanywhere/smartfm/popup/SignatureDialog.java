package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.customviews.SignatureView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lakmal on 6/1/2016.
 */
public class SignatureDialog {

    private Activity mActivity;
    private SignatureCallback mDelegate;

    public SignatureDialog(Activity activity, SignatureCallback delegate) {
        mActivity = activity;
        mDelegate = delegate;
    }

    public void show() {
        final Dialog mSignatureDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(mActivity.getApplicationContext());
        View view = inflater.inflate(R.layout.view_signature, null);

        ImageButton btnClose = (ImageButton) view.findViewById(R.id.imageButton_attachment_close);
        TextView mHeaderTitle = (TextView) view.findViewById(R.id.tv_title);
        final SignatureView signatureView = (SignatureView) view.findViewById(R.id.signature_view);
        CustomButton btnSaveSignature = (CustomButton) view.findViewById(R.id.btn_save_signature);
        CustomButton btnClearSignature = (CustomButton) view.findViewById(R.id.btn_clear_signature);

        mHeaderTitle.setText("Signature");

        btnClose.setOnClickListener(v -> mSignatureDialog.dismiss());

        btnClearSignature.setOnClickListener(v -> signatureView.clearSignature());

        btnSaveSignature.setOnClickListener(v -> {
            if(signatureView.isSignatureViewChanged()) {
                Bitmap image = signatureView.getImage();
                File sd = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
                //File sd = Environment.getExternalStorageDirectory();
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File fichero = new File(sd, "IMG_" + timeStamp + ".jpg");

                try {
                    if (sd.canWrite()) {
                        fichero.createNewFile();
                        OutputStream os = new FileOutputStream(fichero);
                        image.compress(Bitmap.CompressFormat.JPEG, 90, os);
                        os.close();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String mPicturePath = fichero.getPath();

                mDelegate.onSaveSignature(mPicturePath);

            /*mBitmapFile = Bitmap.createBitmap(BitmapFactory.decodeFile(mPicturePath));
            Bitmap TempViewBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(mPicturePath), 1024, 768, false);

            mAttachedImage.setImageBitmap(TempViewBitmap);
            mBtnSaveFile.setEnabled(true);
            mBtnSignature.setVisibility(View.GONE);
            mLayoutButtonHolder.setVisibility(View.GONE);*/
                mSignatureDialog.dismiss();
            } else {
                mSignatureDialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 90/*57*/;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;


        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mSignatureDialog.setContentView(view);
        mSignatureDialog.show();
        mSignatureDialog.setCanceledOnTouchOutside(false);
        mSignatureDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mSignatureDialog.getWindow().setLayout((int) x, (int) y);
    }

    /*private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(SmartConstants.FILE_SAVE_LOCATION, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + makeGUID());

        return mediaFile;
    }*/

    public interface SignatureCallback {
        void onSaveSignature(String mPicturePath);
    }

}
