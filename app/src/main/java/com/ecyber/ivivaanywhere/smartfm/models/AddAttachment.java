package com.ecyber.ivivaanywhere.smartfm.models;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by lakmal on 7/21/16.
 */
public class AddAttachment {

    public String mName;
    public String mId;
    public String mPath;
    public String mGUID;
    public boolean mUploaded;
    public boolean isLocal;
    public String mFullUrl;
    public boolean mIsEditable;
    public boolean mIsUploading;
    public int mAttachmentType;
    public Uri actualFileURI;

    public AddAttachment() {
        mUploaded = true;
    }

    public AddAttachment(String mName, String mId, String mPath, String guid, boolean isEditable, boolean uploaded, boolean isLocal, String fullUrl, boolean isUploading, int type, Uri uri) {
        this.mName = mName;
        this.mId = mId;
        this.mPath = mPath;
        this.mGUID = guid;
        this.mIsEditable = isEditable;
        this.mUploaded = uploaded;
        this.isLocal = isLocal;
        this.mFullUrl = fullUrl;
        this.mIsUploading = isUploading;
        this.mAttachmentType = type;
        this.actualFileURI = uri;
    }
}
