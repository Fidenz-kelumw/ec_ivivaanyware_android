package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SettingsAdvancedActivity;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.services.sync.RegistrationSync;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class AppResetConfirmation {

    Context context;
    private Dialog dialog;

    public AppResetConfirmation(Context context) {
        this.context = context;
    }

    public void showConfirmation() {
        final Dialog bDialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_confirmation, null);

        CustomTextView message = (CustomTextView) view.findViewById(R.id.tv_message);
        CustomButton btnCancel = (CustomButton) view.findViewById(R.id.btn_cancel);
        CustomButton btnOk = (CustomButton) view.findViewById(R.id.btn_ok);

        message.setText("Are you sure\nyou want to reset the app?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bDialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset(bDialog);
            }
        });

        bDialog.setContentView(view);
        bDialog.show();
        bDialog.setCancelable(false);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    }

    private void reset(Dialog bDialog) {
        if (NetworkCheck.IsAvailableNetwork(context)) {
            RegistrationSync.userUnregister(context);
            bDialog.dismiss();
            ((SettingsAdvancedActivity) context).setResult(6);
            ((SettingsAdvancedActivity) context).finish();
            ((SettingsAdvancedActivity) context).overridePendingTransition(R.anim.keep_active, R.anim.slide_to_left);

            new DeleteTokenTask().execute();
        }
    }

    private class DeleteTokenTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                Log.d("Token issue", "Exception deleting token", e);
            }
            return null;
        }
    }
}
