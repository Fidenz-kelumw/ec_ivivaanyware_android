package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;

/**
 * Created by Lakmal on 5/31/2016.
 */
public class ConfirmationDialog {

    private Activity mActivity;
    private int mPosition;
    private String mMessage;
    private ConfirmationCallback mDelegate;

    public ConfirmationDialog(Activity activity,String message, int position, ConfirmationCallback confirmationCallback) {
        mActivity = activity;
        mPosition = position;
        mMessage = message;
        mDelegate = confirmationCallback;
    }

    public void show() {
        final Dialog confirmationDialog = new Dialog(mActivity, R.style.CustomDialog);
        //LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View msgView = inflater.inflate(R.layout.dialog_confirmation, null);

        LayoutInflater inflater = LayoutInflater.from(mActivity);
        View msgView = inflater.inflate(R.layout.dialog_confirmation, null);

        CustomTextView msg = (CustomTextView) msgView.findViewById(R.id.tv_message);
        CustomButton btnCancel = (CustomButton) msgView.findViewById(R.id.btn_cancel);
        CustomButton btnConfirm = (CustomButton) msgView.findViewById(R.id.btn_ok);

        msg.setText(mMessage);
        btnConfirm.setText("Proceed");
        btnCancel.setText("Cancel");

        btnConfirm.setOnClickListener(v -> mDelegate.onClickConfirmation(mMessage,mPosition,confirmationDialog));

        btnCancel.setOnClickListener(v -> confirmationDialog.dismiss());

        confirmationDialog.setContentView(msgView);
        confirmationDialog.show();
        confirmationDialog.setCanceledOnTouchOutside(false);
    }

    public interface ConfirmationCallback {
        void onClickConfirmation(String message, int position, Dialog dialogInstance);
    }
}
