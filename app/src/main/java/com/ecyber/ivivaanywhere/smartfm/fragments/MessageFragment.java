package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.MessageAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.FilterMessages;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.StringEmptyCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.MessageType;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.popup.MessagesAdvanceDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.MessageAddResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.MessagesSync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lakmal on 4/20/2016.
 */
public class MessageFragment extends Fragment implements MessagesSync.MessagesCallBack, MessagesAdvanceDialog.MessageAdvanceCallback {

    private ProgressBar mProgressBar;
    private Context mContext;

    @BindView(R.id.rv_message_list)
    RecyclerView rvMessageList;

    @BindView(R.id.swiperefresh_message_list)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.btn_add_comment)
    CustomButton btnAddComment;

    @BindView(R.id.et_message_box)
    CustomEditTextView etMessageBox;

    @BindView(R.id.editText_messages_search_bar)
    CustomEditTextView etMessageSearch;


    private MessagesSync mMessageSync;
    private AccountPermission mAccountPermission;
    private List<Messages> mMessagesArrayList = new ArrayList<>();
    private MessageAdapter mMessageAdapter;
    private String mObjectType;
    private String mObjectKey;
    private String mTabName;
    private String selectedMessageType;

    private boolean isTabForceLoad;

    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;
    private List<Messages> mAllMessage;

    public MessageFragment() {

    }

    @SuppressLint("ValidFragment")
    public MessageFragment(Context context, ProgressBar progressBar) {
        mContext = context;
        mProgressBar = progressBar;
        mNetwork = new NetworkCheck();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_message, container, false);
        ButterKnife.bind(this, rootView);
        mColoredSnackBar = new ColoredSnackbar(getActivity());
        mNetwork = new NetworkCheck();
        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);
        initViews();
        mAllMessage = new ArrayList<>();
        dataLoad();
        etMessageSearch.addTextChangedListener(mTextWatcher);
        return rootView;
    }

    private void initViews() {
        mAccountPermission = new AccountPermission();
        mMessageSync = new MessagesSync(this);

        selectedMessageType = "-";
        configMessagesViews();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mMessageAdapter.clear();
            dataLoad();
        });

    }

    private void configMessagesViews() {
        rvMessageList.setHasFixedSize(true);
        rvMessageList.setLayoutManager(new LinearLayoutManager(mContext));

        mMessageAdapter = new MessageAdapter(mContext, mMessagesArrayList, mAccountPermission.getAccountInfo().get(0));
        rvMessageList.setAdapter(mMessageAdapter);

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        rvMessageList.addItemDecoration(new RecyclerDecorator(mContext, spanCount, spacing, includeEdge, false));
    }

    private void dataLoad() {
        mAllMessage.clear();
        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
        mProgressBar.setVisibility(View.VISIBLE);
        mTvMessage.setVisibility(View.GONE);

        if (isTabForceLoad) {
            invokeToMessageSync();
        } else {
            if (dataDownloadOptimization.isDataAvailableInTable(Messages.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToMessageSync();
                } else {
                    boolean isAvailable = mMessageSync.getMessagesFromDb(mObjectKey);
                    if (!isAvailable) {
                        invokeToMessageSync();
                    }
                }
            } else {
                invokeToMessageSync();
            }
        }
    }

    private void invokeToMessageSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mMessageSync.getObjectMessages(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName);
        } else {
            mTvMessage.setText("Error in network connection");
            mProgressBar.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String search = s.toString().trim();
            StringEmptyCheck check = new StringEmptyCheck();
            boolean isEmpty = check.isNotNullNotEmptyNotWhiteSpaceOnly(search);
            mTvMessage.setVisibility(View.GONE);
            if (!isEmpty) {
                if (mAllMessage != null) {
                    mMessageAdapter.clearDataSet();
                    for (Messages message : mAllMessage) {
                        mMessageAdapter.addItem(message);
                    }
                }
            } else {
                if (mAllMessage != null) {
                    List<Messages> filteredData = new FilterMessages().filterMessages(Collections.unmodifiableList(mAllMessage), search.toString());
                    if (filteredData.size() > 0) {
                        mMessageAdapter.clearDataSet();
                        for (Messages message : filteredData) {
                            mMessageAdapter.addItem(message);
                        }
                    } else {
                        mMessageAdapter.clearDataSet();
                        mTvMessage.setText("No result for \"" + search + "\"");
                        mTvMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    mTvMessage.setText("Poor internet connection.");
                    mTvMessage.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick(R.id.btn_add_comment)
    public void OnClickAddComment(View v) {
        rvMessageList.scrollToPosition(mMessageAdapter.getItemCount());
        String message = etMessageBox.getText().toString().trim();
        mTvMessage.setVisibility(View.GONE);
        if (message.isEmpty()) {
            Toast.makeText(mContext, "Please Enter Comment", Toast.LENGTH_LONG).show();
        } else {
            boolean state = mNetwork.NetworkConnectionCheck(getActivity());
            if (state) {
                Messages recentMsg = createMessage("", message);

                mAllMessage.add(recentMsg);
                mMessageAdapter.addItem(recentMsg);

                mMessageSync.addNewMessage(mAccountPermission.getAccountInfo().get(0),
                        mAccountPermission.getAccountInfo().get(1),
                        mAccountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, mTabName, message, recentMsg, selectedMessageType);
            } else {
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }

        }

        etMessageBox.setText("");
    }

    @OnClick(R.id.layMessageAdvace)
    public void onAdvanceMessageClick(View view) {
        new MessagesAdvanceDialog(getActivity(), this).showAdvanceMessageDialog(new Messages().getAllMessageTypes(mObjectKey));
    }

    @Override
    public void OnGetMessagesSuccess(Messages messages) {
        mTvMessage.setVisibility(View.GONE);
        mAllMessage.add(messages);
        mMessageAdapter.addItem(messages);

        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
        rvMessageList.scrollToPosition(mMessageAdapter.getItemCount() - 1);
    }


    private Messages createMessage(String messageKey, String comment) {
        User user = new User();
        User exUser = user.getUser(mAccountPermission.getAccountInfo().get(2));

        String nowAsISO = Utility.getDateNowISOFormat();
        Messages messages = new Messages(messageKey.isEmpty() ? "-1" : messageKey, comment, nowAsISO, exUser, mObjectKey);

        return messages;
    }

    @Override
    public void OnGetMessgesError(String message) {
        if (message.equals(SmartConstants.NO_MESSAGE)) {
            mTvMessage.setVisibility(View.VISIBLE);
            mTvMessage.setText("No comments found");
        } else
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnAddCommentSuccess(MessageAddResponse status, String userKey, Messages message) {
        String key = "-1";
        if (status.getMessageKey() == null) {
            key = "-2";//null
        } else if (status.getMessageKey().isEmpty()) {
            key = "-2";//empty
        } else {
            key = status.getMessageKey();
        }

        int position = 0;
        for (Messages messages : mMessageAdapter.getCurrentList()) {
            if (status.mSuccess == 1) {
                message.setMessageKey(key);
                message.save();
                mMessageAdapter.getCurrentList().get(position).setMessageKey(status.getMessageKey());
            } else if (status.mSuccess == 0) {//this condition working if message key is got empty
                mMessageAdapter.getCurrentList().remove(position);
            }
            position++;
        }
        mMessageAdapter.notifyDataSet();
    }

    @Override
    public void OnAddCommentError(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMessageTypesFound(List<MessageType> messageTypes) {

    }

    @Override
    public void onAdvanceMessageSend(String message, String selectedMessageType) {
        rvMessageList.scrollToPosition(mMessageAdapter.getItemCount());
        mProgressBar.setVisibility(View.GONE);
        mTvMessage.setVisibility(View.GONE);

        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            Messages recentMsg = createMessage("", message);

            mAllMessage.add(recentMsg);
            mMessageAdapter.addItem(recentMsg);

            mMessageSync.addNewMessage(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, mTabName, message, recentMsg, selectedMessageType);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }
}
