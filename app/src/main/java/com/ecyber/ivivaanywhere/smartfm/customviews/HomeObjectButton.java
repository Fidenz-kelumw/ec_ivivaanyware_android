package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.models.CreateFromMenu;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;

import java.util.List;

/**
 * Created by Lakmal on 6/20/2016.
 */
public class HomeObjectButton implements View.OnClickListener {

    private Activity mActivity;
    private View mView;
    private CustomTextView mBtn;
    private ImageView mImgView;
    private CImageLoader mCImageLoader;
    private RelativeLayout mRelBtnContainer;
    private HomeButtonClickListener mDelegate;
    private RelativeLayout mPlusContainer;
    private LinearLayout mLinTextContain;
    private List<CreateFromMenu> mCreateFromMenu;
    private TextView mBtnPlus;
    private String mObjectType;
    private boolean mExists;
    private ObjectType objectType;

    public HomeObjectButton(Activity activity, HomeButtonClickListener delegate, List<CreateFromMenu> createFromMenu, ObjectType objectType) {
        mActivity = activity;
        mDelegate = delegate;
        mExists = new Select().from(CreateFromMenu.class).exists();

        this.mCreateFromMenu = createFromMenu;
        this.objectType = objectType;

        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_home_button, null);
        mRelBtnContainer = (RelativeLayout) mView.findViewById(R.id.rel_btn);
        mLinTextContain = (LinearLayout) mView.findViewById(R.id.lin_text_container);
        mBtn = (CustomTextView) mView.findViewById(R.id.btn_home_title);
        mBtnPlus = (TextView) mView.findViewById(R.id.font_awsome_plus);
        mImgView = (ImageView) mView.findViewById(R.id.img_home_btn_icon);
        mPlusContainer = (RelativeLayout) mView.findViewById(R.id.rel_plus_container);
        mCImageLoader = new CImageLoader(mActivity);
        mRelBtnContainer.setOnClickListener(this);
        mPlusContainer.setOnClickListener(this);


        Typeface fontAwesomeFont = Typeface.createFromAsset(mActivity.getAssets(), "fonts/fontawesome-webfont.ttf");
        mBtnPlus.setTypeface(fontAwesomeFont);

    }

    public View getHomeObjectButton(String btnIconUrl, String title, String objectType, boolean isShow) {
        mCImageLoader.DisplayImage(btnIconUrl, mImgView, R.drawable.ic_home_obj_def);
        mRelBtnContainer.setTag(objectType);
        mBtn.setText(title == null ? objectType : title);
        mObjectType = objectType;
        if (isShow) {
            mPlusContainer.setVisibility(View.VISIBLE);
        }
        return mView;
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_plus_container:
                mDelegate.onClickPlusButton(mCreateFromMenu, mObjectType);
                break;
            case R.id.rel_btn:
                mDelegate.homeButtonClick(v.getTag().toString(), objectType);
                break;
        }

    }

    public interface HomeButtonClickListener {
        void homeButtonClick(String buttonName, ObjectType objectType);

        void onClickPlusButton(List<CreateFromMenu> createFromMenus, String objectType);
    }

}
