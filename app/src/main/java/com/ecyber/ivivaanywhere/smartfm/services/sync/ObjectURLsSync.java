package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ObjectURLsResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.ObjectURLs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ObjectURLsSync {
    private Context context;
    private ObjectURLsCallback callback;

    public ObjectURLsSync(Context context, ObjectURLsCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void getObjectURLs(String url, final String objectKey, String objectType, String userKey, String tabName, String apikey){
        ServiceGenerator.CreateService(ObjectURLs.class,url).getObjectURLs(objectKey,objectType,userKey,tabName,apikey).enqueue(new Callback<ObjectURLsResponse>() {
            @Override
            public void onResponse(Call<ObjectURLsResponse> call, Response<ObjectURLsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getObjectURLs().size() != 0) {
                            new ObjectURL().clearTable();
                            List<ObjectURL> objectURLs = response.body().getObjectURLs();
                            for (ObjectURL objectURL : objectURLs) {
                                objectURL.save();
                            }
                            callback.onObjectURLsSuccess(objectURLs);
                            new TimeCap().updateTableTime(tabName);
                        } else {
                            callback.onObjectURLsFail("No items found");
                        }
                    }
                } else {
                    try {
                        String message = response.errorBody().string();
                        Log.d("SmartFm - ErrorLog", message);
                        ErrorLog.saveErrorLog("Get ObjectURLs", message);
                        callback.onObjectURLsFail(SmartConstants.NO_MESSAGE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onObjectURLsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<ObjectURLsResponse> call, Throwable t) {
                callback.onObjectURLsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
            }
        });
    }

    public boolean isDataAvailable(){
        List<ObjectURL> all = new ObjectURL().getAllObjectURLs();
        if (all.size() == 0) {
            return false;
        } else {
            callback.onObjectURLsFoundItems(all);
            return true;
        }
    }

    public interface ObjectURLsCallback{
        void onObjectURLsSuccess(List<ObjectURL> objectURL);
        void onObjectURLsFail(String error);
        void onObjectURLsFoundItems(List<ObjectURL> objectURL);
    }
}
