package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
@Table(name = "Roles")
public class Roles extends Model{

    @Column(name = "RoleID")
    @SerializedName("RoleID")
    @Expose
    public String RoleID;

    @Column(name = "Color")
    @SerializedName("Color")
    @Expose
    public String Color;

    @Column(name = "ObjectType")
    public String ObjectType;

    public Roles(String roleID, String color, String objectType) {
        super();
        RoleID = roleID;
        Color = color;
        ObjectType = objectType;
    }

    public Roles() {
        super();
    }

    public String getRoleID() {
        return RoleID;
    }

    public void setRoleID(String roleID) {
        RoleID = roleID;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public List<Roles> getAllRoles(){
        return new Select()
                .from(Roles.class)
                .execute();
    }

    public List<Roles> selectRolesByObjectType(String objectType){
        return new Select()
                .from(Roles.class).where("ObjectType=?",objectType)
                .execute();
    }

    public void clearTable(){
        new Delete().from(Roles.class).execute();
    }
}
