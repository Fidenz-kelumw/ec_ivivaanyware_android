package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.SubObjectsAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.FilterObjects;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.StringEmptyCheck;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectAction;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectDeafultFilter;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectOptionList;
import com.ecyber.ivivaanywhere.smartfm.popup.ConfirmationDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.SubObjectFilterDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SubObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.SubObjectsSync;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class RelatedWOFragment extends Fragment implements SubObjectsSync.SubObjectsCallback, SubObjectsAdapter.SubObjectActionCallback, SubObjectFilterDialog.SubObjectFilterSelectionCallback {

    private AccountPermission mAccountPermission;
    private NetworkCheck mNetwork;
    private ColoredSnackbar coloredSnackbar;
    private SubObjectsAdapter subObjectsAdapter;

    private SubObjectsSync subObjectsSync;

    private List<SubObject> items, searchItems;
    private List<SubObjectOptionList> optionLists;
    private String mObjectType, mObjectKey, mLocationKey, mFilterID, mPreviousActivityName, mFromActivity, mTabName, defaultFilter, defaultFilterDisplayText;

    private boolean isTabForceLoad;

    @BindView(R.id.editText_search_bar)
    CustomEditTextView search;

    @BindView(R.id.btnSetFilter)
    CustomButton btnfilter;

    @BindView(R.id.recyclerview_objects)
    RecyclerView recyclerviewObjectList;

    @BindView(R.id.swipe_refresh_object_list)
    SwipeRefreshLayout swipeRefreshObjectList;

    @BindView(R.id.tv_message)
    CustomTextView message;

    @BindView(R.id.progressbar)
    ProgressBar pb;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_related_wo, container, false);
        ButterKnife.bind(this, rootView);

        init();
        loadData();

        initListners();

        return rootView;
    }

    private void initListners() {
        swipeRefreshObjectList.setOnRefreshListener(() -> invokeToObjectItemSync(true));

        ItemClickSupport.addTo(recyclerviewObjectList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                SubObject object = items.get(position);

                Intent intent = new Intent(getActivity(), SideNavigationActivity.class);
                intent.putExtra(SmartConstants.OBJECT_TYPE, object.getObjectType());
                intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
                intent.putExtra(SmartConstants.OBJECT_KEY, object.getObjectKey());
                intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
                intent.putExtra(SmartConstants.FILTER_ID, mFilterID);
                intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_ORDER_ACTIVITY);

                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                subObjectsAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void init() {
        items = new ArrayList<>();
        searchItems = new ArrayList<>();
        mNetwork = new NetworkCheck();
        mAccountPermission = new AccountPermission();
        coloredSnackbar = new ColoredSnackbar(getActivity());
        subObjectsSync = new SubObjectsSync(getActivity(), this);

        search.setEnabled(false);

        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mLocationKey = getArguments().getString(SmartConstants.LOCATION_KEY);
        mFilterID = getArguments().getString(SmartConstants.FILTER_ID);
        mPreviousActivityName = getArguments().getString(SmartConstants.ACTIVITY_NAME);
        mFromActivity = getArguments().getString(SmartConstants.FROM_WHERE_ACTIVITY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        recyclerviewObjectList.setHasFixedSize(true);
        recyclerviewObjectList.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));

        defaultFilter = "";
        optionLists = new ArrayList<>();

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        recyclerviewObjectList.addItemDecoration(new RecyclerDecorator(this.getActivity().getApplicationContext(), spanCount, spacing, includeEdge, false));

    }

    private void loadData() {
        search.setText("");
        pb.setVisibility(View.VISIBLE);

        if (isTabForceLoad) {
            invokeToObjectItemSync(true);
        } else {
            DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
            if (dataDownloadOptimization.isDataAvailableInTable(SubObject.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToObjectItemSync(true);
                } else {
                    boolean isAvailable = subObjectsSync.isDataAvailable(mObjectKey);
                    if (!isAvailable) {
                        invokeToObjectItemSync(true);
                    }
                }
            } else {
                invokeToObjectItemSync(true);
            }
        }
    }

    private void invokeToObjectItemSync(boolean isNeedToResetFilters) {
        search.setText("");
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            subObjectsSync.getSubObjects(mAccountPermission.getAccountInfo().get(0),
                    mObjectKey,
                    mObjectType,
                    mAccountPermission.getAccountInfo().get(2),
                    mTabName,
                    mAccountPermission.getAccountInfo().get(1), defaultFilter, isNeedToResetFilters);
        } else {
            coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            pb.setVisibility(View.INVISIBLE);
            message.setVisibility(View.VISIBLE);
            message.setText("Error in network connection");
        }
    }

    private void filterObjects(String search) {
        StringEmptyCheck check = new StringEmptyCheck();
        boolean isEmpty = check.isNotNullNotEmptyNotWhiteSpaceOnly(search);

        message.setVisibility(View.GONE);

        if (!isEmpty) {
            if (searchItems != null) {
                subObjectsAdapter.clear();
                for (SubObject objects : searchItems) {
                    subObjectsAdapter.addItem(objects);
                }
            }
        } else {
            if (searchItems != null) {
                List<SubObject> objList = new FilterObjects().filterSubObjects(searchItems, search);
                if (objList.size() > 0) {
                    subObjectsAdapter.clear();
                    for (SubObject obj : objList) {
                        subObjectsAdapter.addItem(obj);
                    }
                } else {
                    subObjectsAdapter.clear();
                    message.setText("No Result for \"" + search + "\"");
                    message.setVisibility(View.VISIBLE);
                }
            } else {
                message.setText("Poor Internet Connection.");
                message.setVisibility(View.VISIBLE);
            }
        }
    }

    private void performSubObjectAction(SubObject subObject, SubObjectAction action) {
        coloredSnackbar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);

        subObjectsSync.performSubObjectAction(mAccountPermission.getAccountInfo().get(0),
                subObject.getObjectKey(),
                subObject.getObjectType(),
                mAccountPermission.getAccountInfo().get(2),
                action.getActionId(),
                mAccountPermission.getAccountInfo().get(1), subObject, action);
    }

    @OnClick(R.id.btnSetFilter)
    public void onClickFilterButton(View view) {
        new SubObjectFilterDialog(getActivity(), this).showBuildingListDialog(mObjectKey);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onGetSubObjectsSuccess(List<SubObject> subObjects) {
        pb.setVisibility(View.INVISIBLE);
        message.setVisibility(View.INVISIBLE);
        search.setEnabled(true);
        items.clear();
        items = subObjects;

        swipeRefreshObjectList.setRefreshing(false);
        subObjectsAdapter = new SubObjectsAdapter(getActivity(), subObjects, mObjectType, this);
        recyclerviewObjectList.setAdapter(subObjectsAdapter);
    }

    @Override
    public void onGetSubObjectsFail(String error) {
        coloredSnackbar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        message.setVisibility(View.VISIBLE);
        message.setText("No items found");
        swipeRefreshObjectList.setRefreshing(false);
        pb.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onGetSubObjectsFoundItems(List<SubObject> subObjects) {
        pb.setVisibility(View.INVISIBLE);
        message.setVisibility(View.INVISIBLE);
        search.setEnabled(true);
        items.clear();
        items = subObjects;

        swipeRefreshObjectList.setRefreshing(false);
        subObjectsAdapter = new SubObjectsAdapter(getActivity(), subObjects, mObjectType, this);
        recyclerviewObjectList.setAdapter(subObjectsAdapter);
    }

    @Override
    public void onPerformSubObjectActionSuccess(String message, SubObject subObject, SubObjectAction subObjectAction) {
        coloredSnackbar.dismissSnacBar();
        coloredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);

        subObjectsAdapter.updateActionPerformedObject(subObject, subObjectAction);
    }

    @Override
    public void onPerformSubObjectActionFail(String message) {
        coloredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onSubObjectFiltersFound(SubObjectResponse.Filter filter) {
        if (filter != null && filter.getOptionLists() != null) {
            btnfilter.setVisibility(View.VISIBLE);
            if (filter.getDeafultFilter() != null && filter.getDeafultFilter().getSelectedValue() != null) {
                List<SubObjectOptionList> list = filter.getOptionLists();
                if (filter.getDeafultFilter().getSelectedValue() != null && !filter.getDeafultFilter().getSelectedValue().equals("")) {
                    for (SubObjectOptionList optionList : list) {
                        if (optionList.getValue().equals(filter.getDeafultFilter().getSelectedValue())) {
                            btnfilter.setText(optionList.getDisplayText());
                            defaultFilter = filter.getDeafultFilter().getSelectedValue();
                            defaultFilterDisplayText = optionList.getDisplayText();
                            break;
                        }
                    }
                } else {
                    btnfilter.setText(filter.getNoDataText());
                    defaultFilter = "";
                    defaultFilterDisplayText = filter.getNoDataText();
                }

                if (list == null || list.size() == 0) {
                    btnfilter.setText(filter.getNoDataText());
                    defaultFilter = "";
                    defaultFilterDisplayText = filter.getNoDataText();
                }
            } else {
                btnfilter.setText(filter.getNoDataText());
                defaultFilter = "";
                defaultFilterDisplayText = filter.getNoDataText();
            }

            optionLists = filter.getOptionLists();
        } else {
            btnfilter.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSubObjectActionPerformed(SubObject subObject, SubObjectAction action) {
        final SubObject obj = subObject;
        if (action.getUserConfirmation() != null && action.getUserConfirmation().equals("1")) {
            new ConfirmationDialog(getActivity(), "Do you need to perform \nthis action?", 0, (message, position, dialogInstance) -> {
                performSubObjectAction(obj, action);
                dialogInstance.dismiss();
            }).show();
        } else {
            performSubObjectAction(obj, action);
        }
    }

    @Override
    public void onSubObjectsSearchPerform(List<SubObject> objects, String query) {
        message.setVisibility(View.GONE);

        if (objects == null || objects.size() == 0) {
            subObjectsAdapter.clear();
            message.setText("No Result for \"" + query + "\"");
            message.setVisibility(View.VISIBLE);
        }

        searchItems = items = objects;
    }

    @Override
    public void onSubObjectFilterChange(String value, String displayText) {
        defaultFilter = value;
        new Update(SubObjectDeafultFilter.class)
                .set("SelectedValue = ?", value)
                .where("ObjectKey = ?", mObjectKey)
                .execute();

        btnfilter.setText(displayText);
        swipeRefreshObjectList.setRefreshing(true);
        invokeToObjectItemSync(false);
    }
}
