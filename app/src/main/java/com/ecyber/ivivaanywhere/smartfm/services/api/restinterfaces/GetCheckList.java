package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CheckListResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CheckStatusUpdateResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ChecklistInfoResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 4/22/2016.
 */
public interface GetCheckList {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectChecklist")
    Call<CheckListResponse> getCheckList(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "SetCheckItemStatus")
    Call<CheckStatusUpdateResponse> updateCheckStatus(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName, @Field("CheckItemKey") String checkItemKey, @Field("Status")String status);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "SetCheckItemDeadline")
    Call<CheckStatusUpdateResponse> updateCheckDeadLine(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName, @Field("CheckItemKey") String checkItemKey, @Field("Deadline")String deadLine);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "SetCheckItemDescription")
    Call<CheckStatusUpdateResponse> updateCheckDescription(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName, @Field("CheckItemKey") String checkItemKey, @Field("Description")String description);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetCheckItemInfo")
    Call<ChecklistInfoResponse> getInfoText(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("InfoCode") String infoCode);

}
