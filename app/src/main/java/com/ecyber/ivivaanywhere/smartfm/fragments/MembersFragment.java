package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.MembersAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.Roles;
import com.ecyber.ivivaanywhere.smartfm.services.sync.MembersSync;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lakmal on 4/20/2016.
 */
public class MembersFragment extends Fragment implements MembersSync.GetMembersCallback {

    @BindView(R.id.rv_members)
    RecyclerView rvMembers;

    @BindView(R.id.swiperefresh_members)
    SwipeRefreshLayout mSwipeRefreshLayout;


    private MembersSync mMembersSync;
    private AccountPermission mAccountPermission;
    private String mObjectType;
    private String mObjectKey;
    private String mTabaName;

    private boolean isTabForceLoad;

    private List<Roles> allRoles;
    private MembersAdapter mMemberAdapter;
    private List<Members> mMembers = new ArrayList<>();
    private Activity mActivity;
    private ProgressBar mProgressBar;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    public MembersFragment() {

    }

    @SuppressLint("ValidFragment")
    public MembersFragment(Activity activity, ProgressBar progressBar) {
        mActivity = activity;
        mProgressBar = progressBar;
        mNetwork = new NetworkCheck();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_members, container, false);

        ButterKnife.bind(this, rootView);
        mColoredSnackBar = new ColoredSnackbar(getActivity());
        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabaName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        Roles roles = new Roles();

        allRoles = roles.selectRolesByObjectType(mObjectType);

        initViews();

        dataLoad();
        return rootView;
    }

    private void initViews() {
        mAccountPermission = new AccountPermission();
        mMembersSync = new MembersSync(this);
        configMembersView();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mMemberAdapter.clear();
            mMemberAdapter.notifyDataSetChanged();
            dataLoad();
        });
    }

    private void configMembersView() {
        rvMembers.setHasFixedSize(true);
        rvMembers.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));

        mMemberAdapter = new MembersAdapter(this.getActivity().getApplicationContext(), mMembers, mAccountPermission.getAccountInfo().get(0), allRoles);
        rvMembers.setAdapter(mMemberAdapter);
        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        rvMembers.addItemDecoration(new RecyclerDecorator(this.getActivity().getApplicationContext(), spanCount, spacing, includeEdge, false));
    }

    private void dataLoad() {
        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();

        mProgressBar.setVisibility(View.VISIBLE);

        if (isTabForceLoad) {
            invokeToMemberSync();
        } else {
            if (dataDownloadOptimization.isDataAvailableInTable(Members.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabaName, false)) {
                    invokeToMemberSync();
                } else {
                    boolean isAvailable = mMembersSync.getMembersFromDb(mObjectKey);
                    if (!isAvailable) {
                        invokeToMemberSync();
                    }
                }
            } else {
                invokeToMemberSync();
            }
        }
    }

    private void invokeToMemberSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mMembersSync.getMembers(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabaName);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onGetMembersSuccess(Members member) {
        mMemberAdapter.addItem(member);
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onGetMembersError(String error) {
        Toast.makeText(mActivity.getApplicationContext(), error, Toast.LENGTH_LONG).show();
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }
}
