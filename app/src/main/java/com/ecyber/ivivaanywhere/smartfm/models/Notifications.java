package com.ecyber.ivivaanywhere.smartfm.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.activities.SplashActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Lakmal on 5/17/2016.
 */
@Table(name = "Notifications")
public class Notifications extends Model {

    @Column(name = "Message")
    public String mMessage;

    @Column(name = "MessageFrom")
    public String mFrom;

    @Column(name = "ObjectType")
    public String mObjectType;

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "ReadStatus")
    public int mReadStatus;//1 or 0

    @Column(name = "CreatedAt")
    public String mCreatedAt;

    public boolean isExpand;
    public boolean hasIcon;

    public boolean isHasIcon() {
        return hasIcon;
    }

    public void setHasIcon(boolean hasIcon) {
        this.hasIcon = hasIcon;
    }

    public static final int READ = 1;
    public static final int UNREAD = 0;
    public static final int ALL = 2;


    public Notifications() {
        super();
    }

    public Notifications(String message, String from, String objectType, String objectKey, int readStatus) {
        mMessage = message;
        mFrom = from;
        mObjectType = objectType;
        mObjectKey = objectKey;
        mReadStatus = readStatus;
        mCreatedAt = Utility.getDateNowISOFormat();
    }

    public List<Notifications> getAllNotification(int readStatus) {
        List<Notifications> list = null;

        switch (readStatus) {
            case ALL:
                list = new Select().from(Notifications.class).orderBy("Id DESC").execute();
                break;
            case UNREAD:
                list = new Select().from(Notifications.class).where("ReadStatus = ?", readStatus).orderBy("Id DESC").execute();
                break;
            case READ:
                list = new Select().from(Notifications.class).where("ReadStatus = ?", readStatus).orderBy("Id DESC").execute();
                break;
        }
        return list;
    }

    public static String getDateTimeNow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date dateNow = new Date();
        String nDate = simpleDateFormat.format(dateNow);
        return nDate;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getFrom() {
        return mFrom;
    }

    public void setFrom(String from) {
        mFrom = from;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public int getReadStatus() {
        return mReadStatus;
    }

    public void setReadStatus(int readStatus) {
        mReadStatus = readStatus;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }


    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public static void updateStatus(Long id) {
        try {
            Model model = new Select().from(Notifications.class).where("id=?", id).executeSingle();
            if (model.getId().equals(id)) {
                new Update(Notifications.class).set("ReadStatus = ?", Notifications.READ).where("id = ?", id).execute();
            }
        } catch (Exception e) {

        }

    }

    public int getUnreadMessageCount() {
        return new Select().from(Notifications.class).where("ReadStatus = ?", UNREAD).orderBy("Id DESC").execute().size();
    }

    public static void deleteOldMessages(int size) {
        int count = new Select().from(Notifications.class).count();
        int def = count - size;
        for (int i = 0; i < def; i++) {
            Notifications model = new Select().from(Notifications.class).orderBy("Id ASC").executeSingle();
            new Delete().from(Notifications.class).where("Id = ?", model.getId()).execute();
        }
    }

    public static long saveToDB(String message, String from, String objectType, String objectKey) {

        Log.d(SplashActivity.class.getSimpleName(), "Message- "+message+",ObjectType- "+objectType+",From- "+from+",ObjectKEy-" + objectKey);
        int size = new Select().from(Notifications.class).execute().size();
        int count = new Select().from(Notifications.class).count();
        if (count >= Integer.parseInt(Registration.getInboxSize().InboxSize)) {
            Notifications model = new Select().from(Notifications.class).orderBy("Id ASC").executeSingle();
            new Delete().from(Notifications.class).where("Id = ?", model.getId()).execute();
        }

        return new Notifications(message, from, objectType, objectKey, Notifications.UNREAD).save();
    }
}
