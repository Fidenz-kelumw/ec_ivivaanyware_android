package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LookupServiceResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetDataLookupService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/15/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class LookupServiceSync {
    private Context context;
    private LookupServiceCallback callback;

    public LookupServiceSync(Context context, LookupServiceCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void getLookupServiceData(String url, String path, String userkey, String apikey) {

        String locationKey = new Registration().select().LocationKey;

        ServiceGenerator.CreateService(GetDataLookupService.class, url).getLookupService(path, userkey, apikey, locationKey)
                .enqueue(new Callback<LookupServiceResponse>() {
                    @Override
                    public void onResponse(Call<LookupServiceResponse> call, Response<LookupServiceResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getOptionListModels().size() != 0) {
                                    callback.onLookupServiceSuccess(response.body().getOptionListModels());
                                } else {
                                    callback.onLookupServiceFailed(SmartConstants.TRY_AGAIN);
                                }
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get LookupService", message);
                                callback.onLookupServiceFailed(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onLookupServiceFailed(SmartConstants.TRY_AGAIN_EXCEPTION);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LookupServiceResponse> call, Throwable t) {
                        callback.onLookupServiceFailed(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    public interface LookupServiceCallback {
        void onLookupServiceSuccess(List<OptionListModel> optionListModels);

        void onLookupServiceFailed(String error);
    }
}
