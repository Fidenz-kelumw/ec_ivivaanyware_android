package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.FieldTypeSAARAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.popup.SAARDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.LookupServiceSync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Choota on 3/14/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class FieldTypeSAAR implements FieldTypeSAARAdapter.UpdateSAARCallback, SAARDialog.SAARDialogCallback, LookupServiceSync.LookupServiceCallback {
    private Context context;
    private ObjectInfoModel model;
    private FieldTypeSAARCallback callback;
    private SAARDialog saarDialog;

    private View view;
    private RecyclerView recyclerView;
    private CustomTextView title, titleText;
    private FieldTypeSAARAdapter adapter;
    private ImageButton qr;

    private int objectPosition;
    private List<String> selectedMembers;
    private List<OptionListModel> optionListModels;

    public FieldTypeSAAR(Context context, int postion, ObjectInfoModel objectInfoModel, FieldTypeSAARCallback callback) {
        this.context = context;
        this.model = objectInfoModel;
        this.callback = callback;
        this.objectPosition = postion;

        this.getDefValues();
        this.saarDialog = new SAARDialog(context, model.getOptionListModels(), selectedMembers, model, this);

        if (model.getLookupservice() != null && !model.getLookupservice().equals("")) {

            AccountPermission accountPermission = new AccountPermission();
            new LookupServiceSync(context, this).getLookupServiceData(
                    accountPermission.getAccountInfo().get(0), model.getLookupservice(),
                    accountPermission.getAccountInfo().get(2),
                    accountPermission.getAccountInfo().get(1));
        } else {
            saarDialog.clearItems();
//            this.saarDialog = new SAARDialog(context, null, null, model, this);
//            if(optionListModels != null) {
//                optionListModels.clear();
//            }
        }
    }

    public View getView(){
        return generateView();
    }

    private void getDefValues() {
        selectedMembers = new ArrayList<>();
        optionListModels = new ArrayList<>();

        List<ValueModel> models = model.getValueModels();
        optionListModels = Collections.unmodifiableList(model.getOptionListModels());
        List<String> temp = new ArrayList<>();
        if (models != null && models.size() > 0) {
            for (ValueModel valueModel : models) {
                temp.add(valueModel.getValue());
            }
        }

        //region removed validations
        //        if (optionListModels != null && optionListModels.size() > 0) {
//            for (OptionListModel m : optionListModels) {
//                for (String s : temp) {
//                    if (m.getValue().equals(s)) {
//                        selectedMembers.add(s);
//                    }
//                }
//            }
//        } else {
//            selectedMembers = temp;
//        }
        //endregion

        selectedMembers = temp;
        optionListModels = Collections.unmodifiableList(model.getOptionListModels() != null ? model.getOptionListModels() : (new ArrayList<OptionListModel>()));
    }

    public View generateView() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        this.view = layoutInflater.inflate(R.layout.view_saar, null);

        this.recyclerView = (RecyclerView) view.findViewById(R.id.recSupervisors);
        this.title = (CustomTextView) view.findViewById(R.id.title);
        this.titleText = (CustomTextView) view.findViewById(R.id.title_text);
        this.qr = (ImageButton) view.findViewById(R.id.btnQr);

        if(model.getQrcode() != null && model.getQrcode().equals("1"))
            qr.setVisibility(View.VISIBLE);
        else
            qr.setVisibility(View.GONE);

        this.titleText.setText(model.getFieldname());

        this.adapter = new FieldTypeSAARAdapter(context,
                model,
                selectedMembers,
                this,
                model.getEditable());

        callback.onSAARAdapterGenerated(adapter);

        List<ValueModel> valueModels = model.getValueModels();

        this.recyclerView.hasFixedSize();
        this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        this.recyclerView.setAdapter(this.adapter);

        this.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getEditable() != null && model.getEditable().equals("1"))
                    saarDialog.show(false);
            }
        });

        qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getEditable() != null && model.getEditable().equals("1"))
                    saarDialog.show(true);
            }
        });
        return view;
    }

    @Override
    public void onNewSAARAddedToList(String name, String value, int position) {
        callback.onNewSAARAdded(value, name, objectPosition);
    }

    @Override
    public void onSAARRemovedFromList(int position, String value) {
        callback.onSAARRemoved(value, objectPosition);
    }

    @Override
    public void onSAARItemChecked(String value, String data) {
        callback.onSAARDialogItemAdded(value, data);
    }

    @Override
    public void onSAARItemCheckRemoved(String value, String data) {
        callback.onSAARDialogItemRemoved(value, data);
    }

    @Override
    public void onLookupServiceSuccess(List<OptionListModel> optionListModels) {
        this.model.setOptionListModels(optionListModels);
        this.optionListModels = optionListModels;
        this.getDefValues();

        if (this.adapter != null)
            this.adapter.updateOptionList(optionListModels, selectedMembers);

        this.saarDialog = new SAARDialog(context, optionListModels, this.selectedMembers, this.model, this);
    }

    @Override
    public void onLookupServiceFailed(String error) {

    }

    public interface FieldTypeSAARCallback {
        void onSAARAdapterGenerated(FieldTypeSAARAdapter adapter);

        void onNewSAARAdded(String value, String name, int position);

        void onSAARRemoved(String value, int position);

        void onSAARDialogItemAdded(String value, String name);

        void onSAARDialogItemRemoved(String value, String name);
    }
}
