package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectDeafultFilter;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectOptionList;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SubObjectResponse {
    @SerializedName("SubObjects")
    List<SubObject> subObjects;

    @SerializedName("Filter")
    Filter filter;

    public SubObjectResponse() {
    }

    public SubObjectResponse(List<SubObject> subObjects, Filter filter) {
        this.subObjects = subObjects;
        this.filter = filter;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public List<SubObject> getSubObjects() {
        return subObjects;
    }

    public void setSubObjects(List<SubObject> subObjects) {
        this.subObjects = subObjects;
    }

    public static class Filter {
        // @SerializedName("DefaultData")
        SubObjectDeafultFilter deafultFilter;

        @SerializedName("OptionList")
        List<SubObjectOptionList> optionLists;

        @SerializedName("NoDataText")
        String noDataText;

        public Filter() {
        }

        public Filter(SubObjectDeafultFilter deafultFilter, List<SubObjectOptionList> optionLists, String noDataText) {
            this.deafultFilter = deafultFilter;
            this.optionLists = optionLists;
            this.noDataText = noDataText;
        }

        public SubObjectDeafultFilter getDeafultFilter() {
            return deafultFilter;
        }

        public void setDeafultFilter(SubObjectDeafultFilter deafultFilter) {
            this.deafultFilter = deafultFilter;
        }

        public List<SubObjectOptionList> getOptionLists() {
            return optionLists;
        }

        public void setOptionLists(List<SubObjectOptionList> optionLists) {
            this.optionLists = optionLists;
        }

        public String getNoDataText() {
            return noDataText;
        }

        public void setNoDataText(String noDataText) {
            this.noDataText = noDataText;
        }
    }
}
