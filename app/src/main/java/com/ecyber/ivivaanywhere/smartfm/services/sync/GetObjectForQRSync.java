package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetObjectForQrResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectForQR;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 6/6/2016.
 */
public class GetObjectForQRSync {


    private GetObjectForQRCallback mGetObjectForQRCallback;

    public GetObjectForQRSync(GetObjectForQRCallback delegate) {
        mGetObjectForQRCallback = delegate;
    }

    public void getObjectForQR(final String url, final String apiKey, final String userKey, final String qrCode, String lookupService, final String os) {
        ServiceGenerator.CreateService(GetObjectForQR.class, url).getObjectForQR(apiKey, userKey, os, qrCode, lookupService)
                .enqueue(new Callback<GetObjectForQrResponse>() {
                    @Override
                    public void onResponse(Call<GetObjectForQrResponse> call, Response<GetObjectForQrResponse> response) {
                        if (response != null && response.isSuccessful()) {
                            if (response.body() != null) {
                                mGetObjectForQRCallback.getObjectForQRSuccess(response.body());
                            } else {
                                mGetObjectForQRCallback.getObjectForQRError(SmartConstants.QR_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Object For QR", message);
                                mGetObjectForQRCallback.getObjectForQRError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetObjectForQRCallback.getObjectForQRError(SmartConstants.QR_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetObjectForQrResponse> call, Throwable t) {
                        mGetObjectForQRCallback.getObjectForQRError(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    public interface GetObjectForQRCallback {
        void getObjectForQRSuccess(GetObjectForQrResponse response);

        void getObjectForQRError(String message);
    }
}
