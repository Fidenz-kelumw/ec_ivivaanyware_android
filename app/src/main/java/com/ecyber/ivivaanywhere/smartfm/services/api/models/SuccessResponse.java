package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SuccessResponse {

    @SerializedName("Success")
    public String Success;

    @SerializedName("Message")
    public String Message;

    public SuccessResponse() {
    }

    public SuccessResponse(String success, String message) {
        Success = success;
        Message = message;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }
}
