package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 11/28/17.
 */

public class StatusResponse {

    @SerializedName("Status")
    String status;

    public StatusResponse() {
    }

    public StatusResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
