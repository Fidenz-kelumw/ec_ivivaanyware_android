package com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Chathura Jayanath on 8/5/2015.
 */
public class DataDownloadOptimization {

    public DataDownloadOptimization() {
    }

    public boolean needToReload(String lastTime, String waitTime, boolean isHours) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dateLastUpdate = new Date();
        Date dateNow = new Date();
        String nDate = simpleDateFormat.format(dateNow);
        try {
            dateLastUpdate = simpleDateFormat.parse(lastTime);
            dateNow = simpleDateFormat.parse(nDate);
        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        long duration = dateNow.getTime() - dateLastUpdate.getTime();
        long diffInSeconds = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);

        if (isHours) {
            if ((int) diffInSeconds >= (Integer.parseInt(waitTime) * 3600)) {
                return true;
            } else {
                return false;
            }
        } else {
            if ((int) diffInSeconds >= (Integer.parseInt(waitTime))) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void initializeTimeCap() {
        new TimeCap().clearTable();

        String[] tables = {
                SmartConstants.ALL,
                SmartConstants.BUILDINGS,
                SmartConstants.OBJECT_TYPES,
                SmartConstants.FILTERS,
                SmartConstants.MEMBERS,
                SmartConstants.USERS,
                SmartConstants.CHECKLIST,
                SmartConstants.INFO,
                SmartConstants.OBJECT_INFO,
                SmartConstants.OPTION_LIST,
                SmartConstants.PAGE_LINKS,
                SmartConstants.FIELD_DATA,
                SmartConstants.ACTIONS,
                SmartConstants.MESSAGES,
                SmartConstants.ATTACHMENTS,
                SmartConstants.ATTACHMENTS_MASTER,
                SmartConstants.OBJECT_LAYOUT,
                SmartConstants.OBJECT_LIST,
                SmartConstants.DEFAULT,
                SmartConstants.MASTER_SYNC,
                SmartConstants.OBJECTITEMS,
                SmartConstants.SUBOBJECTS,
                SmartConstants.OBJECTURLS,
                SmartConstants.OBJECTITEMSBUTTONS,
                SmartConstants.SPOTSITEM
        };

        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        Date daysAgo = new DateTime(date).minusYears(1).toDate();
        String dateToDBD = simpleDateD.format(daysAgo);

        for (String name : tables) {
            TimeCap timeCap = new TimeCap();
            timeCap.tableName = name;
            timeCap.updatedTime = dateToDBD;
            timeCap.save();
        }
    }

    public boolean isTableDataOld(String tableName, boolean isMaster) {

        List<TimeCap> timeCapList = new TimeCap().getAllTimeUpdates();
        AccountPermission accountPermission = new AccountPermission();

        try {
            for (int i = 0; i < timeCapList.size(); i++) {
                if (timeCapList.get(i).tableName.equals(tableName)) {
                    if (needToReload(timeCapList.get(i).updatedTime, true == isMaster ? accountPermission.getMasterDataRelodTime() : accountPermission.getTransactionDataRelodTime(), true == isMaster ? true : false)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } catch (Exception e){
            return true;
        }

        return true;
    }

    public <D extends Model> boolean  isDataAvailableInTable(Class<D> dbClass) {
        return (new Select().from(dbClass).execute().size() != 0) ? true : false;
    }

    public void resetApplication() {
        new Buildings().clearTable();
        new TimeCap().clearTable();
        new Registration().clearTable();
    }
}
