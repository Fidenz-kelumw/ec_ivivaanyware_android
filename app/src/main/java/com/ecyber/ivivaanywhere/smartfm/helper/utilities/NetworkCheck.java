package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;

/**
 * Created by Chathura Jayanath on 7/21/2015.
 */
public class NetworkCheck {

    private static ColoredSnackbar mColoredSnackBar;
    public NetworkCheck() {
    }

    public static boolean IsAvailableNetwork(Context context) {
        mColoredSnackBar = new ColoredSnackbar(context);
        boolean state = NetworkConnectionCheck(context);
        if (state) {
            return true;
        }else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            return false;
        }
    }

    public static boolean NetworkConnectionCheck(Context con) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager connectivityManager = (ConnectivityManager)con.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
