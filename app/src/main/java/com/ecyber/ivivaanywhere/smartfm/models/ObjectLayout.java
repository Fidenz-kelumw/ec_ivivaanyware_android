package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Update;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Lakmal on 5/15/2016.
 */
@Table(name = "Object_Layout")
public class ObjectLayout extends Model {

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "Path")
    public String mPath;

    @Column(name = "ZoomLevels")
    public String mZoomLevels;

    @Column(name = "X")
    public String mX;

    @Column(name = "Y")
    public String mY;

    @Column(name = "Editable")
    public String mEditable;

    @Column(name = "PinName")
    public String pinName;

    @Column(name = "PinDescription")
    public String pinDescription;

    public ObjectLayout() {
        super();
    }

    public void clearTable() {
        new Delete().from(ObjectLayout.class).execute();
        new Delete().from(Pin.class).execute();
    }

    public static void updateDropPin(String objectKey, Marker newMarker) {
        double latitude = newMarker.getPosition().latitude; // y
        double longitude = newMarker.getPosition().longitude; // x
        new Update(ObjectLayout.class).set("X = ?," + "Y = ?", longitude, latitude).where("ObjectKey = ?", objectKey).execute();
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getZoomLevels() {
        return mZoomLevels;
    }

    public void setZoomLevels(String zoomLevels) {
        mZoomLevels = zoomLevels;
    }

    public String getX() {
        return mX;
    }

    public void setX(String x) {
        mX = x;
    }

    public String getY() {
        return mY;
    }

    public void setY(String y) {
        mY = y;
    }

    public String getEditable() {
        return mEditable;
    }

    public void setEditable(String editable) {
        mEditable = editable;
    }

    public String getPinName() {
        return pinName;
    }

    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    public String getPinDescription() {
        return pinDescription;
    }

    public void setPinDescription(String pinDescription) {
        this.pinDescription = pinDescription;
    }
}
