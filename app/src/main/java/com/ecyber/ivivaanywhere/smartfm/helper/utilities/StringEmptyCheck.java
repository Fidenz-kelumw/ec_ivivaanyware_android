package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

/**
 * Created by Chathura Jayanath on 7/13/2015.
 */
public class StringEmptyCheck {
    public StringEmptyCheck() {
    }

    public static boolean isNotNullNotEmptyNotWhiteSpaceOnly(String string)
    {
        return string != null && !string.isEmpty() && !string.trim().isEmpty();
    }
}
