package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;

import java.util.List;

/**
 * Created by Lakmal on 5/16/2016.
 */
public class ErrorLogAdapter extends RecyclerView.Adapter<ErrorLogAdapter.Holder> {

    private List<ErrorLog> mErrorLogList;
    private Context context;

    public ErrorLogAdapter(List<ErrorLog> errorLogList, Context context) {
        mErrorLogList = errorLogList;
        this.context = context;
    }

    public void addItem(ErrorLog error) {
        mErrorLogList.add(error);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_error_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ErrorLog error = mErrorLogList.get(position);
        holder.tvTitle.setText(error.getTitle());
        holder.tvErrorMsg.setText(error.getError());
        holder.tvDateTime.setText(error.getDate());
    }

    @Override
    public int getItemCount() {
        return mErrorLogList == null ? 0 : mErrorLogList.size();
    }

    public void clear() {
        mErrorLogList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView tvTitle, tvErrorMsg, tvDateTime;

        public Holder(View itemView) {
            super(itemView);
            tvTitle = (CustomTextView) itemView.findViewById(R.id.tv_error_title);
            tvDateTime = (CustomTextView) itemView.findViewById(R.id.tv_error_date);
            tvErrorMsg = (CustomTextView) itemView.findViewById(R.id.tv_error_msg);
        }
    }
}
