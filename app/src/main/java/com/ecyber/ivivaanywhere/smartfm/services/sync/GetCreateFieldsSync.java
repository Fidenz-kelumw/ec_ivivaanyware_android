package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Type;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InputModesModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueType;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetCreateFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class GetCreateFieldsSync {

    private GetCreateFieldsListener callback;

    public GetCreateFieldsSync(GetCreateFieldsListener event) {
        callback = event;
    }

    public void getCreateFields(String _url, String _apiKey, String _userKey, String _objectType, String _locationKey) {
        ServiceGenerator.CreateService(GetCreateFields.class, _url).getCreateFields(_apiKey, _userKey, _objectType, _locationKey)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                convertResponceAndSave(response.body());
                            } else {
                                callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
                            }
                        }else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Create Fields",message);
                                callback.GetCreateFieldsError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        callback.GetCreateFieldsError(SmartConstants.TRY_AGAIN);
                        ErrorLog.saveErrorLog("Get Objects", "System error");
                    }
                });
    }

    public void convertResponceAndSave(ResponseBody body) {

        InfoObject infoObject = new InfoObject();

        try {
            String jsonData = body.string();
            JSONObject objectResponce = new JSONObject(jsonData);

            JSONArray objectInfo = objectResponce.getJSONArray("CreateFields");

            //region objectInfo map section
            List<ObjectInfoModel> objectInfoModels = new ArrayList<>();

            if (objectInfo != null) {
                for (int i = 0; i < objectInfo.length(); i++) {
                    JSONObject object = objectInfo.getJSONObject(i);
                    ObjectInfoModel oInfoModel = new ObjectInfoModel();

                    oInfoModel.setFieldid(object.getString("FieldID"));
                    oInfoModel.setFieldname(object.getString("FieldName"));
                    oInfoModel.setValuetype(object.getString("ValueType"));

                    oInfoModel.setDisableDelete((object.has("DisableDelete") ? object.getString("DisableDelete") : "0"));
                    oInfoModel.setExtended((object.has("Extended") ? object.getString("Extended") : "0"));

                    JSONObject defData = object.getJSONObject("DefaultData");
                    if(defData != null && !defData.toString().equals("{}")) {
                        oInfoModel.setDisplaytext(defData.getString("DisplayText"));

                        // this to store value models, list will be added due to
                        // some objects are returning multiple objects
                        // sorry :) since this will create list if we have single value too
                        List<ValueModel> valueModels = new ArrayList<>();

                        switch (object.getString("ValueType")) {
                            case Type.SLT:
                            case Type.LABEL:
                            case Type.DS:
                            case Type.BUTTONS:
                            case Type.SAS:
                            case Type.MLT:
                            case Type.DT:
                            case Type.DAT:
                            case Type.FEEDBACK:
                                ValueModel valueModel = new ValueModel(object.getString("FieldID"),
                                        ValueType.SINGLE,
                                        defData.getString("Value"),
                                        null,
                                        null);

                                valueModels.add(valueModel);

                                oInfoModel.setValueModels(valueModels);
                                break;

                            case Type.SAAR:
                                JSONArray valueArray = defData.getJSONArray("Value");
                                if (valueArray != null) {
                                    for (int j = 0; j < valueArray.length(); j++) {
                                        ValueModel valueModelArray = new ValueModel(object.getString("FieldID"),
                                                ValueType.ARRAY,
                                                valueArray.getString(j),
                                                null,
                                                null);

                                        valueModels.add(valueModelArray);
                                    }

                                    oInfoModel.setValueModels(valueModels);
                                } else {
                                    oInfoModel.setValueModels(null);
                                }
                                break;
                            case Type.ATT:
                                try {
                                    JSONArray valueObjArray = defData.getJSONArray("Value");
                                    if (valueObjArray != null) {
                                        for (int j = 0; j < valueObjArray.length(); j++) {
                                            JSONObject valueObject = valueObjArray.getJSONObject(j);
                                            ValueModel valueModelArray = new ValueModel(object.getString("FieldID"),
                                                    ValueType.OBJECTARRAY,
                                                    null,
                                                    valueObject.getString("ATTName"),
                                                    valueObject.getString("ATTType"));

                                            valueModels.add(valueModelArray);
                                        }

                                        oInfoModel.setValueModels(valueModels);
                                    } else {
                                        oInfoModel.setValueModels(null);
                                    }
                                } catch (Exception e){
                                    List<ValueModel> valueModels1 = new ArrayList<>();
                                    oInfoModel.setValueModels(valueModels1);
                                }
                                break;
                        }
                    }
                    oInfoModel.setEditable(object.getString("Editable"));

                    if (object.has("OptionList")) {
                        JSONArray optionList = object.getJSONArray("OptionList");
                        List<OptionListModel> optionListModels = new ArrayList<>();

                        for (int j = 0; j < optionList.length(); j++) {
                            JSONObject option = optionList.getJSONObject(j);

                            OptionListModel listModel = new OptionListModel(object.getString("FieldID"),
                                    option.getString("DisplayText"),
                                    option.getString("Value"));

                            optionListModels.add(listModel);
                        }

                        oInfoModel.setOptionListModels(optionListModels);
                    }

                    if (object.has("LookUpService")) {
                        oInfoModel.setLookupservice(object.getString("LookUpService"));
                    }

                    if (object.has("Mandatory")) {
                        oInfoModel.setMandatory(object.getString("Mandatory"));
                    }

                    if (object.has("QRCode")) {
                        oInfoModel.setQrcode(object.getString("QRCode"));
                    }

                    if (object.has("Style")) {
                        oInfoModel.setStyle(object.getString("Style"));
                    }

                    if (object.has("DownloadFolder")) {
                        oInfoModel.setDownloadfolder(object.getString("DownloadFolder"));
                    }

                    if (object.has("UploadFolder")) {
                        oInfoModel.setUploadfolder(object.getString("UploadFolder"));
                    }

                    if (object.has("NoDataValue")) {
                        oInfoModel.setNoDataValue(object.getString("NoDataValue"));
                    }

                    if (object.has("InputModes")) {
                        JSONArray inputModes = object.getJSONArray("InputModes");
                        String[] inputModesArray = new String[inputModes.length()];
                        for (int j = 0; j < inputModes.length(); j++) {
                            InputModesModel model = new InputModesModel(object.getString("FieldID"),
                                    inputModes.getString(j));

                            inputModesArray[j] = inputModes.getString(j);
                        }
                        oInfoModel.setInputModes(inputModesArray);
                    }

                    objectInfoModels.add(oInfoModel);
                }
                infoObject.setmObjectInfo(objectInfoModels);
            } else {
                callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
            }
            //endregion

            callback.GetCreateFieldsCallBack(infoObject);

        } catch (IOException e) {
            e.printStackTrace();
            callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
        } catch (JSONException e) {
            e.printStackTrace();
            callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            callback.GetCreateFieldsError(SmartConstants.ITEM_NOT_FOUND);
        }

    }

    public interface GetCreateFieldsListener {
        void GetCreateFieldsCallBack(InfoObject createFields);

        void GetCreateFieldsError(String message);
    }
}
