package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.FieldTypeSAARAdapter;
import com.ecyber.ivivaanywhere.smartfm.customviews.LabelView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.AddAttachmentView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.DateTimeSelectView;
import com.ecyber.ivivaanywhere.smartfm.customviews.DropDownSelectView;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeButtons;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeSAAR;
import com.ecyber.ivivaanywhere.smartfm.customviews.MultiLineEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.customviews.SingleLineTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.customviews.SubmitButtonVew;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Type;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueType;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CreateObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.CreateObjectSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetCreateFieldsSync;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lakmal on 5/5/2016.
 */
public class CreateObjectActivity extends SmartFMBaseActivity implements AddAttachmentView.AddAttachmentCallback,
        CreateObjectSync.CreateObjectCallBackListener,
        MultiLineEditTextView.MultiLineEditTexListener,
        DropDownSelectView.DropDownSelectTextListener,
        GetCreateFieldsSync.GetCreateFieldsListener,
        SingleLineTextView.SingleLineTextListener,
        SubmitButtonVew.SubmitActionListener,
        DateTimeSelectView.DateTimeSelectListener, FieldTypeButtons.FieldTypeButtonCallback, FieldTypeSAAR.FieldTypeSAARCallback {

    private FieldTypeSAARAdapter saarAdapter;

    @BindView(R.id.img_btn_right)
    ImageButton btnRight;

    @BindView(R.id.lin_lay_create_pg_container)
    LinearLayout linLayCreateContainer;

    @BindView(R.id.tv_title)
    CustomTextView tvTitle;

    @BindView(R.id.progressBar2)
    ProgressBar mProgressBar;

    private ColoredSnackbar mColoredSnackBar;
    private SubmitButtonVew mSubmitButtonVew;
    private List<ObjectInfoModel> mObjectInfoRes;
    private MultiLineEditTextView mMultiLineEditTextView;
    private String mObjectType;
    private String mObjectKey;
    private ArrayList<FieldDataSerializable> mFieldData;
    private NetworkCheck mNetwork;
    private String mLocationKey;
    private String mFromWhere = "";
    private String imagePath = null;
    private AddAttachmentView mAddAttachVew;
    private AccountPermission accountPermission;
    private AddAttachmentView mAttchInstance;
    private int uploadingCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_fields);
        ButterKnife.bind(this);
        mColoredSnackBar = new ColoredSnackbar(this);
        btnRight.setVisibility(View.GONE);
        tvTitle.setText("Create");
        mObjectType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);
        mObjectKey = getIntent().getStringExtra(SmartConstants.OBJECT_KEY);
        mLocationKey = getIntent().getStringExtra(SmartConstants.LOCATION_KEY);
        mFromWhere = getIntent().getStringExtra(SmartConstants.FROM_WHERE_ACTIVITY);
        mFieldData = (ArrayList<FieldDataSerializable>) getIntent().getSerializableExtra(SmartConstants.CUSTOM_FILTER);
        mNetwork = new NetworkCheck();
        SmartConstants.selectedObjectInfo = new ArrayList<>();
        initData();
    }

    private void initData() {
        GetCreateFieldsSync createFieldsSync = new GetCreateFieldsSync(this);
        accountPermission = new AccountPermission();

        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            mProgressBar.setVisibility(View.VISIBLE);
            createFieldsSync.getCreateFields(
                    accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), mObjectType, mLocationKey);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void initFields(List<ObjectInfoModel> createFields) {
        int position = 0;
        int color = this.getResources().getColor(R.color.mdtp_white);
        Log.e("FM", "-------------------------------------------");
        for (ObjectInfoModel info : createFields) {
            String valueType = info.getValuetype();

            List<ValueModel> valueModels = info.getValueModels();
            String value = "";
            if (valueModels != null && valueModels.size() > 0)
                value = info.valueModels.get(0).getValue();

            String displayText = info.getDisplaytext();
            Log.e("FM", "Value = " + value + "|DText = " + displayText);

            for (FieldDataSerializable fieldDataSerializable : mFieldData) {
                if (info.getFieldid().equals(fieldDataSerializable.FieldID)) {
                    List<ValueModel> vModels = info.getValueModels();
                    if (vModels != null && vModels.size() > 0) {
                        info.setDisplaytext(fieldDataSerializable.DisplayText);
                        info.getValueModels().get(0).setValue(fieldDataSerializable.Value);
                    } else {
                        ValueModel valueModel = new ValueModel(info.getFieldid(),
                                ValueType.SINGLE,
                                fieldDataSerializable.Value,
                                null, null);
                        List<ValueModel> valueM = new ArrayList<>();
                        valueM.add(valueModel);
                        info.setValueModels(valueM);
                        info.setDisplaytext(fieldDataSerializable.DisplayText);
                    }
                }
            }
            switch (valueType) {
                case SmartConstants.VALUE_TYPE_SLT:// Create a EditText view if ValueType is Single List Type
                    View viewSLT = new SingleLineTextView(this.getBaseContext(), this, color).getSingleLineTextViewObjectInfo(info, position);
                    linLayCreateContainer.addView(viewSLT);
                    break;
                case SmartConstants.VALUE_TYPE_DT:// Create a Button view if ValueType is Date Time
                    View viewDT = new DateTimeSelectView(this.getBaseContext(), this.getFragmentManager(), this).getDateTimeView(info, false, position, color);
                    linLayCreateContainer.addView(viewDT);
                    break;
                case SmartConstants.VALUE_TYPE_DAT:// Create a Button view if ValueType is only Date
                    View view = new DateTimeSelectView(this.getApplicationContext(), this.getFragmentManager(), this).getDateTimeView(info, true, position, color);
                    linLayCreateContainer.addView(view);
                    break;
                case SmartConstants.VALUE_TYPE_DS:// Create a Button view if ValueType is Drop down select
                    View selectDialog = new DropDownSelectView(CreateObjectActivity.this, this, false, false, color).getSelectDialogObjectInfo(info, position, " Select " + info.getFieldname());
                    linLayCreateContainer.addView(selectDialog);
                    break;
                case SmartConstants.VALUE_TYPE_SAS:// Create a Button view if ValueType is Search and select
                    View viewSAS = new DropDownSelectView(CreateObjectActivity.this, this, true, false, color).getSearchAndSelectDialog(mObjectType, mObjectKey, info, position, "Search");
                    linLayCreateContainer.addView(viewSAS);
                    break;
                case SmartConstants.VALUE_TYPE_MLT:// Create a EditText view if ValueType is Multi line text
                    mMultiLineEditTextView = new MultiLineEditTextView(this.getApplicationContext(), this, color);
                    View viewMLT = mMultiLineEditTextView.getMultiLineEditView(info, position);
                    linLayCreateContainer.addView(viewMLT);
                    break;
                case SmartConstants.VALUE_TYPE_ATT:
                    mAddAttachVew = new AddAttachmentView(getBaseContext(), CreateObjectActivity.this, null, this, accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(2), accountPermission.getAccountInfo().get(1), false);
                    mAddAttachVew.setCreateObject();
                    View viewAddAttach = mAddAttachVew.getAddAttachView(info, position);
                    mAddAttachVew.setInstance(mAddAttachVew);

                    linLayCreateContainer.addView(viewAddAttach);
                    break;
                case SmartConstants.VALUE_TYPE_BUTTONS:
                    View viewButtons = new FieldTypeButtons(this, position, info, this).generateButtons();
                    linLayCreateContainer.addView(viewButtons);
                    break;
                case SmartConstants.VALUE_TYPE_SAAR:
                    createSAARView(position, info);
                    break;
                case SmartConstants.VALUE_TYPE_LABEL:
                    createLabelView(info, position);
                    break;
            }
            position++;
        }

        setSubmitButton();

        checkSubmitIsAvailable();
    }

    private void setSubmitButton() {
        mSubmitButtonVew = new SubmitButtonVew(this, this, null);
        View view = mSubmitButtonVew.getSubmitButtonView();
        linLayCreateContainer.addView(view);
    }

    private void createSAARView(int position, ObjectInfoModel info) {
        try {
            View view = new FieldTypeSAAR(this,
                    position,
                    info,
                    this).generateView();
            linLayCreateContainer.addView(view);
        } catch (Exception e) {
            Log.e("FM", e.getMessage());
        }
        return;
    }

    private void createLabelView(ObjectInfoModel info, int position){
        LabelView view = new LabelView(CreateObjectActivity.this);
        linLayCreateContainer.addView(view.getLabelView(info, position));
    }

    @Override
    public void OnSubmitClick(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            Log.e("WP", uploadingCount + "");
            if (uploadingCount > 0) {
                SnackbarManager.show(
                        com.nispok.snackbar.Snackbar.with(this)
                                .text("Please wait.Images are being uploaded.")
                                .actionLabel("Cancel")
                                .actionColor(this.getResources().getColor(R.color.mdtp_red))
                                .actionListener(snackbar -> snackbar.dismiss())
                        , this);
            } else {
                AccountPermission accountPermission = new AccountPermission();
                CreateObjectSync createObjectSync = new CreateObjectSync(this);

                List<FieldData> fieldDataList = new ArrayList<>();

                mColoredSnackBar.showSnackBar("Processing...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
                JSONArray fieldData = new JSONArray();

                for (ObjectInfoModel objectInfo : mObjectInfoRes) {
                    JSONObject field = new JSONObject();
                    try {
                        field.put("FieldID", objectInfo.getFieldid());
                        List<ValueModel> valueModels = objectInfo.getValueModels();

                        if (!objectInfo.getValuetype().equals(Type.ATT) && !objectInfo.getValuetype().equals(Type.SAAR)) {
                            field.put("Value", (valueModels != null && valueModels.size() > 0) ? valueModels.get(0).getValue() : "");
                        } else {
                            JSONArray array = new JSONArray();
                            if (objectInfo.getValuetype().equals(Type.SAAR)) {
                                for (int i = 0; i < valueModels.size(); i++) {
                                    array.put(valueModels.get(i).getValue());
                                }
                            } else {
                                if (valueModels != null && valueModels.size() > 0) {
                                    for (ValueModel model : valueModels) {
                                        JSONObject attObject = new JSONObject();

                                        attObject.put("ATTName", model.getATTName());
                                        attObject.put("ATTType", model.getATTType());

                                        array.put(attObject);
                                    }
                                }
                            }
                            field.put("Value", array);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    fieldData.put(field);
                }

                String dataValues = fieldData.toString();
                Log.d("FM", dataValues);
                createObjectSync.CreateObject(accountPermission.getAccountInfo().get(0),
                        accountPermission.getAccountInfo().get(1),
                        accountPermission.getAccountInfo().get(2), mObjectType, dataValues);
            }
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }

    }

    private String getDateISOFormat(String selectedDate) {
        String[] splitDate = selectedDate.split("/");

        String[] yer = splitDate[2].split(" ");
        String selected;
        if (yer.length > 1) {
            selected = yer[0] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + yer[1];
        } else {
            selected = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + "00:00" + ":00";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        String d = null;
        try {
            d = dateFormat.format(dateFormat.parse(selected));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    private void checkSubmitIsAvailable() {
        SmartConstants.selectedObjectInfo = mObjectInfoRes;
        for (ObjectInfoModel objectInfo : mObjectInfoRes) {

            String displayTxt = "";
            if (objectInfo.getDisplaytext() != null) {
                displayTxt = objectInfo.getDisplaytext().isEmpty() ? (
                        (objectInfo.getValueModels() != null && objectInfo.getValueModels().size() > 0) ?
                                objectInfo.getValueModels().get(0).getValue() : "") : objectInfo.getDisplaytext();
            }

            boolean isMandatory = objectInfo.getMandatory().equals("1") ? true : false;

            if (isMandatory && displayTxt.length() <= 0) {
                mSubmitButtonVew.setEnableButton(false);
                return;
            } else {
                mSubmitButtonVew.setEnableButton(true);
            }
        }
    }

    private void setObjectInfoDisplayText(int position, String displayText, String value) {
        mObjectInfoRes.get(position).setDisplaytext(displayText);
        if (mObjectInfoRes.get(position).getValueModels() != null && mObjectInfoRes.get(position).getValueModels().size() > 0) {
            mObjectInfoRes.get(position).getValueModels().get(0).setValue(value);
        } else {
            List<ValueModel> valueModels = new ArrayList<>();
            ValueModel valueModel = new ValueModel(mObjectInfoRes.get((position)).getFieldid(),
                    ValueType.SINGLE,
                    value,
                    null, null);
            valueModels.add(valueModel);
            mObjectInfoRes.get((position)).setValueModels(valueModels);
        }
        checkSubmitIsAvailable();
    }

    @OnClick(R.id.btn_action_left)
    public void onClickBackButton(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    @Override
    public void onSingleLineTextFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }

    @Override
    public void onDateTimeSelectTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, getDateISOFormat(displayText));
    }

    @Override
    public void onDropDownSelectFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, value);
    }

    @Override
    public void onMultiLineEditTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }

    @Override
    public void GetCreateFieldsCallBack(InfoObject createFields) {
        mProgressBar.setVisibility(View.GONE);
        mObjectInfoRes = createFields.mObjectInfo;
        SmartConstants.selectedObjectInfo = mObjectInfoRes;
        initFields(mObjectInfoRes);
    }

    @Override
    public void GetCreateFieldsError(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnCreateObjectSuccess(CreateObjectResponse response) {
        //mProgressBar.setVisibility(View.GONE);
        mColoredSnackBar.dismissSnacBar();
        String objectKey = response.getObjectKey();
        String detailsPage = response.getDetailsPage();
        String message = response.getMessage();
        int status = response.getSuccess();

        if (status == 1) {
            message = Utility.IsEmpty(message) ? SmartConstants.OBJ_CREATE_SUCCESS : message;
        } else {
            message = Utility.IsEmpty(message) ? SmartConstants.OBJ_CREATE_UN_SUCCESS : message;
        }

        String finalMessage = message;
        new Handler().postDelayed(() -> Toast.makeText(getApplicationContext(), finalMessage, Toast.LENGTH_LONG).show(), 200);

        if (detailsPage.equals("1")) {
            Intent intent = new Intent(getApplicationContext(), SideNavigationActivity.class);
            intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
            intent.putExtra(SmartConstants.OBJECT_KEY, objectKey);
            intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
            intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, mFromWhere);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
        }
    }

    @Override
    public void OnCreateObjectError(String message) {
        //mProgressBar.setVisibility(View.GONE);
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SmartConstants.RESULT_IMAGE_CAMERA) {
            if (RESULT_OK == resultCode) {
                if (imagePath != null) {
                    Bitmap TempViewBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imagePath), 1024, 768, false);
                    addAttchToList(imagePath, null, SmartConstants.ATTACHMENT_IMG, false);
                }

            }

        } else if (requestCode == SmartConstants.RESULT_IMAGE_GALLERY) {
            if (RESULT_OK == resultCode) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                addAttchToList(picturePath, selectedImage, SmartConstants.ATTACHMENT_IMG, false);
            }
        } else if (requestCode == SmartConstants.RESULT_VIDEO_CAPTURE) {
            if (SmartConstants.RESULT_OK == resultCode && mAddAttachVew != null) {
                Uri videoUri = data.getData();
                mAttchInstance.addVideo(videoUri);
            } else {
                Toast.makeText(CreateObjectActivity.this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == SmartConstants.RESULT_SKETCH) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Bundle b = data.getExtras();
                String filePath = b.getString(SmartConstants.SKETCH_IMAGE);
                addAttchToList(filePath, null, SmartConstants.ATTACHMENT_IMG, true);
            }
        }
    }

    @Override
    public void onCapturePath(String path, AddAttachmentView view) {
        imagePath = path;
        mAttchInstance = view;
    }

    @Override
    public void onUploadingStatus(int count) {
        uploadingCount = uploadingCount + (count);
    }

    @Override
    public void addFileToValue(String fileName, int position) {
        ObjectInfoModel model = mObjectInfoRes.get(position);

        String ext = fileName.split(Pattern.quote("."))[1];
        String type = "";

        switch (ext) {
            case "jpg": {
                type = "IMG";
                break;
            }

            case "mp4": {
                type = "AUD";
                break;
            }

            case "mov": {
                type = "VID";
                break;
            }
        }

        ValueModel att = new ValueModel(model.getFieldid(), ValueType.OBJECTARRAY, null, fileName, type);

        List<ValueModel> values = model.getValueModels();

        if (values == null) {
            values = new ArrayList<>();
        }

        values.add(att);
        mObjectInfoRes.get(position).setValueModels(values);
    }

    @Override
    public void onAttachedItemRemove(int position, int removeItemPosition, String guId) {

        ObjectInfoModel model = mObjectInfoRes.get(position);
        List<ValueModel> values = model.getValueModels();

        if (values != null) {
            for (int i = 0; i < values.size(); i++) {

                if (values.get(i).getATTName().equals(guId)) {
                    values.remove(i);
                    model.setValueModels(values);
                    mObjectInfoRes.add(position, model);

                    break;
                }

            }
        }
    }

    private int getRemoveItemIndex(List<String> filterList, String guId) {
        int i = 0;
        for (String item : filterList) {
            if (item.equals(guId)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private void addAttchToList(String picturePath, Uri uri, int type, boolean isFromSketch) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            mAddAttachVew.addAttachment(picturePath, uri, type, isFromSketch);// TODO: 20/10/2016 changed
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFieldTypeButtonStateChange(int objectPosition, String value, String displayText) {
        setObjectInfoDisplayText(objectPosition, displayText, value);
    }

    @Override
    public void onSAARAdapterGenerated(FieldTypeSAARAdapter adapter) {
        this.saarAdapter = adapter;
    }

    @Override
    public void onNewSAARAdded(String value, String name, int position) {
        mObjectInfoRes.get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.get(position).getValueModels();
        ValueModel valueModel = new ValueModel(mObjectInfoRes.get(position).getFieldid(),
                ValueType.ARRAY, value, null, null);

        if (valueModels != null) {
            valueModels.add(valueModel);
        } else {
            valueModels = new ArrayList<>();
            valueModels.add(valueModel);
        }

        mObjectInfoRes.get(position).setValueModels(valueModels);
    }

    @Override
    public void onSAARRemoved(String value, int position) {
        mObjectInfoRes.get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.get(position).getValueModels();
        for (int i = 0; i < valueModels.size(); i++) {
            if (valueModels.get(i).getValue().equals(value)) {
                valueModels.remove(i);
                break;
            }
        }

        mObjectInfoRes.get(position).setValueModels(valueModels);
    }

    @Override
    public void onSAARDialogItemAdded(String value, String name) {
        saarAdapter.addItem(value);
    }

    @Override
    public void onSAARDialogItemRemoved(String value, String name) {
        saarAdapter.removeItem(value);
    }
}