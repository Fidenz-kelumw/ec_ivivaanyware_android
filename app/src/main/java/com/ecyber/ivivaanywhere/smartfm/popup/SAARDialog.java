package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.SAARAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetObjectForQrResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectForQRSync;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import java.util.Collections;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Choota on 3/14/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SAARDialog implements SAARAdapter.SAARAdapterCallback, GetObjectForQRSync.GetObjectForQRCallback {
    private Context context;
    private SAARDialogCallback callback;
    private Dialog dialog;

    private ObjectInfoModel model;

    private CompoundBarcodeView qr;
    private ImageButton close, btnQr, btnBack;

    private CustomEditTextView search;
    private CustomTextView mTvMessage;
    private RecyclerView recyclerView;

    private LinearLayout listData;
    private RelativeLayout layQr;

    private List<OptionListModel> items;
    private List<OptionListModel> itemsTemp;
    private List<String> selectedItems;

    private boolean isOnlyQR;

    public SAARDialog(Context context, List<OptionListModel> items, List<String> selectedItems, ObjectInfoModel model, SAARDialogCallback callback) {
        this.context = context;
        this.items = items;
        this.selectedItems = selectedItems;
        this.callback = callback;
        this.model = model;

        this.itemsTemp = Collections.unmodifiableList(items);
    }

    public void clearItems() {
        if(items != null) {
            items= null;
        }
    }

    public void show(boolean isOnlyQR) {
        this.isOnlyQR = isOnlyQR;

        dialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_saar, null);

        close = (ImageButton) view.findViewById(R.id.close);
        btnQr = (ImageButton) view.findViewById(R.id.btnQr);
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);

        search = (CustomEditTextView) view.findViewById(R.id.search);
        mTvMessage = (CustomTextView) view.findViewById(R.id.tv_message);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        listData = (LinearLayout) view.findViewById(R.id.layDataContainer);
        layQr = (RelativeLayout) view.findViewById(R.id.QR_fragment_cam);

        qr = (CompoundBarcodeView) view.findViewById(R.id.zxing_barcode_scanner);


        if (model.getQrcode() != null && model.getQrcode().equals("1"))
            btnQr.setVisibility(View.VISIBLE);
        else
            btnQr.setVisibility(View.GONE);

        close.setOnClickListener(view1 -> dialog.dismiss());

        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        final SAARAdapter adapter = new SAARAdapter(context,
                items,
                selectedItems,
                this);

        recyclerView.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        qr.decodeContinuous(qrCallback);
        btnQr.setOnClickListener(view12 -> {
            search.setText("");

            mTvMessage.setVisibility(View.GONE);
            btnQr.setVisibility(View.INVISIBLE);
            btnBack.setVisibility(View.VISIBLE);

            layQr.setVisibility(View.VISIBLE);
            listData.setVisibility(View.INVISIBLE);

            try {
                qr.resume();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnBack.setOnClickListener(view13 -> {
            btnQr.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.INVISIBLE);

            layQr.setVisibility(View.INVISIBLE);
            listData.setVisibility(View.VISIBLE);
            try {
                qr.pause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        if (isOnlyQR)
            setIsOnlyQR();

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 60;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);
        y = converter.dipToPixels(context, dpHeight);

        if(items == null) {
            mTvMessage.setVisibility(View.VISIBLE);
            mTvMessage.setText("Could not fetch data.");
        }

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout((int) x, (int) y);
        dialog.show();
    }

    public void setIsOnlyQR() {
        search.setText("");

        mTvMessage.setVisibility(View.GONE);
        btnQr.setVisibility(View.INVISIBLE);
        btnBack.setVisibility(View.GONE);

        layQr.setVisibility(View.VISIBLE);
        listData.setVisibility(View.INVISIBLE);

        try {
            qr.resume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BarcodeCallback qrCallback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            try {
                qr.pause();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (result.getText() != null && result.getBarcodeFormat().toString().equals("QR_CODE")) {
                getObjectForQr(result.getText());
            } else {
                qr.resume();
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };

    private void getObjectForQr(String data) {
        boolean state = NetworkCheck.IsAvailableNetwork(context);

        Style style = new Style.Builder()
                .setTextColor(R.color.pure_white)
                .setBackgroundColor(R.color.update)
                .build();

        Crouton.makeText((Activity) context, "Processing QR code", style, layQr).show();

        Log.e("SMartFM", data.toString());
        if (state) {
            AccountPermission accountPermission = new AccountPermission();
            new GetObjectForQRSync(this).getObjectForQR(
                    accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), data, model.getLookupservice(), SmartConstants.OS);
        } else {
            new ColoredSnackbar(context).showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            qr.resume();
        }
    }

    @Override
    public void onSAARAdapterItemChecked(String value, String data) {
        callback.onSAARItemChecked(value, data);
    }

    @Override
    public void onSAARAdapterItemCheckRemoved(String value, String data) {
        callback.onSAARItemCheckRemoved(value, data);
    }

    @Override
    public void getObjectForQRSuccess(GetObjectForQrResponse response) {
        if (!response.mMessage.isEmpty()) {
            Crouton.cancelAllCroutons();
            Crouton.makeText((Activity) context, "QR not supported", Style.ALERT, layQr).show();
            qr.resume();
            return;
        }

        String value = response.mObjectKey;
        String displayText = response.mObjectID;

        callback.onSAARItemChecked(value, displayText);

        if (!isOnlyQR) {
            btnQr.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.INVISIBLE);

            layQr.setVisibility(View.INVISIBLE);
            listData.setVisibility(View.VISIBLE);

            try {
                qr.pause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            qr.pause();
        }

        Log.e("SmartFM", displayText + "|" + value);
    }

    @Override
    public void getObjectForQRError(String message) {
        if (qr != null) {
            Crouton.makeText((Activity) context, "QR not supported", Style.ALERT, layQr).show();
            try {
                qr.resume();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface SAARDialogCallback {
        void onSAARItemChecked(String value, String data);

        void onSAARItemCheckRemoved(String value, String data);
    }
}
