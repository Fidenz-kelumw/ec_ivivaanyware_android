package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/3/2016.
 */
public class DataResponse {

    @SerializedName("Data")
    private List<DataLookUp> mDataLookUps;

    public List<DataLookUp> getDataLookUps() {
        return mDataLookUps;
    }



}
