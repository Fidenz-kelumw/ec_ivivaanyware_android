package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by Lakmal on 5/3/2016.
 */
public class SubmitButtonVew implements View.OnClickListener {
    private Activity mActivity;
    private CustomButton mBtnSubmit;
    private View mView;
    private boolean isDisable;
    private SubmitActionListener mSubmitActionListener;
    private String customText;

    public SubmitButtonVew(Activity activity, SubmitActionListener submitActionListener, String customText) {
        mActivity = activity;
        mSubmitActionListener = submitActionListener;
        this.customText = customText;
        initView();
    }

    private void initView() {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_submit_button, null);
        mBtnSubmit = (CustomButton) mView.findViewById(R.id.btn_submit);
        mBtnSubmit.setText((customText != null && !customText.isEmpty()) ? customText : "Submit");
        mBtnSubmit.setOnClickListener(this);
    }

    public void setEnableButton(boolean disable) {
        mBtnSubmit.setEnabled(disable);
        isDisable = false;
        if (!disable) {
            //mBtnSubmit.setBackgroundResource(R.drawable.button_rounded_disable_info);
            mBtnSubmit.setEnabled(false);
            isDisable = true;
        } else {
            //mBtnSubmit.setBackgroundResource(R.drawable.button_rounded_red);
            mBtnSubmit.setEnabled(true);
        }
    }

    public boolean isDisable() {
        return isDisable;
    }

    public void setIsDisable(boolean isDisable) {
        this.isDisable = isDisable;
    }

    public View getSubmitButtonView() {
        return mView;
    }

    @Override
    public void onClick(View v) {
        mSubmitActionListener.OnSubmitClick(v);
    }

    public interface SubmitActionListener {
        void OnSubmitClick(View v);
    }
}
