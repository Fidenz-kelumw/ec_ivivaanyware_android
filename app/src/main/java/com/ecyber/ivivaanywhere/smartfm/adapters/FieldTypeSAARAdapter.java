package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class FieldTypeSAARAdapter extends RecyclerView.Adapter<FieldTypeSAARAdapter.Holder> {

    private Context context;
    private UpdateSAARCallback callback;

    private ObjectInfoModel info;
    private List<OptionListModel> optionListModels;
    private List<String> selectedMembers;
    private String isEditable;

    public FieldTypeSAARAdapter(Context context, ObjectInfoModel info, List<String> selectedMembers, UpdateSAARCallback callback, String isEditable) {
        this.context = context;
        this.callback = callback;
        this.selectedMembers = selectedMembers;
        this.info = info;
        this.optionListModels = info.getOptionListModels();
        this.isEditable = (isEditable != null ? isEditable : "0");
    }

    public void updateOptionList(List<OptionListModel> newData, List<String> selectedMembers) {
        this.info.setOptionListModels(newData);
        // this.optionListModels = newData;
        this.selectedMembers = selectedMembers;
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        System.out.println(position);
        String removedValue = selectedMembers.get(position);

        selectedMembers.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, selectedMembers.size());

        callback.onSAARRemovedFromList(position, removedValue);
        System.out.println("Position : " + position + " " + Arrays.toString(selectedMembers.toArray()));
    }

    public void addItem(String value) {
        selectedMembers.add(value);

        Set<String> removeDuplicates = new LinkedHashSet<>();
        removeDuplicates.addAll(selectedMembers);

        System.out.println(Arrays.toString(removeDuplicates.toArray()));
        selectedMembers.clear();
        selectedMembers.addAll(removeDuplicates);

        System.out.println(Arrays.toString(selectedMembers.toArray()));

        notifyItemInserted(selectedMembers.size());
        notifyItemRangeChanged(0, selectedMembers.size());

        callback.onNewSAARAddedToList("", value, selectedMembers.size() - 1);
    }

    public void removeItem(String value) {
        String removedValue = "";
        int indexRemoved = -1;

        for (int i = 0; i < selectedMembers.size(); i++) {
            if (selectedMembers.get(i).equals(value)) {
                removedValue = selectedMembers.get(i);
                indexRemoved = i;
                selectedMembers.remove(i);
                break;
            }
        }

        if (!removedValue.equals("") && indexRemoved != -1) {
            notifyItemRemoved(indexRemoved);
            notifyItemRangeChanged(indexRemoved, selectedMembers.size());

            callback.onSAARRemovedFromList(indexRemoved, removedValue);
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_saar_selected_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        String value = selectedMembers.get(position);
        boolean isValueFound = false;

        holder.name.setText("");

        for (OptionListModel optionListModel : optionListModels) {
            if (value.equals(optionListModel.getValue())) {
                isValueFound = true;
                holder.name.setText("" + optionListModel.getDisplayText());
                break;
            }
        }

        if (!isValueFound)
            holder.name.setText(value);

        holder.close.setOnClickListener(view -> {
            if (isEditable.equals("1"))
                removeItem(position);
        });
    }

    @Override
    public int getItemCount() {
        return selectedMembers != null ? selectedMembers.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView name;
        ImageButton close;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.supervisorName);
            close = (ImageButton) itemView.findViewById(R.id.close);
        }
    }

    public interface UpdateSAARCallback {
        void onNewSAARAddedToList(String name, String value, int position);

        void onSAARRemovedFromList(int position, String value);
    }
}
