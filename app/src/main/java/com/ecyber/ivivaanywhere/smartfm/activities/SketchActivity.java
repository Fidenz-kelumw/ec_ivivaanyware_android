package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.SignatureView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SketchActivity extends AppCompatActivity {

    @BindView(R.id.sketch_btn_black)
    Button mColorBlack;
    @BindView(R.id.sketch_btn_white)
    Button mColorWhite;
    @BindView(R.id.sketch_btn_red)
    Button mColorRed;
    @BindView(R.id.sketch_btn_yellow)
    Button mColorYellow;
    @BindView(R.id.sketch_btn_blue)
    Button mColorBlue;
    @BindView(R.id.sketch_btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.sketch_btn_save)
    Button mBtnSave;
    @BindView(R.id.sketch_btn_clear)
    Button mBtnClear;
    @BindView(R.id.sketch_image)
    SignatureView mSketchImage;

    private Activity mActivity;
    private Bitmap mBitmap;
    private boolean mSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sketch);
        ButterKnife.bind(this);

        String filePath = (String) getIntent().getExtras().get(SmartConstants.SKETCH_IMAGE);

        mActivity = this;
        onClickBlack(null);

        try {
            mBitmap = Bitmap.createBitmap(BitmapFactory.decodeFile(filePath));
            mSketchImage.initWithBitmap(mBitmap);
        } catch(Exception e) {
            mBtnClear.setEnabled(false);
            ImageLoader.getInstance().loadImage(filePath, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    mBtnClear.setEnabled(true);
                    mBitmap = loadedImage;
                    mSketchImage.initWithBitmap(mBitmap);
                    mSketchImage.clearSignature();
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        if(mSuccess) {
            String tempPath = Utility.createNewPath();

            Utility.createdTempImageFile(mSketchImage.getImage(), tempPath);
            Intent intent = new Intent();
            intent.putExtra(SmartConstants.SKETCH_IMAGE, tempPath);
            setResult(SmartConstants.RESULT_OK, intent);
        } else {
            Intent intent = new Intent();
            intent.putExtra(SmartConstants.SKETCH_IMAGE, "NO_IMAGE");
            setResult(SmartConstants.RESULT_CANCEL, intent);
        }

        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_display_bottom);
    }

    @Override
    public void finish() {
        super.finish();
    }

    @OnClick(R.id.sketch_btn_clear)
    public void onClickClear(View view) {
        mSketchImage.clearSignature();
    }

    @OnClick(R.id.sketch_btn_save)
    public void onClickSave(View view) {

        mSuccess = true;
        onBackPressed();
    }

    @OnClick(R.id.sketch_btn_cancel)
    public void onClickCancel(View view) {

        mSuccess = false;
        onBackPressed();
    }

    @OnClick(R.id.sketch_btn_black)
    public void onClickBlack(View view) {
        mColorBlack.setBackgroundResource(R.drawable.shape_sketch_color_black_selected);
        mColorBlue.setBackgroundResource(R.drawable.shape_sketch_color_blue);
        mColorWhite.setBackgroundResource(R.drawable.shape_sketch_color_white);
        mColorRed.setBackgroundResource(R.drawable.shape_sketch_color_red);
        mColorYellow.setBackgroundResource(R.drawable.shape_sketch_color_yellow);

        mSketchImage.setSigColor(Color.BLACK);
    }

    @OnClick(R.id.sketch_btn_blue)
    public void onClickBlue(View view) {
        mColorBlack.setBackgroundResource(R.drawable.shape_sketch_color_black);
        mColorBlue.setBackgroundResource(R.drawable.shape_sketch_color_blue_selected);
        mColorWhite.setBackgroundResource(R.drawable.shape_sketch_color_white);
        mColorRed.setBackgroundResource(R.drawable.shape_sketch_color_red);
        mColorYellow.setBackgroundResource(R.drawable.shape_sketch_color_yellow);

        mSketchImage.setSigColor(Color.BLUE);
    }

    @OnClick(R.id.sketch_btn_yellow)
    public void onClickYellow(View view) {
        mColorBlack.setBackgroundResource(R.drawable.shape_sketch_color_black);
        mColorBlue.setBackgroundResource(R.drawable.shape_sketch_color_blue);
        mColorWhite.setBackgroundResource(R.drawable.shape_sketch_color_white);
        mColorRed.setBackgroundResource(R.drawable.shape_sketch_color_red);
        mColorYellow.setBackgroundResource(R.drawable.shape_sketch_color_yellow_selected);

        mSketchImage.setSigColor(Color.YELLOW);
    }

    @OnClick(R.id.sketch_btn_red)
    public void onClickRed(View view) {
        mColorBlack.setBackgroundResource(R.drawable.shape_sketch_color_black);
        mColorBlue.setBackgroundResource(R.drawable.shape_sketch_color_blue);
        mColorWhite.setBackgroundResource(R.drawable.shape_sketch_color_white);
        mColorRed.setBackgroundResource(R.drawable.shape_sketch_color_red_selected);
        mColorYellow.setBackgroundResource(R.drawable.shape_sketch_color_yellow);

        mSketchImage.setSigColor(Color.RED);
    }

    @OnClick(R.id.sketch_btn_white)
    public void onClickWhite(View view) {
        mColorBlack.setBackgroundResource(R.drawable.shape_sketch_color_black);
        mColorBlue.setBackgroundResource(R.drawable.shape_sketch_color_blue);
        mColorWhite.setBackgroundResource(R.drawable.shape_sketch_color_white_selected);
        mColorRed.setBackgroundResource(R.drawable.shape_sketch_color_red);
        mColorYellow.setBackgroundResource(R.drawable.shape_sketch_color_yellow);

        mSketchImage.setSigColor(Color.WHITE);
    }
}
