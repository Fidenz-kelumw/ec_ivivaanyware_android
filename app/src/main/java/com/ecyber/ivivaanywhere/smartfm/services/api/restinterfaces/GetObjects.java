package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ObjectsResponce;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public interface GetObjects {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjects")
    Call<ObjectsResponce> getObjects(@Field("ObjectType") String ObjectType,
                                     @Field("UserKey") String UserKey,
                                     @Field("LocationKey") String LocationKey,
                                     @Field("FilterID") String FilterID,
                                     @Field("CustomFilter") String CustomFilter,
                                     @Field("More") String More,
                                     @Field("apikey") String apikey);
}
