package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectOptionList;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class SubObjectFilterAdapter extends RecyclerView.Adapter<SubObjectFilterAdapter.Holder>{

    private List<SubObjectOptionList> items;
    private Context context;

    public SubObjectFilterAdapter(List<SubObjectOptionList> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_single_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        SubObjectOptionList list = items.get(position);
        holder.name.setText(list.getDisplayText());
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_single_item_value);
        }
    }
}
