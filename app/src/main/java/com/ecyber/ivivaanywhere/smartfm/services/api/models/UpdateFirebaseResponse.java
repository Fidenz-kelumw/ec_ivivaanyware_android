package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diharaw on 3/22/17.
 */

public class UpdateFirebaseResponse {
    @SerializedName("Success")
    @Expose
    public int mSuccess;

    @SerializedName("Message")
    @Expose
    public String mMessage;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
