package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class ObjectsAdapter extends RecyclerView.Adapter<ObjectsAdapter.Holder> {

    private List<Objects> items;
    private List<Objects> orig;

    private Context context;
    private List<Stages> stages;
    private ObjectClickListner mObjectClickListner;
    private String mObjectType;

    public ObjectsAdapter(List<Objects> items, Context context, ObjectClickListner objectClickListner, String objectType) {
        this.items = items;
        this.mObjectType = objectType;
        this.context = context;
        this.stages = new Stages().selectByObjectType(mObjectType);
        this.mObjectClickListner = objectClickListner;
    }

    public void addItem(Objects objects) {
        items.add(objects);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<Objects> results = new ArrayList<>();
                if (orig == null)
                    orig = items;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final Objects g : orig) {
                            if (g.getDescrition().toLowerCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                items = (List<Objects>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_object_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Objects objects = items.get(position);
        holder.name.setText(objects.getObjectID());
        holder.description.setText(objects.mDescription);

        boolean isColorSet = false;
        for (int i = 0; i < stages.size(); i++) {
            if (stages.get(i).Stage != null) {
                if (items.get(position).getStage().equals(stages.get(i).Stage)) {
                    GradientDrawable gd = new GradientDrawable();
                    gd.setColor(Color.parseColor(stages.get(i).Color));
                    gd.setCornerRadii(new float[]{5, 5, 0, 0, 0, 0, 5, 5});
                    holder.layLeft.setBackground(gd);
                    isColorSet = true;
                    break;
                }
            }
        }

        if (!isColorSet) {
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(Color.parseColor("#FFFFFF"));
            gd.setCornerRadii(new float[]{5, 5, 0, 0, 0, 0, 5, 5});
            holder.layLeft.setBackground(gd);
        }

    }

    @Override
    public int getItemCount() {
        return null != items ? items.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView name, description;
        RelativeLayout layLeft;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tv_filter_name);
            description = (CustomTextView) itemView.findViewById(R.id.tv_filter_description);
            layLeft = (RelativeLayout) itemView.findViewById(R.id.lay_indicator);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mObjectClickListner.ObjectClick(items.get(this.getAdapterPosition()));
        }
    }


    public interface ObjectClickListner {
        void ObjectClick(Objects object);
    }
}
