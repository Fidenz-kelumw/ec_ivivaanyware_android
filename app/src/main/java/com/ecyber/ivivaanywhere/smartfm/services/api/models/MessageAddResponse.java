package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/9/2016.
 */
public class MessageAddResponse {
    @SerializedName("Success")
    public int mSuccess;

    @SerializedName("Message")
    public String mMessage;

    @SerializedName("MessageKey")
    public String mMessageKey;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessageKey() {
        return mMessageKey;
    }

    public void setMessageKey(String messageKey) {
        mMessageKey = messageKey;
    }
}
