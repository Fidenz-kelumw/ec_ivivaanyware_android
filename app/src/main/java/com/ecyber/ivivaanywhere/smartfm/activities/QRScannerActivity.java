package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetObjectForQrResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectForQRSync;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QRScannerActivity extends SmartFMBaseActivity implements GetObjectForQRSync.GetObjectForQRCallback {

    @BindView(R.id.zxing_barcode_scanner)
    CompoundBarcodeView barcodeView;

    @BindView(R.id.QR_btn_close)
    CustomButton QRBtnClose;

    String mObjectType = null;
    String mObjectKey = null;

    private ColoredSnackbar mColoredSnackBar;
    private boolean isObjectScanner = false;
    private boolean hasScan = false;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            barcodeView.pause();
            if (result.getText() != null && result.getBarcodeFormat().toString().equals("QR_CODE")) {
                System.out.println(result.getText());
                if (isObjectScanner) {
                    boolean isTrue = onQRScannedObject(result.getText());
                    if (!isTrue) {
                        reStartQR();
                    }
                } else {
                    System.out.println("is scanned user");
                    onQRScannedUser(result.getText());
                }

            } else {
                reStartQR();
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };
    private boolean isCallAPI = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);
        ButterKnife.bind(this);

        isObjectScanner = getIntent().getExtras().getBoolean(SmartConstants.IS_OBJECT_SCANNER);

        if (isObjectScanner){
            QRBtnClose.setBackgroundResource(R.drawable.ic_keyboard_arrow_left_36dp);
            QRBtnClose.setText("");
        } else {
            QRBtnClose.setBackgroundResource(0);
            QRBtnClose.setText("Close");
        }

        mColoredSnackBar = new ColoredSnackbar(this);
        setup_spinner();
        barcodeView.decodeContinuous(callback);
    }

    @OnClick(R.id.QR_btn_close)
    public void onClickBack(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isObjectScanner)
            overridePendingTransition(R.anim.keep_active, R.anim.slide_display_top);
        else
            overridePendingTransition(R.anim.keep_active, R.anim.slide_display_bottom);
    }

    @Override
    protected void onResume() {
        super.onResume();
        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hasScan = false;
        barcodeView.pause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        setResult(RegistrationActivity.PICK_BACK_PRESS);
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    private void reStartQR() {
        hasScan = true;
        try {
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (hasScan) {
                    barcodeView.resume();
                }
            }, 2000);
        } catch (Exception e) {

        }
    }

    private boolean onQRScannedObject(String data) {

// new change according to eutech, remove reg checking

//        if (matchRegX(data)) {
//            invokeToObjectInfo();
//            return true;
//        } else {
//            boolean available = checkObjectTypeIsAvailable(data);
//            if (available) {
//                boolean state = NetworkCheck.IsAvailableNetwork(this);
//                if (state) {
//                    AccountPermission accountPermission = new AccountPermission();
//                    new GetObjectForQRSync(this).getObjectForQR(
//                            accountPermission.getAccountInfo().get(0),
//                            accountPermission.getAccountInfo().get(1),
//                            accountPermission.getAccountInfo().get(2), data, "", SmartConstants.OS);
//                }
//                return true;
//            } else
//                return false;
//        }

        mColoredSnackBar.showSnackBar("Processing QR Code...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
        boolean state = NetworkCheck.IsAvailableNetwork(this);
        if (state) {
            AccountPermission accountPermission = new AccountPermission();
            new GetObjectForQRSync(this).getObjectForQR(
                    accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), data, "", SmartConstants.OS);
        }
        return true;
    }

    public void onQRScannedUser(String data) {
        //{"QRType":"UserRegistration", "Account":"fmdev.ivivacloud.com", "ApiKey":"SC:fmdev:6fc66b50f4f636a2", "DomainID":"emaar", "UserID":"admin", "RegistrationCode":"1234"}
        try {
            JSONObject object = new JSONObject(data);
            String qrType = object.has("QRType") ? object.getString("QRType") : "";
            if (qrType.equalsIgnoreCase("UserRegistration")) {
                String userID = object.has("UserID") ? object.getString("UserID") : "";
                String domainID = object.has("DomainID") ? object.getString("DomainID") : "";
                String regCode = object.has("RegistrationCode") ? object.getString("RegistrationCode") : "";
                String account = object.has("Account") ? object.getString("Account") : "";
                String apiKey = object.has("ApiKey") ? object.getString("ApiKey") : "";
                String ssl = object.has("ssl") ? object.getString("ssl") : "";
                // String ssl = object.has("SSL") ? object.getString("SSL") : "";

                Intent intent = new Intent();
                intent.putExtra(SmartConstants.USER_ID, userID);
                intent.putExtra(SmartConstants.DOMAIN_ID, domainID);
                intent.putExtra(SmartConstants.REG_CODE, regCode);
                intent.putExtra(SmartSharedPreferences.ACCOUNT, account);
                intent.putExtra(SmartSharedPreferences.API_KEY, apiKey);
                intent.putExtra(SmartSharedPreferences.SSL_KEY, ssl);
                setResult(RegistrationActivity.PICK_QR_REQUEST, intent);
                barcodeView.pause();
                finish();
            } else {
                reStartQR();
            }
        } catch (Exception e) {
            reStartQR();
        }
    }

    private boolean matchRegX(String qrVal) {
        List<ObjectType> allObjectTypes = new ObjectType().getAllObjectTypes();
        boolean result = false;
        for (ObjectType objectType : allObjectTypes) {
            Pattern regPattern = Pattern.compile(objectType.getQRFormatAndroid());
            Matcher matcher = regPattern.matcher(qrVal);
            if (matcher.find()) {
                mObjectType = objectType.getObjectType();
                mObjectKey = matcher.group(1);
                return true;
            } else {
                result = false;
            }
        }
        return result;
    }

    private boolean checkObjectTypeIsAvailable(String qrVal) {
        boolean returnVal = false;
        String sObjectType = "";
        String[] split = qrVal.split(",");
        for (String s : split) {
            if (s.contains("ObjectType")) {
                String[] split1 = s.split("=");
                sObjectType = split1[1];
            }
        }

        List<ObjectType> allObjectTypes = new ObjectType().getAllObjectTypes();
        for (ObjectType objectType : allObjectTypes) {
            if (sObjectType.equals(objectType.getObjectType()))
                return true;
            else
                returnVal = false;
        }

        return returnVal;
    }

    private void setup_spinner() {
        try {
            ImageView view = (ImageView) findViewById(R.id.QR_img_spinner);
            Animation a = AnimationUtils.loadAnimation(this, R.anim.anim_spinner);
            a.setDuration(1000);
            a.setInterpolator(new Interpolator() {
                private final int frameCount = 60;

                @Override
                public float getInterpolation(float input) {
                    return (float) Math.floor(input * frameCount) / frameCount;
                }
            });

            view.startAnimation(a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invokeToObjectInfo() {
        finish();
        Intent intent = new Intent(getApplicationContext(), SideNavigationActivity.class);
        intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
        intent.putExtra(SmartConstants.OBJECT_KEY, mObjectKey);
        intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    @Override
    public void getObjectForQRSuccess(GetObjectForQrResponse response) {

        mColoredSnackBar.dismissSnacBar();
        if (response != null && response.mObjectKey != null && response.mObjectType != null) {

            List<ObjectType> objectTypes = new ObjectType().getAllObjectTypes();

            for (ObjectType type : objectTypes) {
                if (type.getObjectType().equals(response.mObjectType)) {
                    mObjectKey = response.mObjectKey;
                    mObjectType = response.mObjectType;
                    invokeToObjectInfo();
                    break;
                }
            }

            mColoredSnackBar.showSnackBar(((response.mMessage != null && !response.mMessage.isEmpty()) ? response.mMessage : "QR not supported"), ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
            reStartQR();

        } else {
            mColoredSnackBar.showSnackBar(((response.mMessage != null && !response.mMessage.isEmpty()) ? response.mMessage : "QR not supported"), ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
            reStartQR();
        }
    }

    @Override
    public void getObjectForQRError(String message) {
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(((message != null && !message.isEmpty()) ? message : "QR not supported"), ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
        reStartQR();
    }
}
