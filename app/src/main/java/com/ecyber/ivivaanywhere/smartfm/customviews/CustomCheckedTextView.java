package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by Lakitha on 2/24/16.
 */
public class CustomCheckedTextView extends android.support.v7.widget.AppCompatCheckedTextView {

    Context context;

    public CustomCheckedTextView(Context _context, AttributeSet attrs, int defStyle) {
        super(_context, attrs, defStyle);

        context = _context;
        init(attrs);
    }

    public CustomCheckedTextView(Context _context, AttributeSet attrs) {
        super(_context, attrs);
        context = _context;
        init(attrs);

    }

    public CustomCheckedTextView(Context _context) {
        super(_context);
        context = _context;
        init(null);
    }

    private void init(AttributeSet attrs) {
        //if (attrs!=null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomCheckedTextView);
            String fontName = "MelbourneRegularBasic.otf";//a.getString(R.styleable.CustomTextView_fontName);
            if (fontName!=null) {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        //}
    }

}

