package com.ecyber.ivivaanywhere.smartfm.services.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;

/**
 * Created by Choota on 1/26/18.
 */

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (new SmartSharedPreferences(context).isLocationSyncEnabled()) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                Intent pushIntent = new Intent(context, BackgroundLocationService.class);
                context.startService(pushIntent);
            }
        }
    }
}