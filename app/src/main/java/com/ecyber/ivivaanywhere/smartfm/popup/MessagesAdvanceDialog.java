package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.models.MessageType;

import java.util.List;

/**
 * Created by Choota on 3/8/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class MessagesAdvanceDialog implements MessageTypeSelectDialog.MessageTypeSelectCallback {
    private Context context;
    private MessageAdvanceCallback callback;
    private MessageTypeSelectDialog messageTypeSelectDialog;

    private CustomButton btnAdd;
    private CustomEditTextView message;
    private CustomButton btnSelectType;
    private ImageButton btnClose;

    private String selectedMessageType;
    private List<MessageType> messageTypes;

    public MessagesAdvanceDialog(Context context, MessageAdvanceCallback callback) {
        this.context = context;
        this.callback = callback;
        
        this.messageTypeSelectDialog = new MessageTypeSelectDialog(context, this);
        this.selectedMessageType = "-";
    }

    // TODO: 3/8/17 get message types
    public void showAdvanceMessageDialog(final List<MessageType> messageTypes) {

        this.messageTypes = messageTypes;

        final Dialog dialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_message_advance, null);

        btnAdd = (CustomButton) view.findViewById(R.id.btnAdd);
        message = (CustomEditTextView) view.findViewById(R.id.message);
        btnSelectType = (CustomButton) view.findViewById(R.id.btnSelectType);
        btnClose = (ImageButton) view.findViewById(R.id.btnClose);

        btnAdd.setEnabled(false);
        btnAdd.setAlpha(0.8f);

        if(messageTypes!=null && messageTypes.size()>0){
            btnSelectType.setText(messageTypes.get(0).getTypeName());
            selectedMessageType = messageTypes.get(0).getTypeID();
        }

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);

        btnClose.setOnClickListener(view1 -> dialog.dismiss());
        
        btnSelectType.setOnClickListener(view12 -> messageTypeSelectDialog.showTypeSelect(messageTypes));

        btnAdd.setOnClickListener(view13 -> {
            callback.onAdvanceMessageSend(message.getText().toString(), selectedMessageType);
            dialog.dismiss();
        });

        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String s = charSequence.toString();
                if(!s.trim().equals("") && s.trim().length()>0){
                    btnAdd.setEnabled(true);
                    btnAdd.setAlpha(1.0f);
                } else {
                    btnAdd.setEnabled(false);
                    btnAdd.setAlpha(0.8f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setLayout((int) x, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    @Override
    public void onMessageTypeSelect(String typeId) {
        selectedMessageType = (typeId != null ? typeId:"-");
        for (MessageType messageType : messageTypes) {
            if(messageType.getTypeID().endsWith(typeId)){
                btnSelectType.setText(messageType.getTypeName());
                break;
            }
        }
    }

    public interface MessageAdvanceCallback {
        void onAdvanceMessageSend(String message, String messageType);
    }
}
