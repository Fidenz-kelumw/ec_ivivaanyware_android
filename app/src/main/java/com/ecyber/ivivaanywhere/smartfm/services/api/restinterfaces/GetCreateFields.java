package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;


import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/4/2016.
 */
public interface GetCreateFields {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetCreateFields")
    Call<ResponseBody> getCreateFields(@Field("apikey") String apiKey,
                                       @Field("UserKey") String userKey,
                                       @Field("ObjectType") String objectType,
                                       @Field("LocationKey") String locationKey);
}
