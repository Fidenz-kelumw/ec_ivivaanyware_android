package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.HomeObjectButton;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.CreateFromMenu;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.popup.AppInstallRequestDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.BuildingsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.DefaultSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.FiltersSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectAttachmentsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.MasterDataSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectTypesSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.RegistrationSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateUserGPSCoordinatesSync;
import com.ecyber.ivivaanywhere.smartfm.services.system.LocationHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends SmartFMBaseActivity implements MasterDataSync.MasterDataSyncCallback, HomeObjectButton.HomeButtonClickListener, RegistrationSync.ValidateRegistrationCallback, BuildingsSync.GetBuildingCallBack, DefaultSync.DefaultSyncCallback, ObjectTypesSync.GetObjectCallBack, FiltersSync.GetFiltersCallBack, LocationHelper.LocationHelperCallback {

    @BindView(R.id.profile_image)
    CircleImageView profileImage;

    @BindView(R.id.tv_user_name)
    CustomTextView userName;

    @BindView(R.id.tvApplicationName)
    CustomTextView tvApplicationName;

    @BindView(R.id.lay_profile_pic_holder)
    RelativeLayout profilePicHolder;

    @BindView(R.id.lin_home_btn_container)
    LinearLayout mBtnContainer;

    @BindView(R.id.btn_inbox)
    ImageButton btnInbox;

    private ColoredSnackbar mColoredSnackBar;
    private NetworkCheck mNetwork;
    private DataDownloadOptimization mDataDownloadOptimization;
    private MasterDataSync mMasterDataSync;
    private static boolean isForeground;
    private ProgressDialog progressDialog;
    private String[] insideSpots;

    private boolean isGPSNotificationUsed;


    CImageLoader loader;
    NotificationReceiver notificationReceiver;
    AccountPermission accountPermission;
    static BadgeView badge;

    String locReqKey = "0";
    String oType = "";
    String oKey = "";
    long msgId = 0;


    //region Lifecycle Callbacks
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loader = new CImageLoader(this);
        accountPermission = new AccountPermission();
        badge = new BadgeView(this, btnInbox);
        badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        mColoredSnackBar = new ColoredSnackbar(this);
        mDataDownloadOptimization = new DataDownloadOptimization();
        mMasterDataSync = new MasterDataSync(this, MainActivity.this);
        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("Receiving Configuration Data");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(true);

        oType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);
        oKey = getIntent().getStringExtra(SmartConstants.OBJECT_KEY);
        locReqKey = getIntent().getStringExtra(SmartConstants.LOCATION_REQ_KEY);
        msgId = getIntent().getLongExtra(SmartConstants.MESSAGE_ID, 0);

        isGPSNotificationUsed = false;

        if (locReqKey != null && locReqKey.equals("1"))
            this.forceUserLocationToServer();

        try {
            if (getIntent().getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS) != null) {
                insideSpots = getIntent().getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS);
                callToObject(getIntent().getStringExtra(SmartConstants.OBJECT_TYPE), null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isFreshUser = (getIntent().getStringExtra(SmartConstants.ISLOGINNOW) != null && getIntent().getStringExtra(SmartConstants.ISLOGINNOW).equals("YES"));

        if (isFreshUser) {
            try {
                String name = getIntent().getStringExtra(SmartConstants.LOGIN_USER_NAME);
                Toast.makeText(this, "Welcome " + name, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            loadProfileInfo();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        oType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);
        oKey = getIntent().getStringExtra(SmartConstants.OBJECT_KEY);
        locReqKey = getIntent().getStringExtra(SmartConstants.LOCATION_REQ_KEY);

        tvApplicationName.setText(new SmartSharedPreferences(MainActivity.this).getApplicationName());

        if (locReqKey != null && locReqKey.equals("1"))
            this.forceUserLocationToServer();

        try {
            if (getIntent().getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS) != null && !isGPSNotificationUsed)
                insideSpots = getIntent().getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        isValidateUser();
        populateScreen();
        firebaseSubscribe();
        fireNotification();

        isForeground = true;
        int unreadMessageCount = new Notifications().getUnreadMessageCount();
        int count = Integer.parseInt(badge.getText().toString().isEmpty() ? "0" : badge.getText().toString());
        int def = unreadMessageCount - count;
        badge.increment(def);
        badge.show();
        if (def > 0)
            badge.show();
        if (unreadMessageCount == 0)
            badge.hide();

        registerNotificationReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isForeground = false;
        unRegisterNotificationReceiver();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 6) {
            Intent intent = new Intent(this, RegistrationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> MainActivity.this.finish())
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    //endregion

    private void forceUserLocationToServer() {
        LocationHelper.init(MainActivity.this, this);
        if (LocationHelper.isLocationEnabled()) {
            LocationHelper.requestlocation(true);
        }
    }

    private void downloadMasterData() {
        try {
            if (NetworkCheck.IsAvailableNetwork(this)) {
                loadProfileInfo();

                if (mMasterDataSync.getMasterData(false) && progressDialog != null && !progressDialog.isShowing())
                    progressDialog.show();

                removeDiskCacheBeforeWeek();
                fireNotification();
            }
        } catch (Exception e) {

        }
    }

    private void fireNotification() {

        if ((!Utility.IsEmpty(oType)) && (!Utility.IsEmpty(oKey))) {

            Intent launchActivity = new Intent(this, SideNavigationActivity.class);
            launchActivity.putExtra(SmartConstants.OBJECT_TYPE, oType);
            launchActivity.putExtra(SmartConstants.OBJECT_KEY, oKey);
            launchActivity.putExtra(SmartConstants.MESSAGE_ID, msgId);
            launchActivity.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
            launchActivity.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
            launchActivity.putExtra(SmartConstants.NEAR_LOCATION_SPOTS, insideSpots);

            startActivity(launchActivity);

            oType = "";
            oKey = "";

            getIntent().removeExtra(SmartConstants.OBJECT_TYPE);
            getIntent().removeExtra(SmartConstants.OBJECT_KEY);
            getIntent().removeExtra(SmartConstants.LOCATION_REQ_KEY);

            insideSpots = new String[0];
            isGPSNotificationUsed = true;
        }
    }

    private void firebaseSubscribe() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("FM", token != null ? token : "");
        //FirebaseMessaging.getInstance().subscribeToTopic("Test");
    }

    private void removeDiskCacheBeforeWeek() {
        new GetObjectAttachmentsSync(this).RemoveDiskCacheBeforeWeek();
    }

    private void isValidateUser() {
        if (mDataDownloadOptimization.isDataAvailableInTable(ObjectType.class)) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.OBJECT_TYPES, true)) {
                if (NetworkCheck.IsAvailableNetwork(this))
                    new RegistrationSync(this).GetValidateRegistration(accountPermission.getAccountInfo().get(0),
                            accountPermission.getAccountInfo().get(1),
                            accountPermission.getAccountInfo().get(2));
            } else {
                if (NetworkCheck.IsAvailableNetwork(this))
                    downloadMasterData();
            }
        } else {
            if (NetworkCheck.IsAvailableNetwork(this))
                downloadMasterData();
        }
    }

    /**
     * increment inbox badge number
     */
    public void badgeIncrement() {
        if (isForeground) {
            int size = Integer.parseInt(Registration.getInboxSize().InboxSize);
            int currentBadge = Integer.parseInt(badge.getText().toString().trim().isEmpty() ? "0" : badge.getText().toString().trim());
            if (currentBadge >= size)
                return;

            badge.increment(1);
            badge.show();

        }
    }

    private void callToObject(String type, ObjectType objType) {

        switch (isAppOrBrowser(objType)) {
            case SmartConstants.MENU_APP:
                String app = objType.MenuLink;

                if (app != null && !app.equals("")) {
                    if (appInstalledOrNot(app)) {
                        launchThirdPartyApplication(app);
                    } else {
                        AppInstallRequestDialog.init(MainActivity.this).show(app, appPackageNameResolver(app));
                    }
                } else {
                    mColoredSnackBar.showSnackBar("Invalid application name", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
                }

                break;
            case SmartConstants.MENU_BROWSER:
                String url = objType.URL;

                if (url != null && !url.equals("")) {
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);

                } else {
                    mColoredSnackBar.showSnackBar("Invalid URL found", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
                }

                break;
            case SmartConstants.MENU_OBJECT:

                ObjectType objectType = new Select().from(ObjectType.class).where("ObjectType=?", type).executeSingle();

                if (objectType != null) {
                    String filterPagePage = objectType.getFilterListPage();

                    if (objectType.getMovetoDetailPage() != null && objectType.getMovetoDetailPage().equals("1")) {

                        Intent intent = new Intent(this, SideNavigationActivity.class);
                        intent.putExtra(SmartConstants.OBJECT_TYPE, objectType.getObjectType());
                        intent.putExtra(SmartConstants.OBJECT_KEY, "");
                        intent.putExtra(SmartConstants.NEAR_LOCATION_SPOTS, insideSpots);
                        intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
                        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_MAIN_ACTIVITY);
                        startActivity(intent);
                        insideSpots = new String[0];
                        isGPSNotificationUsed = true;

                        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                    } else {
                        if (filterPagePage != null && filterPagePage.equals("1")) {
                            Intent intent = new Intent(this, FilterActivity.class);
                            intent.putExtra(SmartConstants.OBJECT_TYPE, type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                        } else {
                            ObjectsSync.foreExpireObjectList();
                            Intent intent = new Intent(this, WorkOrderListActivity.class);
                            intent.putExtra(SmartConstants.OBJECT_TYPE, type);
                            intent.putExtra(SmartConstants.CUSTOM_FILTER, "");
                            intent.putExtra(SmartConstants.MORE, "");
                            intent.putExtra(SmartConstants.PAGE, "filter");
                            intent.putExtra(SmartConstants.LOCATION_NAME, "");
                            intent.putExtra(SmartConstants.LOCATION_KEY, "");
                            intent.putExtra(SmartConstants.SHOW_ASSETS_HOME, true);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                        }
                    }
                } else {
                    mColoredSnackBar.showSnackBar("No object found", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                }

                break;
        }
    }

    private String isAppOrBrowser(ObjectType ob) {
        if (ob != null && ob.MenuType != null && (ob.MenuType.equals(SmartConstants.MENU_APP) || ob.MenuType.equals(SmartConstants.MENU_BROWSER)))
            if (ob.MenuType.equals(SmartConstants.MENU_APP))
                return SmartConstants.MENU_APP;
            else if (ob.MenuType.equals(SmartConstants.MENU_BROWSER))
                return SmartConstants.MENU_BROWSER;
            else
                return SmartConstants.MENU_OBJECT;
        else
            return SmartConstants.MENU_OBJECT;
    }

    private String appPackageNameResolver(String appName) {
        return SmartConstants.APP_BASE.get(appName);
    }

    private boolean appInstalledOrNot(String appName) {

        String packageName = appPackageNameResolver(appName);

        if (packageName != null) {
            PackageManager pm = getPackageManager();
            try {
                pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
            } catch (Exception e) {
            }
        }

        return false;
    }

    private void launchThirdPartyApplication(String appName) {

        String packageName = appPackageNameResolver(appName);

        if (packageName != null) {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packageName);
            if (launchIntent != null)
                startActivity(launchIntent);
            else
                AppInstallRequestDialog.init(MainActivity.this).show(appName, packageName);
        } else {
            AppInstallRequestDialog.init(MainActivity.this).show(appName, packageName);
        }
    }

    private void loadProfileInfo() {
        //getProfilePicHolderHeight();
        List<Registration> registrations = new Registration().getAllRegistrations();
        userName.setText(registrations.get(0).UserName);
        if (DomainData.getSsl() != null && !DomainData.getSsl().equals("")) {
            String checkSsl = DomainData.getSsl();
            if (checkSsl.equals("1")) {
                loader.DisplayImage("https://" + registrations.get(0).Account + registrations.get(0).ImagePath, profileImage);
            } else {
                loader.DisplayImage("http://" + registrations.get(0).Account + registrations.get(0).ImagePath, profileImage);
            }
        }
    }

    private void populateScreen() {
        if (((LinearLayout) mBtnContainer).getChildCount() > 0)
            mBtnContainer.removeAllViews();

        tvApplicationName.setText(new SmartSharedPreferences(MainActivity.this).getApplicationName());
        List<ObjectType> allObjectTypes = new ObjectType().getAllObjectTypes();
        if (allObjectTypes != null) {

            for (ObjectType objectType : allObjectTypes) {
                addObjectMenuButton(objectType);
            }
        }
    }

    private void addObjectMenuButton(ObjectType objectType) {
        String url = "";
        if (objectType != null) {
            String menuName = "";

            if (Utility.IsEmpty(objectType.MenuName)) {
                return;
            }

            menuName = objectType.MenuName;
            String objectIconPath = objectType.ObjectIconPath;


            String checkSsl = DomainData.getSsl();
            if (checkSsl != null && !checkSsl.equals("")) {
                if (checkSsl.equals("1")) {
                    url = "https://" + accountPermission.getAccountInfo().get(0) + objectIconPath;
                } else {
                    url = "http://" + accountPermission.getAccountInfo().get(0) + objectIconPath;
                }


                List<CreateFromMenu> createFromMenus = new CreateFromMenu().selectByObjectType(objectType.getObjectType());
                String createPage = objectType.getCreatePage();
                String oType = objectType.getObjectType();
                boolean show = false;
                if (createPage.equals("1")) {
                    show = true;
                }
                View view = new HomeObjectButton(MainActivity.this, this, createFromMenus, objectType).getHomeObjectButton(url, menuName, objectType.getObjectType(), show);

                if (view != null) {
                    mBtnContainer.addView(view);
                }
            }
        }
    }

    //region Sync Callbacks
    @Override
    public void onMasterDataSyncComplete() {
        progressDialog.hide();
        populateScreen();
    }

    @Override
    public void onMasterDataSyncFailed(String error) {
        progressDialog.hide();
    }

    @Override
    public void onValidateUserSuccess(String status) {
        switch (status) {
            case RegistrationSync.REG_SUCCESS:
                if (NetworkCheck.IsAvailableNetwork(this))
                    downloadMasterData();
                break;
            case RegistrationSync.REG_UN_SUCCESS:
                RegistrationSync.userUnregister(this);
                Intent intent = new Intent(this, RegistrationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
                finish();
                break;
        }
    }

    @Override
    public void onValidateUserError(String message) {
        if (NetworkCheck.IsAvailableNetwork(this))
            downloadMasterData();
    }
    //endregion

    //region UI Events
    @Nullable
    @OnClick(R.id.btn_action_left)
    public void onClickSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_toplayer_display, R.anim.keep_active);
    }

    @OnClick(R.id.btn_qr_scanner)
    public void onClickQRScanner(View view) {
        Intent intent = new Intent(this, QRScannerActivity.class);
        intent.putExtra(SmartConstants.IS_OBJECT_SCANNER, true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_toplayer_display, R.anim.keep_active);
    }

    @OnClick(R.id.btn_inbox)
    public void onClickInbox(View v) {
        Intent intent = new Intent(this, InboxActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    @Override
    public void onClickPlusButton(List<CreateFromMenu> createFromMenus, String objectType) {

        try {
            boolean state = mNetwork.NetworkConnectionCheck(this);
            if (state) {
                Intent intent = new Intent(getApplicationContext(), CreateObjectActivity.class);
                intent.putExtra(SmartConstants.OBJECT_TYPE, objectType);
                Buildings defaultLocation = new Registration().getDefaultLocation();
                String locationKey = "";
                String locationName = "";
                if (defaultLocation != null) {
                    locationKey = defaultLocation.LocationKey == "" ? "" : defaultLocation.LocationKey;
                    locationName = defaultLocation.LocationName == "" ? "" : defaultLocation.LocationName;
                }

                intent.putExtra(SmartConstants.LOCATION_KEY, locationKey);
                intent.putExtra(SmartConstants.LOCATION_NAME, locationName);

                ArrayList<FieldDataSerializable> fieldDataList = new ArrayList<FieldDataSerializable>();
                for (CreateFromMenu createFromMenu : createFromMenus) {
                    FieldDataSerializable field = new FieldDataSerializable(createFromMenu.mFieldID, createFromMenu.mValue, createFromMenu.mDisplayText);
                    fieldDataList.add(field);
                }
                intent.putExtra(SmartConstants.CUSTOM_FILTER, fieldDataList);
                intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
            } else {
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void homeButtonClick(String buttonName, ObjectType objectType) {
        callToObject(buttonName, objectType);
    }

    @Override
    public void onLocationFound(boolean status, double lon, double lat, Location location) {
        if (status && location != null) {
            String latX, lanX;

            latX = String.valueOf(lat);
            lanX = String.valueOf(lon);

            new UpdateUserGPSCoordinatesSync(this).update(new UpdateUserGPSCoordinatesSync.UpdateUserGPSCoordinateCallback() {
                @Override
                public void onUpdateUserGPSCoordinateSuccess() {
                    Log.i("UpdateUserGPSCoordinate", "Successfully updated");
                }

                @Override
                public void onUpdateUserGPSCoordinateFail(String message) {
                    Log.i("UpdateUserGPSCoordinate", "Failed to updated");
                }
            }, latX, lanX);
        }
    }
    //endregion

    //region Notification Receiver
    class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            badgeIncrement();
        }
    }

    private void registerNotificationReceiver() {
        IntentFilter filter = new IntentFilter("NotifyCatch");
        notificationReceiver = new NotificationReceiver();
        registerReceiver(notificationReceiver, filter);
    }

    private void unRegisterNotificationReceiver() {
        unregisterReceiver(notificationReceiver);
    }
    //endregion

    //region Legacy
    private void masterDataLoading_OLD() {
        loadProfileInfo();
        loadObjectTypes();
        loadFilters();
        loadBuildings();
        loadDefaults();
        removeDiskCacheBeforeWeek();
        fireNotification();
        // TODO: 07/09/16 some times doesn't increment badge when coming app is background after app get from recent app list get to foreground and click that notification but doesnt increment badge count
    }

    @Override
    public void onDefaultDataSuccess(DefaultResponse defaults) {
        Log.d("SmartFm", defaults.mDefaults.getLocationKey());
        loadProfileInfo();
    }

    @Override
    public void onDefaultDataError(String message) {
        System.out.println(message);
    }

    @Override
    public void onBuildingsDownload(Buildings buildings) {

    }

    @Override
    public void onBuildingsDownloadComplete() {

    }

    @Override
    public void onBuildingsDownloadError(String error) {

    }

    @Override
    public void onObjectTypesDownload(ObjectType objectType) {
        addObjectMenuButton(objectType);
    }

    @Override
    public void onObjectTypesDownloadComplete() {
        System.out.println("object downloaded");
    }

    @Override
    public void onObjectTypesDownloadError(String error) {
        System.out.println(error);
    }

    @Override
    public void onFiltersDownload(Filter filter) {
    }

    @Override
    public void onFiltersDownloadComplete() {
        System.out.println("filters downloaded");
    }

    @Override
    public void onFiltersDownloadError(String error) {
        System.out.println(error);
    }

    private void loadObjectTypes() {

        ObjectTypesSync objectTypesSync = new ObjectTypesSync(this);
        if (((LinearLayout) mBtnContainer).getChildCount() > 0)
            mBtnContainer.removeAllViews();

        if (mDataDownloadOptimization.isDataAvailableInTable(ObjectType.class)) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.OBJECT_TYPES, true)) {
                if (NetworkCheck.IsAvailableNetwork(this))
                    objectTypesSync.getObjectTypes(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
            } else {
                List<ObjectType> allObjectTypes = new ObjectType().getAllObjectTypes();
                if (allObjectTypes != null) {

                    for (ObjectType objectType : allObjectTypes) {
                        addObjectMenuButton(objectType);
                    }

                } else {
                    if (NetworkCheck.IsAvailableNetwork(this))
                        objectTypesSync.getObjectTypes(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
                }

            }
        } else {
            if (NetworkCheck.IsAvailableNetwork(this))
                objectTypesSync.getObjectTypes(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
        }
    }

    private void loadBuildings() {
        BuildingsSync buildingsSync = new BuildingsSync(this);
        if (mDataDownloadOptimization.isDataAvailableInTable(Buildings.class)) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.BUILDINGS, true)) {
                buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), false);
            }
        } else {
            buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), false);
        }
    }

    private void loadFilters() {
        FiltersSync filtersSync = new FiltersSync(this);

        if (mDataDownloadOptimization.isDataAvailableInTable(Filter.class)) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.FILTERS, true)) {
                if (NetworkCheck.IsAvailableNetwork(this))
                    filtersSync.getFilters(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
            }
        } else {
            if (NetworkCheck.IsAvailableNetwork(this))
                filtersSync.getFilters(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
        }
    }

    private void loadDefaults() {
        Registration registration = new Select().from(Registration.class).executeSingle();
        if (registration.MasterDataExpiryHours != null) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.DEFAULT, true)) {
                if (NetworkCheck.IsAvailableNetwork(this))
                    getDefaultDataFromCloud();
            }
        } else {
            if (NetworkCheck.IsAvailableNetwork(this))
                getDefaultDataFromCloud();
        }
    }

    private void getDefaultDataFromCloud() {
        DefaultSync defaultSync = new DefaultSync(this, MainActivity.this);
        Log.e("FM", "Domain- " + accountPermission.getAccountInfo().get(0));
        Log.e("FM", "ApiKey- " + accountPermission.getAccountInfo().get(1));
        Log.e("FM", "UserKey- " + accountPermission.getAccountInfo().get(2));
        defaultSync.getDefaultData(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
    }

    private void getProfilePicHolderHeight() {
        ViewTreeObserver viewTreeObserver = profilePicHolder.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                profilePicHolder.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams params = profileImage.getLayoutParams();
                params.height = profilePicHolder.getMeasuredHeight();
                params.width = profilePicHolder.getMeasuredHeight();
                profileImage.setLayoutParams(params);
            }
        });
    }
    //endregion
}