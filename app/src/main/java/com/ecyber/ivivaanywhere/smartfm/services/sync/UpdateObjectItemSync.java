package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class UpdateObjectItemSync {
    private Context context;
    private UpdateItemCallback callback;

    public UpdateObjectItemSync(Context context) {
        this.context = context;
    }

    public void updateItem(final UpdateItemCallback callback, String url, String objecttype, String objectkey, String tabname, String userkey, String itemkey, String itemdata, String apikey) {
        this.callback = callback;
        ServiceGenerator.CreateService(com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.UpdateObjectItem.class, url)
                .updateObjectItem(objectkey, objecttype, userkey, tabname, itemkey, itemdata, apikey)
                .enqueue(new Callback<SuccessResponse>() {
                    @Override
                    public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getSuccess().equals("1"))
                                callback.onUpdateItemSuccess();
                            else
                                callback.onUpdateItemFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                        } else
                            callback.onUpdateItemFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }

                    @Override
                    public void onFailure(Call<SuccessResponse> call, Throwable t) {
                        callback.onUpdateItemFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                });
    }

    public interface UpdateItemCallback {
        void onUpdateItemSuccess();

        void onUpdateItemFail(String message);
    }
}
