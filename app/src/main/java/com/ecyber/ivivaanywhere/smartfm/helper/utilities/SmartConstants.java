package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/5/16.
 */
public class SmartConstants {

    public static final String MENU_APP = "App";
    public static final String MENU_BROWSER = "Browser";
    public static final String MENU_OBJECT = "Object";

    public static final String APP_NOT_INSTALLED = "not installed in this device. Press install to download application from Google Play Store.";
    public static final String APP_NOT_FOUND_ON_DEFAULTS = "is not included in supported applications.";
    public static final HashMap<String, String> APP_BASE = new HashMap<String, String>() {
        {
            put("Condeco", "com.condecosoftware.roombooking");
            put("Smart WP", "com.ecyber.ivivaanywhere.smartoffice");
            put("HeadCount", "com.ecyber.ivivaanywhere.headcount");
        }
    };

    public static final String OS = "Android";
    public static final String GET_DOMAIN_URL = "domains.fxelle.com";
    public static final String API_PATH = "/OI/SmartFMAPI-2/";
    public static final String LOOKUPSERVICE = "LookUpService";

    public static final String errorMessage = "Something went wrong, please try again.";

    public static final String MENU_NAME = "Menu_Name";
    public static final String FILTER_NAME = "Filter_Name";
    public static final String OBJECT_TYPE = "Object_type";
    public static final String OBJECT_KEY = "Object_Key";
    public static final String MSG_ID = "Message_Id";
    public static final String TAB_NAME = "tab_name";

    public static final String USER_ID = "user_id";
    public static final String REG_CODE = "reg_code";
    public static final String DOMAIN_ID = "domain_name";

    public static final String FORCE_RELOAD = "force_reload";
    public static final String LOCATION_NAME = "location_name";
    public static final String MEMBERS = "Members";
    public static final String USERS = "Users";
    public static final String CHECKLIST = "CheckList";
    public static final String INFO = "InfoObject";
    public static final String OBJECT_INFO = "ObjectInfo";
    public static final String OPTION_LIST = "OptionList";
    public static final String PAGE_LINKS = "PageLinks";
    public static final String FIELD_DATA = "FieldData";
    public static final String ACTIONS = "Actions";
    public static final String ALL = "All";
    public static final String BUILDINGS = "Buildings";
    public static final String OBJECT_TYPES = "ObjectTypes";
    public static final String FILTERS = "Filters";
    public static final String MESSAGES = "Messages";
    public static final String OBJECTITEMS = "ObjectItems";
    public static final String OBJECTITEMSBUTTONS = "ObjectItemsButtons";
    public static final String SUBOBJECTS = "SubObjects";
    public static final String OBJECTURLS = "ObjectURLs";
    public static final String DEFAULT = "Default";
    public static final String MASTER_SYNC = "MasterSync";
    public static final String SPOTSITEM = "SpotsItem";


    public static final String VALUE_TYPE_SLT = "SLT";
    public static final String VALUE_TYPE_DT = "DT";
    public static final String VALUE_TYPE_DS = "DS";
    public static final String VALUE_TYPE_SAS = "SAS";
    public static final String VALUE_TYPE_DAT = "DAT";
    public static final String VALUE_TYPE_MLT = "MLT";
    public static final String VALUE_TYPE_ATT = "ATT";
    public static final String VALUE_TYPE_BUTTONS = "BUTTONS";
    public static final String VALUE_TYPE_FEEDBACK = "FEEDBACK";
    public static final String VALUE_TYPE_SAAR = "SAAR";
    public static final String VALUE_TYPE_LABEL = "LABEL";

    public static final String FEEDBACK_STYLE_SMILEY = "SMILEY";
    public static final String FEEDBACK_STYLE_STARS = "STARS";
    public static final String FEEDBACK_STYLE_THUMBS = "THUMBS";
    public static final String FEEDBACK_STYLE_FACES = "FACES";

    public static final String LINK_TYPE_DETAILS = "Details";
    public static final String LINK_TYPE_LIST = "List";
    public static final String LINK_TYPE_CREATE = "Create";
    public static final String NO_ITEMS_FOUND = "No Itmes found.";
    public static final String LOCATION_KEY = "locationkey";
    public static final String PG_ACTION_REFRESH = "Refresh";
    public static final String PG_ACTION_OBJECT_LIST = "ObjectList";

    public static final String PG_ACTION_HOME = "Home";
    public static final String CUSTOM_FILTER = "customfilter";
    public static final String ATTACHMENTS = "Attachments";
    public static final String ATTACHMENTS_MASTER = "AttachmentMaster";
    public static final String OBJECT_LAYOUT = "Object_Layout";
    public static final String OBJECT_LIST = "ObjectList";
    public static final String SHOW_ASSETS_HOME = "ShowHomeAssets";
    public static final int RESULT_IMAGE_GALLERY = 1;
    public static final int RESULT_IMAGE_CAMERA = 2;
    public static final int RESULT_VIDEO_CAPTURE = 3;
    public static final int RESULT_SKETCH = 4;
    public static final int RESULT_OK = -1;
    public static final int RESULT_CANCEL = 0;
    public static final String NO_INTERNET_CONNECTION = "No connection";
    public static final String FILE_SAVE_LOCATION = "SmartFM";
    public static final java.lang.String ACTIVITY_NAME = "activity_name";
    public static final String FILTER_ID = "filterid";

    public static final String MESSAGE_ID = "message_id";
    public static final String LOCATION_REQ_KEY = "location_key";
    public static final String ITEM_NOT_FOUND = "No attachments found";
    public static final String ITEM_NOT_AVAILABLE = "No Items Found";
    public static final String TRY_AGAIN = "Sorry, this request cannot be processed right now. Please Try again.";
    public static final String TRY_AGAIN_EXCEPTION = "Sorry, this request is failed. Please Try again.";
    public static final String NO_MESSAGE = "No items found";
    public static final String PAGE = "page";
    public static final String MORE = "more";

    public static final String TYPE_WORK_ORDER = "WorkOrder";
    public static final String TYPE_ASSET = "Asset";
    public static final String TYPE_INCIDENT = "Incident";
    public static final String TYPE_INVENTORY = "Inventory";
    public static final String SHOW_HOME = "show_home";
    public static final String RELATED_WORK_ORDER = "related_work_order";
    public static final String DOMAIN_ID_FAILED = "Incorrect domain ID";
    public static final String REGISTRATION_FAILED = "Failed to Register";
    public static final String FORWARD_FAILED = "Forward is not successful.please try again";
    public static final String ADD_ATTACH_FAILED = "Add attachment failed.";
    public static final String DESCRIPTION_UPDATE_FAILED = "Description update failed";
    public static final String DEAD_LINE_UPDATE_FAILED = "Deadline update failed";
    public static final String STATUS_UPDATE_FAILED = "Status update failed";
    public static final String EXECUTE_FAILED = "Execute failed";
    public static final String SUBMIT_FAILED = "Submit failed,please try again";
    public static final String SUBMIT_SUCCESS = "Successfully updated";
    public static final String MESSAGE_SENT_FAILED = "Message sending failed";
    public static final String DROP_PIN_UPDATE_FAILED = "Drop pin update failed, Please try again";
    public static final String LAYOUT_NOT_FOUND = "Layout details not found";
    public static final java.lang.String CACHE_DIR_ATTACHMETNS = "SmartFM_Attachments";

    public static List<ObjectInfoModel> selectedObjectInfo;

    public static final java.lang.String IS_OBJECT_SCANNER = "isObjectScanner";

    public static final String DOWNLOAD_FAILED = "Retry,Data Download failed.";
    public static final String FROM_WHERE_ACTIVITY = "From_Where";
    public static final String FROM_FILTER_ACTIVITY = "filter_activity";
    public static final String FROM_ORDER_ACTIVITY = "order_activity";
    public static final String FROM_GPS_ACTIVITY = "gps_activity";
    public static final String FROM_INVALID_ACTIVITY = "INVALID_ACTIVITY";
    public static final String FROM_MAIN_ACTIVITY = "MAIN_ACTIVITY";

    public static final int SUBMIT_BUTTON = -1000;
    public static final String REFRESH_LIST = "Request failed. Please pull down to refresh.";

    public static final String PLEASE_FILL_FIELDS = "Please fill all fields to continue";
    public static final String QR_NOT_SUPPORT = "QR code not supported.";
    public static final String OBJ_CREATE_SUCCESS = "Successfully Created";
    public static final String OBJ_CREATE_UN_SUCCESS = "Unsuccessfully Created";
    public static final String MESSAGE_NOT_FOUND = "No messages to display";
    public static final String QR_FAILED = "Failed to identify QR code";
    public static final String QR_SCANNED_SUCCESS = "QR scanned successfully.";

    public static final String SMALL = "Small";
    public static final String MEDIUM = "Medium";
    public static final String LARGE = "Large";
    public static final String ORIGINAL = "Original";

    public static final int ATTACHMENT_IMG = 0;
    public static final int ATTACHMENT_VID = 1;
    public static final int ATTACHMENT_AUD = 2;

    public static final String SKETCH_IMAGE = "SKETCH_IMAGE";
    public static final String SKETCH_IMAGE_REMOTE = "SKETCH_IMAGE_REMOTE";
    public static final String SKETCH_IMAGE_OPENED = "SKETCH_IMAGE_OPENED";

    public static final String NEAR_LOCATION_SPOTS = "NEAR_LOCATION_SPOTS";
    public static final String NEAR_LOCATION_BROADCAST = "NEAR_LOCATION_BROADCAST";

    public static final String VIDEO_URL = "VIDEO_URL";
    public static final String LOGIN_USER_NAME = "LOGIN_USER_NAME";
    public static final String ISLOGINNOW = "ISLOGINNOW";
}
