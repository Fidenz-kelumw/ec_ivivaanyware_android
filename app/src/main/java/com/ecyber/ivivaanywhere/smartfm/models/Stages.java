package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
@Table(name = "Stages")
public class Stages extends Model{

    @Column(name = "Stage")
    @SerializedName("Stage")
    @Expose
    public String Stage;

    @Column(name = "Color")
    @SerializedName("Color")
    @Expose
    public String Color;

    @Column(name = "ObjectType")
    public String ObjectType;

    public Stages(String stage, String color, String objectType) {
        super();
        Stage = stage;
        Color = color;
        ObjectType = objectType;
    }

    public Stages() {
        super();
    }

    public String getStage() {
        return Stage;
    }

    public void setStage(String stage) {
        Stage = stage;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public List<Stages> getAllStages(){
        return new Select()
                .from(Stages.class)
                .execute();
    }

    public List<Stages> selectByObjectType(String objectType) {
        return new Select()
                .from(Stages.class)
                .where("ObjectType=?", objectType)
                .execute();
    }

    public void clearTable(){
        new Delete().from(Stages.class).execute();
    }
}
