package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.TypeData;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateObjectItemSync;

import java.util.List;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ObjectItemsButtonsAdapter extends RecyclerView.Adapter<ObjectItemsButtonsAdapter.Holder> {

    private List<TypeData> items;
    private Context context;
    private UpdateObjectItemSync updateObjectItemSync;
    private UpdateButtonStatusCallback callback;
    private ColoredSnackbar coloredSnackbar;

    private String currentBtn;
    private String obType, obKey, tabName, user, apikey, url, itemKey;

    public ObjectItemsButtonsAdapter(Context context, List<TypeData> items, String currentBtn, UpdateButtonStatusCallback callback) {
        this.items = items;
        this.context = context;
        this.currentBtn = currentBtn;
        this.callback = callback;

        this.updateObjectItemSync = new UpdateObjectItemSync(context);
        this.coloredSnackbar = new ColoredSnackbar(context);
    }

    public void setCallData(String obType, String obKey, String tabName, String user, String apikey, String url, String itemKey){
        this.obKey = obKey;
        this.obType = obType;
        this.tabName = tabName;
        this.user = user;
        this.apikey = apikey;
        this.url = url;
        this.itemKey = itemKey;
    }

    public void updateCurrent(String selection){
        this.currentBtn = selection;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_list_item_button, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        final TypeData item = items.get(position);

        if (item != null) {
            holder.button.setText(item.getName());
            if(currentBtn.equals(item.getValue())){
                holder.button.setBackgroundResource(R.drawable.drawable_button);
            } else {
                holder.button.setBackgroundResource(R.drawable.drawable_button_black);
            }
        }

        holder.button.setOnClickListener(view -> {
            boolean state = new NetworkCheck().NetworkConnectionCheck(context);
            if(state) {
                if (currentBtn != item.getValue()) {
                    coloredSnackbar.showSnackBar("Updating...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_SHORT);
                    updateObjectItemSync.updateItem(new UpdateObjectItemSync.UpdateItemCallback() {
                        @Override
                        public void onUpdateItemSuccess() {
                            callback.onButtonStatusUpdateSuccess(item.getValue(), item.getName());
                            currentBtn = item.getValue();
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onUpdateItemFail(String message) {
                            coloredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                        }
                    }, url, obType, obKey, tabName, user, itemKey, item.getValue(), apikey);
                }
            } else {
                coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public interface UpdateButtonStatusCallback{
        void onButtonStatusUpdateSuccess(String data, String name);
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomButton button;

        public Holder(View itemView) {
            super(itemView);
            button = (CustomButton) itemView.findViewById(R.id.btnData);
        }
    }
}
