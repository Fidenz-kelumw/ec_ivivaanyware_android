package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/22/2016.
 */
@Table(name = "Members")
public class Members extends Model{

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "RoleID")
    @SerializedName("RoleID")
    @Expose
    public String mRoleID;

    //@Column(name = "User")
    @SerializedName("User")
    @Expose
    public User mUser;

    @Column(name = "UserKey")
    @Expose
    public String mUserKey;

    @Column(name = "UserName")
    @Expose
    public String mUserName;

    @Column(name = "ImagePath")
    @Expose
    public String mImagePath;

    @Column(name = "Phone")
    @Expose
    public String mPhone;

    public Members() {
        super();
    }

    public Members(String objectKey,String roleID, User user, String phone) {
        super();
        mRoleID = roleID;
        mUser = user;
        mObjectKey = objectKey;
        mPhone = phone;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public void clearTable() {
        new Delete().from(Members.class).execute();
        //new Delete().from(User.class).execute();
    }

    public List<Members> select(String objectKey) {
        return new Select().from(Members.class).where("ObjectKey =?",objectKey).execute();
    }

    public String getRoleID() {
        return mRoleID;
    }

    public void setRoleID(String roleID) {
        mRoleID = roleID;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }



}
