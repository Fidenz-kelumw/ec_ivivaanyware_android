package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "OptionListModel")
public class OptionListModel extends Model {

    @Column(name = "fieldID")
    String fieldID;

    @Column(name = "displayText")
    @SerializedName("DisplayText")
    @Expose
    String displayText;

    @Column(name = "value")
    @SerializedName("Value")
    @Expose
    String value;

    public OptionListModel() {
    }

    public OptionListModel(String fieldID, String displayText, String value) {
        this.fieldID = fieldID;
        this.displayText = displayText;
        this.value = value;
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void clearTable(){
        new Delete().from(OptionListModel.class).execute();
    }

    public List<OptionListModel> getOptionList(String fieldID){
        return new Select()
                .from(OptionListModel.class)
                .where("fieldID = ?",fieldID)
                .execute();
    }
}
