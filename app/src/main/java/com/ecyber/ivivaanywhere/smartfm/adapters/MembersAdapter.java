package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.Roles;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.Holder>{

    private Context mContext;
    private List<Members> mMembersList;
    private String mBaseUrl;
    private List<Roles> mAllRoles;
    private CImageLoader mCImageLoader;

    public MembersAdapter(Context context, List<Members> membersList, String baseUrl, List<Roles> allRoles) {
        mContext = context;
        mMembersList = membersList;
        mBaseUrl = baseUrl;
        mAllRoles = allRoles;
        mCImageLoader = new CImageLoader(mContext);
    }

    public void addItem(Members member) {
        mMembersList.add(member);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_member_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        for (int i = 0; i < mAllRoles.size(); i++) {
            if ((mMembersList.get(position).getRoleID()).equals(mAllRoles.get(i).getRoleID())) {
                //((GradientDrawable) holder.mLayLeft.getBackground()).setColor((Color.parseColor(mAllRoles.get(i).getColor())));
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor(mAllRoles.get(i).getColor()));
                gd.setCornerRadii(new float[]{5,5,0,0,0,0,5,5});
                holder.mLayLeft.setBackground(gd);
            }
        }
        Members member = mMembersList.get(position);
        //User user = mMembersList.get(position).getUser();
        holder.tvMemberName.setText(member.mUserName);
        holder.tvMemberPosition.setText(mMembersList.get(position).getRoleID());

        String checkSsl = DomainData.getSsl();
        if(checkSsl != null && !checkSsl.equals("")){
            if(checkSsl.equals("1")){
                mCImageLoader.DisplayImage("https://" + mBaseUrl + member.mImagePath, holder.imgMemberItemUserImage);
            }
            else {
                mCImageLoader.DisplayImage("http://" + mBaseUrl + member.mImagePath, holder.imgMemberItemUserImage);
            }
        }


        if(Utility.IsEmpty(member.mPhone)) {
            holder.imgBtnMemberPhone.setVisibility(View.GONE);
        } else {
            holder.imgBtnMemberPhone.setVisibility(View.VISIBLE);
            holder.mPhoneNumber = member.mPhone;
        }
    }

    @Override
    public int getItemCount() {
        return null != mMembersList ? mMembersList.size() : 0;
    }

    public void clear() {
        mMembersList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvMemberName, tvMemberPosition;
        ImageView imgMemberItemUserImage;
        ImageButton imgBtnMemberPhone;
        View mLayLeft;
        String mPhoneNumber = null;

        public Holder(View itemView) {
            super(itemView);
            imgMemberItemUserImage = (ImageView) itemView.findViewById(R.id.image_view_member_item_user_image);
            tvMemberName = (TextView) itemView.findViewById(R.id.text_view_sin_member_name);
            tvMemberPosition = (TextView) itemView.findViewById(R.id.text_view_sin_member_position);
            imgBtnMemberPhone = (ImageButton) itemView.findViewById(R.id.img_btn_member_phone);
            mLayLeft = (View) itemView.findViewById(R.id.member_left_bg_item);
            imgBtnMemberPhone.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(!Utility.IsEmpty(mPhoneNumber)) {
                Uri call = Uri.parse("tel:" + mPhoneNumber);
                Intent surf = new Intent(Intent.ACTION_DIAL, call);
                surf.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(surf);
            }
        }
    }
}
