package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.Tabs;

import java.util.List;

/**
 * Created by Lakmal on 4/20/2016.
 */
public class SideNavigationTypeNameAdapter extends RecyclerView.Adapter<SideNavigationTypeNameAdapter.Holder> {


    private List<Tabs> mItems;
    private Context mContext;
    private SideNavTabNameClickListener mSideNavTabNameClickListener;

    public SideNavigationTypeNameAdapter(List<Tabs> mItems, Context mContext, SideNavTabNameClickListener sideNavTabClickListener) {
        this.mItems = mItems;
        this.mContext = mContext;
        mSideNavTabNameClickListener = sideNavTabClickListener;
    }

    public void addItem(Tabs tabs) {
        mItems.add(tabs);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_side_navigation_type_name, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        String tabName = mItems.get(position).TabName;
        holder.tvNavTypeName.setText(tabName);
        holder.llayoutTabItem.setTag(mItems.get(position).TabType);
    }

    @Override
    public int getItemCount() {
        return null != mItems ? mItems.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNavTypeName;
        LinearLayout llayoutTabItem;

        public Holder(View itemView) {
            super(itemView);
            tvNavTypeName = (TextView) itemView.findViewById(R.id.tv_nav_type_name);
            llayoutTabItem = (LinearLayout) itemView.findViewById(R.id.tab_item);
            llayoutTabItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mSideNavTabNameClickListener.OnClickTabName(v, this.getAdapterPosition(), mItems.get(getAdapterPosition()).getTabName());
        }
    }

    public interface SideNavTabNameClickListener {
        void OnClickTabName(View v, int position, String tabName);

    }
}
