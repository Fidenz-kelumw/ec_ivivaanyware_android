package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AnimateFirstDisplayListener;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentMaster;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

/**
 * Created by Lakmal on 5/11/2016.
 */
public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.Holder> {

    private Context mContext;
    private List<AttachmentMaster> mAttachmentList;
    private String mBaseUrl;
    private AttachmentItemClickListener mItemClickListener;
    private DisplayImageOptions mImageOptions;
    private ImageLoadingListener mAnimateFirstDisplayListener = new AnimateFirstDisplayListener();

    public AttachmentAdapter(Context context, List<AttachmentMaster> attachmentList, String baseUrl, AttachmentItemClickListener itemClickListener) {
        mContext = context;
        mAttachmentList = attachmentList;
        mBaseUrl = baseUrl;
        mItemClickListener = itemClickListener;

        mImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * add item to attachment list
     *
     * @param attachment
     */
    public void addItem(AttachmentMaster attachment) {
        mAttachmentList.add(attachment);
        notifyDataSetChanged();
    }

    /**
     * list items clear
     */
    public void clear() {
        if (mAttachmentList.size() > 0)
            mAttachmentList.clear();

        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_attachment_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        AttachmentMaster attachment = mAttachmentList.get(position);

        if (attachment != null) {
            String ATTType = attachment.getmATTType();

            String url = "";
            String checkSsl = DomainData.getSsl();
            if (checkSsl != null && !checkSsl.equals("")) {
                if (checkSsl.equals("1")) {
                    url = "https://" + mBaseUrl + attachment.getFilePath();
                } else {
                    url = "http://" + mBaseUrl + attachment.getFilePath();
                }
            }
            holder.setAttachment(attachment);

            if (ATTType == null || ATTType.equals("IMG") || ATTType.equals("")) {
                ImageLoader.getInstance().displayImage(url, holder.imgAttachment, mImageOptions, mAnimateFirstDisplayListener);
                holder.imgAttType.setVisibility(View.GONE);
            } else if (ATTType.equals("AUD")) {
                holder.imgAttachment.setImageDrawable(null);
                holder.imgAttType.setImageResource(R.drawable.att_audio);
                holder.imgAttType.setVisibility(View.VISIBLE);
                holder.imgAttachment.setBackgroundColor(Color.rgb(0, 0, 0));
            } else if (ATTType.equals("VID")) {
                holder.imgAttachment.setImageDrawable(null);
                holder.imgAttType.setImageResource(R.drawable.att_video);
                holder.imgAttType.setVisibility(View.VISIBLE);
                holder.imgAttachment.setBackgroundColor(Color.rgb(0, 0, 0));
            }

            if (attachment.getAttachmentName() == null || attachment.getAttachmentName().equals("")) {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText("");
            } else {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(attachment.getAttachmentName());
            }

            holder.tvType.setVisibility(View.VISIBLE);
            holder.tvType.setText((attachment.getAttachmentType() != null ? attachment.getAttachmentType() : ""));

            User user = attachment.getUploadedUser();
            holder.tvMemberName.setText((user != null && user.getUserName() != null) ? user.getUserName():"");
            holder.tvDownloadUrl.setText(url);

            if (attachment.getUploadedAt() != null) {
                if (!attachment.getUploadedAt().isEmpty()) {
                    String formattedTime;
                    formattedTime = Utility.convertDate(attachment.mUploadedAt);
                    holder.tvTime.setText(formattedTime);
                } else {
                    holder.tvTime.setText("");
                }
            } else {
                holder.tvTime.setText("");
            }
        }
    }

    @Override
    public int getItemCount() {
        return null != mAttachmentList ? mAttachmentList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView tvTitle, tvType, tvTime, tvMemberName, tvDownloadUrl;
        ImageView imgAttachment;
        ImageView imgAttType;
        AttachmentMaster attachment;


        public AttachmentMaster getAttachment() {
            return attachment;
        }

        public void setAttachment(AttachmentMaster attachment) {
            this.attachment = attachment;
        }

        public Holder(View itemView) {
            super(itemView);
            imgAttachment = (ImageView) itemView.findViewById(R.id.imageView_attachment_image);
            imgAttType = (ImageView) itemView.findViewById(R.id.img_att_type);
            tvTitle = (CustomTextView) itemView.findViewById(R.id.textView_attachment_title);
            tvType = (CustomTextView) itemView.findViewById(R.id.txt_att_type);
            tvTime = (CustomTextView) itemView.findViewById(R.id.textView_time);
            tvMemberName = (CustomTextView) itemView.findViewById(R.id.textView_attachment_member_name);
            tvDownloadUrl = (CustomTextView) itemView.findViewById(R.id.textView_image_url_to_download);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.OnAttachmentItemClick(v, getAdapterPosition(), attachment);
        }
    }

    public interface AttachmentItemClickListener {
        void OnAttachmentItemClick(View v, int position, AttachmentMaster attachment);
    }
}
