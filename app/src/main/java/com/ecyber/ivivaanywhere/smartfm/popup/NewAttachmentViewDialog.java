package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DipPixConverter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.PicassoRegionDecoder;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.TouchImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.okhttp.OkHttpClient;


/**
 * Created by Lakmal on 6/1/2016.
 */
public class NewAttachmentViewDialog implements PicassoRegionDecoder.PicassoRegionCallback {

    private Activity mActivity;
    //private SubsamplingScaleImageView zImage;
    private TouchImageView imageView;
    private ProgressBar pb_zoom_pan;
    private CustomTextView mTvMessage;
    private PicassoRegionDecoder.PicassoRegionCallback mCallback;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    public NewAttachmentViewDialog(Activity activity) {
        mActivity = activity;
        mNetwork = new NetworkCheck();
        mColoredSnackBar = new ColoredSnackbar(mActivity);
    }

    public void show(View v) {
        CustomTextView tvDownloadUrl = (CustomTextView) v.findViewById(R.id.textView_image_url_to_download);

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density));
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density));

        final float x, y;
        DipPixConverter converter = new DipPixConverter();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        final String titleImage = (((TextView) v.findViewById(R.id.textView_attachment_title)).getText()).toString();

        final Dialog dialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater1 = LayoutInflater.from(mActivity);
        View view = inflater1.inflate(R.layout.dialog_attachment_image_view, null);
        dialog.setContentView(view);

        //zImage = (SubsamplingScaleImageView) view.findViewById(R.id.imageView_att_zoom_image);
        imageView = (TouchImageView) view.findViewById(R.id.imageView_att_zoom_image);
        //imageView = (ImageView) view.findViewById(R.id.imageView_att_zoom_image);

        final TextView title = (TextView) view.findViewById(R.id.textView_att_zoom_title);
        ImageButton cls = (ImageButton) view.findViewById(R.id.imageButton_att_zoom_close);
        mTvMessage = (CustomTextView) view.findViewById(R.id.tv_message);
        pb_zoom_pan = (ProgressBar) view.findViewById(R.id.progressBar_zoom_pan);
        //-----------
        final String url = tvDownloadUrl.getText().toString();
        OkHttpClient okHttpClient = new OkHttpClient();
        mCallback = this;

        //---------------
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoader.getInstance().displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                /*progressBar.setProgress(0);
                progressBar.setVisibility(View.VISIBLE);*/
                //pb_zoom_pan.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = "Please check internet and try again.";
                pb_zoom_pan.setVisibility(View.GONE);
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                pb_zoom_pan.setVisibility(View.GONE);
            }
        }, (imageUri, view1, current, total) -> {
            //progressBar.setProgress(Math.round(100.0f * current / total));
            Log.v("elemele","onProgressUpdate ############################### " + current + "/" + total);
        });
        //---------------
        /*boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            //loadImageByUrl(url, okHttpClient);
        } else {
            Toast.makeText(mActivity, SmartConstants.NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
        }*/

        title.setText(titleImage);
        dialog.show();
        //pb_zoom_pan.setVisibility(View.VISIBLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout((int) x, (int) y);

        cls.setOnClickListener(v1 -> dialog.dismiss());
    }

    /*private void loadImageByUrl(final String url, final OkHttpClient okHttpClient) {
        imageView.setMaxScale(5.0f);

        imageView.setRegionDecoderFactory(new DecoderFactory<ImageRegionDecoder>() {
            @Override
            public ImageRegionDecoder make() throws IllegalAccessException, java.lang.InstantiationException {
                //pb_zoom_pan.setVisibility(View.GONE);
                return new PicassoRegionDecoder(okHttpClient, mCallback);
            }

        });

        imageView.setOnImageEventListener(new SubsamplingScaleImageView.DefaultOnImageEventListener());
        imageView.setImage(ImageSource.uri(url));
    }*/

    @Override
    public void onImageDownloaded(boolean status) {
        mActivity.runOnUiThread(() -> {
            //pb_zoom_pan.setVisibility(View.GONE);
        });
    }
}
