package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "ObjectURL")
public class ObjectURL extends Model{

    @Column(name = "URLName")
    @SerializedName("URLName")
    @Expose
    public String URLName;

    @Column(name = "URLPath")
    @SerializedName("URLPath")
    @Expose
    public String URLPath;

    @Column(name = "UpdatedAt")
    @SerializedName("UpdatedAt")
    @Expose
    public String UpdatedAt;

    public ObjectURL() {
    }

    public ObjectURL(String URLName, String URLPath, String updatedAt) {
        this.URLName = URLName;
        this.URLPath = URLPath;
        UpdatedAt = updatedAt;
    }

    public String getURLName() {
        return URLName;
    }

    public void setURLName(String URLName) {
        this.URLName = URLName;
    }

    public String getURLPath() {
        return URLPath;
    }

    public void setURLPath(String URLPath) {
        this.URLPath = URLPath;
    }

    public String getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        UpdatedAt = updatedAt;
    }

    public List<ObjectURL> getAllObjectURLs() {
        return new Select().from(ObjectURL.class).execute();
    }

    public void clearTable() {
        new Delete().from(ObjectURL.class).execute();
    }

}
