package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectItem;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateObjectItemSync;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.List;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ObjectItemsAdapter extends RecyclerView.Adapter<ObjectItemsAdapter.Holder> implements ObjectItemsButtonsAdapter.UpdateButtonStatusCallback {

    private List<ObjectItem> items;
    private Context context;
    private UpdateObjectItemSync updateObjectItemSync;
    private ColoredSnackbar coloredSnackbar;

    private boolean enableFadeView;
    private int currentSelecton;
    private String obType, obKey, tabName, user, apikey, url;
    private boolean state;
    private NetworkCheck mNetwork;

    public ObjectItemsAdapter(Context context, List<ObjectItem> items) {
        this.items = items;
        this.context = context;
        this.currentSelecton = -1;

        this.mNetwork = new NetworkCheck();
        this.updateObjectItemSync = new UpdateObjectItemSync(context);
        this.coloredSnackbar = new ColoredSnackbar(context);
        this.state = mNetwork.NetworkConnectionCheck(context);
    }

    public void setCallData(String obType, String obKey, String tabName, String user, String apikey, String url) {
        this.obKey = obKey;
        this.obType = obType;
        this.tabName = tabName;
        this.user = user;
        this.apikey = apikey;
        this.url = url;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void add(ObjectItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void addFadeView() {
        enableFadeView = true;
        notifyDataSetChanged();
    }

    public void removeFadeView() {
        enableFadeView = false;
        notifyDataSetChanged();
    }

    public void collapseAndAddFadeToOthers(int currentSelection) {
        if(currentSelection == 0 || currentSelection != this.currentSelecton) {
            enableFadeView = true;
            this.currentSelecton = currentSelection;
            notifyDataSetChanged();
        }
    }

    public void resetCollapseAndFade() {
        enableFadeView = false;
        this.currentSelecton = -1;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_item_list_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ObjectItem item = items.get(position);

        holder.name.setText(item.getItemName());
        holder.description.setTextColor(Color.parseColor(item.getDataColor()));

        if (item.getTypeID() != null && !item.getTypeID().equals("null")) {
            if (item.getTypeID().equals("VALUE")) {
                numberPickerController(holder, item);
            } else if (item.getTypeID().equals("SWITCH")) {
                switchController(holder, item);
            } else if (item.getTypeID().equals("BUTTONS")) {
                buttonSelector(holder, item);
            }
        } else {
            holder.layNumberPicker.setVisibility(View.GONE);
            holder.layButtons.setVisibility(View.GONE);
            holder.laySwitch.setVisibility(View.GONE);

            holder.description.setText("" + item.getItemData());
        }

        if (enableFadeView) {
            if (position == currentSelecton && item.getTypeID() != null && !item.getTypeID().equals("null"))
                holder.fade.setVisibility(View.INVISIBLE);
            else
                holder.fade.setVisibility(View.VISIBLE);
        } else {
            holder.fade.setVisibility(View.INVISIBLE);
        }

        if (item.getTypeID() != null && !item.getTypeID().equals("null")) {
            if (position != currentSelecton) {
                holder.expandableLayout.collapse();
            } else {
                holder.expandableLayout.expand();
            }
        }
    }

    private void buttonSelector(Holder holder, ObjectItem item) {
        holder.layNumberPicker.setVisibility(View.GONE);
        holder.layButtons.setVisibility(View.VISIBLE);
        holder.laySwitch.setVisibility(View.GONE);

        for (int i = 0; i < item.getTypeData().size(); i++) {
            if (item.getItemData().equals(item.getTypeData().get(i).getValue())) {
                holder.description.setText(item.getTypeData().get(i).getName());
                break;
            }
        }

        ObjectItemsButtonsAdapter objectItemsButtonsAdapter = new ObjectItemsButtonsAdapter(context, item.getTypeData(), item.getItemData(), this);
        holder.recyclerviewButtons.hasFixedSize();
        holder.recyclerviewButtons.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.recyclerviewButtons.setAdapter(objectItemsButtonsAdapter);
        objectItemsButtonsAdapter.setCallData(obType, obKey, tabName, user, apikey, url, item.getItemKey());
    }

    private void numberPickerController(final Holder holder, ObjectItem item) {
        holder.layNumberPicker.setVisibility(View.VISIBLE);
        holder.layButtons.setVisibility(View.GONE);
        holder.laySwitch.setVisibility(View.GONE);

        if (item.getTempData() == null)
            item.setTempData(item.getItemData());

        holder.description.setText(item.getItemData());
        holder.tvMinMax.setText(item.getTempData());

        if (item.getItemEdit().equals("1")) {
            holder.btnSet.setEnabled(true);
            holder.btnSet.setAlpha(1.0f);
        } else {
            holder.btnSet.setEnabled(false);
            holder.btnSet.setAlpha(0.8f);

            holder.btnDown.setEnabled(false);
            holder.btnUp.setEnabled(false);
            holder.btnDown.setAlpha(0.8f);
            holder.btnUp.setAlpha(0.8f);
        }

        if (holder.tvMinMax.getText().equals("" + item.getMAX())) {
            holder.btnUp.setEnabled(false);
            holder.btnUp.setAlpha(0.8f);
        } else if (holder.tvMinMax.getText().equals("" + item.getMIN())) {
            holder.btnDown.setEnabled(false);
            holder.btnDown.setAlpha(0.8f);
        } else {
            holder.btnDown.setEnabled(true);
            holder.btnUp.setEnabled(true);
            holder.btnDown.setAlpha(1.0f);
            holder.btnUp.setAlpha(1.0f);
        }

        holder.btnUp.setOnClickListener(view -> {
            float x = Float.parseFloat(holder.tvMinMax.getText().toString());
            x += item.getINC();

            holder.tvMinMax.setText("" + x);
            item.setTempData("" + x);

            if (x == item.getMAX()) {
                holder.btnUp.setEnabled(false);
                holder.btnUp.setAlpha(0.8f);
            }

            if (x > item.getMIN()) {
                holder.btnDown.setEnabled(true);
                holder.btnDown.setAlpha(1.0f);
            }

        });

        holder.btnDown.setOnClickListener(view -> {
            float x = Float.parseFloat(holder.tvMinMax.getText().toString());
            x -= item.getINC();

            holder.tvMinMax.setText("" + x);
            item.setTempData("" + x);

            if (x == item.getMIN()) {
                holder.btnDown.setEnabled(false);
                holder.btnDown.setAlpha(0.8f);
            }

            if (x < item.getMAX()) {
                holder.btnUp.setEnabled(true);
                holder.btnUp.setAlpha(1.0f);
            }
        });

        holder.tvMinMax.addTextChangedListener(new TextWatcher() {

            String beforeValue = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s == null)
                    beforeValue = "";
                else
                    beforeValue = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !s.toString().isEmpty()) {
                    float x = Float.parseFloat(holder.tvMinMax.getText().toString());

                    if (x >= item.getMIN() && x <= item.getMAX()) {
                        holder.btnSet.setEnabled(true);
                        beforeValue = s.toString();
                    } else {
                        holder.btnSet.setEnabled(false);
                    }

                    item.setTempData("" + x);
                } else {
                    holder.btnSet.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.btnSet.setOnClickListener(view -> {

            float x = Float.parseFloat(holder.tvMinMax.getText().toString());
            if (x >= item.getMIN() && x <= item.getMAX()) {

                state = mNetwork.NetworkConnectionCheck(context);
                if (state) {
                    holder.btnSet.setEnabled(false);
                    coloredSnackbar.showSnackBar("Updating...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_SHORT);
                    updateObjectItemSync.updateItem(new UpdateObjectItemSync.UpdateItemCallback() {
                        @Override
                        public void onUpdateItemSuccess() {
                            holder.description.setText(holder.tvMinMax.getText().toString());
                            item.setItemData(holder.tvMinMax.getText().toString());
                            item.setTempData("" + x);
                            coloredSnackbar.showSnackBar("Updated successfully", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
                            holder.btnSet.setEnabled(true);
                        }

                        @Override
                        public void onUpdateItemFail(String message) {
                            coloredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                            holder.tvMinMax.setText(item.getItemData());
                            holder.btnSet.setEnabled(true);
                            if (holder.tvMinMax.getText().equals("" + item.getMAX())) {
                                holder.btnUp.setEnabled(false);
                                holder.btnUp.setAlpha(0.8f);
                            } else if (holder.tvMinMax.getText().equals("" + item.getMIN())) {
                                holder.btnDown.setEnabled(false);
                                holder.btnDown.setAlpha(0.8f);
                            } else {
                                holder.btnDown.setEnabled(true);
                                holder.btnUp.setEnabled(true);
                                holder.btnDown.setAlpha(1.0f);
                                holder.btnUp.setAlpha(1.0f);
                            }
                        }
                    }, url, obType, obKey, tabName, user, item.getItemKey(), holder.tvMinMax.getText().toString(), apikey);
                } else {
                    coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                }
            } else {
                coloredSnackbar.showSnackBar("Value not between " + item.getMIN() + "-" + item.getMAX() + " range", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }
        });
    }

    private void switchController(final Holder holder, final ObjectItem item) {
        holder.layNumberPicker.setVisibility(View.GONE);
        holder.layButtons.setVisibility(View.GONE);
        holder.laySwitch.setVisibility(View.VISIBLE);

        if (item.getItemData().equals("1")) {
            holder.switchBtn.setChecked(true);
            holder.description.setText("ON");
        } else {
            holder.switchBtn.setChecked(false);
            holder.description.setText("OFF");
        }

        if (item.getItemEdit().equals("1")) {
            holder.switchBtn.setOnCheckedChangeListener((compoundButton, b) -> {
                state = mNetwork.NetworkConnectionCheck(context);
                if (state) {
                    holder.switchBtn.setEnabled(false);
                    coloredSnackbar.showSnackBar("Updating...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_SHORT);
                    updateObjectItemSync.updateItem(new UpdateObjectItemSync.UpdateItemCallback() {
                        @Override
                        public void onUpdateItemSuccess() {
                            coloredSnackbar.showSnackBar("Updated successfully", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
                            holder.description.setText((holder.switchBtn.isChecked() ? "ON" : "OFF"));
                            item.setItemData((holder.switchBtn.isChecked() ? "1" : "0"));
                            holder.switchBtn.setEnabled(true);
                        }

                        @Override
                        public void onUpdateItemFail(String message) {
                            coloredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                            holder.switchBtn.setChecked(!holder.switchBtn.isChecked());
                            holder.switchBtn.setEnabled(true);
                        }
                    }, url, obType, obKey, tabName, user, item.getItemKey(), (holder.switchBtn.isChecked() ? "1" : "0"), apikey);
                } else {
                    coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                }
            });
        } else {
            holder.switchBtn.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public void onButtonStatusUpdateSuccess(String data, String name) {
        items.get(currentSelecton).setItemData(data);
        notifyDataSetChanged();
        coloredSnackbar.showSnackBar("Updated successfully", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView name, description;
        ExpandableRelativeLayout expandableLayout;
        RelativeLayout layNumberPicker, laySwitch, layButtons;
        CustomEditTextView tvMinMax;

        ImageButton btnUp, btnDown;
        Button btnSet;

        SwitchButton switchBtn;

        RecyclerView recyclerviewButtons;
        View fade;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tvName);
            description = (CustomTextView) itemView.findViewById(R.id.tvData);
            tvMinMax = (CustomEditTextView) itemView.findViewById(R.id.tvMinMax);

            expandableLayout = (ExpandableRelativeLayout) itemView.findViewById(R.id.expandableLayout);
            layButtons = (RelativeLayout) itemView.findViewById(R.id.layButtonList);
            layNumberPicker = (RelativeLayout) itemView.findViewById(R.id.layNumberPicker);
            laySwitch = (RelativeLayout) itemView.findViewById(R.id.laySwitch);

            btnDown = (ImageButton) itemView.findViewById(R.id.btnDown);
            btnUp = (ImageButton) itemView.findViewById(R.id.btnUp);
            btnSet = (Button) itemView.findViewById(R.id.btnSet);

            switchBtn = (SwitchButton) itemView.findViewById(R.id.switchBtn);
            recyclerviewButtons = (RecyclerView) itemView.findViewById(R.id.recyclerviewButtons);
            fade = (View) itemView.findViewById(R.id.viewFade);
        }
    }

}
