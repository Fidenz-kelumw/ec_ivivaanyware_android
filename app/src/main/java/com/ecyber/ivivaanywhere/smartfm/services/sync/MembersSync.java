package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.app.Activity;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.MembersResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetMembers;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class MembersSync {

    private GetMembersCallback mGetMembersCallbackDelegate;
    private Activity mActivity;

    public MembersSync(GetMembersCallback getMembersCallbackDelegate) {
        mGetMembersCallbackDelegate = getMembersCallbackDelegate;
    }

    public void getMembers(String url, String apiKey, String userKey, String objType, final String objKey, String tabName) {

        ServiceGenerator.CreateService(GetMembers.class, url).getMembers(apiKey, userKey, objType, objKey, tabName)
                .enqueue(new Callback<MembersResponse>() {
                    @Override
                    public void onResponse(Call<MembersResponse> call, Response<MembersResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getMembers().size() != 0) {
                                    new Members().clearTable();
                                    for (Members member : response.body().getMembers()) {
                                        saveMembersToLocalDB(member, objKey);
                                        mGetMembersCallbackDelegate.onGetMembersSuccess(member);
                                    }
                                    new TimeCap().updateTableTime(tabName);

                                } else {
                                    mGetMembersCallbackDelegate.onGetMembersError("No members found");
                                }
                            } else {
                                mGetMembersCallbackDelegate.onGetMembersError("Fetching data failed");
                            }
                        } else {
                                mGetMembersCallbackDelegate.onGetMembersError("Fetching data failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<MembersResponse> call, Throwable t) {
                        mGetMembersCallbackDelegate.onGetMembersError(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    private void saveMembersToLocalDB(Members member, String objectKey) {
        User user = member.getUser();
        //User exist = new Select().from(User.class).where("UserKey = ?", user.getUserKey()).executeSingle();

        /*if (exist != null) {
            member.mObjectKey = objectKey;
            member.mUser = exist;
            member.save();
        } else {*/
            /*User nUser = new User(user.getUserKey(), user.getUserName(), user.getImagePath());
            nUser.save();*/
        member.mUserKey = user.mUserKey;
        member.mUserName = user.mUserName;
        member.mImagePath = user.mImagePath;
        member.mObjectKey = objectKey;
        member.mPhone = "";

        if(user.mPhone != null) {
            if(!Utility.IsEmpty(user.mPhone)){
                member.mPhone = user.mPhone;
            }
        }
        //member.mUser = nUser;
        member.save();
        //}
    }

    /**
     * Get object members from local database
     *
     * @param objectKey
     * @return
     */
    public boolean getMembersFromDb(String objectKey) {
        List<Members> allMembers = new Members().select(objectKey);
        if (allMembers.size() == 0) {
            return false;
        } else {
            for (Members member : allMembers) {
                mGetMembersCallbackDelegate.onGetMembersSuccess(member);
            }
            return true;
        }
    }

    public interface GetMembersCallback {

        void onGetMembersSuccess(Members member);

        void onGetMembersError(String error);

    }
}
