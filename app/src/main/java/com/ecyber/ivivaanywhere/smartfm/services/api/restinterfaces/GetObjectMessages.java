package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.MessagesStatus;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.MessageAddResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/9/2016.
 */
public interface GetObjectMessages {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectMessages")
    Call<MessagesStatus> getObjectMessage(@Field("apikey") String apiKey,
                                               @Field("UserKey") String userKey,
                                               @Field("ObjectType") String objectType,
                                               @Field("ObjectKey") String objectKey,
                                               @Field("TabName") String tabName);



    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "AddNewMessage")
    Call<MessageAddResponse> addNewMessage(@Field("apikey") String apiKey,
                                          @Field("UserKey") String userKey,
                                          @Field("ObjectType") String objectType,
                                          @Field("ObjectKey") String objectKey,
                                          @Field("TabName") String tabName,
                                          @Field("MessageText") String messagetext,
                                          @Field("MessageType") String messageType);
}
