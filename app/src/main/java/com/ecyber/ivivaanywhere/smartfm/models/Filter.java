package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
@Table(name = "Filters")
public class Filter extends Model {

    @SerializedName("ObjectType")
    @Expose
    @Column(name = "ObjectType")
    public String ObjectType;

    @SerializedName("FilterID")
    @Expose
    @Column(name = "FilterID")
    public String FilterID;

    @SerializedName("FilterName")
    @Expose
    @Column(name = "FilterName")
    public String FilterName;

    @SerializedName("Description")
    @Expose
    @Column(name = "Description")
    public String Description;

    public Filter() {
        super();
    }

    public Filter(String objectType, String filterID, String filterName, String description) {
        super();
        ObjectType = objectType;
        FilterID = filterID;
        FilterName = filterName;
        Description = description;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public String getFilterID() {
        return FilterID;
    }

    public void setFilterID(String filterID) {
        FilterID = filterID;
    }

    public String getFilterName() {
        return FilterName;
    }

    public void setFilterName(String filterName) {
        FilterName = filterName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Filter> getAllFilters(){
        return new Select()
                .from(Filter.class)
                .execute();
    }

    public List<Filter> getAllFiltersByType(String objectType){
        return new Select()
                .from(Filter.class).where("ObjectType=?",objectType)
                .execute();
    }

    public void clearTable(){
        new Delete().from(Filter.class).execute();
    }
}
