package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.DroppedPin;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/16/2016.
 */
public class LayoutResponse {
    @SerializedName("Layout")
    @Expose
    public Layout mLayout;

    @SerializedName("DroppedPin")
    @Expose
    public DroppedPin mDroppedPin;

    @SerializedName("Pins")
    @Expose
    public List<Pins> mPinList;

    public Layout getLayout() {
        return mLayout;
    }

    public void setLayout(Layout layout) {
        mLayout = layout;
    }

    public DroppedPin getDroppedPin() {
        return mDroppedPin;
    }

    public void setDroppedPin(DroppedPin droppedPin) {
        mDroppedPin = droppedPin;
    }

    public List<Pins> getPinList() {
        return mPinList;
    }

    public void setPinList(List<Pins> pinList) {
        mPinList = pinList;
    }

    public class Layout {
        @SerializedName("Path")
        @Expose
        public String mPath;

        @SerializedName("ZoomLevels")
        @Expose
        public String mZoomLevels;

        @SerializedName("PinLabel")
        @Expose
        public PinLabel pinLabel;

        public String getmPath() {
            return mPath;
        }

        public void setmPath(String mPath) {
            this.mPath = mPath;
        }

        public String getmZoomLevels() {
            return mZoomLevels;
        }

        public void setmZoomLevels(String mZoomLevels) {
            this.mZoomLevels = mZoomLevels;
        }

        public PinLabel getPinLabel() {
            return pinLabel;
        }

        public void setPinLabel(PinLabel pinLabel) {
            this.pinLabel = pinLabel;
        }
    }

    public class Pins{

        @SerializedName("Name")
        @Expose
        public String mName;

        @SerializedName("Description")
        @Expose
        public String mDescription;

        @SerializedName("X")
        @Expose
        public String mX;

        @SerializedName("Y")
        @Expose
        public String mY;

        @SerializedName("Color")
        @Expose
        public String mColor;

        @SerializedName("ObjectType")
        @Expose
        public String mObjectType;

        @SerializedName("ObjectKey")
        @Expose
        public String mObjectKey;

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public String getX() {
            return mX;
        }

        public void setX(String x) {
            mX = x;
        }

        public String getY() {
            return mY;
        }

        public void setY(String y) {
            mY = y;
        }

        public String getColor() {
            return mColor;
        }

        public void setColor(String color) {
            mColor = color;
        }

        public String getObjectType() {
            return mObjectType;
        }

        public void setObjectType(String objectType) {
            mObjectType = objectType;
        }

        public String getObjectKey() {
            return mObjectKey;
        }

        public void setObjectKey(String objectKey) {
            mObjectKey = objectKey;
        }
    }

    public class PinLabel{
        @SerializedName("Name")
        @Expose
        String name;

        @SerializedName("Description")
        @Expose
        String description;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            description = description;
        }
    }
}
