package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.FieldTypeSAARAdapter;
import com.ecyber.ivivaanywhere.smartfm.customviews.LabelView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.DateTimeSelectView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.customviews.DropDownSelectView;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeButtons;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeSAAR;
import com.ecyber.ivivaanywhere.smartfm.customviews.MultiLineEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.customviews.SingleLineTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.customviews.SubmitButtonVew;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueType;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetFilterFieldsSync;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Lakmal on 6/10/2016.
 */
public class CustomFilterDialog implements GetFilterFieldsSync.GetFilterFieldsCallback, MultiLineEditTextView.MultiLineEditTexListener, DropDownSelectView.DropDownSelectTextListener, SingleLineTextView.SingleLineTextListener, SubmitButtonVew.SubmitActionListener, DateTimeSelectView.DateTimeSelectListener, FieldTypeButtons.FieldTypeButtonCallback, FieldTypeSAAR.FieldTypeSAARCallback {

    private final CustomFilterCallback mDelegate;
    private ArrayList<FieldDataSerializable> mFieldData;
    private String mObjectKey;
    private String mObjectType;
    private Activity mActivity;
    private GetFilterFieldsSync mGetFilterFieldsSync;
    private String mLocationKey = "";
    private NetworkCheck mNetwork;
    private MultiLineEditTextView mMultiLineEditTextView;
    private SubmitButtonVew mSubmitButtonVew;
    private List<ObjectInfoModel> mObjectInfoRes;
    private FieldTypeSAARAdapter saarAdapter;

    private ProgressBar mProgressBar;
    LinearLayout linLayCreateContainer;
    private Dialog mDialog;
    private RelativeLayout mRelLayHidden;

    public CustomFilterDialog(Activity activity, CustomFilterCallback delegate, String objectType, String locationKey, ArrayList<FieldDataSerializable> fieldList) {
        mActivity = activity;
        mDelegate = delegate;
        mObjectType = objectType;
        mLocationKey = locationKey;
        mObjectKey = "";
        mFieldData = fieldList;
        mNetwork = new NetworkCheck();
        SmartConstants.selectedObjectInfo = new ArrayList<>();
    }

    public void show() {
        mDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View view = i.inflate(R.layout.dialog_custom_filter, null);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
        linLayCreateContainer = (LinearLayout) view.findViewById(R.id.lin_lay_container);
        mRelLayHidden = (RelativeLayout) view.findViewById(R.id.rel_show_hidden);
        ImageButton close = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);

        close.setOnClickListener(v -> mDialog.dismiss());

        initData();

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 60;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mDialog.setContentView(view);
        mDialog.show();
        mDialog.setCancelable(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().setLayout((int) x, (int) y);
    }

    private void initData() {
        mGetFilterFieldsSync = new GetFilterFieldsSync(mActivity, this);
        AccountPermission accountPermission = new AccountPermission();

        boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            mProgressBar.setVisibility(View.VISIBLE);
            mGetFilterFieldsSync.getFilterFields(
                    accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), mObjectType, mLocationKey);
        } else {
            Toast.makeText(mActivity, SmartConstants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void initFields(List<ObjectInfoModel> createFields) {
        int color = mActivity.getResources().getColor(R.color.info_title_color);
        int position = 0;
        mRelLayHidden.setVisibility(View.GONE);
        SmartConstants.selectedObjectInfo = createFields;
        for (ObjectInfoModel info : createFields) {
            String valueType = info.getValuetype();
            for (FieldDataSerializable fieldDataSerializable : mFieldData) {
                if (info.getFieldid().equals(fieldDataSerializable.FieldID)) {
                    if (info.getValueModels() == null || info.getValueModels().size() < 0) {
                        ValueModel valueModel = new ValueModel(info.getFieldid(),
                                ValueType.SINGLE,
                                fieldDataSerializable.Value,
                                null, null);
                        List<ValueModel> valueModels = new ArrayList<>();
                        valueModels.add(valueModel);
                        info.setValueModels(valueModels);
                        info.setDisplaytext(fieldDataSerializable.DisplayText);
                    }
                }
            }


            switch (valueType) {
                case SmartConstants.VALUE_TYPE_SLT:
                    View viewSLT = new SingleLineTextView(mActivity, this, color).getSingleLineTextViewObjectInfo(info, position);
                    linLayCreateContainer.addView(viewSLT);
                    break;
                case SmartConstants.VALUE_TYPE_DT:
                    View viewDT = new DateTimeSelectView(mActivity, mActivity.getFragmentManager(), this).getDateTimeView(info, false, position, color);
                    linLayCreateContainer.addView(viewDT);
                    break;
                case SmartConstants.VALUE_TYPE_DAT:
                    View view = new DateTimeSelectView(mActivity, mActivity.getFragmentManager(), this).getDateTimeView(info, true, position, color);
                    linLayCreateContainer.addView(view);
                    break;
                case SmartConstants.VALUE_TYPE_DS:
                    View selectDialog = new DropDownSelectView(mActivity, this, false, false, color).getSelectDialogObjectInfo(info, position, " Select " + info.getFieldname());
                    linLayCreateContainer.addView(selectDialog);
                    break;
                case SmartConstants.VALUE_TYPE_SAS:
                    View viewSAS = new DropDownSelectView(mActivity, this, true, false, color).getSearchAndSelectDialog(mObjectType, mObjectKey, info, position, "Search");
                    linLayCreateContainer.addView(viewSAS);
                    break;
                case SmartConstants.VALUE_TYPE_MLT:
                    mMultiLineEditTextView = new MultiLineEditTextView(mActivity, this, color);
                    View viewMLT = mMultiLineEditTextView.getMultiLineEditView(info, position);
                    linLayCreateContainer.addView(viewMLT);
                    break;
                case SmartConstants.VALUE_TYPE_BUTTONS:
                    View viewButtons = new FieldTypeButtons(mActivity, position, info, this).generateButtons();
                    linLayCreateContainer.addView(viewButtons);
                    break;
                case SmartConstants.VALUE_TYPE_SAAR:
                    createSAARView(position, info);
                    break;
                case SmartConstants.VALUE_TYPE_LABEL:
                    createLabelView(info, position);
                    break;

            }
            position++;
        }
        setSubmitButton();

        checkSubmitIsAvailable();

    }

    private void setSubmitButton() {
        mSubmitButtonVew = new SubmitButtonVew(mActivity, this, null);
        View view = mSubmitButtonVew.getSubmitButtonView();
        linLayCreateContainer.addView(view);
    }

    private void createSAARView(int position, ObjectInfoModel info) {
        try {
            View view = new FieldTypeSAAR(mActivity,
                    position,
                    info,
                    this).generateView();
            linLayCreateContainer.addView(view);
        } catch (Exception e) {
            Log.e("FM", e.getMessage());
        }
        return;
    }

    private void createLabelView(ObjectInfoModel info, int position) {
        LabelView view = new LabelView(mActivity);
        linLayCreateContainer.addView(view.getLabelView(info, position));
    }

    @Override
    public void OnSubmitClick(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            ArrayList<FieldDataSerializable> fieldDataList = new ArrayList<FieldDataSerializable>();

            mProgressBar.setVisibility(View.VISIBLE);

            for (ObjectInfoModel objectInfo : mObjectInfoRes) {
                String value = "";
                String[] values = null;

                if (objectInfo.getValueModels() != null && objectInfo.getValueModels().size() > 0) {
                    if (objectInfo.getValuetype().equals(SmartConstants.VALUE_TYPE_SAAR)) {
                        List<ValueModel> valueModels = objectInfo.getValueModels();
                        values = new String[valueModels.size()];
                        for (int i = 0; i < valueModels.size(); i++)
                            values[i] = valueModels.get(i).getValue();
                    } else {
                        List<ValueModel> valueModels = objectInfo.getValueModels();
                        value = valueModels.get(0).getValue();
                    }
                }

                fieldDataList.add(new FieldDataSerializable(objectInfo.getFieldid(),
                        value,
                        objectInfo.getDisplaytext(),
                        values,
                        objectInfo.getValuetype()));
            }

            mDelegate.onClickSubmitFilter(fieldDataList);
            mDialog.dismiss();
            /*Intent intent = new Intent();
            intent.putExtra(SmartConstants.CUSTOM_FILTER, fieldDataList);
            setResult(RegistrationActivity.PICK_QR_REQUEST, intent);*/
            //onBackPressed();

        } else {
            Toast.makeText(mActivity, SmartConstants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSingleLineTextFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }

    @Override
    public void onDateTimeSelectTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, getDateISOFormat(displayText));
    }

    @Override
    public void onDropDownSelectFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, value);
    }

    @Override
    public void onMultiLineEditTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }

    private void setObjectInfoDisplayText(int position, String displayText, String value) {
        Log.d("SmartFM", displayText);
        mObjectInfoRes.get(position).setDisplaytext(displayText);

        if (mObjectInfoRes.get(position).getValueModels() != null && mObjectInfoRes.get(position).getValueModels().size() > 0) {
            mObjectInfoRes.get(position).getValueModels().get(0).setValue(value);
        } else {
            List<ValueModel> valueModels = new ArrayList<>();
            ValueModel valueModel = new ValueModel(mObjectInfoRes.get(position).getFieldid(),
                    ValueType.SINGLE,
                    value,
                    null, null);

            valueModels.add(valueModel);
            mObjectInfoRes.get(position).setValueModels(valueModels);
        }
        checkSubmitIsAvailable();
    }

    private void checkSubmitIsAvailable() {
        // TODO: 3/13/17 submit button will not work until add all new components
        SmartConstants.selectedObjectInfo = mObjectInfoRes;
        for (ObjectInfoModel objectInfo : mObjectInfoRes) {
            String displayTxt = "";
            if (objectInfo.getDisplaytext() != null) {
                List<ValueModel> valueModels = objectInfo.getValueModels();
                displayTxt = objectInfo.getDisplaytext().isEmpty() ? valueModels.get(0).getValue() : objectInfo.displaytext;
            }

            boolean isMandatory = (objectInfo.getMandatory() != null && objectInfo.getMandatory().equals("1")) ? true : false;

            if (isMandatory && displayTxt.length() <= 0) {
                mSubmitButtonVew.setEnableButton(false);
                return;
            } else {
                mSubmitButtonVew.setEnableButton(true);
            }
        }
    }

    private String getDateISOFormat(String selectedDate) {
        String[] splitDate = selectedDate.split("/");

        String[] yer = splitDate[2].split(" ");
        String selected;
        if (yer.length > 1) {
            selected = yer[0] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + yer[1];
        } else {
            selected = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + "00:00" + ":00";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        String d = null;
        try {
            d = dateFormat.format(dateFormat.parse(selected));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    @Override
    public void getFilterFieldsSuccess(List<ObjectInfoModel> filterfields) {
        mProgressBar.setVisibility(View.GONE);
        List<ObjectInfoModel> filterFieldsList = filterfields;
        mObjectInfoRes = filterFieldsList;
        initFields(filterFieldsList);

    }

    @Override
    public void getFilterFieldsError(String message) {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFieldTypeButtonStateChange(int objectPosition, String value, String displayText) {
        setObjectInfoDisplayText(objectPosition, displayText, value);
    }

    @Override
    public void onSAARAdapterGenerated(FieldTypeSAARAdapter adapter) {
        saarAdapter = adapter;
    }

    @Override
    public void onNewSAARAdded(String value, String name, int position) {
        mObjectInfoRes.get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.get(position).getValueModels();
        ValueModel valueModel = new ValueModel(mObjectInfoRes.get(position).getFieldid(),
                ValueType.ARRAY, value, null, null);

        if (valueModels != null) {
            valueModels.add(valueModel);
        } else {
            valueModels = new ArrayList<>();
            valueModels.add(valueModel);
        }

        mObjectInfoRes.get(position).setValueModels(valueModels);
    }

    @Override
    public void onSAARRemoved(String value, int position) {
        mObjectInfoRes.get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.get(position).getValueModels();
        for (int i = 0; i < valueModels.size(); i++) {
            if (valueModels.get(i).getValue().equals(value)) {
                valueModels.remove(i);
                break;
            }
        }

        mObjectInfoRes.get(position).setValueModels(valueModels);
    }

    @Override
    public void onSAARDialogItemAdded(String value, String name) {
        saarAdapter.addItem(value);
    }

    @Override
    public void onSAARDialogItemRemoved(String value, String name) {
        saarAdapter.removeItem(value);
    }

    public interface CustomFilterCallback {
        void onClickSubmitFilter(ArrayList<FieldDataSerializable> fieldDataList);
    }
}