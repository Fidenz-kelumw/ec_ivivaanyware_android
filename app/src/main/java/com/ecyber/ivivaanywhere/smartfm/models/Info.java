package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/29/2016.
 */
@Table(name="Info")
public class Info extends Model{

    @Column(name="Object_Key")
    public String mObjectKey;

    @Column(name = "ObjectInfo")
    @SerializedName("ObjectInfo")
    @Expose
    public List<ObjectInfo> mObjectInfo;

    @Column(name = "UpdateObjectInfo")
    @SerializedName("UpdateObjectInfo")
    @Expose
    public int mUpdateObjectInfo;

    @Column(name = "UpdateConfirmation")
    @SerializedName("UpdateConfirmation")
    @Expose
    public int mUpdateConfirmation;

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public int getUpdateConfirmation() {
        return mUpdateConfirmation;
    }

    public void setUpdateConfirmation(int updateConfirmation) {
        mUpdateConfirmation = updateConfirmation;
    }

    @SerializedName("PageLinks")
    @Expose
    public List<PageLinks> mPageLinks;

    @SerializedName("Actions")
    @Expose
    public List<Actions> mActions;

    public Info() {
        super();
    }

    public List<ObjectInfo> getObjectInfoByObjKey(String objectKey) {
        Info info = new Select().from(Info.class).where("Object_Key = ?", objectKey).executeSingle();
        Long id = info.getId();
        List<ObjectInfo> objectList = new Select().from(ObjectInfo.class).where("InfoID = ?", id).execute();

        return objectList;
    }

    public List<ObjectInfo> getAllObjectInfo() {
        return new Select().from(ObjectInfo.class).execute();
    }

    public List<PageLinks> getAllPageLinks() {
        return new Select().from(PageLinks.class).execute();
    }

    public List<Actions> getAllActions() {
        return new Select().from(Actions.class).execute();
    }

    public void clearTable() {
        new Delete().from(Info.class).execute();
        new Delete().from(ObjectInfo.class).execute();
        new Delete().from(OptionList.class).execute();
        new Delete().from(DefaultData.class).execute();
        new Delete().from(PageLinks.class).execute();
        new Delete().from(FieldData.class).execute();
        new Delete().from(Actions.class).execute();
    }

    public List<ObjectInfo> getObjectInfo() {
        return mObjectInfo;
    }

    public void setObjectInfo(List<ObjectInfo> objectInfo) {
        mObjectInfo = objectInfo;
    }

    public int getUpdateObjectInfo() {
        return mUpdateObjectInfo;
    }

    public void setUpdateObjectInfo(int updateObjectInfo) {
        mUpdateObjectInfo = updateObjectInfo;
    }

    public List<PageLinks> getPageLinks() {
        return mPageLinks;
    }

    public void setPageLinks(List<PageLinks> pageLinks) {
        mPageLinks = pageLinks;
    }

    public List<Actions> getActions() {
        return mActions;
    }

    public void setActions(List<Actions> actions) {
        mActions = actions;
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    /*public List<ObjectInfo> getAllObjectInfo() {
        return new Select().from(ObjectInfo.class).execute();
    }*/
}
