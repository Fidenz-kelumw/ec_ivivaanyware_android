package com.ecyber.ivivaanywhere.smartfm;

import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import com.crashlytics.android.Crashlytics;
import com.ecyber.ivivaanywhere.smartfm.activities.AlertActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.MainActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.AppManager;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.ecyber.ivivaanywhere.smartfm.services.broadcast.InternetConnectivity;
import com.jaredrummler.android.device.DeviceName;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ChathuraHettiarachchi on 4/5/16.
 */
public class SmartFMApplication extends com.activeandroid.app.Application implements InternetConnectivity.InternetConnectivityListener  {

    private boolean IS_DEVELOPING = true;
    public static boolean appIsForeground;
    public static List<Objects> mObjectList = new ArrayList<>();
    private static Application mInstance;
    private static AlertActivity mAlertActivity;

    public static synchronized Application getInstance() {
        return mInstance;
    }

    public static void setAlertActivity(AlertActivity alert) {
        mAlertActivity = alert;
    }

    @Override

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        InternetConnectivity.connectionReceiverListener = this;

        //if (IS_DEVELOPING) {
            Fabric.with(this, new Crashlytics());
        //}

        //set user name to Crashlytics
        //if (IS_DEVELOPING) {
            Crashlytics.setUserName(DeviceName.getDeviceName());
        //}


        //Initialization ActiveAndroid
        Configuration configuration = new Configuration.Builder(this).setDatabaseVersion(55).create();
        ActiveAndroid.initialize(configuration);

        //initialize image caching
        iniImageLoader(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
    }

    private static boolean isApplicationGoingToBackground(final Context context) {

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }

    private static void iniImageLoader(Context context) {
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, SmartConstants.CACHE_DIR_ATTACHMETNS);
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCache(new UnlimitedDiskCache(cacheDir));
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.tasksProcessingOrder(QueueProcessingType.FIFO);
        config.writeDebugLogs();

        ImageLoader.getInstance().init(config.build());
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected) {
            if(mAlertActivity != null) {
                mAlertActivity.closeAlert();
                mAlertActivity = null;
            }
        } else {
            if(!AppManager.isAppIsInBackground(getBaseContext())) {
                Intent intent = new Intent(getBaseContext(), AlertActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }
}
