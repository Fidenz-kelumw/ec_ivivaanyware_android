package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.BuildingsResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetBuildingsService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class BuildingsSync {

    private GetBuildingCallBack delegate;

    public BuildingsSync(GetBuildingCallBack delegate) {
        this.delegate = delegate;
    }

    public void  getBuildingList(String account, String apikey, String userkey, final boolean isNoDataNeeded){
        ServiceGenerator.CreateService(GetBuildingsService.class, account)
                .getBuildingList(userkey,apikey)
                .enqueue(new Callback<BuildingsResponce>() {
                    @Override
                    public void onResponse(Call<BuildingsResponce> call, Response<BuildingsResponce> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getBuildings() != null) {
                                    new Buildings().clearTable();
                                    for (int i = 0; i < response.body().getBuildings().size(); i++) {
                                        saveToDb(response.body().getBuildings().get(i));
                                    }

                                    if(isNoDataNeeded){
                                        List<Buildings> buildingsList = response.body().getBuildings();
                                        buildingsList.add(0, new Buildings("Any Location",""));
                                    }
                                    for (int i = 0; i < response.body().getBuildings().size(); i++) {
                                        delegate.onBuildingsDownload(response.body().getBuildings().get(i));
                                    }

                                    new TimeCap().updateTableTime(SmartConstants.BUILDINGS);
                                    delegate.onBuildingsDownloadComplete();
                                } else {
                                    delegate.onBuildingsDownloadError(SmartConstants.DOWNLOAD_FAILED);
                                }
                            } else {
                                delegate.onBuildingsDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        } else {

                            try {
                                String message = response.errorBody().string();
                                ErrorLog.saveErrorLog("Get Building List",message);
                                Log.d("SmartFm - ErrorLog",message);
                                delegate.onBuildingsDownloadError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onBuildingsDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }

                        }

                    }
                    @Override
                    public void onFailure(Call<BuildingsResponce> call, Throwable t) {
                        delegate.onBuildingsDownloadError(SmartConstants.DOWNLOAD_FAILED);
                    }
                });
    }

    private void saveToDb(Buildings buildings) {
        buildings.save();
    }

    public interface GetBuildingCallBack{
        void onBuildingsDownload(Buildings buildings);
        void onBuildingsDownloadComplete();
        void onBuildingsDownloadError(String error);
    }
}
