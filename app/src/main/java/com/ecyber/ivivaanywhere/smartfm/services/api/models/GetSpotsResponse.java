package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import java.util.List;

import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.google.gson.annotations.SerializedName;

public class GetSpotsResponse{

	@SerializedName("Spots")
	private List<SpotsItem> spots;

	public GetSpotsResponse() {
	}

	public GetSpotsResponse(List<SpotsItem> spots) {
		this.spots = spots;
	}

	public void setSpots(List<SpotsItem> spots){
		this.spots = spots;
	}

	public List<SpotsItem> getSpots(){
		return spots;
	}

	@Override
 	public String toString(){
		return 
			"GetSpotsResponse{" + 
			"spots = '" + spots + '\'' + 
			"}";
		}
}