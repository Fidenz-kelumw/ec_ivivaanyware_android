package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AnimateFirstDisplayListener;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.AddAttachment;
import com.ecyber.ivivaanywhere.smartfm.popup.ConfirmationDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lakmal on 7/21/16.
 */
public class AddAttachAdapter extends RecyclerView.Adapter<AddAttachAdapter.Holder> {

    private Context mContext;
    private Activity mActivity;
    private List<AddAttachment> mList = new ArrayList<>();
    private String mBaseUrl;
    private int lastPosition = -1;
    private AddAttachAdapterCallback mDelegate;
    private InfoAttachmentCallback mInfoAttCallback;
    private DisplayImageOptions mImageOptions;
    private ImageLoadingListener mAnimateFirstDisplayListener = new AnimateFirstDisplayListener();
    private boolean isDisableDelete;

    public AddAttachAdapter(Context context, Activity activity, AddAttachAdapterCallback delegate, InfoAttachmentCallback infoAttCallback, String baseUrl, boolean isDisableDelete) {
        this.mContext = context;
        this.mActivity = activity;
        this.mBaseUrl = baseUrl;
        this.mDelegate = delegate;
        this.mInfoAttCallback = infoAttCallback;
        this.isDisableDelete = isDisableDelete;

        mImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_no_photo)
                .showImageForEmptyUri(R.drawable.icon_no_photo)
                .showImageOnFail(R.drawable.icon_no_photo)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public void addItem(AddAttachment item) {
        mList.add(item);
        notifyDataSetChanged();

    }

    public void insert(AddAttachment item, int position) {
        mList.add(position, item);
        notifyItemInserted(position);
    }

    public void updateItem(int position, String fullUrl) {
        mList.get(position).mUploaded = true;
        mList.get(position).mFullUrl = fullUrl;
        mList.get(position).mIsUploading = false;
        notifyItemChanged(position);
    }

    public void remove(int position) {
        String guid = mList.get(position).mGUID;
        mList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
        mDelegate.onItemRemove(position, guid);
    }

    public List<AddAttachment> getArrayList() {
        return mList;
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_attach_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        AddAttachment attachment = mList.get(position);
        String url = attachment.mPath;//"http://" + mBaseUrl + attachment.mPath;
        holder.tvDownloadUrl.setText(attachment.mFullUrl);
        holder.relProgress.setVisibility(attachment.mUploaded ? View.GONE : View.VISIBLE);
        //holder.imgBtnClose.setVisibility(!isDisableDelete ? View.VISIBLE : View.GONE); // old one
        
        if (attachment.mIsEditable)
            holder.imgBtnClose.setVisibility(!isDisableDelete ? View.VISIBLE : View.GONE);
        else
            holder.imgBtnClose.setVisibility(attachment.mIsEditable ? View.VISIBLE : View.GONE);

        if (attachment.isLocal) {
            url = "file://" + url;
        }

        holder.attType.setVisibility(View.GONE);

        switch (attachment.mAttachmentType) {
            case SmartConstants.ATTACHMENT_IMG: {
                ImageLoader.getInstance().displayImage(url, holder.imgVwAttach, mImageOptions, mAnimateFirstDisplayListener);

                break;
            }
            case SmartConstants.ATTACHMENT_AUD: {
                holder.imgVwAttach.setImageResource(R.drawable.drawable_button_black);
                holder.attType.setImageResource(R.drawable.att_audio);
                holder.attType.setVisibility(View.VISIBLE);
                break;
            }
            case SmartConstants.ATTACHMENT_VID: {
                holder.imgVwAttach.setImageResource(R.drawable.drawable_button_black);
                holder.attType.setImageResource(R.drawable.att_video);
                holder.attType.setVisibility(View.VISIBLE);
                break;
            }
        }

        holder.tvTitle.setText("");
    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public AddAttachment getSelectedItem(int position){
        try{
            return mList.get(position);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position < lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else if (position > -1) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_right/*android.R.anim.slide_in_left*/);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener, ConfirmationDialog.ConfirmationCallback {

        ImageView imgVwAttach;
        ImageButton imgBtnClose;
        CustomTextView tvDownloadUrl;
        TextView tvTitle;
        RelativeLayout relProgress;
        ImageView attType;

        public Holder(View itemView) {
            super(itemView);
            imgVwAttach = (ImageView) itemView.findViewById(R.id.img_vw_attach);
            imgBtnClose = (ImageButton) itemView.findViewById(R.id.img_btn_close);
            tvDownloadUrl = (CustomTextView) itemView.findViewById(R.id.textView_image_url_to_download);
            tvTitle = (TextView) itemView.findViewById(R.id.textView_attachment_title);
            relProgress = (RelativeLayout) itemView.findViewById(R.id.rel_uploading_progress);
            attType = (ImageView) itemView.findViewById(R.id.att_type_cell);
            imgBtnClose.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getPosition();
            if (!mList.get(position).mIsUploading) {
                switch (view.getId()) {
                    case R.id.img_btn_close:
                        //new ConfirmationDialog(mActivity, "Are you sure remove this attachment?", position, this).show();
                        String guid = mList.get(position).mGUID;
                        mList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getItemCount());
                        mDelegate.onItemRemove(position, guid);
                        break;
                    default: {
                        AddAttachment att = mList.get(position);
                        mInfoAttCallback.onAttachmentClicked(att, position);
                    }
                }
            }

        }

        @Override
        public void onClickConfirmation(String message, int position, Dialog dialogInstance) {
            dialogInstance.dismiss();
        }
    }

    public interface AddAttachAdapterCallback {
        void onItemRemove(int position, String guId);
    }

    public interface InfoAttachmentCallback {
        void onAttachmentClicked(AddAttachment attachment, int position);
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

}
