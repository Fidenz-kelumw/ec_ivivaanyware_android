package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.Holder>{

    List<Filter> items;
    Context context;

    public FiltersAdapter(List<Filter> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_filter_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        Filter filter = items.get(position);
        holder.name.setText(filter.FilterName);
        holder.description.setText(filter.Description);
    }

    @Override
    public int getItemCount() {
        return null != items ? items.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder{

        CustomTextView name, description;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tv_filter_name);
            description = (CustomTextView) itemView.findViewById(R.id.tv_filter_description);
        }
    }
}
