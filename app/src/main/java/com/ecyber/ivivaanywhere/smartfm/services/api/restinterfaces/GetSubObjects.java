package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SubObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/6/2016.
 */
public interface GetSubObjects {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetSubObjects")
    Call<SubObjectResponse> getSubObjects(@Field("ObjectKey") String objectKey,
                                          @Field("ObjectType") String objectType,
                                          @Field("UserKey") String userKey,
                                          @Field("TabName") String tabName,
                                          @Field("FilterValue") String filter,
                                          @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "SubObjectAction")
    Call<SuccessResponse> performSubObjectAction(@Field("ObjectKey") String objectKey,
                                                 @Field("ObjectType") String objectType,
                                                 @Field("UserKey") String userKey,
                                                 @Field("ActionID") String actionId,
                                                 @Field("apikey") String apikey);
}
