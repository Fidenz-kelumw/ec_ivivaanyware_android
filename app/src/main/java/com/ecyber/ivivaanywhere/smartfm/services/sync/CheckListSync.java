package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.app.Activity;
import android.util.Log;

import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CheckListResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CheckStatusUpdateResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ChecklistInfoResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetCheckList;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class CheckListSync {

    private GetCheckListCallback mGetCheckListCallback;
    private Activity mActivity;

    public CheckListSync() {
    }

    public CheckListSync(GetCheckListCallback getCheckListCallback) {
        mGetCheckListCallback = getCheckListCallback;
    }

    private void foreCheckListExpire() {
        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        Date daysAgo = new DateTime(date).minusYears(1).toDate();
        String dateToDBD = simpleDateD.format(daysAgo);

        new Update(TimeCap.class)
                .set("UpdatedTime = ?", dateToDBD)
                .where("TableName = ?", SmartConstants.CHECKLIST)
                .execute();
    }

    public void getCheckList(String url, String apiKey, String userKey, String objType, final String objKey, String tabName) {

        ServiceGenerator.CreateService(GetCheckList.class, url).getCheckList(apiKey, userKey, objType, objKey, tabName)
                .enqueue(new Callback<CheckListResponse>() {
                    @Override
                    public void onResponse(Call<CheckListResponse> call, Response<CheckListResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getmCheckList() != null) {
                                    if (response.body().getmCheckList().size() != 0) {
                                        new CheckList().clearTable();
                                        for (CheckList checkList : response.body().getmCheckList()) {
                                            checkList.mObjectKey = objKey;
                                            checkList.save();

                                            mGetCheckListCallback.onGetCheckListSuccess(checkList,
                                                    checkList.getmSetItemStatus(),
                                                    checkList.getmSetItemDeadline(),
                                                    checkList.getmSetItemDescription());
                                        }
                                        new TimeCap().updateTableTime(tabName);
                                        Log.e("FM", "Checklist updated.");
                                    } else {
                                        mGetCheckListCallback.onGetCheckListError(SmartConstants.NO_ITEMS_FOUND);
                                    }
                                } else {
                                    mGetCheckListCallback.onGetCheckListError(SmartConstants.NO_ITEMS_FOUND);
                                }
                            } else {
                                mGetCheckListCallback.onGetCheckListError(SmartConstants.NO_ITEMS_FOUND);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Check List", message);
                                mGetCheckListCallback.onGetCheckListError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetCheckListCallback.onGetCheckListError(SmartConstants.NO_ITEMS_FOUND);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CheckListResponse> call, Throwable t) {
                        mGetCheckListCallback.onGetCheckListError(SmartConstants.NO_ITEMS_FOUND);
                    }
                });
    }

    public void updateCheckListDescription(final UpdateDescriptionCallback delegate, String url, String apiKey, String userKey, String objType, String objKey, String tabName, final String checkItemKey, final String description, final int itemPosition) {
        ServiceGenerator.CreateService(GetCheckList.class, url).updateCheckDescription(apiKey, userKey, objType, objKey, tabName, checkItemKey, description)
                .enqueue(new Callback<CheckStatusUpdateResponse>() {
                    @Override
                    public void onResponse(Call<CheckStatusUpdateResponse> call, Response<CheckStatusUpdateResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    new CheckList().updateDescription(description, checkItemKey);
                                    delegate.onUpdateCheckDescriptionSuccess(response.body().getSuccess(), response.body().getMessage(), checkItemKey, itemPosition, description);
                                    foreCheckListExpire();
                                } else {
                                    delegate.onUpdateCheckDescriptionError(response.body().getMessage());
                                }
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Update Check Description", message);
                                delegate.onUpdateCheckDescriptionError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onUpdateCheckDescriptionError(SmartConstants.DESCRIPTION_UPDATE_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckStatusUpdateResponse> call, Throwable t) {
                        delegate.onUpdateCheckDescriptionError(SmartConstants.DESCRIPTION_UPDATE_FAILED);
                    }
                });
    }

    public void updateCheckListDeadLine(String url, String apiKey, String userKey, String objType, String objKey, String tabName, final String checkItemKey, final String deadLine, final int itemPosition) {
        ServiceGenerator.CreateService(GetCheckList.class, url).updateCheckDeadLine(apiKey, userKey, objType, objKey, tabName, checkItemKey, deadLine)
                .enqueue(new Callback<CheckStatusUpdateResponse>() {
                    @Override
                    public void onResponse(Call<CheckStatusUpdateResponse> call, Response<CheckStatusUpdateResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    new CheckList().updateDeadLine(deadLine, checkItemKey);
                                    mGetCheckListCallback.onUpdateCheckDeadLineSuccess(response.body().getSuccess(), response.body().getMessage(), checkItemKey, itemPosition);
                                    foreCheckListExpire();
                                } else {
                                    String message = response.body().getMessage();
                                    message = Utility.IsEmpty(message) ? SmartConstants.DEAD_LINE_UPDATE_FAILED : message;
                                    mGetCheckListCallback.onUpdateCheckDeadLineError(message);
                                }
                            } else {
                                mGetCheckListCallback.onUpdateCheckDeadLineError(SmartConstants.DEAD_LINE_UPDATE_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Update Check Dead Line", message);
                                mGetCheckListCallback.onUpdateCheckDeadLineError(SmartConstants.DEAD_LINE_UPDATE_FAILED);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetCheckListCallback.onUpdateCheckDeadLineError(SmartConstants.DEAD_LINE_UPDATE_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckStatusUpdateResponse> call, Throwable t) {
                        mGetCheckListCallback.onUpdateCheckDeadLineError(SmartConstants.DEAD_LINE_UPDATE_FAILED);
                    }
                });
    }

    public void updateCheckListStatus(String url, String apiKey, String userKey, String objType, String objKey, String tabName, final String checkItemKey, final String checkBoxStatus, final int position) {
        ServiceGenerator.CreateService(GetCheckList.class, url).updateCheckStatus(apiKey, userKey, objType, objKey, tabName, checkItemKey, checkBoxStatus)
                .enqueue(new Callback<CheckStatusUpdateResponse>() {
                    @Override
                    public void onResponse(Call<CheckStatusUpdateResponse> call, Response<CheckStatusUpdateResponse> response) {
                        if (response.isSuccessful()) {

                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    new CheckList().updateStatus(checkBoxStatus, checkItemKey);
                                    mGetCheckListCallback.onUpdateCheckStatusSuccess(response.body().getMessage(), response.body().getSuccess(), checkItemKey, position, checkBoxStatus);
                                    foreCheckListExpire();
                                } else {
                                    mGetCheckListCallback.onUpdateCheckStatusSuccess(response.body().getMessage(), response.body().getSuccess(), checkItemKey, position, checkBoxStatus);
                                }
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Update Check Status", message);
                                mGetCheckListCallback.onUpdateCheckStatusError(SmartConstants.STATUS_UPDATE_FAILED,position);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetCheckListCallback.onUpdateCheckStatusError(SmartConstants.STATUS_UPDATE_FAILED,position);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckStatusUpdateResponse> call, Throwable t) {
                        mGetCheckListCallback.onUpdateCheckStatusError(SmartConstants.STATUS_UPDATE_FAILED,position);
                    }
                });

    }

    public void getChecklistInfo(final ChecklistInfoCallback checklistInfoCallback, String url, String apiKey, String userKey, String objType, String objKey, String infoCode){
        ServiceGenerator.CreateService(GetCheckList.class, url).getInfoText(apiKey, userKey, objType, objKey, infoCode)
                .enqueue(new Callback<ChecklistInfoResponse>() {
                    @Override
                    public void onResponse(Call<ChecklistInfoResponse> call, Response<ChecklistInfoResponse> response) {
                        if(response != null && response.body() != null && response.isSuccessful()){
                            checklistInfoCallback.onChecklistInfoSuccess(response.body().getInfoText());
                        } else {
                            checklistInfoCallback.onChecklistInfoFail();
                        }
                    }

                    @Override
                    public void onFailure(Call<ChecklistInfoResponse> call, Throwable t) {
                        checklistInfoCallback.onChecklistInfoFail();
                    }
                });
    }
    public void getFromLocalDb(String objectKey) {
        List<CheckList> allCheckList = new CheckList().getAll(objectKey);
        for (CheckList checkList : allCheckList) {
            mGetCheckListCallback.onGetCheckListSuccess(checkList, checkList.getmSetItemStatus(), checkList.getmSetItemDeadline(), checkList.getmSetItemDescription());
        }
    }

    public interface GetCheckListCallback {
        void onGetCheckListSuccess(CheckList checkList, String setItemStatus, String setItemDeadline, String setItemDescription);

        void onGetCheckListError(String error);

        void onUpdateCheckStatusSuccess(String message, int status, String itemKey, int position, String checkBoxStatus);

        void onUpdateCheckStatusError(String error, int position);

        void onUpdateCheckDeadLineSuccess(int status, String message, String checkItemKey, int itemPosition);

        void onUpdateCheckDeadLineError(String error);
    }

    public interface UpdateDescriptionCallback {

        void onUpdateCheckDescriptionSuccess(int status, String message, String checkItemKey, int itemPosition, String description);

        void onUpdateCheckDescriptionError(String error);
    }

    public interface ChecklistInfoCallback{
        void onChecklistInfoSuccess(String message);
        void onChecklistInfoFail();
    }
}
