package com.ecyber.ivivaanywhere.smartfm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/3/2016.
 */
public class DataLookUp {
    @SerializedName("DisplayText")
    public String mDisplayText;

    @SerializedName("Value")
    public String mValue;

    public String getValue() {
        return mValue;
    }

    public String getDisplayText() {

        return mDisplayText;
    }


}
