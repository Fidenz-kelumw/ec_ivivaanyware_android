package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lakmal on 6/2/2016.
 */
public class FilterBuildings {

    public FilterBuildings() {
    }

    public List<Buildings> filterBuildings(List<Buildings> buildingsList, String query) {
        List<Buildings> filteredList = new ArrayList<>();

        for (Buildings building : buildingsList) {
            boolean result = StringLikes.like(building.LocationKey.toString(), query);
            if (!result) {
                if (Utility.IsEmpty(building.LocationName)) {
                    result = false;
                } else {
                    result = StringLikes.like(building.LocationName.toString(), query);
                }
            }

            if (result) {
                filteredList.add(building);
            }
        }

        return filteredList;
    }
}
