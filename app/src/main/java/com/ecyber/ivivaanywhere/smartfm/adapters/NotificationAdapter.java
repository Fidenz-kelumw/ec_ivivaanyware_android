package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;

import java.util.List;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.Holder> {

    private List<Notifications> mNotificationsList;
    private Context mContext;
    private NotificationItemClickListener mItemClickListener;

    private static int previousPosition = -1;

    public NotificationAdapter(List<Notifications> notificationsList, Context context, NotificationItemClickListener itemClickListener) {
        mNotificationsList = notificationsList;
        mContext = context;
        mItemClickListener = itemClickListener;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_notification_item, parent, false);
        return new Holder(row);
    }

    public void clear() {
        previousPosition = -1;
        mNotificationsList.clear();
        notifyDataSetChanged();
    }

    public void addItem(Notifications item) {
        mNotificationsList.add(item);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Notifications notify = mNotificationsList.get(position);

        holder.tvFrom.setText(notify.getFrom());
        holder.tvMsg.setText(notify.getMessage());
        holder.tvDetails.setVisibility(View.INVISIBLE);
        holder.imgViewType.setVisibility(View.INVISIBLE);
        //holder.tvMsg.setSingleLine(true);

        if (notify.isHasIcon() == true) {
            Drawable objectTypeIcon = getObjectTypeIcon(notify.getObjectType());
            holder.imgViewType.setBackground(objectTypeIcon);
            holder.tvDetails.setVisibility(View.VISIBLE);
            holder.imgViewType.setVisibility(View.VISIBLE);
        }

        holder.tvMsg.setSingleLine(notify.isExpand() == true ? false : true);

        holder.tvCreatedAt.setText(String.valueOf(Utility.getDateTimeDifferent(notify.getCreatedAt())));
        if (mNotificationsList.get(position).getReadStatus() == Notifications.UNREAD) {
            holder.msgContainer.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_white));
            Drawable drawableBlue = mNotificationsList.get(position).isExpand() == true ? mContext.getResources().getDrawable(R.drawable.ic_blue_arrow_up) : mContext.getResources().getDrawable(R.drawable.ic_blue_arrow_down);
            holder.btnExpand.setImageDrawable(drawableBlue);
            holder.tvMsg.setTextColor(mContext.getResources().getColor(R.color.pure_black));
            holder.tvFrom.setTextColor(mContext.getResources().getColor(R.color.pure_black));

        } else {
            holder.msgContainer.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_black_box_with_border));
            holder.tvMsg.setTextColor(mContext.getResources().getColor(R.color.mdtp_white));
            holder.tvFrom.setTextColor(mContext.getResources().getColor(R.color.mdtp_white));
            Drawable drawableBlack = mNotificationsList.get(position).isExpand() == true ? mContext.getResources().getDrawable(R.drawable.ic_black_arrow_up) : mContext.getResources().getDrawable(R.drawable.ic_black_arrow_down);
            holder.btnExpand.setImageDrawable(drawableBlack);
        }

    }

    private Drawable getObjectTypeIcon(String objectType) {
        Drawable returnDraw = null;
        switch (objectType) {
            case SmartConstants.TYPE_WORK_ORDER:
                returnDraw = mContext.getResources().getDrawable(R.drawable.ic_msg_work_order);
                break;

            case SmartConstants.TYPE_ASSET:
                returnDraw = mContext.getResources().getDrawable(R.drawable.ic_msg_asset);
                break;

            case SmartConstants.TYPE_INCIDENT:
                returnDraw = mContext.getResources().getDrawable(R.drawable.ic_msg_incidents);
                break;

            case SmartConstants.TYPE_INVENTORY:
                returnDraw = mContext.getResources().getDrawable(R.drawable.ic_msg_inventory);
                break;

        }
        return returnDraw;
    }

    @Override
    public int getItemCount() {
        return mNotificationsList == null ? 0 : mNotificationsList.size();
    }

    public void Clear() {
        mNotificationsList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView tvCreatedAt;
        CustomTextView tvDetails;
        LinearLayout msgContainer;
        ImageButton btnExpand;
        ImageView imgViewType;
        CustomTextView tvFrom, tvMsg;

        public Holder(View itemView) {
            super(itemView);
            tvFrom = (CustomTextView) itemView.findViewById(R.id.tv_notify_from);
            btnExpand = (ImageButton) itemView.findViewById(R.id.btn_expand);
            imgViewType = (ImageView) itemView.findViewById(R.id.img_btn_msg_type);
            tvMsg = (CustomTextView) itemView.findViewById(R.id.tv_msg);
            tvDetails = (CustomTextView) itemView.findViewById(R.id.tv_details);
            tvCreatedAt = (CustomTextView) itemView.findViewById(R.id.tv_created_at);
            msgContainer = (LinearLayout) itemView.findViewById(R.id.msg_container);
            tvDetails.setOnClickListener(this);
            btnExpand.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_expand:
                    messageExpand(getAdapterPosition());
                    break;
                case R.id.tv_details:
                    showDetails(getAdapterPosition());
                    break;
            }
        }
    }

    private void showDetails(int adapterPosition) {
        Notifications.updateStatus(mNotificationsList.get(adapterPosition).getId());
        mNotificationsList.get(adapterPosition).setReadStatus(Notifications.READ);
        notifyDataSetChanged();
        mItemClickListener.onClickItem(mNotificationsList.get(adapterPosition));

    }

    private void messageExpand(int position) {
        Notifications notifications = mNotificationsList.get(position);

        if (notifications.getObjectType().isEmpty()) {
            Notifications.updateStatus(notifications.getId());
            notifications.setReadStatus(Notifications.READ);
        }

        if (!mNotificationsList.get(position).isExpand()) {
            if (previousPosition != -1) {
                Notifications preNotifications = mNotificationsList.get(previousPosition);
                preNotifications.setExpand(false);
            }
            previousPosition = position;
        }
        notifications.setExpand(mNotificationsList.get(position).isExpand() == true ? false : true);
        notifyDataSetChanged();
    }

    public interface NotificationItemClickListener {
        void onClickItem(Notifications message);
    }
}
