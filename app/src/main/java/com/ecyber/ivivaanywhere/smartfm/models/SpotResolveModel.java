package com.ecyber.ivivaanywhere.smartfm.models;

/**
 * Created by Choota on 12/6/17.
 */

public class SpotResolveModel {
    private SpotsItem spotItem;
    private boolean isInside;
    private boolean isNotified;
    private double distenceToLocation;

    public SpotResolveModel(SpotsItem spotItem, boolean isInside, boolean isNotified, double distenceToLocation) {
        this.spotItem = spotItem;
        this.isInside = isInside;
        this.isNotified = isNotified;
        this.distenceToLocation = distenceToLocation;
    }

    public SpotsItem getSpotItem() {
        return spotItem;
    }

    public void setSpotItem(SpotsItem spotItem) {
        this.spotItem = spotItem;
    }

    public boolean isInside() {
        return isInside;
    }

    public void setInside(boolean inside) {
        isInside = inside;
    }

    public boolean isNotified() {
        return isNotified;
    }

    public void setNotified(boolean notified) {
        isNotified = notified;
    }

    public double getDistenceToLocation() {
        return distenceToLocation;
    }

    public void setDistenceToLocation(double distenceToLocation) {
        this.distenceToLocation = distenceToLocation;
    }
}
