package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by Choota on 4/27/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "SubObjectDeafultFilter")
public class SubObjectDeafultFilter extends Model {

//    @SerializedName("DisplayText")
//    @Expose
//    @Column(name = "DisplayText")
//    String displayText;
//
//    @SerializedName("Value")
//    @Expose
//    @Column(name = "Value")
//    String value;

    @Column(name = "SelectedValue")
    String selectedValue;

    @Column(name = "NoDataText")
    String noDataText;

    @Column(name = "ObjectKey")
    String objectKey;

    public SubObjectDeafultFilter() {
    }

//    public String getDisplayText() {
//        return displayText;
//    }
//
//    public void setDisplayText(String displayText) {
//        this.displayText = displayText;
//    }
//
//    public String getValue() {
//        return value;
//    }
//
//    public void setValue(String value) {
//        this.value = value;
//    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getNoDataText() {
        return noDataText;
    }

    public void setNoDataText(String noDataText) {
        this.noDataText = noDataText;
    }

    public SubObjectDeafultFilter(String selectedValue, String noDataText, String objectKey) {
        this.selectedValue = selectedValue;
        this.noDataText = noDataText;
        this.objectKey = objectKey;
    }

    public SubObjectDeafultFilter getSubObjectDeafultFilter(String objectKey){
        return new Select()
                .from(SubObjectDeafultFilter.class)
                .where("ObjectKey = ?", objectKey)
                .executeSingle();
    }

}
