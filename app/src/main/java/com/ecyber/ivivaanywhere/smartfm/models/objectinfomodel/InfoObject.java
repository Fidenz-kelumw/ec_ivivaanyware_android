package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.models.Actions;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectItem;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;
import com.ecyber.ivivaanywhere.smartfm.models.PageLinks;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.TypeData;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "InfoObject")
public class InfoObject extends Model{

    @Column(name="ObjectKey")
    public String mObjectKey;

    public List<ObjectInfoModel> mObjectInfo;

    public List<PageLinks> pageLinkses;

    public List<Actions> actions;

    @Column(name = "UpdateObjectInfo")
    public String mUpdateObjectInfo;

    @Column(name = "UpdateConfirmation")
    public String mUpdateConfirmation;

    @Column(name = "HideUpdateButton")
    public String hideUpdateButton;

    @Column(name = "UpdateButtonText")
    public String updateButtonText;

    public InfoObject(String mObjectKey, List<ObjectInfoModel> mObjectInfo, List<PageLinks> pageLinkses, List<Actions> actions, String mUpdateObjectInfo, String mUpdateConfirmation) {
        this.mObjectKey = mObjectKey;
        this.mObjectInfo = mObjectInfo;
        this.pageLinkses = pageLinkses;
        this.actions = actions;
        this.mUpdateObjectInfo = mUpdateObjectInfo;
        this.mUpdateConfirmation = mUpdateConfirmation;
    }

    public InfoObject(String mObjectKey, List<ObjectInfoModel> mObjectInfo, List<PageLinks> pageLinkses, List<Actions> actions, String mUpdateObjectInfo, String mUpdateConfirmation, String hideUpdateButton, String updateButtonText) {
        this.mObjectKey = mObjectKey;
        this.mObjectInfo = mObjectInfo;
        this.pageLinkses = pageLinkses;
        this.actions = actions;
        this.mUpdateObjectInfo = mUpdateObjectInfo;
        this.mUpdateConfirmation = mUpdateConfirmation;
        this.hideUpdateButton = hideUpdateButton;
        this.updateButtonText = updateButtonText;
    }

    public InfoObject() {
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    public List<ObjectInfoModel> getmObjectInfo() {
        return mObjectInfo;
    }

    public void setmObjectInfo(List<ObjectInfoModel> mObjectInfo) {
        this.mObjectInfo = mObjectInfo;
    }

    public List<PageLinks> getPageLinkses() {
        return pageLinkses;
    }

    public void setPageLinkses(List<PageLinks> pageLinkses) {
        this.pageLinkses = pageLinkses;
    }

    public List<Actions> getActions() {
        return actions;
    }

    public void setActions(List<Actions> actions) {
        this.actions = actions;
    }

    public String getmUpdateObjectInfo() {
        return mUpdateObjectInfo;
    }

    public void setmUpdateObjectInfo(String mUpdateObjectInfo) {
        this.mUpdateObjectInfo = mUpdateObjectInfo;
    }

    public String getmUpdateConfirmation() {
        return mUpdateConfirmation;
    }

    public void setmUpdateConfirmation(String mUpdateConfirmation) {
        this.mUpdateConfirmation = mUpdateConfirmation;
    }

    public String getHideUpdateButton() {
        return hideUpdateButton;
    }

    public void setHideUpdateButton(String hideUpdateButton) {
        this.hideUpdateButton = hideUpdateButton;
    }

    public String getUpdateButtonText() {
        return updateButtonText;
    }

    public void setUpdateButtonText(String updateButtonText) {
        this.updateButtonText = updateButtonText;
    }

    public List<InfoObject> getInfoObject(){
        return new Select()
                .from(InfoObject.class)
                .execute();
    }

    public void clearTable(){
        new InputModesModel().clearTable();
        new ObjectInfoModel().clearTable();
        new OptionListModel().clearTable();
        new ValueModel().clearTable();
        new Delete().from(InfoObject.class).execute();
        new Delete().from(PageLinks.class).execute();
        new Delete().from(Actions.class).execute();
        
        new Messages().clearTable();
        new Members().clearTable();
        new CheckList().clearTable();
        new Attachments().clearTable();
        new ObjectLayout().clearTable();
        new ObjectItem().clearTable();
        new TypeData().clearTable();
        new SubObject().clearTable();
        new ObjectURL().clearTable();
    }
}