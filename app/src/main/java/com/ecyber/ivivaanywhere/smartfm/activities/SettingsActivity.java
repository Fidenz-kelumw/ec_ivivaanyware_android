package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.SettingsAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.Settings;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.BuildingsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.DefaultSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.FiltersSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectAttachmentsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetSpotsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.MasterDataSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectTypesSync;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsActivity extends SmartFMBaseActivity implements DefaultSync.DefaultSyncCallback, BuildingsSync.GetBuildingCallBack, ObjectTypesSync.GetObjectCallBack, FiltersSync.GetFiltersCallBack, GetSpotsSync.GetSpotsCallback, MasterDataSync.MasterDataSyncCallback {

    @BindView(R.id.btn_close)
    ImageButton mCloseButton;
    @BindView(R.id.recyclerview_settinfs_sync)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_user)
    TextView mUserName;
    @BindView(R.id.tv_name)
    CustomTextView mAdvanced;
    @BindView(R.id.tv_item_count)
    CustomTextView mCount;
    @BindView(R.id.tv_lastdate)
    CustomTextView mDate;
    @BindView(R.id.tv_version_value)
    CustomTextView mVersionValue;
    @BindView(R.id.tv_version_lbl)
    CustomTextView mVersionLbl;
    @BindView(R.id.tv_build_value)
    CustomTextView mBuildValue;
    @BindView(R.id.tv_build_lbl)
    CustomTextView mBuildLbl;
    @BindView(R.id.lay_advanced_settings)
    RelativeLayout mAdvancedSettings;

    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    private SettingsAdapter settingsAdapter;
    private List<Settings> settingsList;

    //sync settings
    private ColoredSnackbar mColoredSnackbar;
    AccountPermission accountPermission;

    private BuildingsSync buildingsSync;
    private ObjectTypesSync objectTypesSync;
    private FiltersSync filtersSync;
    private DefaultSync mDefaultSync;

    private boolean isDownloadAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        mColoredSnackBar = new ColoredSnackbar(this);
        mNetwork = new NetworkCheck();
        accountPermission = new AccountPermission();
        mColoredSnackbar = new ColoredSnackbar(this);

        syncConfigClickListener();
        loadSyncConfig();
        loadSettingsFields();

        setInfo();
        buildingsSync = new BuildingsSync(this);
        objectTypesSync = new ObjectTypesSync(this);
        filtersSync = new FiltersSync(this);
        mDefaultSync = new DefaultSync(this, SettingsActivity.this);

        isDownloadAll = false;
    }

    //region OnClickEvents
    @OnClick(R.id.btn_close)
    public void onClickClose(View view) {
        onBackPressed();
    }


    @OnClick(R.id.lay_advanced_settings)
    public void onClickAdvanced(View view) {
        Intent intent = new Intent(this, SettingsAdvancedActivity.class);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_display_top);
    }

    /**
     * create downloads fields into settings Activity
     */
    private void loadSettingsFields() {
        AccountPermission accountPermission = new AccountPermission();
        mUserName.setText(new SmartSharedPreferences(this).getUserId());

        settingsList = new ArrayList<>();

        List<Buildings> buildingsList = new Buildings().getAllBuildings();
        List<ObjectType> objectTypeList = new ObjectType().getAllObjectTypes();
        List<Filter> filterList = new Filter().getAllFilters();

        List<TimeCap> buildingsTimeCap = new TimeCap().getTableLastUpdateTime("Buildings");
        List<TimeCap> objectTypeTimeCap = new TimeCap().getTableLastUpdateTime("ObjectTypes");
        List<TimeCap> filtersTimeCap = new TimeCap().getTableLastUpdateTime("Filters");


        settingsList.add(new Settings("Download All Data", "", ""));
        settingsList.add(new Settings("Buildings", "[" + buildingsList.size() + "]", buildingsTimeCap.get(0).updatedTime));
        settingsList.add(new Settings("Object Types", "[" + objectTypeList.size() + "]", objectTypeTimeCap.get(0).updatedTime));
        settingsList.add(new Settings("Filters", "[" + filterList.size() + "]", filtersTimeCap.get(0).updatedTime));
        settingsList.add(new Settings("Default Data", "", TimeCap.getTableUpdatedTime(SmartConstants.DEFAULT)));

        settingsAdapter = new SettingsAdapter(settingsList, this);
        mRecyclerView.setAdapter(settingsAdapter);

        // to set customsize to recyclerview using its item count
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mRecyclerView.getLayoutParams();
        params.height = (int) (settingsList.size() * (new DisplayPixelCalculator().dipToPixels(this, 35.5f)));
        mRecyclerView.setLayoutParams(params);
    }

    private void loadSyncConfig() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        mAdvanced.setText("Advanced Settings");
        mCount.setText("");
        mDate.setText("");
    }

    private void syncConfigClickListener() {


        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener((recyclerView, position, v) -> {
            boolean state = mNetwork.NetworkConnectionCheck(SettingsActivity.this);
            if (state) {
                switch (position) {
                    case 0:
                        isDownloadAll = true;
                        mColoredSnackbar.showSnackBar("Downloading master data..", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
                        new TimeCap().updateTableTime(SmartConstants.ALL);
                        new MasterDataSync(this, SettingsActivity.this).getMasterData(true);
                        removeDiskCacheBeforeWeek();
                        break;
                    case 1:
                        isDownloadAll = false;
                        mColoredSnackbar.showSnackBar("Downloading Building list..", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
                        buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), false);
                        break;
                    case 2:
                        isDownloadAll = false;
                        mColoredSnackbar.showSnackBar("Downloading Object Types..", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);

                        objectTypesSync.getObjectTypes(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
                        break;
                    case 3:
                        isDownloadAll = false;
                        mColoredSnackbar.showSnackBar("Downloading Filers..", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
                        filtersSync.getFilters(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
                        break;
                    case 4:
                        isDownloadAll = false;
                        mColoredSnackbar.showSnackBar("Downloading Default..", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
                        mDefaultSync.getDefaultData(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
                        break;
                }
            } else {
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);

            }

        });


    }

    private void removeDiskCacheBeforeWeek() {
        new GetObjectAttachmentsSync(this).RemoveDiskCacheBeforeWeek();
    }

    private void setInfo() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionName = pInfo.versionName;
        int versionCode = pInfo.versionCode;

        mVersionLbl.setText("App version");
        mVersionValue.setText("" + versionName);
        mBuildLbl.setText("Build version");
        mBuildValue.setText("" + versionCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 5) {
            finish();
            overridePendingTransition(R.anim.keep_active, R.anim.slide_display_top);
        } else if (resultCode == 6) {
            setResult(6);
            finish();
            overridePendingTransition(R.anim.keep_active, R.anim.slide_display_top);
        }
    }

    //region sync data callbacks
    @Override
    public void onBuildingsDownload(Buildings buildings) {

    }

    @Override
    public void onBuildingsDownloadComplete() {
        if (!isDownloadAll) {
            mColoredSnackbar.dismissSnacBar();
            mColoredSnackbar.showSnackBar("Downloading completed", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        }
        loadSettingsFields();
    }

    @Override
    public void onBuildingsDownloadError(String error) {
        if (!isDownloadAll) {
            mColoredSnackbar.dismissSnacBar();
            mColoredSnackbar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onObjectTypesDownload(ObjectType objectType) {

    }

    @Override
    public void onObjectTypesDownloadComplete() {
        if (!isDownloadAll) {
            mColoredSnackbar.dismissSnacBar();
            mColoredSnackbar.showSnackBar("Downloading completed", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        }
        loadSettingsFields();
    }

    @Override
    public void onObjectTypesDownloadError(String error) {
        if (!isDownloadAll) {
            mColoredSnackbar.dismissSnacBar();
            mColoredSnackbar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onFiltersDownload(Filter filter) {
    }

    @Override
    public void onFiltersDownloadComplete() {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar("Downloading completed", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        loadSettingsFields();
    }

    @Override
    public void onFiltersDownloadError(String error) {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onDefaultDataSuccess(DefaultResponse defaultResponse) {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar("Downloading completed", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        loadSettingsFields();
    }

    @Override
    public void onDefaultDataError(String message) {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onSpotsFound(boolean status, List<SpotsItem> spots) {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar((status ? "Downloading completed" : "Something went wrong"), (status ? ColoredSnackbar.TYPE_OK : ColoredSnackbar.TYPE_ERROR), Snackbar.LENGTH_SHORT);
        loadSettingsFields();
    }

    @Override
    public void onMasterDataSyncComplete() {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar("Downloading completed", ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        loadSettingsFields();
    }

    @Override
    public void onMasterDataSyncFailed(String error) {
        mColoredSnackbar.dismissSnacBar();
        mColoredSnackbar.showSnackBar("Something went wrong", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        loadSettingsFields();
    }
    //endregion


}