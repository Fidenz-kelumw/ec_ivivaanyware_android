package com.ecyber.ivivaanywhere.smartfm.services.firebaseservices;

import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateFireBaseSync;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Lakmal on 7/11/2016.
 */
 public class FMIDService extends FirebaseInstanceIdService {

    private static final String TAG = "WP Firebase";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // Reset Firebase Token flag.
        new SmartSharedPreferences(this).setFirebaseTokenStatus(false);

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        try {
            UpdateFireBaseSync fireBaseSync = new UpdateFireBaseSync();
            AccountPermission permission = new AccountPermission();

            fireBaseSync.UpdateFirebaseToken(permission.getAccountInfo().get(0),
                    permission.getAccountInfo().get(1),
                    permission.getAccountInfo().get(2), token,
                    this);
        } catch (Exception e) {

        }
    }
}
