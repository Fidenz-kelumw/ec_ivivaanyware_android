package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.FiltersAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.popup.BuildingDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectsSync;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterActivity extends SmartFMBaseActivity implements BuildingDialog.LocationCallback {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.btn_select_buildings)
    Button mSelectBuildings;

    @BindView(R.id.recyclerview_filters)
    RecyclerView mRecyclerViewFilters;

    @BindView(R.id.img_btn_right)
    ImageButton imgBtnRight;

    private String mLocationKey = "";
    private String mLocationName = "";
    private String mObjectType;
    private String mMenuName;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;
    List<Filter> filters;
    BuildingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        ButterKnife.bind(this);
        mNetwork = new NetworkCheck();
        mColoredSnackBar = new ColoredSnackbar(this);
        mObjectType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);

        ObjectType objectType = new Select().from(ObjectType.class).where("ObjectType=?", mObjectType).executeSingle();
        mMenuName = objectType.MenuName;

        String createPage = objectType.getCreatePage();
        if (createPage.equals("0")) {
            imgBtnRight.setVisibility(View.GONE);
        }
        imgBtnRight.setBackgroundResource(R.drawable.ic_nav_create_pg);
        title.setText(mMenuName +  " - Filters");
        dialog = new BuildingDialog(this, true);

        loadFilters();
        filtersClickSupport();
    }

    //region OnClickEvents
    @OnClick(R.id.btn_action_left)
    public void onClickBack(View view) {
        onBackPressed();
    }

    /**
     * click event of select location button
     * open a location dialog
     *
     * @param view
     */
    @OnClick(R.id.btn_select_buildings)
    public void onClickSelectBuildings(View view) {
        if (NetworkCheck.IsAvailableNetwork(this)) {
            dialog.showBuildingListDialog(SmartConstants.FROM_FILTER_ACTIVITY, this);
        }
    }

    /**
     * create button click event
     * open create page
     *
     * @param v
     */
    @OnClick(R.id.img_btn_right)
    public void onClickCreatePageButton(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            Intent intent = new Intent(getApplicationContext(), CreateObjectActivity.class);
            intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
            intent.putExtra(SmartConstants.LOCATION_KEY, dialog.getLocationKey());
            intent.putExtra(SmartConstants.LOCATION_NAME, mLocationName);
            ArrayList<FieldDataSerializable> fieldDataList = new ArrayList<FieldDataSerializable>();
            intent.putExtra(SmartConstants.CUSTOM_FILTER, fieldDataList);
            intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }
    //endregion

    /**
     * loading filter list from database
     */
    private void loadFilters() {
        mRecyclerViewFilters.setHasFixedSize(true);
        mRecyclerViewFilters.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewFilters.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        if (!mObjectType.isEmpty()|| mObjectType!=null) {
            filters = new Filter().getAllFiltersByType(mObjectType);
        }

        FiltersAdapter adapter = new FiltersAdapter(filters, this);

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;

        //Get default location details
        Buildings defaultLocation = Registration.getDefaultLocation();
        if (defaultLocation != null) {
            mLocationKey = defaultLocation.LocationKey.isEmpty() ? "" : defaultLocation.LocationKey;
            mLocationName = defaultLocation.LocationName.isEmpty() ? "" : defaultLocation.LocationName;
        } else {
            mLocationKey = "";
            mLocationName = "Any Location";
        }

        mSelectBuildings.setText(mLocationName.isEmpty() ? "Select Location" : mLocationName);
        mSelectBuildings.setEnabled(false);

        mRecyclerViewFilters.setAdapter(adapter);
        mRecyclerViewFilters.addItemDecoration(new RecyclerDecorator(this, spanCount, spacing, includeEdge, true));
    }

    /**
     * Filter list itme click event
     */
    private void filtersClickSupport() {

        ItemClickSupport.addTo(mRecyclerViewFilters).setOnItemClickListener((recyclerView, position, v) -> {
            boolean state = mNetwork.NetworkConnectionCheck(FilterActivity.this);
            if (state) {
                ObjectsSync.foreExpireObjectList();
                Intent intent = new Intent(getApplicationContext(), WorkOrderListActivity.class);
                intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
                intent.putExtra(SmartConstants.MENU_NAME, mMenuName);
                intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
                intent.putExtra(SmartConstants.LOCATION_NAME, mLocationName);
                intent.putExtra(SmartConstants.FILTER_ID, filters.get(position).FilterID);
                intent.putExtra(SmartConstants.FILTER_NAME, filters.get(position).FilterName);
                intent.putExtra(SmartConstants.CUSTOM_FILTER, "");
                intent.putExtra(SmartConstants.MORE, "");
                intent.putExtra(SmartConstants.PAGE, "filter");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
            } else {
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((new Filter().getAllFilters()).size() == 0) {
            ObjectsSync.foreExpireObjectList();
            Intent intent = new Intent(getApplicationContext(), WorkOrderListActivity.class);
            intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
            intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
            intent.putExtra(SmartConstants.LOCATION_NAME, "");
            intent.putExtra(SmartConstants.FILTER_ID, "");
            intent.putExtra(SmartConstants.CUSTOM_FILTER, "");
            intent.putExtra(SmartConstants.MORE, "");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
            finish();
        }
    }

    /**
     * Call back event. Select a item on Location dialog list
     *
     * @param locationKey  selected location key
     * @param locationName selected location names
     */
    @Override
    public void onSelectItem(String locationKey, String locationName) {
        mLocationName = locationName;
        mLocationKey = locationKey;
        mSelectBuildings.setText(mLocationName);
    }
}
