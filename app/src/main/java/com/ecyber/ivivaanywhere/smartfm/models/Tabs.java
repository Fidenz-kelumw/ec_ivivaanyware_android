package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
@Table(name = "Tabs")
public class Tabs extends Model{

    @Column(name = "TabType")
    @SerializedName("TabType")
    @Expose
    public String TabType;

    @Column(name = "TabName")
    @SerializedName("TabName")
    @Expose
    public String TabName;

    @Column(name = "TabHide")
    @SerializedName("TabHide")
    @Expose
    public String TabHide;

    @Column(name = "ObjectType")
    public String ObjectType;

    public boolean isNeedForceReload;

    public boolean isSelected;

    public Tabs(String tabType, String tabName, String tabHide, String objectType) {
        TabType = tabType;
        TabName = tabName;
        TabHide = tabHide;
        ObjectType = objectType;
    }

    public Tabs() {
        super();
    }

    public String getTabType() {
        return TabType;
    }

    public void setTabType(String tabType) {
        TabType = tabType;
    }

    public String getTabName() {
        return TabName;
    }

    public void setTabName(String tabName) {
        TabName = tabName;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public String getTabHide() {
        return TabHide;
    }

    public void setTabHide(String tabHide) {
        TabHide = tabHide;
    }

    public boolean isNeedForceReload() {
        return isNeedForceReload;
    }

    public void setNeedForceReload(boolean needForceReload) {
        isNeedForceReload = needForceReload;
    }

    public List<Tabs> getAllTabs(){
        return new Select()
                .from(Tabs.class)
                .execute();
    }

    public void clearTable(){
        new Delete().from(Tabs.class).execute();
    }

    public List<Tabs> getFilteredTabs(String objectType){
        return new Select()
                .from(Tabs.class)
                .where("ObjectType = ?",objectType)
                .execute();
    }
}
