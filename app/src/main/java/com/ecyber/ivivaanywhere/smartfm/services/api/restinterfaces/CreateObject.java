package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CreateObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/6/2016.
 */
public interface CreateObject {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "CreateObject")
    Call<CreateObjectResponse> getCreateObject(@Field("apikey") String apiKey,
                                               @Field("UserKey") String userKey,
                                               @Field("ObjectType") String objectType,
                                               @Field("FieldData") String fieldData);

}
