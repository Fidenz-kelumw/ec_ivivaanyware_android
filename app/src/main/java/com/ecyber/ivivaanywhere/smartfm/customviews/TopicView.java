package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by Lakmal on 5/5/2016.
 */
public class TopicView {

    private Activity mActivity;
    private View mView;
    private CustomTextView mTvTopic;
    private static LinearLayout mContainer;

    public TopicView(Activity activity) {
        mActivity = activity;
        initView();
    }

    public View setTopic(String topic) {
        mTvTopic.setText(topic);
        return mView;
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.view_page_topic, null);
        mTvTopic = (CustomTextView) mView.findViewById(R.id.tv_main_title);
        mContainer = (LinearLayout) mView.findViewById(R.id.action_button_container);
    }

    public static LinearLayout getButtonContainer() {
        return mContainer;
    }
}
