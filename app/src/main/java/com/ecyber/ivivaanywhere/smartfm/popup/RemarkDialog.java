package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class RemarkDialog {
    private Activity mActivity;
    private Dialog dialog;
    private CheckList mCheckItem;
    private int mPosition;

    public RemarkDialog(Activity activity, CheckList checkItem, int position) {
        mCheckItem = checkItem;
        mActivity = activity;
        mPosition = position;
    }

    public void show(final RemarkDialogCallback delegate) {
        final Dialog bDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View view = i.inflate(R.layout.dialog_remark, null);

        final CustomEditTextView edtRemark = (CustomEditTextView) view.findViewById(R.id.et_mlt_edit_area);
        ImageButton btnCancel = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);
        CustomButton btnUpdate = (CustomButton) view.findViewById(R.id.btn_update);

        if (mCheckItem.getmSetItemDescription().equals("0")) {
            btnUpdate.setBackgroundResource(R.drawable.button_rounded_disable_info);
            btnUpdate.setEnabled(false);
        }

        edtRemark.setOnClickListener(v -> edtRemark.setCursorVisible(true));

        edtRemark.setText(mCheckItem.getmDescription());
        btnCancel.setOnClickListener(v -> bDialog.dismiss());

        btnUpdate.setOnClickListener(v -> {
            bDialog.dismiss();
            edtRemark.getText().toString().trim();
            delegate.onClickRemarkUpdate(v, mCheckItem,edtRemark.getText().toString().trim(),mPosition);
        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 40;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        bDialog.setContentView(view);
        bDialog.show();
        bDialog.setCancelable(true);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout((int) x, (int) y);
    }

    public interface RemarkDialogCallback {
        void onClickRemarkUpdate(View v, CheckList checkList, String description, int position);
    }

}
