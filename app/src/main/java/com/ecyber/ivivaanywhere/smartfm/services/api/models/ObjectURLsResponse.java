package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ObjectURLsResponse {
    @SerializedName("ObjectURLs")
    List<ObjectURL> objectURLs;

    public ObjectURLsResponse() {
    }

    public ObjectURLsResponse(List<ObjectURL> objectURLs) {
        this.objectURLs = objectURLs;
    }

    public List<ObjectURL> getObjectURLs() {
        return objectURLs;
    }

    public void setObjectURLs(List<ObjectURL> objectURLs) {
        this.objectURLs = objectURLs;
    }
}
