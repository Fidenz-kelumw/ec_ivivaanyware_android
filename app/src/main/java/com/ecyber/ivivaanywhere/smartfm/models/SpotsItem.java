package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Table(name = "SpotsItem")
public class SpotsItem extends Model{

	@Column(name = "ObjectType")
	@SerializedName("ObjectType")
	private String objectType;

	@Column(name = "ObjectKey")
	@SerializedName("ObjectKey")
	private String objectKey;

	@Column(name = "Lan")
	@SerializedName("Lan")
	private String lan;

	@Column(name = "SpotID")
	@SerializedName("SpotID")
	private String spotID;

	@Column(name = "NotifyNearestSpot")
	@SerializedName("NotifyNearestSpot")
	private String notifyNearestSpot;

	@Column(name = "Lat")
	@SerializedName("Lat")
	private String lat;

	@Column(name = "Distance")
	@SerializedName("Distance")
	private String distance;

	@Column(name = "LocationKey")
	@SerializedName("LocationKey")
	private String locationKey;

	private boolean isInside;

	public SpotsItem() {
	}

	public SpotsItem(String objectType, String objectKey, String lan, String spotID, String notifyNearestSpot, String lat, String distance, String locationKey, boolean isInside) {
		this.objectType = objectType;
		this.objectKey = objectKey;
		this.lan = lan;
		this.spotID = spotID;
		this.notifyNearestSpot = notifyNearestSpot;
		this.lat = lat;
		this.distance = distance;
		this.locationKey = locationKey;
		this.isInside = isInside;
	}

	public void setObjectType(String objectType){
		this.objectType = objectType;
	}

	public String getObjectType(){
		return objectType;
	}

	public void setObjectKey(String objectKey){
		this.objectKey = objectKey;
	}

	public String getObjectKey(){
		return objectKey;
	}

	public void setLan(String lan){
		this.lan = lan;
	}

	public String getLan(){
		return lan;
	}

	public void setSpotID(String spotID){
		this.spotID = spotID;
	}

	public String getSpotID(){
		return spotID;
	}

	public void setNotifyNearestSpot(String notifyNearestSpot){
		this.notifyNearestSpot = notifyNearestSpot;
	}

	public String getNotifyNearestSpot(){
		return notifyNearestSpot;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setDistance(String distance){
		this.distance = distance;
	}

	public String getDistance(){
		return distance;
	}

	public void setLocationKey(String locationKey){
		this.locationKey = locationKey;
	}

	public String getLocationKey(){
		return locationKey;
	}

	public List<SpotsItem> getAllSpots(){
		return new Select()
				.from(SpotsItem.class)
				.execute();
	}

	public void clearTable(){
		new Delete().from(SpotsItem.class).execute();
	}

	public boolean isInside() {
		return isInside;
	}

	public void setInside(boolean inside) {
		isInside = inside;
	}

	@Override
	public String toString() {
		return "SpotsItem{" +
				"objectType='" + objectType + '\'' +
				", objectKey='" + objectKey + '\'' +
				", lan='" + lan + '\'' +
				", spotID='" + spotID + '\'' +
				", notifyNearestSpot='" + notifyNearestSpot + '\'' +
				", lat='" + lat + '\'' +
				", distance='" + distance + '\'' +
				", locationKey='" + locationKey + '\'' +
				", isInside=" + isInside +
				'}';
	}
}