package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LayoutResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.UpdateDroppedPinResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/15/2016.
 */
public interface GetObjectLayout {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectLayout")
    Call<LayoutResponse> getObjectLayout(@Field("apikey") String apiKey,
                                         @Field("UserKey") String userKey,
                                         @Field("ObjectType") String objectType,
                                         @Field("ObjectKey") String objectKey,
                                         @Field("TabName") String tabName);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "UpdateDroppedPin")
    Call<UpdateDroppedPinResponse> getUpdateDroppedPin(@Field("apikey") String apiKey,
                                                       @Field("UserKey") String userKey,
                                                       @Field("ObjectType") String objectType,
                                                       @Field("ObjectKey") String objectKey,
                                                       @Field("TabName") String tabName,
                                                       @Field("X") double x,
                                                       @Field("Y") double y);


}
