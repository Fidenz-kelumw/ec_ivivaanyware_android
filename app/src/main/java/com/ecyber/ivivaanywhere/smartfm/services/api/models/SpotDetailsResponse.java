package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.SerializedName;

public class SpotDetailsResponse {

    @SerializedName("NextPageObjectKey")
    private String nextPageObjectKey;

    @SerializedName("FocusTab")
    private String focusTab;

    @SerializedName("SpotPopupImageURL")
    private String spotPopupImageURL;

    @SerializedName("SpotPopupHeading")
    private String spotPopupHeading;

    @SerializedName("SpotPopupDescription")
    private String spotPopupDescription;

    @SerializedName("NextpageObjectType")
    private String nextpageObjectType;

    @SerializedName("ButtonText")
    private String buttonText;

    @SerializedName("EnableActionButton")
    private String enableActionButton;

    public SpotDetailsResponse() {
    }

    public SpotDetailsResponse(String nextPageObjectKey, String focusTab, String spotPopupImageURL, String spotPopupHeading, String spotPopupDescription, String nextpageObjectType, String buttonText, String enableActionButton) {
        this.nextPageObjectKey = nextPageObjectKey;
        this.focusTab = focusTab;
        this.spotPopupImageURL = spotPopupImageURL;
        this.spotPopupHeading = spotPopupHeading;
        this.spotPopupDescription = spotPopupDescription;
        this.nextpageObjectType = nextpageObjectType;
        this.buttonText = buttonText;
        this.enableActionButton = enableActionButton;
    }

    public void setNextPageObjectKey(String nextPageObjectKey) {
        this.nextPageObjectKey = nextPageObjectKey;
    }

    public String getNextPageObjectKey() {
        return nextPageObjectKey;
    }

    public void setFocusTab(String focusTab) {
        this.focusTab = focusTab;
    }

    public String getFocusTab() {
        return focusTab;
    }

    public void setSpotPopupImageURL(String spotPopupImageURL) {
        this.spotPopupImageURL = spotPopupImageURL;
    }

    public String getSpotPopupImageURL() {
        return spotPopupImageURL;
    }

    public void setSpotPopupHeading(String spotPopupHeading) {
        this.spotPopupHeading = spotPopupHeading;
    }

    public String getSpotPopupHeading() {
        return spotPopupHeading;
    }

    public void setSpotPopupDescription(String spotPopupDescription) {
        this.spotPopupDescription = spotPopupDescription;
    }

    public String getSpotPopupDescription() {
        return spotPopupDescription;
    }

    public void setNextpageObjectType(String nextpageObjectType) {
        this.nextpageObjectType = nextpageObjectType;
    }

    public String getNextpageObjectType() {
        return nextpageObjectType;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getEnableActionButton() {
        return enableActionButton;
    }

    public void setEnableActionButton(String enableActionButton) {
        this.enableActionButton = enableActionButton;
    }

    @Override
    public String toString() {
        return "SpotDetailsResponse{" +
                "nextPageObjectKey='" + nextPageObjectKey + '\'' +
                ", focusTab='" + focusTab + '\'' +
                ", spotPopupImageURL='" + spotPopupImageURL + '\'' +
                ", spotPopupHeading='" + spotPopupHeading + '\'' +
                ", spotPopupDescription='" + spotPopupDescription + '\'' +
                ", nextpageObjectType='" + nextpageObjectType + '\'' +
                ", buttonText='" + buttonText + '\'' +
                ", enableActionButton='" + enableActionButton + '\'' +
                '}';
    }
}