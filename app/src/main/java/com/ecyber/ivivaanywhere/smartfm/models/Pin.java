package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LayoutResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/16/2016.
 */
@Table(name = "Pins")
public class Pin extends Model {

    @Column(name = "Layout_ID")
    public long mLayoutId;

    @Column(name = "Name")
    public String mName;

    @Column(name = "Description")
    public String mDescription;

    @Column(name = "X")
    public String mX;

    @Column(name = "Y")
    public String mY;

    @Column(name = "Color")
    public String mColor;

    @Column(name = "ObjectType")
    public String mObjectType;

    @Column(name = "ObjectKey")
    public String mObjectKey;

    public Pin() {
        super();
    }

    public List<Pin> getAllPin(long layoutId) {
        return new Select().from(Pin.class).where("Layout_ID = ? ", layoutId).execute();
    }

    public long getLayoutId() {
        return mLayoutId;
    }

    public void setLayoutId(long layoutId) {
        mLayoutId = layoutId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getX() {
        return mX;
    }

    public void setX(String x) {
        mX = x;
    }

    public String getY() {
        return mY;
    }

    public void setY(String y) {
        mY = y;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }
}
