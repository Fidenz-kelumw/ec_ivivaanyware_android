package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.StatusResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Choota on 11/28/17.
 */

public interface UpdateNearestSpot {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "NearestSpot")
    Call<StatusResponse> updateNearestSpot(@Field("UserKey") String userKey,
                                           @Field("apikey") String apikey,
                                           @Field("SpotID") String spotID,
                                           @Field("Proximity") String proximity,
                                           @Field("Distance") String distence);
}
