package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectItem;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.TypeData;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class GetObjectItemsSync {
    private Context context;
    private GetObjectItemsCallback callback;

    public GetObjectItemsSync(Context context, GetObjectItemsCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void getObjectItems(String url, String objectKey, String objectType, String userKey, String tabName, String apikey){
        ServiceGenerator.CreateService(GetObjectItems.class,url).getObjectItems(objectKey,objectType,userKey,tabName,apikey).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        new ObjectItem().clearTable();
                        new TypeData().clearTable();

                        new TimeCap().updateTableTime(tabName);
                        new TimeCap().updateTableTime(SmartConstants.OBJECTITEMSBUTTONS);

                        convertResponceData(response.body());
                    }
                } else {
                    try {
                        String message = response.errorBody().string();
                        Log.d("SmartFm - ErrorLog", message);
                        ErrorLog.saveErrorLog("Get Object Items", message);
                        callback.onGetObjectItemsFail("Fetching items failed");
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onGetObjectItemsFail("Fetching items failed");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onGetObjectItemsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
            }
        });
    }

    public void convertResponceData(ResponseBody body){
        try {
            String jsonData = body.string();
            JSONObject objectItem = new JSONObject(jsonData);

            if(objectItem.has("ObjectItems")){
                JSONArray objectItemArray = objectItem.getJSONArray("ObjectItems");
                List<ObjectItem> objectItems = new ArrayList<>();

                for(int i=0; i<objectItemArray.length(); i++){
                    JSONObject currentObject = objectItemArray.getJSONObject(i);
                    ObjectItem item = new ObjectItem();

                    try{
                        item.setItemName(currentObject.getString("ItemName"));
                        item.setItemKey(currentObject.getString("ItemKey"));
                        item.setItemData(currentObject.getString("ItemData"));
                        item.setDataColor(currentObject.getString("DataColor"));
                        item.setItemEdit(currentObject.getString("ItemEdit"));
                        item.setTypeID(currentObject.getString("TypeID"));

                        if(item.getTypeID()!=null){
                            switch (item.getTypeID()){
                                case "VALUE":
                                    try {
                                        JSONObject object = currentObject.getJSONObject("TypeData");

                                        item.setMIN((float) (object.getDouble("MIN") * 1.0));
                                        item.setMAX((float) (object.getDouble("MAX") * 1.0));
                                        item.setINC((float) (object.getDouble("INC") * 1.0));
                                    } catch (Exception e){
                                        continue;
                                    }
                                    break;
                                case "BUTTONS":
                                    try {
                                        if (currentObject.has("TypeData")) {
                                            JSONArray typeArray = currentObject.getJSONArray("TypeData");
                                            List<TypeData> typeDatas = new ArrayList<>();

                                            for (int a = 0; a < typeArray.length(); a++) {
                                                JSONObject jsonObject = typeArray.getJSONObject(a);
                                                TypeData typeData = new TypeData(jsonObject.getString("Name"), jsonObject.getString("Value"));
                                                typeData.ItemKey = item.getItemKey();
                                                typeData.save();

                                                typeDatas.add(typeData);
                                            }

                                            item.setTypeData(typeDatas);
                                        }
                                    } catch (Exception e){
                                        continue;
                                    }
                                    break;
                                case "SWITCH":
                                    break;
                            }
                        }

                        objectItems.add(item);
                        item.save();

                    } catch (Exception e){
                        e.printStackTrace();
                        callback.onGetObjectItemsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                }

                callback.onGetObjectItemsSuccess(objectItems);
            } else {
                callback.onGetObjectItemsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
            }


        } catch (IOException e) {
            e.printStackTrace();
            callback.onGetObjectItemsFail(SmartConstants.TRY_AGAIN_EXCEPTION);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isObjectItemsAvailableInDB(){
        List<ObjectItem> items = new ObjectItem().getAllObjectItems();
        if(items!=null && items.size()>0){
            for (int i = 0; i < items.size(); i++) {
                ObjectItem item = items.get(i);
                item.setTypeData(new TypeData().getAllButtons(item.getItemKey()));
            }
            callback.onGetObjectItemsFromDBSuccess(items);
            return true;
        } else {
            return false;
        }
    }

    public interface GetObjectItemsCallback{
        void onGetObjectItemsSuccess(List<ObjectItem> objectItems);
        void onGetObjectItemsFail(String error);
        void onGetObjectItemsFromDBSuccess(List<ObjectItem> objectItems);
    }
}
