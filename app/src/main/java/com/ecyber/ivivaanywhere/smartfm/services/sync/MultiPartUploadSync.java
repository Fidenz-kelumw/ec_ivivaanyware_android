package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.activeandroid.util.IOUtils;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.ImageUpload;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lakmal on 7/27/16.
 */
public class MultiPartUploadSync {

    public void uploadFile(final AttachmentUploadCallback delegate, String url, String apiKey, String uploadPath, final File attachment, final String attachedName) {
        long length = attachment.length();
        Log.e("FM-M", length + "");
        Log.e("FM-M", "Path:" + uploadPath);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), attachment);
        MultipartBody.Part file = MultipartBody.Part.createFormData("file", attachment.getName(), requestFile);
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), attachedName);

        ServiceGenerator.CreateService(ImageUpload.class, url)
                .uploadMultipartImage(uploadPath, apiKey, file, fileName)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            delegate.attachmentSuccess(attachedName, attachment);
                        } else {
                            delegate.attachmentFailed(attachedName, attachment);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        delegate.attachmentFailed(attachedName, attachment);
                    }
                });
    }

    public interface AttachmentUploadCallback {
        void attachmentSuccess(String fileName, File file);

        void attachmentFailed(String fileName, File file);
    }

}
