package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LayoutResponse;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class PinDialog {
    private Activity mActivity;
    private Dialog dialog;
    private LayoutResponse.Pins mPins;
    private NetworkCheck mNetwork;


    public PinDialog(Activity activity, LayoutResponse.Pins pin) {
        this.mPins = pin;
        mActivity = activity;
        mNetwork = new NetworkCheck();
    }

    public void showConfirmation(String name, String description) {
        final Dialog bDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View view = i.inflate(R.layout.dialog_pin_detail, null);

        CustomTextView tvDescription = (CustomTextView) view.findViewById(R.id.tv_description);
        CustomTextView tvTitle = (CustomTextView) view.findViewById(R.id.tv_title);

        CustomTextView tvTitleLbl = (CustomTextView) view.findViewById(R.id.tv_title_lbl);
        CustomTextView tvDescriptionLbl = (CustomTextView) view.findViewById(R.id.tv_desc_lbl);

        ImageButton btnCancel = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);
        CustomButton btnView = (CustomButton) view.findViewById(R.id.btn_view);
        LinearLayout viewContainer = (LinearLayout) view.findViewById(R.id.li_lay_view_container);

        tvDescription.setText(mPins.getDescription());
        tvTitle.setText(mPins.getName());

        if(name != null && !name.isEmpty())
            tvTitleLbl.setText(name);
        if(description != null && !description.isEmpty())
            tvDescriptionLbl.setText(description);

        if (mPins.getObjectKey()== null || mPins.getObjectType()== null) {
            btnView.setEnabled(false);
        }
        btnCancel.setOnClickListener(v -> bDialog.dismiss());

        if (mPins.getObjectKey().isEmpty() || mPins.getObjectType().isEmpty()) {
            btnView.setEnabled(false);
        }

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state = mNetwork.NetworkConnectionCheck(mActivity);
                if (state) {
                    bDialog.dismiss();
                    Intent intent = new Intent(mActivity, SideNavigationActivity.class);
                    intent.putExtra(SmartConstants.OBJECT_TYPE, mPins.getObjectType());
                    intent.putExtra(SmartConstants.OBJECT_KEY, mPins.getObjectKey());
                    intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
                    mActivity.startActivity(intent);

                    mActivity.overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                } else {
                    Toast.makeText(mActivity, "No connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);

        bDialog.setContentView(view);
        bDialog.show();
        bDialog.setCancelable(true);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout((int) x, WindowManager.LayoutParams.WRAP_CONTENT);
    }
}
