package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ObjectURLsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/6/2016.
 */
public interface ObjectURLs {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectURLs")
    Call<ObjectURLsResponse> getObjectURLs(@Field("ObjectKey") String objectKey,
                                              @Field("ObjectType") String objectType,
                                              @Field("UserKey") String userKey,
                                              @Field("TabName") String tabName,
                                              @Field("apikey") String apikey);
}
