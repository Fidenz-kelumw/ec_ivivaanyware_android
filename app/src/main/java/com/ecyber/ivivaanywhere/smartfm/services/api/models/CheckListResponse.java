package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class CheckListResponse {
    @SerializedName("CheckItems")
    @Expose
    private List<CheckList> mCheckList;

    public CheckListResponse(List<CheckList> mCheckList) {
        this.mCheckList = mCheckList;
    }

    public CheckListResponse() {
    }

    public List<CheckList> getmCheckList() {
        return mCheckList;
    }

    public void setmCheckList(List<CheckList> mCheckList) {
        this.mCheckList = mCheckList;
    }
}

