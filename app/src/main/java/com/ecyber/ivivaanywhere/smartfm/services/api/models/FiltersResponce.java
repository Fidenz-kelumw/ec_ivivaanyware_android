package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class FiltersResponce {

    @SerializedName("Filters")
    @Expose
    public List<Filter> Filters;

    public FiltersResponce(List<Filter> filters) {
        Filters = filters;
    }

    public FiltersResponce() {
    }

    public List<Filter> getFilters() {
        return Filters;
    }

    public void setFilters(List<Filter> filters) {
        Filters = filters;
    }
}
