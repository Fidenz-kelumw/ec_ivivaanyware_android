package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "ObjectInfoModel")
public class ObjectInfoModel extends Model {

    @Column(name = "objectKey")
    public String objectKey;

    @Column(name = "fieldid")
    public String fieldid;

    @Column(name = "fieldname")
    public String fieldname;

    @Column(name = "valuetype")
    public String valuetype;

    @Column(name = "displaytext")
    public String displaytext;

    @Column(name = "editable")
    public String editable;

    @Column(name = "lookupservice")
    public String lookupservice;

    @Column(name = "mandatory")
    public String mandatory;

    @Column(name = "qrcode")
    public String qrcode;

    @Column(name = "style")
    public String style;

    @Column(name = "downloadfolder")
    public String downloadfolder;

    @Column(name = "uploadfolder")
    public String uploadfolder;

    @Column(name = "noDataValue")
    public String noDataValue;

    @Column(name = "disableDelete")
    public String disableDelete;

    @Column(name = "extended")
    public String extended;

    public List<ValueModel> valueModels;

    public List<OptionListModel> optionListModels;

    public String[] inputModes;

    public ObjectInfoModel() {
    }

    public ObjectInfoModel(String objectKey, String fieldid, String fieldname, String valuetype, String displaytext, String editable, String lookupservice, String mandatory, String qrcode, String style, String downloadfolder, String uploadfolder, String noDataValue, String disableDelete, String extended, List<ValueModel> valueModels, List<OptionListModel> optionListModels, String[] inputModes) {
        this.objectKey = objectKey;
        this.fieldid = fieldid;
        this.fieldname = fieldname;
        this.valuetype = valuetype;
        this.displaytext = displaytext;
        this.editable = editable;
        this.lookupservice = lookupservice;
        this.mandatory = mandatory;
        this.qrcode = qrcode;
        this.style = style;
        this.downloadfolder = downloadfolder;
        this.uploadfolder = uploadfolder;
        this.noDataValue = noDataValue;
        this.disableDelete = disableDelete;
        this.extended = extended;
        this.valueModels = valueModels;
        this.optionListModels = optionListModels;
        this.inputModes = inputModes;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getFieldid() {
        return fieldid;
    }

    public void setFieldid(String fieldid) {
        this.fieldid = fieldid;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getValuetype() {
        return valuetype;
    }

    public void setValuetype(String valuetype) {
        this.valuetype = valuetype;
    }

    public String getDisplaytext() {
        return displaytext;
    }

    public void setDisplaytext(String displaytext) {
        this.displaytext = displaytext;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getLookupservice() {
        return lookupservice;
    }

    public void setLookupservice(String lookupservice) {
        this.lookupservice = lookupservice;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDownloadfolder() {
        return downloadfolder;
    }

    public void setDownloadfolder(String downloadfolder) {
        this.downloadfolder = downloadfolder;
    }

    public String getUploadfolder() {
        return uploadfolder;
    }

    public void setUploadfolder(String uploadfolder) {
        this.uploadfolder = uploadfolder;
    }

    public void setValueModels(List<ValueModel> valueModels) {
        this.valueModels = valueModels;
    }

    public List<OptionListModel> getOptionListModels() {
        return optionListModels;
    }

    public void setOptionListModels(List<OptionListModel> optionListModels) {
        this.optionListModels = optionListModels;
    }

    public String getNoDataValue() {
        return noDataValue;
    }

    public void setNoDataValue(String noDataValue) {
        this.noDataValue = noDataValue;
    }

    public String[] getInputModes() {
        return inputModes;
    }

    public String getDisableDelete() {
        return disableDelete;
    }

    public void setDisableDelete(String disableDelete) {
        this.disableDelete = disableDelete;
    }

    public void setInputModes(String[] inputModes) {
        this.inputModes = inputModes;
    }

    public List<ValueModel> getValueModels() {
        return valueModels;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public List<ObjectInfoModel> getObjectInfo(String objectKey){
        return new Select()
                .from(ObjectInfoModel.class)
                .where("objectKey = ?", objectKey)
                .execute();
    }

    public List<ObjectInfoModel> getAll(){
        return new Select()
                .from(ObjectInfoModel.class)
                .execute();
    }

    public void clearTable(){
        new Delete().from(ObjectInfoModel.class).execute();
    }
}
