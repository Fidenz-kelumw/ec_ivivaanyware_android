package com.ecyber.ivivaanywhere.smartfm.services.sync;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SpotDetailsResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetSpotDetails;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 11/28/17.
 */

public class GetSpotDetailsSync {

    private AccountPermission account;

    public void requestSpots(GetSpotDetailsCallback callback, String objectType, String objectKey, String locationKey, String distence) {
        this.account = new AccountPermission();

        ServiceGenerator.CreateService(GetSpotDetails.class, account.getAccountInfo().get(0))
                .getSpotDetails(account.getAccountInfo().get(2), account.getAccountInfo().get(1), objectType, objectKey, locationKey, distence)
                .enqueue(new Callback<SpotDetailsResponse>() {
                    @Override
                    public void onResponse(Call<SpotDetailsResponse> call, Response<SpotDetailsResponse> response) {
                        if (response != null && response.isSuccessful() && response.body() != null) {
                            callback.onSpotDetailsFound(true, response.body());
                        } else {
                            try {
                                String message = response.errorBody().string();
                                ErrorLog.saveErrorLog("Get Spots Details", message);
                                callback.onSpotDetailsFound(false, null);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onSpotDetailsFound(false, null);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SpotDetailsResponse> call, Throwable t) {
                        try {
                            String message = t.getMessage();
                            ErrorLog.saveErrorLog("Get Spot Details", message);
                            callback.onSpotDetailsFound(false, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.onSpotDetailsFound(false, null);
                        }
                    }
                });
    }

    public interface GetSpotDetailsCallback {
        void onSpotDetailsFound(boolean status, SpotDetailsResponse response);
    }
}
