package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/9/2016.
 */
@Table(name="Messages")
public class Messages extends Model{


    @Column(name = "MessageKey")
    @SerializedName("MessageKey")
    @Expose
    public String mMessageKey;

    @Column(name = "MessageText")
    @SerializedName("MessageText")
    @Expose
    public String mMessageText;

    @Column(name = "CreatedAt")
    @SerializedName("CreatedAt")
    @Expose
    public String mCreatedAt;

    @Column(name = "CreatedUser")
    @SerializedName("CreatedUser")
    @Expose
    public User mUser;

    @Column(name = "UserKey")
    public String mUserKey;

    @Column(name = "UserName")
    public String mUserName;

    @Column(name = "ImagePath")
    public String mImagePath;

    @Column(name = "Object_Key")
    @Expose
    public String mObjectKey;

    public Messages() {
        super();
    }

    public Messages(String messageKey, String messageText, String createdAt, User user, String objectKey) {
        mMessageKey = messageKey;
        mMessageText = messageText;
        mCreatedAt = createdAt;
        mUser = user;
        mUserKey = user.mUserKey;
        mUserName = user.mUserName;
        mImagePath = user.mImagePath;
        mObjectKey = objectKey;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public String getMessageKey() {
        return mMessageKey;
    }

    public void setMessageKey(String messageKey) {
        mMessageKey = messageKey;
    }

    public String getMessageText() {
        return mMessageText;
    }

    public void setMessageText(String messageText) {
        mMessageText = messageText;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User createdUser) {
        mUser = createdUser;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getmMessageKey() {
        return mMessageKey;
    }

    public void setmMessageKey(String mMessageKey) {
        this.mMessageKey = mMessageKey;
    }

    public String getmMessageText() {
        return mMessageText;
    }

    public void setmMessageText(String mMessageText) {
        this.mMessageText = mMessageText;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    public String getmUserKey() {
        return mUserKey;
    }

    public void setmUserKey(String mUserKey) {
        this.mUserKey = mUserKey;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmImagePath() {
        return mImagePath;
    }

    public void setmImagePath(String mImagePath) {
        this.mImagePath = mImagePath;
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    public List<Messages> getAllMessages(String objectKey) {
        return new Select().from(Messages.class).where("Object_Key =?",objectKey).execute();
    }

    public List<MessageType> getAllMessageTypes(String objectKey) {
        return new Select().from(MessageType.class).where("Object_Key =?",objectKey).execute();
    }

    public void clearTable() {
        new Delete().from(Messages.class).execute();
        new Delete().from(MessageType.class).execute();
    }
}
