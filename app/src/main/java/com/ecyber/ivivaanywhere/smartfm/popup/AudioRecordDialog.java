package com.ecyber.ivivaanywhere.smartfm.popup;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;

import java.io.IOException;


/**
 * Created by diharaw on 3/6/17.
 */

public class AudioRecordDialog extends AppCompatActivity {

    private Activity mActivity;
    private AudioRecordCallback mDelegate;
    private ImageButton mBtnPlayStop;
    private ImageButton mBtnRecord;
    private ImageButton mBtnReset;
    private Chronometer mRecordTime;
    private CustomButton mBtnSaveFile;
    private boolean mIsRecording;
    private boolean mIsPlaying;
    private Context mContext;
    private String mFileName = "recording.3gp";
    private String mUserId;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    public AudioRecordDialog(Context context, Activity activity, AudioRecordCallback delegate) {
        mActivity = activity;
        mDelegate = delegate;
        mContext = context;
        mUserId = new AccountPermission().getAccountInfo().get(2);
    }

    public void show() {
        final Dialog mAudioDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(mActivity.getApplicationContext());
        View view = inflater.inflate(R.layout.view_audio_dialog, null);

        mIsRecording = false;

        ImageButton btnClose = (ImageButton) view.findViewById(R.id.audio_dialog_close_btn);
        mBtnPlayStop = (ImageButton) view.findViewById(R.id.img_btn_play);
        mBtnRecord = (ImageButton) view.findViewById(R.id.img_btn_record);
        mBtnReset = (ImageButton) view.findViewById(R.id.img_btn_reset);
        mRecordTime = (Chronometer) view.findViewById(R.id.lbl_record_time);
        mBtnSaveFile = (CustomButton) view.findViewById(R.id.btn_save_audio);

        mRecordTime.setOnChronometerTickListener(cArg -> {
            long time = SystemClock.elapsedRealtime() - cArg.getBase();
            int h   = (int)(time /3600000);
            int m = (int)(time - h*3600000)/60000;
            int s= (int)(time - h*3600000- m*60000)/1000 ;
            String hh = h < 10 ? "0"+h: h+"";
            String mm = m < 10 ? "0"+m: m+"";
            String ss = s < 10 ? "0"+s: s+"";
            cArg.setText(hh+":"+mm+":"+ss);
        });

        mRecordTime.setText("00:00:00");

        disableResetButton();
        disablePlayButton();
        enableRecordButton();

        btnClose.setOnClickListener(view1 -> mAudioDialog.dismiss());

        mBtnPlayStop.setOnClickListener(view12 -> {
            if(mIsPlaying) {
                stopPlayback();
            } else {
                startPlayback();
                mBtnPlayStop.setImageResource(R.drawable.stop_player);
                mIsPlaying = true;
                mRecordTime.setBase(SystemClock.elapsedRealtime());
                mRecordTime.start();
            }
        });

        mBtnRecord.setOnClickListener(view13 -> {

            if(mIsRecording) {
                stopRecording();
                mBtnRecord.setImageResource(R.drawable.mic);
                mRecordTime.setBase(SystemClock.elapsedRealtime());
                mRecordTime.stop();
                mRecordTime.setText("00:00:00");
                mIsRecording = false;
            } else {
                startRecording();
                mBtnRecord.setImageResource(R.drawable.stop);
                mRecordTime.setBase(SystemClock.elapsedRealtime());
                mRecordTime.start();
                mIsRecording = true;
            }
        });

        mBtnReset.setOnClickListener(view14 -> {
            enableRecordButton();
            disablePlayButton();
            disableResetButton();
            mRecordTime.stop();
            mRecordTime.setText("00:00:00");
            mBtnSaveFile.setEnabled(false);
        });

        mBtnSaveFile.setOnClickListener(view15 -> {
            if(!Utility.IsEmpty(mFileName)) {
                mDelegate.onSaveAudio(mFileName);
                mAudioDialog.dismiss();
            }
        });

        mAudioDialog.setContentView(view);
        mAudioDialog.show();
        mAudioDialog.setCanceledOnTouchOutside(false);
        mAudioDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 57;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;


        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mAudioDialog.getWindow().setLayout((int)x, ViewGroup.LayoutParams.WRAP_CONTENT);

        mBtnSaveFile.setEnabled(false);
    }

    private void startRecording() {
        enableRecordButton();
        disablePlayButton();
        disableResetButton();

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        mRecorder.setOutputFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mRecorder.prepare();
        } catch (IOException e) {

        }

        mRecorder.start();
    }

    private void stopRecording() {
        disableRecordButton();
        enablePlayButton();
        enableResetButton();

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        mBtnSaveFile.setEnabled(true);
    }

    private void enablePlayButton() {
        mBtnPlayStop.setEnabled(true);
        mBtnPlayStop.setAlpha(1.0f);
    }

    private void disablePlayButton() {
        mBtnPlayStop.setEnabled(false);
        mBtnPlayStop.setAlpha(0.5f);
    }

    private void enableRecordButton() {
        mBtnRecord.setEnabled(true);
        mBtnRecord.setAlpha(1.0f);
    }

    private void disableRecordButton() {
        mBtnRecord.setEnabled(false);
        mBtnRecord.setAlpha(0.5f);
    }

    private void enableResetButton() {
        mBtnReset.setEnabled(true);
        mBtnReset.setAlpha(1.0f);
    }

    private void disableResetButton() {
        mBtnReset.setEnabled(false);
        mBtnReset.setAlpha(0.5f);
    }

    private void startPlayback() {
        disableRecordButton();
        enablePlayButton();
        enableResetButton();

        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mFileName);

            mPlayer.setOnCompletionListener(mp -> stopPlayback());

            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {

        }
    }

    private void stopPlayback() {
        mPlayer.release();
        mPlayer = null;
        mBtnPlayStop.setImageResource(R.drawable.play);
        mIsPlaying = false;
        mRecordTime.setBase(SystemClock.elapsedRealtime());
        mRecordTime.stop();
        mRecordTime.setText("00:00:00");
    }

    public interface AudioRecordCallback {
        void onSaveAudio(String fileName);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

}
