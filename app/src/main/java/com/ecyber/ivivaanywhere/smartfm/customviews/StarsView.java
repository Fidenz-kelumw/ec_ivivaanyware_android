package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

/**
 * Created by diharaw on 3/14/17.
 */

public class StarsView {

    private final View mView;
    private CustomTextView mTitle;
    private Context mContext;
    private String mFieldId;
    private int mPosition;
    private ObjectInfoModel mObjectInfo;
    private ImageButton mStar1;
    private ImageButton mStar2;
    private ImageButton mStar3;
    private ImageButton mStar4;
    private ImageButton mStar5;
    private ImageButton mReset;
    private int mValue;
    private StarCallback mCallback;
    private boolean mIsEditable;

    public StarsView(Context context, StarCallback callback, @ColorInt int color) {
        mContext = context;
        mCallback = callback;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_stars, null);

        mTitle = (CustomTextView) mView.findViewById(R.id.tv_stars_title);
        mTitle.setTextColor(color);

        mStar1 = (ImageButton) mView.findViewById(R.id.btn_star_1);
        mStar2 = (ImageButton) mView.findViewById(R.id.btn_star_2);
        mStar3 = (ImageButton) mView.findViewById(R.id.btn_star_3);
        mStar4 = (ImageButton) mView.findViewById(R.id.btn_star_4);
        mStar5 = (ImageButton) mView.findViewById(R.id.btn_star_5);

        mReset = (ImageButton) mView.findViewById(R.id.btn_reset_stars);

        registerCallbacks();
    }

    public View getView(ObjectInfoModel info, int position) {
        boolean isEditable = info.getEditable().equals("1") ? true : false;
        mFieldId = info.getFieldid();
        mPosition = position;
        mObjectInfo = info;

        setFieldName(info.getFieldname());
        String defVal = "";
        if (info.getValueModels() != null) {
            List<ValueModel> valueModels = info.getValueModels();
            defVal = valueModels.get(0).getValue();
        }

        setDefaultData();
        mIsEditable = info.getEditable().equals("1") ? true : false;
        setEditable();

        return mView;
    }

    private void setFieldName(String fieldName) {
        mTitle.setText(fieldName);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setValue(int value) {
            mValue = value;

            if(mValue == 1) {
                mStar1.setImageResource(R.drawable.img_feedback_star);
                mStar2.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar3.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar4.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar5.setImageResource(R.drawable.img_feedback_star_unselected);
            } else if (mValue == 2) {
                mStar1.setImageResource(R.drawable.img_feedback_star);
                mStar2.setImageResource(R.drawable.img_feedback_star);
                mStar3.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar4.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar5.setImageResource(R.drawable.img_feedback_star_unselected);
            } else if (mValue == 3) {
                mStar1.setImageResource(R.drawable.img_feedback_star);
                mStar2.setImageResource(R.drawable.img_feedback_star);
                mStar3.setImageResource(R.drawable.img_feedback_star);
                mStar4.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar5.setImageResource(R.drawable.img_feedback_star_unselected);
            } else if (mValue == 4) {
                mStar1.setImageResource(R.drawable.img_feedback_star);
                mStar2.setImageResource(R.drawable.img_feedback_star);
                mStar3.setImageResource(R.drawable.img_feedback_star);
                mStar4.setImageResource(R.drawable.img_feedback_star);
                mStar5.setImageResource(R.drawable.img_feedback_star_unselected);
            } else if (mValue == 5) {
                mStar1.setImageResource(R.drawable.img_feedback_star);
                mStar2.setImageResource(R.drawable.img_feedback_star);
                mStar3.setImageResource(R.drawable.img_feedback_star);
                mStar4.setImageResource(R.drawable.img_feedback_star);
                mStar5.setImageResource(R.drawable.img_feedback_star);
            } else {
                mStar1.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar2.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar3.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar4.setImageResource(R.drawable.img_feedback_star_unselected);
                mStar5.setImageResource(R.drawable.img_feedback_star_unselected);
            }
    }

    private void setEditable() {
        mStar1.setEnabled(mIsEditable);
        mStar2.setEnabled(mIsEditable);
        mStar3.setEnabled(mIsEditable);
        mStar4.setEnabled(mIsEditable);
        mStar5.setEnabled(mIsEditable);
        mReset.setEnabled(mIsEditable);
    }

    private void registerCallbacks() {

        mStar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(1);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });

        mStar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(2);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });

        mStar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(3);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });

        mStar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(4);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });

        mStar5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(5);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(0);
                mCallback.onStarValueChanged(mPosition, mFieldId, mValue);
            }
        });
    }

    private void setDefaultData() {
        List<ValueModel> defaultValue = mObjectInfo.getValueModels();

        if(defaultValue.size() > 0) {

            try {
                mValue =  Integer.parseInt(defaultValue.get(0).getValue());
            } catch(NumberFormatException e) {
                mValue = 0;
            }

            setValue(mValue);
        }
    }

    public interface StarCallback {
        void onStarValueChanged(int position, String fieldId, int value);
    }
}
