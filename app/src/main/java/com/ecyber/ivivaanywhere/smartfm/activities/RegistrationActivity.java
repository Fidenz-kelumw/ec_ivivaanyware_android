package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.RegistrationResponce;
import com.ecyber.ivivaanywhere.smartfm.services.sync.RegistrationSync;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends Activity implements RegistrationSync.GetDomainDataCallback, RegistrationSync.RegistrationEvents/*, RegistrationSync.RegistrationV2Events*/ {

    public static final int PICK_QR_REQUEST = 1;
    public static final int PICK_BACK_PRESS = 2;
    @BindView(R.id.edt_domainid)
    EditText tvDomainID;
    @BindView(R.id.edt_userid)
    EditText tvUserID;
    @BindView(R.id.edt_registrationid)
    EditText tvRegID;
    @BindView(R.id.registration_btn_qr)
    ImageButton mRegisterQr;
    @BindView(R.id.tv_version)
    CustomTextView tvVersion;
    @BindView(R.id.progressBarRegistration)
    ProgressBar mProgressBarReg;
    @BindView(R.id.btn_register)
    CustomButton mBtnRegister;

    private RegistrationSync registrationSync;
    private String mUserID;
    private String mDomainName;
    private String mRegCode;
    private ColoredSnackbar mColoredSnackBar;
    private NetworkCheck mNetwork;
    private ProgressDialog mProgressDialog;
    private String mApiKey;
    private String mAccount;
    private String mSSL;
    private boolean isFillByQR = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        createApplicationCacheDirectory();

        mColoredSnackBar = new ColoredSnackbar(this);
        mNetwork = new NetworkCheck();
        registrationSync = new RegistrationSync(this, this);
        Log.e(SplashActivity.class.getSimpleName(), "Starting Registrations");

        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCanceledOnTouchOutside(false);

        setVersionInfo();
    }

    private void setVersionInfo() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionName = pInfo.versionName;
        int versionCode = pInfo.versionCode;

        tvVersion.setText("Version "+versionName+" ("+versionCode+")");
    }

    @OnClick(R.id.btn_register)
    public void onClickRegister(View view) {
        if (isValidDomainId(tvDomainID.getText().toString().trim()) && isValidRegistration(tvRegID.getText().toString().trim()) && isValidUserId(tvUserID.getText().toString().trim())) {
            boolean state = mNetwork.NetworkConnectionCheck(this);
            if (state) {
                invokeRegistrationSync(
                        tvDomainID.getText().toString().trim(),
                        tvUserID.getText().toString().trim(),
                        tvRegID.getText().toString().trim()
                );
            } else {
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }

        } else {
            Toast.makeText(this, SmartConstants.PLEASE_FILL_FIELDS, Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.registration_btn_qr)
    public void onClickQR(View view) {
        Intent intent = new Intent(this, QRScannerActivity.class);
        intent.putExtra(SmartConstants.IS_OBJECT_SCANNER, false);
        startActivityForResult(intent, PICK_QR_REQUEST);
        overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_QR_REQUEST) {
            if (resultCode != PICK_BACK_PRESS && data != null) {
                mUserID = data.getStringExtra(SmartConstants.USER_ID);
                mDomainName = data.getStringExtra(SmartConstants.DOMAIN_ID);
                mRegCode = data.getStringExtra(SmartConstants.REG_CODE);
                mAccount = data.getStringExtra(SmartSharedPreferences.ACCOUNT);
                mApiKey = data.getStringExtra(SmartSharedPreferences.API_KEY);
                mSSL = data.getStringExtra(SmartSharedPreferences.SSL_KEY);

                tvDomainID.setText(mDomainName);
                tvUserID.setText(mUserID);
                tvRegID.setText(mRegCode);
                isFillByQR = true;
                mBtnRegister.performClick();
            }
        }
    }

    @Override
    public void onDomainDataSuccess(DomainData domainData) {
        String token = FirebaseInstanceId.getInstance().getToken();
        registrationSync.signIn(mDomainName, mUserID, mRegCode, domainData.mAccount, domainData.mApikey, Utility.IsEmpty(token) ? "" : token);
    }

    @Override
    public void onDomainDataError(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void invokeRegistrationSync(String domainName, String userId, String regCode) {
        mProgressDialog.show();

        mDomainName = domainName;
        mUserID = userId;
        mRegCode = regCode;
        registrationSync.getDomainData(this, mDomainName, mAccount, mApiKey, mSSL);
    }

    public boolean isValidDomainId(CharSequence target) {
        return !TextUtils.isEmpty(target);
    }

    public boolean isValidUserId(CharSequence target) {
        return !TextUtils.isEmpty(target);
    }

    public boolean isValidRegistration(CharSequence target) {
        return !TextUtils.isEmpty(target);
    }

    /**
     * Login successful callback method
     *
     * @param userRegResponse
     */
    @Override
    public void onLoginSuccess(RegistrationResponce userRegResponse) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        DataDownloadOptimization optimization = new DataDownloadOptimization();
        optimization.initializeTimeCap();
        //Toast.makeText(this, "Welcome " + userRegResponse.getUser().getUserName(), Toast.LENGTH_LONG).show();

        //save user details
        new User(userRegResponse.getUser().getUserKey(), userRegResponse.getUser().getUserName(), userRegResponse.getUser().getImagePath(), "").save();

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(SmartConstants.LOGIN_USER_NAME, userRegResponse.getUser().getUserName());
            intent.putExtra(SmartConstants.ISLOGINNOW, "YES");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
            finish();
        }, 200);
    }

    /**
     * Login unsuccessful call back method
     *
     * @param message
     */
    @Override
    public void onLoginError(String message) {
        mAccount = "";
        mApiKey = "";
        if (isFillByQR) {
            tvDomainID.setText("");
            tvUserID.setText("");
            tvRegID.setText("");
        }
        DomainData.clearTable();
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SmartSharedPreferences preferences = new SmartSharedPreferences(this);
        String userKey = preferences.getUserKey();
        Log.e(RegistrationActivity.class.getSimpleName(), "UserKey =" + userKey);

        if (userKey != null) {
            if (!userKey.isEmpty()) {
                //Save Domain & User data in DB
                registrationSync.updateUserData();

                String oType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);
                String oKey = getIntent().getStringExtra(SmartConstants.OBJECT_KEY);
                String locReqKey = getIntent().getStringExtra(SmartConstants.LOCATION_REQ_KEY);

                long msgId = getIntent().getLongExtra(SmartConstants.MESSAGE_ID, 0);

                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(SmartConstants.OBJECT_TYPE, oType);
                intent.putExtra(SmartConstants.OBJECT_KEY, oKey);
                intent.putExtra(SmartConstants.MESSAGE_ID, msgId);
                intent.putExtra(SmartConstants.LOCATION_REQ_KEY, locReqKey);

                startActivity(intent);
                RegistrationActivity.this.finish();
            }
        }
        setDefaultImageUploadSize();
    }

    private void setDefaultImageUploadSize() {
        SmartSharedPreferences fxSharedPreferences = new SmartSharedPreferences(this);
        if (Utility.IsEmpty(fxSharedPreferences.getResizedImage())) {
            fxSharedPreferences.setResizedImage(SmartConstants.MEDIUM);//set default resized image
        }
    }

    private void createApplicationCacheDirectory(){
        File externalStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), SmartConstants.FILE_SAVE_LOCATION);
        if (!externalStorageDirectory.exists()) {
            externalStorageDirectory.mkdir();
        }
    }
}
