package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class DataUpdateStatus {
    @SerializedName("Success")
    @Expose
    private int mSuccess;

    @SerializedName("Message")
    @Expose
    private String mMessage;

    @SerializedName("PageAction")
    @Expose
    private String mPageAction;

    public String getPageAction() {
        return mPageAction;
    }

    public void setPageAction(String pageAction) {
        mPageAction = pageAction;
    }

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
