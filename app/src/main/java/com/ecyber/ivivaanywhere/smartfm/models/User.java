package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 4/22/2016.
 */
@Table(name = "Users")
public class User extends Model{
    @Column(name = "UserKey")
    @SerializedName("UserKey")
    @Expose
    public String mUserKey;

    @Column(name = "UserName")
    @SerializedName("UserName")
    @Expose
    public String mUserName;

    @Column(name = "ImagePath")
    @SerializedName("ImagePath")
    @Expose
    public String mImagePath;

    @SerializedName("Phone")
    @Expose
    public String mPhone;

    public User() {
        super();
    }

    public User(String mUserKey, String mUserName, String mImagePath, String phone) {
        this.mUserKey = mUserKey;
        this.mUserName = mUserName;
        this.mImagePath = mImagePath;
        this.mPhone = phone;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getUserKey() {
        return mUserKey;
    }

    public void setUserKey(String userKey) {
        mUserKey = userKey;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public User getUser(String userKey) {
        return new Select().from(User.class)
                .where("UserKey = ?", userKey)
                .executeSingle();
    }
}
