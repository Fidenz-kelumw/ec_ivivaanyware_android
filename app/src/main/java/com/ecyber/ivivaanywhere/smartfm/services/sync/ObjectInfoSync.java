package com.ecyber.ivivaanywhere.smartfm.services.sync;

/**
 * Created by Lakmal on 4/29/2016.
 */
public class ObjectInfoSync {

//    private GetObjectInfoCallback mObjectInfoCallback;
//
//    public ObjectInfoSync(GetObjectInfoCallback objectInfoCallback) {
//        mObjectInfoCallback = objectInfoCallback;
//    }
//
//    public void getLookUpData(String url, String apiKey, String userKey, String objType, final String path, String objKey, String fieldData) {
//        ServiceGenerator.CreateService(GetObjectInfo.class, url).getWOCategories(path, apiKey, userKey, objType, objKey, fieldData)
//                .enqueue(new Callback<DataResponse>() {
//                    @Override
//                    public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
//                        if (response.isSuccessful()) {
//                            if (response.body() != null) {
//                                if (response.body().getDataLookUps() != null) {
//                                    if (response.body().getDataLookUps().size() != 0) {
//                                        mObjectInfoCallback.OnGetWOCategoriesSuccess(response.body().getDataLookUps());
//                                    } else {
//                                        mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                                    }
//                                } else {
//                                    mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                                }
//
//
//                            } else {
//                                mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                            }
//                        } else {
//                            try {
//                                String message = response.errorBody().string();
//                                Log.d("SmartFm - ErrorLog", message);
//                                ErrorLog.saveErrorLog("Get " + path, message);
//                                mObjectInfoCallback.OnGetObjectInfoError(message);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                            }
//                        }
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<DataResponse> call, Throwable t) {
//                        mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                    }
//                });
//    }
//
//    public void getObjectInfo(String url, String apiKey, String userKey, String objType, final String objKey) {
//        ServiceGenerator.CreateService(GetObjectInfo.class, url).getObjectInfo(apiKey, userKey, objType, objKey)
//                .enqueue(new Callback<Info>() {
//                    @Override
//                    public void onResponse(Call<Info> call, Response<Info> response) {
//                        if (response.isSuccessful()) {
//                            if (response.body() != null) {
//                                if (response.body().getObjectInfo() != null) {
//                                    if (response.body().getObjectInfo().size() > 0) {
//                                        saveInfoToLocalDB(response.body(), objKey);
//                                        mObjectInfoCallback.OnGetObjectInfoSuccess(response.body());
//                                        Log.e("FM", "ObjectInfo Updated.");
//                                        new TimeCap().updateTableTime(SmartConstants.INFO);
//                                    } else {
//                                        mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                                    }
//
//                                } else {
//                                    mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                                }
//
//                            } else {
//                                mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.ITEM_NOT_FOUND);
//                            }
//                        } else {
//                            try {
//                                String message = response.errorBody().string();
//                                Log.d("SmartFm - ErrorLog", message);
//                                ErrorLog.saveErrorLog("Get Object Info", message);
//                                mObjectInfoCallback.OnGetObjectInfoError(message);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.TRY_AGAIN);
//                            }
//                        }
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<Info> call, Throwable t) {
//                        mObjectInfoCallback.OnGetObjectInfoError(t.getMessage());
//                    }
//                });
//    }
//
//    public void UpdateObjectInfo(String url, String apiKey, String userKey, String objType, final String objKey, String fieldData, final JSONArray jsonfieldData) {
//        ServiceGenerator.CreateService(GetObjectInfo.class, url).getUpdateObjectInfo(apiKey, userKey, objType, objKey, fieldData)
//                .enqueue(new Callback<DataUpdateStatus>() {
//                    @Override
//                    public void onResponse(Call<DataUpdateStatus> call, Response<DataUpdateStatus> response) {
//                        if (response.isSuccessful()) {
//                            if (response.body() != null) {
//                                if (response.body().getSuccess() == 1) {
//                                    ObjectsSync.foreExpireObjectList();
//                                    mObjectInfoCallback.OnGetUpdateObjectSuccess(response.body(), objKey, jsonfieldData);
//                                } else {
//                                    mObjectInfoCallback.OnGetUpdateObjectSuccess(response.body(), objKey, jsonfieldData);
//                                }
//                            } else {
//                                mObjectInfoCallback.OnGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
//                            }
//                        } else {
//                            try {
//                                String message = response.errorBody().string();
//                                Log.d("SmartFm - ErrorLog", message);
//                                ErrorLog.saveErrorLog("Update Object Info", message);
//                                mObjectInfoCallback.OnGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                mObjectInfoCallback.OnGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DataUpdateStatus> call, Throwable t) {
//                        mObjectInfoCallback.OnGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
//                    }
//                });
//
//    }
//
//    public void ExecuteAction(String url, String apiKey, String userKey, String objType, String objKey, String actionId, String cargo) {
//        ServiceGenerator.CreateService(GetObjectInfo.class, url).getExcecuteAction(apiKey, userKey, objType, objKey, actionId, cargo)
//                .enqueue(new Callback<DataUpdateStatus>() {
//                    @Override
//                    public void onResponse(Call<DataUpdateStatus> call, Response<DataUpdateStatus> response) {
//                        if (response.isSuccessful()) {
//                            if (response.body() != null) {
//                                //if (response.body().getSuccess() == 1) {
//                                Log.e("FM", response.body().toString());
//                                mObjectInfoCallback.OnExecuteActionSuccess(response.body());
//                                /*} else {
//                                    mObjectInfoCallback.OnGetObjectInfoError(response.body().getMessage());
//                                }*/
//                            } else {
//                                mObjectInfoCallback.OnExecuteActionError(response.body());
//                            }
//                        } else {
//                            try {
//                                String message = response.errorBody().string();
//                                Log.d("SmartFm - ErrorLog", message);
//                                ErrorLog.saveErrorLog("Execute Action", message);
//                                mObjectInfoCallback.OnGetObjectInfoError(message);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                mObjectInfoCallback.OnGetObjectInfoError(SmartConstants.EXECUTE_FAILED);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DataUpdateStatus> call, Throwable t) {
//                        mObjectInfoCallback.OnGetUpdateObjectError(SmartConstants.EXECUTE_FAILED);
//                    }
//                });
//    }
//
//    public interface GetObjectInfoCallback {
//        void OnGetObjectInfoSuccess(Info infoResponse);
//
//        void OnGetObjectInfoError(String error);
//
//        void OnGetWOCategoriesSuccess(List<DataLookUp> dataResponse);
//
//        void OnGetUpdateObjectSuccess(DataUpdateStatus updateStatus, String objectKey, JSONArray jsonfieldData);
//
//        void OnGetUpdateObjectError(String error);
//
//        void OnExecuteActionSuccess(DataUpdateStatus response);
//
//        void OnExecuteActionError(DataUpdateStatus response);
//    }
//
//    private void saveInfoToLocalDB(Info objectInfoRes, String _objectKey) {
//        objectInfoRes.setmObjectKey(_objectKey);
//        long infoID = objectInfoRes.save();
//
//        List<ObjectInfo> objectList = objectInfoRes.getObjectInfo();
//        for (ObjectInfo objectInfo : objectList) {
//
//            DefaultData def = objectInfo.getDefaultData();
//
//            //save to db default value
//            DefaultData defaultData = new DefaultData(def.getDisplayText(), def.getValue());
//            long defID = defaultData.save();
//
//            // save objectInfo to db
//            ObjectInfo objInfo = new ObjectInfo(infoID,
//                    objectInfo.mFieldID,
//                    objectInfo.mFieldName,
//                    objectInfo.getValueType(),
//                    defID,
//                    objectInfo.mEditable,
//                    objectInfo.mLookUpService,
//                    objectInfo.mMandatory,
//                    objectInfo.mDownloadFolder,
//                    objectInfo.mUploadFolder);
//
//            Long objInfoId = objInfo.save();
//
//            // save OptionList to db
//            List<OptionList> optionList = objectInfo.getOptionList();
//            for (OptionList option : optionList) {
//                OptionList opt = new OptionList(objInfoId, option.getDisplayText(), option.getValue());
//                opt.save();
//            }
//
//        }
//
//        //Save PageLinks to DB
//        List<PageLinks> pageLinkList = objectInfoRes.getPageLinks();
//        for (PageLinks pageLinks : pageLinkList) {
//            PageLinks link = new PageLinks(infoID,
//                    pageLinks.getLinkType(),
//                    pageLinks.getLinkName(),
//                    pageLinks.getObjectType(),
//                    pageLinks.getObjectKey());
//            Long linkId = link.save();
//
//            //Save FieldData to DB
//            List<FieldData> fieldDataList = pageLinks.getFieldData();
//            for (FieldData fieldData : fieldDataList) {
//                FieldData data = new FieldData(linkId,
//                        fieldData.getFieldId(),
//                        fieldData.getValue(),
//                        fieldData.getDisplayText());
//                data.save();
//            }
//        }
//
//        //Save Actions to DB
//        List<Actions> actionList = objectInfoRes.getActions();
//        for (Actions actions : actionList) {
//            Actions actn = new Actions(
//                    actions.getActionID(),
//                    actions.getActionName(),
//                    actions.getEnabled(),
//                    actions.getConfirmation(),
//                    actions.getCargo());
//            actn.save();
//        }
//    }
//
//    public Info getInfoDetailFromDb(Info info) {
//        long infId = info.getId();
//
//        int nUpdateObjectInfo = info.getUpdateObjectInfo();
//        int nUpdateConfirmation = info.getUpdateConfirmation();
//        List<ObjectInfo> newObjectInfoArray = new ArrayList<>();
//        List<PageLinks> newPageLinksArray = new ArrayList<>();
//        List<Actions> newActionsArray = new ArrayList<>();
//
//        //ObjectInfo
//        List<ObjectInfo> objectInfoList = new Select().from(ObjectInfo.class).where("InfoID = ?", infId).execute();
//        for (ObjectInfo objectInfo : objectInfoList) {
//            long nInfoID = objectInfo.getId();
//
//            DefaultData nDefaultData = new Select().from(DefaultData.class).where("Id = ?", objectInfo.getDefaultDataID()).executeSingle();
//
//            List<OptionList> nOptionListArray = new ArrayList<>();
//
//            List<OptionList> optionLists = new Select().from(OptionList.class).where("ObjectInfoId = ?", nInfoID).execute();
//            for (OptionList optionList : optionLists) {
//                OptionList newOption = new OptionList();
//
//                newOption.setDisplayText(optionList.getDisplayText());
//                newOption.setValue(optionList.getValue());
//                nOptionListArray.add(newOption);
//
//            }
//
//            ObjectInfo newObjectInfo = new ObjectInfo();
//            newObjectInfo.setFieldID(objectInfo.getFieldID());
//            newObjectInfo.setFieldName(objectInfo.getFieldName());
//            newObjectInfo.setValueType(objectInfo.getValueType());
//            newObjectInfo.setDefaultData(nDefaultData);
//            Log.e("FM", nDefaultData.getDisplayText() + "?" + nDefaultData.getValue());
//            newObjectInfo.setEditable(objectInfo.getEditable());
//            newObjectInfo.setOptionList(nOptionListArray);
//            newObjectInfo.setLookUpService(objectInfo.getLookUpService());
//            newObjectInfo.setMandatory(objectInfo.getMandatory());
//            newObjectInfo.setDownloadFolder(objectInfo.getDownloadFolder());
//            newObjectInfo.setUploadFolder(objectInfo.getUploadFolder());
//
//            newObjectInfoArray.add(newObjectInfo);
//
//        }
//
//        //PageLinks
//        List<PageLinks> linksList = new Select().from(PageLinks.class).where("InfoID = ?", infId).execute();
//        for (PageLinks pageLinks : linksList) {
//            long linkId = pageLinks.getId();
//
//            List<FieldData> newFieldDataArray = new ArrayList<>();
//
//            List<FieldData> fieldDataList = new Select().from(FieldData.class).where("PageLinkID = ?", linkId).execute();
//            for (FieldData fieldData : fieldDataList) {
//                FieldData newField = new FieldData();
//                newField.setFieldId(fieldData.getFieldId());
//                newField.setValue(fieldData.getValue());
//                newField.setDisplayText(fieldData.getDisplayText());
//                newFieldDataArray.add(newField);
//            }
//
//            PageLinks newPageLinks = new PageLinks();
//            newPageLinks.setLinkType(pageLinks.getLinkType());
//            newPageLinks.setLinkName(pageLinks.getLinkName());
//            newPageLinks.setObjectKey(pageLinks.getObjectKey());
//            newPageLinks.setObjectType(pageLinks.getObjectType());
//            newPageLinks.setFieldData(newFieldDataArray);
//
//            newPageLinksArray.add(newPageLinks);
//        }
//
//        // Actions
//        List<Actions> actionsList = new Select().from(Actions.class).where("InfoID = ?", infId).execute();
//        for (Actions actions : actionsList) {
//
//
//            Actions newAction = new Actions();
//            newAction.setActionID(actions.getActionID());
//            newAction.setActionName(actions.getActionName());
//            newAction.setEnabled(actions.getEnabled());
//            newAction.setConfirmation(actions.getConfirmation());
//            newAction.setCargo(actions.getCargo());
//
//            newActionsArray.add(newAction);
//        }
//
//        Info newInfo = new Info();
//        newInfo.setUpdateObjectInfo(nUpdateObjectInfo);
//        newInfo.setUpdateConfirmation(nUpdateConfirmation);
//        newInfo.setObjectInfo(newObjectInfoArray);
//        newInfo.setPageLinks(newPageLinksArray);
//        newInfo.setActions(newActionsArray);
//
//        return newInfo;
//    }
//
//    public void updateInfoObjectInfoDetails(String objectKey, JSONArray fieldData) {
//        List<ObjectInfo> objectInfoList = new Info().getObjectInfoByObjKey(objectKey);
//
//        for (ObjectInfo objectInfo : objectInfoList) {
//            if (fieldData != null) {
//                for (int i = 0; i < fieldData.length(); i++) {
//                    try {
//                        String fieldID = fieldData.getJSONObject(i).get("FieldID").toString();
//                        if (fieldID.equals(objectInfo.getFieldID())) {
//                            String value = fieldData.getJSONObject(i).get("Value").toString();
//                            String displayText = fieldData.getJSONObject(i).get("DisplayText").toString();
//
//                            Long id = objectInfo.getDefaultDataID();
//
//                            new Update(DefaultData.class)
//                                    .set("Value = ?,DisplayText=?", value, displayText)
//                                    .where("id = ?", id)
//                                    .execute();
//                        }
//
//                    } catch (Exception e) {
//
//                    }
//
//                }
//            }
//
//
//        }
//    }


}
