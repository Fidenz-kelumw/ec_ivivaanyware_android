package com.ecyber.ivivaanywhere.smartfm.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/23/2016.
 */
public class FilterFields {

    @SerializedName("FilterFields")
    @Expose
    public List<ObjectInfo> mFilterFieldsList;

    public List<ObjectInfo> getFilterFieldsList() {
        return mFilterFieldsList;
    }

    public void setFilterFieldsList(List<ObjectInfo> filterFieldsList) {
        mFilterFieldsList = filterFieldsList;
    }
}
