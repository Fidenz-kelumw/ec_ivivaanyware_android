package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by lakmal on 8/30/16.
 */
public class SmartSharedPreferences {

    private SharedPreferences mPreferences;
    private Context mContext;

    public static final String APPLICATION_NAME = "appName";
    public static final String ACCOUNT = "account";
    public static final String API_KEY = "apiKey";
    public static final String SSL_KEY = "sslKey";
    public static final String IVIVA_STATUS = "ivivaStatus";
    public static final String INVALID_STRING = "";

    private static final String USER_KEY = "userKey";
    private static final String USER_ID = "userId";
    private static final String USER_NAME = "userName";
    private static final String USER_ACCOUNT_IMAGE = "accountImage";
    private static final String USER_DOMAIN = "userDomain";
    private static final String RESIZE_IMAGE = "resize_image";
    private static final String FIREBASE_TOKEN_STATUS = "firebase_token";

    private static final String NOTIFY_KEY = "notify_key";
    private static final String NEAREST_SPOT_UPDTE_DURATION = "nearest_spot_update_duration";
    private static final String LOCATION_SYNC_DURATION = "location_sync_duration";
    private static final String LOCATION_SYNC_TOGGLE = "location_sync_toggle";

    public SmartSharedPreferences(Context context) {
        mContext = context;
        getSharedPreferences();
    }

    private void getSharedPreferences() {
        final String appName = mContext.getString(R.string.app_name);
        mPreferences = mContext.getSharedPreferences(appName, Context.MODE_PRIVATE);
    }

    public void clearSharefPref() {
        SharedPreferences.Editor edit = mPreferences.edit();
        edit.clear();
        edit.commit();
    }

    public void setFirebaseTokenStatus(boolean status) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(FIREBASE_TOKEN_STATUS, status);
        editor.commit();
    }

    public boolean getFirebaseTokenStatus() {
        return mPreferences.getBoolean(FIREBASE_TOKEN_STATUS, false);
    }

    public void setAccount(String account) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(ACCOUNT, account);
        editor.commit();
    }

    public String getAccount() {
        return mPreferences.getString(ACCOUNT, "");
    }

    public void setApiKey(String apiKey) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(API_KEY, apiKey);
        editor.commit();
    }

    public String getApiKey() {
        return mPreferences.getString(API_KEY, "");
    }

    public void setSSLKey(String sslKey) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(SSL_KEY, sslKey);
        editor.commit();
    }

    public String getSSLKey() {
        return mPreferences.getString(SSL_KEY, "");
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public String getUserId() {
        return mPreferences.getString(USER_ID, "");
    }

    public void setIvivaStatus(int ivivaStatus) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(IVIVA_STATUS, ivivaStatus);
        editor.commit();
    }

    public int getIvivaStatus() {
        return mPreferences.getInt(IVIVA_STATUS,1);
    }

    public void setUserKey(String userKey) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_KEY, userKey);
        editor.commit();
    }

    public String getUserKey() {
        return mPreferences.getString(USER_KEY, INVALID_STRING);
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getUserName() {
        return mPreferences.getString(USER_NAME, "");
    }

    public void setUserAccountImage(String accountImage) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_ACCOUNT_IMAGE, accountImage);
        editor.commit();
    }

    public String getUserAccountImage() {
        return mPreferences.getString(USER_ACCOUNT_IMAGE, "");
    }

    public void setUserDomain(String userDomain) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_DOMAIN, userDomain);
        editor.commit();
    }

    public String getUserDomain() {
        return mPreferences.getString(USER_DOMAIN, "");
    }

    public void setNotifyID(String msgKey) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(NOTIFY_KEY, msgKey);
        editor.commit();
    }

    public String getNotifyID() {
        return mPreferences.getString(NOTIFY_KEY, "");
    }


    public void setResizedImage(String size) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(RESIZE_IMAGE, size);
        editor.commit();
    }

    public String getResizedImage() {
        return mPreferences.getString(RESIZE_IMAGE, "");
    }

    public void setLookupService(String lookupService) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(SmartConstants.LOOKUPSERVICE, lookupService);
        editor.commit();
    }

    public String getLookupService() {
        return mPreferences.getString(SmartConstants.LOOKUPSERVICE, "");
    }

    public void setNearestSpotUpdateDurarion(String duration) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(NEAREST_SPOT_UPDTE_DURATION, (duration != null ? duration:"30"));
        editor.commit();
    }

    public String getNearestSpotUpdateDurarion() {
        return mPreferences.getString(NEAREST_SPOT_UPDTE_DURATION, "30");
    }

    public void setLocationSyncDurarion(String duration) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(LOCATION_SYNC_DURATION, (duration != null ? duration:"10"));
        editor.commit();
    }

    public String getLocationSyncDurarion() {
        return mPreferences.getString(LOCATION_SYNC_DURATION, "10");
    }

    public void setIsLocationSyncEnabled(boolean state) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(LOCATION_SYNC_TOGGLE, state);
        editor.commit();
    }

    public boolean isLocationSyncEnabled() {
        return mPreferences.getBoolean(LOCATION_SYNC_TOGGLE, false);
    }

    public void setApplicationName(String name) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(APPLICATION_NAME, (name != null ? name:"Smart FM"));
        editor.commit();
    }

    public String getApplicationName() {
        return mPreferences.getString(APPLICATION_NAME, "Smart FM");

    }
}
