package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lakmal on 5/27/2016.
 */
public class FilterMessages {

    public FilterMessages() {
    }

    public List<Messages> filterMessages(List<Messages> messageList, String query) {
        List<Messages> filteredList = new ArrayList<>();
        for (Messages message : messageList) {
            boolean result = StringLikes.like(message.getMessageText().toString(),query);
            if(!result)
                result = StringLikes.like(message.mUserName.toString(),query);

            if(result){
                filteredList.add(message);
            }
        }
        return filteredList;
    }
}
