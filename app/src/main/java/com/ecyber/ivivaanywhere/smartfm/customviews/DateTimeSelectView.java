package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Lakmal on 5/2/2016.
 */
public class DateTimeSelectView implements View.OnClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Context mContext;
    private FragmentManager mFragmentManager;
    private boolean mIsEnableTime = false;
    private String mDate;
    private CustomButton mBtnSelectDate;
    private String mTime;
    private CustomTextView mTvDateTitle;
    private boolean isFillData;
    private String mFieldId;
    private int mPosition;
    private DateTimeSelectListener mDateTimeSelectListener;

    public DateTimeSelectView(Context context, FragmentManager fragmentManager, DateTimeSelectListener selectListener) {
        mContext = context;
        mFragmentManager = fragmentManager;
        mDateTimeSelectListener = selectListener;
    }

    private String getDateTimeFormat(String dateTime) {
        String dateString = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        try {
            Date dt = new Date();
            dt = dateFormat.parse(dateTime);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            //SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            dateString = newFormat.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;

    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public View getDateTimeView(ObjectInfoModel info, boolean isEnableTime, int position, @ColorInt int color) {
        mPosition = position;
        String defValue = "Select Date";

        mIsEnableTime = isEnableTime;
        mFieldId = info.getFieldid();

        boolean isEditable = info.getEditable().equals("1") ? true : false;
        if (info.getDisplaytext() != null) {
            List<ValueModel> valueModels = info.getValueModels();

            if (info.getDisplaytext().length() > 0 || valueModels.get(0).getValue().length() > 0) {
                defValue = info.getDisplaytext().isEmpty() ? getDateTimeFormat(info.valueModels.get(0).getValue()):info.getDisplaytext();
                defValue = defValue.isEmpty() ? "Select Date" : defValue;
            }
        }

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.view_date_select, null);
        mTvDateTitle = (CustomTextView) view.findViewById(R.id.tv_date_select_title);
        mBtnSelectDate = (CustomButton) view.findViewById(R.id.btn_date_select);

        mTvDateTitle.setTextColor(color);
        mTvDateTitle.setText(info.getFieldname());

        /*try {
            if(!defValue.equals("Select")) {
                DateFormat inputFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
                Date date = inputFormatter.parse(defValue);

                DateFormat outputFormatter = new SimpleDateFormat("dd/MM/yyyy");
                defValue = outputFormatter.format(date);
            }
        } catch (Exception e) {
        }*/

        if (!mIsEnableTime) {
            defValue = defValue.split(" ")[0];
        }

        mBtnSelectDate.setText(defValue);
        mBtnSelectDate.setTag(mPosition);
        mBtnSelectDate.setEnabled(isEditable);
        if (!isEditable) {
            mBtnSelectDate.setBackgroundResource(R.drawable.button_rounded_disable_info);
        }

        mBtnSelectDate.setOnClickListener(this);

        return view;
    }

    public boolean isFillData() {
        return isFillData;
    }

    public void setIsFillData(boolean isFillData) {
        this.isFillData = isFillData;
    }

    @Override
    public void onClick(View v) {

        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        datePickerDialog.show(mFragmentManager, "DatePickerDialog");
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear++;
        mDate = "" + new DecimalFormat("00").format(dayOfMonth) + "/" + new DecimalFormat("00").format(monthOfYear) + "/" + year;
        mBtnSelectDate.setText(mDate);
        mDateTimeSelectListener.onDateTimeSelectTextFocusOut(mPosition, mDate);
        if (mIsEnableTime == true) {
            Calendar now = Calendar.getInstance();
            TimePickerDialog dlg = TimePickerDialog.newInstance(this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true);

            dlg.setStartTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE));
            dlg.setCancelable(false);
            dlg.show(mFragmentManager, "Timepickerdialog");
        }

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        mTime = " " + hourOfDay + ":" + minute + ":00";
        mBtnSelectDate.setText(mDate + mTime);
        mDateTimeSelectListener.onDateTimeSelectTextFocusOut(mPosition, mDate + mTime);
    }

    public interface DateTimeSelectListener {
        void onDateTimeSelectTextFocusOut(int position, String displayText);
    }
}
