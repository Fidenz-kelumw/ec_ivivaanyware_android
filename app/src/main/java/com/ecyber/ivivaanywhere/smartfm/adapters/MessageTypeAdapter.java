package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.MessageType;

import java.util.List;

/**
 * Created by Choota on 3/8/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class MessageTypeAdapter extends RecyclerView.Adapter<MessageTypeAdapter.Holder> {

    private Context mContext;
    private List<MessageType> items;

    public MessageTypeAdapter(Context mContext, List<MessageType> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.widget_single_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.tvItemValue.setText(items.get(position).getTypeName());
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size():0;
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView tvItemValue;

        public Holder(View itemView) {
            super(itemView);
            tvItemValue = (CustomTextView) itemView.findViewById(R.id.tv_single_item_value);
        }
    }

}
