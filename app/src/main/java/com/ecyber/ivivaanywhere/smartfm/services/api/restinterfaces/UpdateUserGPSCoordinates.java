package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Choota on 1/26/18.
 */

public interface UpdateUserGPSCoordinates {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "UpdateUserGPSCoordinates")
    Call<SuccessResponse> updateUserGPSCoordinates(@Field("UserKey") String userKey,
                                                   @Field("lat") String lat,
                                                   @Field("lang") String lang,
                                                   @Field("apikey") String apikey);
}
