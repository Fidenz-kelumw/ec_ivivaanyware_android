package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by lakmal on 7/27/16.
 */
public interface ImageUpload {

    @Multipart
    @POST("{path}")
    Call<ResponseBody> uploadMultipartImage(@Path("path") String uploadPath,
                                            @Header("Authorization") String apiKey,
                                            @Part MultipartBody.Part file,
                                            @Part("filename") RequestBody filename);
}
