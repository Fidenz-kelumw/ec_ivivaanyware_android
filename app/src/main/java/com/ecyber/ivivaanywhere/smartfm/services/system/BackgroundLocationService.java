package com.ecyber.ivivaanywhere.smartfm.services.system;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateUserGPSCoordinatesSync;

/**
 * Created by Choota on 1/26/18.
 */

public class BackgroundLocationService extends Service implements UpdateUserGPSCoordinatesSync.UpdateUserGPSCoordinateCallback {

    private static final String TAG = "MyLocationService";

    private LocationManager mLocationManager = null;
    private Location mLastLocation;

    private static float MIN_DURATION = 10;
    private static int LOCATION_INTERVAL = (int) MIN_DURATION * 60 * 1000;
    private static final float LOCATION_DISTANCE = 500f;
    private boolean isNotified;
    private boolean isFirstUpdate;

    private LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.PASSIVE_PROVIDER),
            new LocationListener(LocationManager.GPS_PROVIDER)
    };

    @Override
    public void onCreate() {

        Log.i(TAG, "onCreate");
        this.isNotified = true;
        this.isFirstUpdate = true;

        initializeLocationManager();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    mLocationListeners[1]
            );
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
    }

    private void initializeLocationManager() {
        Log.i(TAG, "initializeLocationManager - LOCATION_INTERVAL: " + LOCATION_INTERVAL + " LOCATION_DISTANCE: " + LOCATION_DISTANCE);
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private Location getLastLocation() {
        return mLastLocation;
    }

    private void updateServerUserLocation() {

        String lat, lan;

        lat = String.valueOf(getLastLocation().getLatitude());
        lan = String.valueOf(getLastLocation().getLongitude());

        new UpdateUserGPSCoordinatesSync(this).update(this, lat, lan);
    }

    private class LocationListener implements android.location.LocationListener {

        public LocationListener(String provider) {
            Log.i(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.i(TAG, "onLocationChanged: " + location);

            if (location != null && location.getLatitude() == 0.0 && location.getLongitude() == 0.0) {
                isFirstUpdate = false;
            } else if (location != null) {
                Log.i(TAG, "setIsNotifiedTo - False");
                isNotified = false;
                mLastLocation.set(location);

                try {
                    if (getLastLocation() != null && !isNotified) {
                        isNotified = true;
                        Log.i(TAG, "onLocationChanged: " + getLastLocation());

                        updateServerUserLocation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.i(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.i(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();

        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listener, ignore", ex);
                }
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onUpdateUserGPSCoordinateSuccess() {
        Log.i("UpdateUserGPSCoordinate", "Successfully updated");
        this.isNotified = true;
        Log.i(TAG, "setIsNotifiedTo - True");
    }

    @Override
    public void onUpdateUserGPSCoordinateFail(String message) {
        Log.i("UpdateUserGPSCoordinate", "Failed to updated");
        this.isNotified = false;
        Log.i(TAG, "setIsNotifiedTo - False");
    }
}
