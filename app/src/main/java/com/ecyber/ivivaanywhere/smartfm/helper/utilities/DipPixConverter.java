package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by Chathura Jayanath on 7/16/2015.
 */
public class DipPixConverter {
    public DipPixConverter() {
    }
    public float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }
}
