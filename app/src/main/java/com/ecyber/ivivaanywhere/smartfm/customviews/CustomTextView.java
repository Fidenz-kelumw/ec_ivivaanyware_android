package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by Lakitha on 2/24/16.
 */
public class CustomTextView extends TextView {

    Context context;

    public CustomTextView(Context _context, AttributeSet attrs, int defStyle) {
        super(_context, attrs, defStyle);

        context = _context;
        init(attrs);
    }

    public CustomTextView(Context _context, AttributeSet attrs) {
        super(_context, attrs);
        context = _context;
        init(attrs);

    }

    public CustomTextView(Context _context) {
        super(_context);
        context = _context;
        init(null);
    }

    private void init(AttributeSet attrs) {
        //if (attrs!=null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = "MelbourneRegularBasic.otf";//a.getString(R.styleable.CustomTextView_fontName);
            if (fontName!=null) {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        //}
    }

}

