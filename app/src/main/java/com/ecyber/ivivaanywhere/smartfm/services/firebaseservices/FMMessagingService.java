package com.ecyber.ivivaanywhere.smartfm.services.firebaseservices;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.MainActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.SplashActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by lakmal on 9/1/16.
 */
public class FMMessagingService extends FirebaseMessagingService {

    private Context mContext;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(FMMessagingService.class.getSimpleName(), "From: " + remoteMessage.getFrom());
        mContext = getApplicationContext();

        try {
            getDataForObject(new JSONObject(new Gson().toJson(remoteMessage.getData())), remoteMessage.getNotification().getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDataForObject(JSONObject data, String alert) {
        try {
            String message = alert;

            String from = data.has("F") ? data.getString("F") : "";
            String objectType = data.has("OT") ? data.getString("OT") : "";
            String objectKey = data.has("OK") ? data.getString("OK") : "";
            String locReqKey = data.has("LK") ? data.getString("LK") : "0";

            Intent launchActivity = new Intent(mContext, SideNavigationActivity.class);
            launchActivity.putExtra(SmartConstants.OBJECT_TYPE, objectType);
            launchActivity.putExtra(SmartConstants.OBJECT_KEY, objectKey);
            launchActivity.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
            launchActivity.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
            launchActivity.putExtra(SmartConstants.LOCATION_REQ_KEY, locReqKey);

            if (objectKey.isEmpty() || objectType.isEmpty()) {
                launchActivity = new Intent(mContext, MainActivity.class);
                launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }

            long msgId = 0;
            Registration registration = new Registration();
            List<Registration> registrationList = registration.getAllRegistrations();
            if (registrationList != null) {
                if (registrationList.size() != 0) {
                    //Save to db
                    if (Integer.parseInt(Registration.getInboxSize().InboxSize) != 0) {
                        msgId = /*saveToDB(message, from, objectType, objectKey);*/Notifications.saveToDB(alert, from, objectType, objectKey);
                        //Increment message badge
                        launchActivity.putExtra(SmartConstants.MESSAGE_ID, msgId);
                        broadcastNotify();
                    }

                } else {
                    launchActivity = new Intent(mContext, SplashActivity.class);
                    launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
            } else {
                launchActivity = new Intent(mContext, SplashActivity.class);
                launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }

            launchActivity.putExtra(SmartConstants.MESSAGE_ID, msgId);

            showNotification(message, launchActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void broadcastNotify() {
        Intent broadcaster = new Intent();
        broadcaster.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        broadcaster.setAction("NotifyCatch");
        sendBroadcast(broadcaster);
    }

    private void showNotification(String message, Intent launchActivity) {
        Random rand = new Random();
        int x = rand.nextInt(9999);

        PendingIntent pi = PendingIntent.getActivity(mContext, x, launchActivity, 0);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = {500, 500, 500, 500};
        Notification noti = new NotificationCompat.Builder(mContext)
                .setContentTitle("Smart FM")
                .setSmallIcon(R.drawable.ic_fm_notify)
                .setContentText(message)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(pattern)
                .setSound(alarmSound)
                .setColor(mContext.getResources().getColor(R.color.mdtp_button_color))
                .build();

        android.app.NotificationManager nm = (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(x, noti);
    }

    /*private long saveToDB(String message, String from, String objectType, String objectKey) {

        int size = new Select().from(Notifications.class).execute().size();
        int count = new Select().from(Notifications.class).count();
        if (count >= Integer.parseInt(Registration.getInboxSize().InboxSize)) {
            Notifications model = new Select().from(Notifications.class).orderBy("Id ASC").executeSingle();
            new Delete().from(Notifications.class).where("Id = ?", model.getId()).execute();
        }

        return new Notifications(message, from, objectType, objectKey, Notifications.UNREAD).save();
    }*/

}
