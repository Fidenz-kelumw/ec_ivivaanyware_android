package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/11/2016.
 */
@Table(name = "AttachmentTypes")
public class AttachmentTypesMaster extends Model {

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "AttachmentID")
    @SerializedName("AttachmentID")
    @Expose
    public long mAttachmentID;

    @Column(name = "TypeName")
    @SerializedName("TypeName")
    @Expose
    public String mTypeName;

    @Column(name = "Signature")
    @SerializedName("Signature")
    @Expose
    public int mSignature;

    public AttachmentTypesMaster() {
        super();
    }

    public AttachmentTypesMaster(long attachmentID, String typeName, int signature, String objectKey) {
        mAttachmentID = attachmentID;
        mTypeName = typeName;
        mSignature = signature;
        mObjectKey = objectKey;
    }

    public long getAttachmentID() {
        return mAttachmentID;
    }

    public void setAttachmentID(long attachmentID) {
        mAttachmentID = attachmentID;
    }

    public String getTypeName() {
        return mTypeName;
    }

    public void setTypeName(String typeName) {
        mTypeName = typeName;
    }

    public int getSignature() {
        return mSignature;
    }

    public void setSignature(int signature) {
        mSignature = signature;
    }

    public List<AttachmentTypesMaster> select(String objectKey) {
        return new Select().from(AttachmentTypesMaster.class).where("ObjectKey =?",objectKey).execute();
    }
}
