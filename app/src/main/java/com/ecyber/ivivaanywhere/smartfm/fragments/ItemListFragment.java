package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.ObjectItemsAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectItem;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectItemsSync;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ecyber.ivivaanywhere.smartfm.R.id.btnEditMode;

/**
 * Created by Choota on 3/3/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ItemListFragment extends Fragment implements GetObjectItemsSync.GetObjectItemsCallback {

    private AccountPermission mAccountPermission;
    private ObjectItemsAdapter objectItemsAdapter;
    private GetObjectItemsSync sync;
    private ColoredSnackbar coloredSnackbar;
    private NetworkCheck mNetwork;

    private List<ObjectItem> items;
    private String mObjectType, mObjectKey, mTabName;
    private boolean isInEditMode;
    private boolean isTabForceLoad;

    @BindView(R.id.recycle_item_list)
    RecyclerView recyclerviewListItems;

    @BindView(R.id.progressBar3)
    ProgressBar pb;

    @BindView(R.id.pbRefresh)
    ProgressBar pbRefresh;

    @BindView(R.id.tvNoItemsFound)
    CustomTextView tvNoItemsFound;

    @BindView(btnEditMode)
    ImageButton editMode;

    @BindView(R.id.btnRefresh)
    ImageButton refresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_item_list, container, false);
        ButterKnife.bind(this, rootView);

        mAccountPermission = new AccountPermission();
        mNetwork = new NetworkCheck();

        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        configMainData();
        loadData();

        return rootView;
    }

    public void configMainData() {
        sync = new GetObjectItemsSync(getActivity(), this);
        items = new ArrayList<>();
        coloredSnackbar = new ColoredSnackbar(getActivity());

        recyclerviewListItems.setHasFixedSize(true);
        recyclerviewListItems.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        recyclerviewListItems.addItemDecoration(new RecyclerDecorator(this.getActivity().getApplicationContext(), spanCount, spacing, includeEdge, false));

        ItemClickSupport.addTo(recyclerviewListItems).setOnItemClickListener((recyclerView, position, v) -> {
            if (isInEditMode) {
                objectItemsAdapter.collapseAndAddFadeToOthers(position);
            }
        });
    }

    private void loadData() {

        pb.setVisibility(View.VISIBLE);
        editMode.setVisibility(View.INVISIBLE);
        refresh.setVisibility(View.INVISIBLE);

        if (isTabForceLoad){
            invokeToObjectItemSync();
        } else {
            DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
            if (dataDownloadOptimization.isDataAvailableInTable(ObjectItem.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToObjectItemSync();
                } else {
                    boolean isAvailable = sync.isObjectItemsAvailableInDB();
                    if (!isAvailable) {
                        invokeToObjectItemSync();
                    }
                }
            } else {
                invokeToObjectItemSync();
            }
        }
    }

    private void invokeToObjectItemSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            sync.getObjectItems(mAccountPermission.getAccountInfo().get(0),
                    mObjectKey,
                    mObjectType,
                    mAccountPermission.getAccountInfo().get(2),
                    mTabName,
                    mAccountPermission.getAccountInfo().get(1));
        } else {
            coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            pb.setVisibility(View.INVISIBLE);
            refresh.setVisibility(View.VISIBLE);
            tvNoItemsFound.setVisibility(View.VISIBLE);
            pbRefresh.setVisibility(View.INVISIBLE);
            editMode.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(btnEditMode)
    public void onEditPressed(View view) {
        if (isInEditMode) {
            editMode.setBackgroundResource(R.drawable.ic_edit_normal);
        } else {
            editMode.setBackgroundResource(R.drawable.ic_edit_selected);
        }

        isInEditMode = !isInEditMode;

        if (isInEditMode) {
            if (objectItemsAdapter != null)
                objectItemsAdapter.addFadeView();
        } else {
            if (objectItemsAdapter != null) {
                objectItemsAdapter.removeFadeView();
                objectItemsAdapter.resetCollapseAndFade();
            }
        }
    }

    @OnClick(R.id.btnRefresh)
    public void onRefreshPressed(View view) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            editMode.setVisibility(View.INVISIBLE);
            pbRefresh.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.INVISIBLE);

            editMode.setBackgroundResource(R.drawable.ic_edit_normal);

            if (isInEditMode) {
                if (objectItemsAdapter != null) {
                    objectItemsAdapter.removeFadeView();
                    objectItemsAdapter.resetCollapseAndFade();
                }
            }

            sync.getObjectItems(mAccountPermission.getAccountInfo().get(0),
                    mObjectKey,
                    mObjectType,
                    mAccountPermission.getAccountInfo().get(2),
                    mTabName,
                    mAccountPermission.getAccountInfo().get(1));

            isInEditMode = false;
        } else {
            coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onGetObjectItemsSuccess(List<ObjectItem> objectItems) {
        items.clear();

        if(objectItems != null && objectItems.size()>0) {
            for (ObjectItem objectItem : objectItems) {
                System.out.println(objectItem);
                items.add(objectItem);
            }

            objectItemsAdapter = new ObjectItemsAdapter(getActivity(), objectItems);
            objectItemsAdapter.setCallData(mObjectType,
                    mObjectKey,
                    mTabName,
                    mAccountPermission.getAccountInfo().get(2),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(0));

            recyclerviewListItems.setAdapter(objectItemsAdapter);
            pb.setVisibility(View.INVISIBLE);
            refresh.setVisibility(View.VISIBLE);

            if (objectItems.size() > 0) {
                pbRefresh.setVisibility(View.INVISIBLE);
                editMode.setVisibility(View.VISIBLE);
            } else {
                pbRefresh.setVisibility(View.INVISIBLE);
                tvNoItemsFound.setVisibility(View.VISIBLE);
            }
        } else {
            onGetObjectItemsFail("Fetching items failed");
        }
    }

    @Override
    public void onGetObjectItemsFail(String error) {
        Log.d("Items list", error);
        pb.setVisibility(View.INVISIBLE);
        refresh.setVisibility(View.VISIBLE);
        tvNoItemsFound.setVisibility(View.VISIBLE);
        pbRefresh.setVisibility(View.INVISIBLE);
        editMode.setVisibility(View.INVISIBLE);
        coloredSnackbar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onGetObjectItemsFromDBSuccess(List<ObjectItem> objectItems) {
        onGetObjectItemsSuccess(objectItems);
    }
}
