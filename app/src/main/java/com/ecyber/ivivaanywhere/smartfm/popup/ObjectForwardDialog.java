package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.customviews.DropDownSelectView;

/**
 * Created by Lakmal on 6/1/2016.
 */
public class ObjectForwardDialog implements DropDownSelectView.DropDownSelectTextListener {

    private Activity mActivity;
    private String mMemberKey;
    private Dialog mDialog;
    private ForwardObjectDialogCallback mDelegate;

    public ObjectForwardDialog(Activity activity, ForwardObjectDialogCallback delegate) {
        mActivity = activity;
        mDelegate = delegate;
    }

    public void show(final String objectType, final String objectKey) {
        mMemberKey = "";
        mDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View view = i.inflate(R.layout.dialog_send_message, null);

        final CustomEditTextView edtMessage = (CustomEditTextView) view.findViewById(R.id.edt_message);
        ImageButton btnCancel = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);
        CustomButton btnSend = (CustomButton) view.findViewById(R.id.btn_send);
        LinearLayout viewContainer = (LinearLayout) view.findViewById(R.id.lin_lay_view_container);
        int color = mActivity.getResources().getColor(R.color.mdtp_white);

        View selectDialog = new DropDownSelectView(mActivity, this, true, true, color).getUsersBox(objectType, objectKey, false);
        viewContainer.addView(selectDialog);

        btnCancel.setOnClickListener(v -> mDialog.dismiss());

        btnSend.setOnClickListener(v -> {

            String message = edtMessage.getText().toString().trim();
            if (mMemberKey.isEmpty()) {
                Toast.makeText(mActivity.getApplicationContext(), "Please select a member.", Toast.LENGTH_SHORT).show();
                return;
            }

            mDelegate.onForwardObjectSend(mMemberKey, message, mDialog, objectKey, objectType);

        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 55;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mDialog.setContentView(view);
        mDialog.show();
        mDialog.setCancelable(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().setLayout((int) x, (int) y);

    }

    @Override
    public void onDropDownSelectFocusOut(int position, String displayText, String value) {
        mMemberKey = value;
    }

    public interface ForwardObjectDialogCallback {
        void onForwardObjectSend(String memberKey, String message, Dialog dialogInstance, String objectKEy, String objectType);
    }

}
