package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.UpdateFirebaseResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.RegistrationService;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lakmal on 7/26/16.
 */
public class UpdateFireBaseSync {

    public UpdateFireBaseSync() {

    }

    public void UpdateFirebaseToken(String url, String apiKey, String userKey, String fireBaseToken, final Context context) {
        ServiceGenerator.CreateService(RegistrationService.class, url)
                .UpdateFirebaseToken(apiKey, userKey, fireBaseToken)
                .enqueue(new Callback<UpdateFirebaseResponse>() {
                    @Override
                    public void onResponse(Call<UpdateFirebaseResponse> call, Response<UpdateFirebaseResponse> response) {

                        if(response.isSuccessful()) {
                            if(response.body() != null) {
                                //if(response.body().getSuccess() == 1) {
                                    Log.i(UpdateFireBaseSync.class.getSimpleName(), "Firebase token updated.");
                                    new SmartSharedPreferences(context).setFirebaseTokenStatus(true);
                                //}
                            }
                        }

                        Log.e("Firebase token", FirebaseInstanceId.getInstance().getToken());
                    }

                    @Override
                    public void onFailure(Call<UpdateFirebaseResponse> call, Throwable t) {
                        Log.i(UpdateFireBaseSync.class.getSimpleName(), "Firebase token update failed.");
                    }
                });
    }
}
