package com.ecyber.ivivaanywhere.smartfm.helper.errormanager;


import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;

import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class ErrorUtils {

    public static APIError parseError(Response<?> response) {
        String url = new AccountPermission().getAccountInfo().get(0);
        Converter<okhttp3.ResponseBody, APIError> converter =
                        ServiceGenerator.retrofit(url)
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }
}