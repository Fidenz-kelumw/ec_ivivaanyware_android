package com.ecyber.ivivaanywhere.smartfm.services.system;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

/**
 * Created by Choota on 10/2/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class LocationHelper {

    private static LocationHelperCallback callback;
    private static LocationHelper helper;
    private static Activity context;

    private static LocationManager locationManager;
    private static AlertDialog.Builder dialog;
    private static Location lastLocationFound;

    private static double longitudeGPS, latitudeGPS;
    private static int locationSearchTimer = 100;
    private static boolean isLocatinoFound;

    private final static LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            System.out.println("Location found " + location.toString());
            isLocatinoFound = true;
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();

            callback.onLocationFound(true, longitudeGPS, latitudeGPS, location);
            lastLocationFound = location;
            LocationHelper.requestlocation(false);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            System.out.println("onStatusChanged");
        }

        @Override
        public void onProviderEnabled(String s) {
            System.out.println("onProviderEnabled");
        }

        @Override
        public void onProviderDisabled(String s) {
            System.out.println("onProviderDisabled");
        }
    };

    public static LocationHelper init(Activity appContext, LocationHelperCallback helperCallback) {
        if (helper == null)
            helper = new LocationHelper();

        context = appContext;
        callback = helperCallback;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        lastLocationFound = null;
        isLocatinoFound = false;

        return helper;
    }

    public static boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static void alertEnableLocation() {
        dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to use this feature")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public static void requestlocation(boolean needToEnabled) {
        try {
            if (!needToEnabled) {
                locationManager.removeUpdates(locationListenerGPS);
                System.out.println("Location helper stopped");
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, locationListenerGPS);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListenerGPS);

                new Handler().postDelayed(() -> {
                    if (!isLocatinoFound) {
                        System.out.println("Auto stop reading location");
                        locationManager.removeUpdates(locationListenerGPS);
                        callback.onLocationFound(false, 0.0, 0.0, null);
                    }
                }, (locationSearchTimer * 1000));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface LocationHelperCallback {
        void onLocationFound(boolean status, double lon, double lat, Location location);
    }
}
