package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

/**
 * Created by Lakmal on 5/3/2016.
 */
public class MultiLineEditTextView {

    private View mView;
    private CustomEditTextView mEtMlt;
    private CustomTextView mTvMlt;
    private Context mContext;
    private String mFieldId;
    private int mPosition;
    private MultiLineEditTexListener mMultiLineEditTexListener;

    public MultiLineEditTextView(Context context, MultiLineEditTexListener multiLineEditTexListener, @ColorInt int color) {
        mContext = context;
        mMultiLineEditTexListener = multiLineEditTexListener;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_multi_line_text, null);

        mEtMlt = (CustomEditTextView) mView.findViewById(R.id.et_mlt_edit_area);
        mTvMlt = (CustomTextView) mView.findViewById(R.id.tv_mlt_title);
        mTvMlt.setTextColor(color);
    }

    public View getMultiLineEditView(ObjectInfoModel info, int position) {
        boolean isEditable = info.getEditable().equals("1") ? true : false;
        mFieldId = info.getFieldid();
        mPosition = position;
        String defVal = "";
        if (info.getValueModels() != null) {
            List<ValueModel> valueModels = info.getValueModels();
            ValueModel valueModel = valueModels.get(0);
            //defVal = /*info.getDefaultData().getDisplayText();*/0 < info.getDefaultData().getDisplayText().length() ? info.getDefaultData().getDisplayText() : info.getDefaultData().getValue();
            defVal = valueModel.getValue();
        }
        mEtMlt.setTag(mPosition + "");
        mTvMlt.setText(info.getFieldname());
        setFieldName(info.getFieldname());
        mEtMlt.setText(defVal);
        mEtMlt.setTextColor(Color.BLACK);
        setEnableEditText(isEditable);

        mEtMlt.addTextChangedListener(mTextWatcher);

        return mView;
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            setInfoObjectDisplayText(mEtMlt.getText().toString());
        }
    };

    private void setInfoObjectDisplayText(String displayText) {
        mMultiLineEditTexListener.onMultiLineEditTextFocusOut(mPosition, displayText);
    }

    private void setEnableEditText(boolean isEditable) {
        //mEtMlt.setTextColor(isEditable == false ? mContext.getResources().getColor(R.color.text_disable) :mContext.getResources().getColor(R.color.pure_black));
        mEtMlt.setEnabled(isEditable);
    }

    private void setFieldName(String fieldName) {
        mTvMlt.setText(fieldName);
    }


    public interface MultiLineEditTexListener {
        void onMultiLineEditTextFocusOut(int position, String displayText);
    }
}
