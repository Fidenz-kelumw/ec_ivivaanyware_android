package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 4/29/2016.
 */
@Table(name = "FieldData")
public class FieldData extends Model {
    @Column(name = "PageLinkID")
    public long mPageLinkID;

    @Column(name = "FieldID")
    @SerializedName("FieldID")
    @Expose
    public String mFieldId;

    @Column(name = "Value")
    @SerializedName("Value")
    @Expose
    public String mValue;

    @Column(name = "DisplayText")
    @SerializedName("DisplayText")
    @Expose
    public String mDisplayText;

    public FieldData(long pageLinkID, String fieldId, String value, String displayText) {
        mPageLinkID = pageLinkID;
        mFieldId = fieldId;
        mValue = value;
        mDisplayText = displayText;
    }

    public FieldData() {
        super();
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getDisplayText() {
        return mDisplayText;
    }

    public void setDisplayText(String displayText) {
        mDisplayText = displayText;
    }

    public long getPageLinkID() {
        return mPageLinkID;
    }

    public void setPageLinkID(long pageLinkID) {
        mPageLinkID = pageLinkID;
    }
}


