package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.MessageType;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;
import com.ecyber.ivivaanywhere.smartfm.models.MessagesStatus;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.MessageAddResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectMessages;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/9/2016.
 */
public class MessagesSync {
    private MessagesCallBack mMessagesCallBack;

    public MessagesSync(MessagesCallBack messagesCallBack) {
        mMessagesCallBack = messagesCallBack;
    }

    public void getObjectMessages(String url, String apiKey, String userKey, String objType, final String objectKey, String tabName) {
        ServiceGenerator.CreateService(GetObjectMessages.class, url).getObjectMessage(apiKey, userKey, objType, objectKey, tabName)
                .enqueue(new Callback<MessagesStatus>() {
                    @Override
                    public void onResponse(Call<MessagesStatus> call, Response<MessagesStatus> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getMessagesList() != null) {
                                if (response.body().getMessagesList().size() != 0) {
                                    new Messages().clearTable();
                                    List<Messages> messagesList = response.body().getMessagesList();
                                    for (Messages message : messagesList) {
                                        saveMessageToLocalDB(message, objectKey);
                                        mMessagesCallBack.OnGetMessagesSuccess(message);
                                    }

                                    new TimeCap().updateTableTime(tabName);
                                } else {
                                    new Messages().clearTable();
                                    new TimeCap().updateTableTime(tabName);
                                    mMessagesCallBack.OnGetMessgesError(SmartConstants.NO_MESSAGE);
                                }

                                if (response.body().getMessageTypes().size() != 0) {
                                    List<MessageType> messageTypes = response.body().getMessageTypes();
                                    new Delete().from(MessageType.class).execute();

                                    saveMessageType(messageTypes, objectKey);
                                }
                            } else {
                                mMessagesCallBack.OnGetMessgesError(SmartConstants.TRY_AGAIN_EXCEPTION);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Object Messages", message);
                                mMessagesCallBack.OnGetMessgesError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mMessagesCallBack.OnGetMessgesError(SmartConstants.TRY_AGAIN_EXCEPTION);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MessagesStatus> call, Throwable t) {
                        mMessagesCallBack.OnGetMessgesError(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    private void foreMessageExpire() {
        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        Date daysAgo = new DateTime(date).minusYears(1).toDate();
        String dateToDBD = simpleDateD.format(daysAgo);

        new Update(TimeCap.class)
                .set("UpdatedTime = ?", dateToDBD)
                .where("TableName = ?", SmartConstants.MESSAGES)
                .execute();
    }

    public void addNewMessage(String url, String apiKey, final String userKey, final String objType, final String objectKey, String tabName, final String messageText, final Messages message, String messagetype) {
        ServiceGenerator.CreateService(GetObjectMessages.class, url).addNewMessage(apiKey, userKey, objType, objectKey, tabName, messageText, messagetype)
                .enqueue(new Callback<MessageAddResponse>() {
                    @Override
                    public void onResponse(Call<MessageAddResponse> call, Response<MessageAddResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    mMessagesCallBack.OnAddCommentSuccess(response.body(), userKey, message);
                                    foreMessageExpire();
                                } else {
                                    String message1 = response.body().getMessage();
                                    mMessagesCallBack.OnAddCommentError(Utility.IsEmpty(message1) ? SmartConstants.MESSAGE_SENT_FAILED : message1);
                                }
                            } else {
                                mMessagesCallBack.OnAddCommentError(SmartConstants.MESSAGE_SENT_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Add New Message", message);
                                mMessagesCallBack.OnAddCommentError(SmartConstants.MESSAGE_SENT_FAILED);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mMessagesCallBack.OnAddCommentError(SmartConstants.MESSAGE_SENT_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MessageAddResponse> call, Throwable t) {
                        mMessagesCallBack.OnAddCommentError(SmartConstants.MESSAGE_SENT_FAILED);
                    }
                });
    }

    private void saveMessageToLocalDB(Messages message, String objectKey) {
        User user = message.getUser();
        message.mObjectKey = objectKey;

        message.mUserKey = user.mUserKey;
        message.mUserName = user.mUserName;
        message.mImagePath = user.mImagePath;
        message.save();
    }

    private void saveMessageType(List<MessageType> messageTypes, String objectKey) {
        for (MessageType messageType : messageTypes) {
            messageType.setmObjectKey(objectKey);
            messageType.save();
        }
    }

    public boolean getMessagesFromDb(String objectKey) {
        List<Messages> allMessages = new Messages().getAllMessages(objectKey);
        if (allMessages != null) {
            if (allMessages.size() != 0) {
                for (Messages message : allMessages) {
                    mMessagesCallBack.OnGetMessagesSuccess(message);
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public boolean getMessagesTypesFromDb(String objectKey) {
        List<MessageType> all = new Messages().getAllMessageTypes(objectKey);
        if (all != null) {
            if (all.size() != 0) {
                mMessagesCallBack.onMessageTypesFound(all);
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public interface MessagesCallBack {
        void OnGetMessagesSuccess(Messages messageList);

        void OnGetMessgesError(String message);

        void OnAddCommentSuccess(MessageAddResponse status, String userKey, Messages message);

        void OnAddCommentError(String message);

        void onMessageTypesFound(List<MessageType> messageTypes);
    }
}
