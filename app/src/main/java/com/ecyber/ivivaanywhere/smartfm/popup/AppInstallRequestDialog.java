package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;

/**
 * Created by Choota on 3/19/18.
 */

public class AppInstallRequestDialog {

    private static Context context;
    private static Dialog dialog;
    private static AppInstallRequestDialog requestDialog;

    private static CustomTextView tvMessage;
    private static CustomButton btnInstall;
    private static CustomButton btnCancel;

    public static AppInstallRequestDialog init(Context appContext) {
        context = appContext;

        if (requestDialog == null) {
            requestDialog = new AppInstallRequestDialog();
            dialog = new Dialog(context, R.style.CustomDialog);
        }

        return requestDialog;
    }

    public static void show(String appName, String packageName) {
        dialog.setContentView(R.layout.dialog_app_install_request);
        initView();

        if (packageName == null) {
            tvMessage.setText("Sorry, \""+appName+"\" "+ SmartConstants.APP_NOT_FOUND_ON_DEFAULTS);
            btnInstall.setEnabled(false);
            btnInstall.setTextColor(context.getResources().getColor(R.color.btn_action_disable_color_gray));
        } else {
            tvMessage.setText("\""+appName+"\" "+ SmartConstants.APP_NOT_INSTALLED);
            btnInstall.setEnabled(true);
            btnInstall.setTextColor(context.getResources().getColor(R.color.color_accent));
        }

        btnInstall.setOnClickListener(v -> {
            dismiss();
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET|Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(marketIntent);
        });

        btnCancel.setOnClickListener(v -> dismiss());

        if (dialog != null && !dialog.isShowing()) {
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private static void initView() {
        tvMessage = (CustomTextView) dialog.findViewById(R.id.tvMessage);
        btnInstall = (CustomButton) dialog.findViewById(R.id.btnInstall);
        btnCancel = (CustomButton) dialog.findViewById(R.id.btnCancel);
    }

    private static void dismiss(){
        if(dialog != null && dialog.isShowing())
            dialog.dismiss();
    }
}