package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class NotificationDialog {
    private Activity mActivity;
    private Dialog dialog;
    private Notifications mNotifications;


    public NotificationDialog(Activity activity, Notifications notification) {
        mNotifications = notification;
        mActivity = activity;
    }


    public void show(){
        final Dialog bDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(mActivity);
        View view = i.inflate(R.layout.dialog_notify_detail, null);

        CustomTextView tvMessage = (CustomTextView) view.findViewById(R.id.tv_message);
        CustomTextView tvFrom = (CustomTextView) view.findViewById(R.id.tv_from);
        ImageButton btnCancel = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);
        CustomButton btnaDetail = (CustomButton) view.findViewById(R.id.btn_detail);
        LinearLayout detailContainer = (LinearLayout) view.findViewById(R.id.li_lay_detail_container);

        tvMessage.setText(mNotifications.getMessage());
        tvFrom.setText(mNotifications.getFrom());
        btnCancel.setOnClickListener(v -> bDialog.dismiss());

        if (mNotifications.getObjectKey().isEmpty()|| mNotifications.getObjectType().isEmpty()) {
            btnaDetail.setEnabled(false);
        }

        btnaDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bDialog.dismiss();
                Intent intent = new Intent(mActivity, SideNavigationActivity.class);
                intent.putExtra(SmartConstants.OBJECT_TYPE, mNotifications.getObjectType());
                intent.putExtra(SmartConstants.OBJECT_KEY, mNotifications.getObjectKey());
                intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
                mActivity.startActivity(intent);

                mActivity.overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
            }
        });

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 40;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 95;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        bDialog.setContentView(view);
        bDialog.show();
        bDialog.setCancelable(true);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout((int) x, (int) y);
    }
}
