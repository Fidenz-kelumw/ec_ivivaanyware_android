package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ForwardObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.ForwardObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/23/2016.
 */
public class ForwardSync {

    private Context mContext;
    private ForwardObjectCallback mForwardObjectCallback;

    public ForwardSync(ForwardObjectCallback forwardObjectCallback, Context context) {
        mForwardObjectCallback = forwardObjectCallback;
        mContext = context;
    }

    public void forwardObject(String url, String apiKey, String userKey, String objType, String objectKey, String memberKey,String message) {
        ServiceGenerator.CreateService(ForwardObject.class, url).forwardObject(apiKey, userKey, objType, objectKey, memberKey, message)
                .enqueue(new Callback<ForwardObjectResponse>() {
                    @Override
                    public void onResponse(Call<ForwardObjectResponse> call, Response<ForwardObjectResponse> response) {
                        if (response.isSuccessful()) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getSuccess() != null) {
                                        if (response.body().getSuccess().equals("1")) {
                                            mForwardObjectCallback.forwardObjectSuccess(response.body());
                                        } else {
                                            mForwardObjectCallback.forwardObjectError(SmartConstants.FORWARD_FAILED);
                                        }
                                    } else {
                                        mForwardObjectCallback.forwardObjectError(SmartConstants.FORWARD_FAILED);
                                    }
                                } else {
                                    mForwardObjectCallback.forwardObjectError(SmartConstants.FORWARD_FAILED);
                                }
                            } else {
                                mForwardObjectCallback.forwardObjectError(SmartConstants.FORWARD_FAILED);
                            }
                        }else {
                            try {
                                String message = response.errorBody().string();
                                //Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Forward Object",message);
                                mForwardObjectCallback.forwardObjectError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mForwardObjectCallback.forwardObjectError(SmartConstants.TRY_AGAIN_EXCEPTION);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ForwardObjectResponse> call, Throwable t) {
                        //Log.d("SmartFm Error", t.getMessage());
                        mForwardObjectCallback.forwardObjectError(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    public interface ForwardObjectCallback {

        void forwardObjectSuccess(ForwardObjectResponse object);

        void forwardObjectError(String message);
    }

}
