package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.SubObjectFilterAdapter;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectDeafultFilter;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectOptionList;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class SubObjectFilterDialog {
    private Context context;
    private SubObjectFilterSelectionCallback callback;
    private List<SubObjectOptionList> optionLists;
    private SubObjectFilterAdapter adapter;
    private Dialog dialog;

    private CustomTextView mTvMessage;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    private ImageButton close;

    public SubObjectFilterDialog(Context context, SubObjectFilterSelectionCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void showBuildingListDialog(String objectKey) {
        List<SubObjectOptionList> optList = new SubObjectOptionList().getSubObjectOptionLists(objectKey);
        SubObjectDeafultFilter deafultFilter = new SubObjectDeafultFilter().getSubObjectDeafultFilter(objectKey);
        if (deafultFilter != null) {
            optList.add(0, new SubObjectOptionList(deafultFilter.getNoDataText(), "", objectKey));
        }

        this.optionLists = optList;

        dialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_select_filter, null);

        this.mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_buildings);
        this.mTvMessage = (CustomTextView) view.findViewById(R.id.tv_message);
        this.close = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);

        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        this.adapter = new SubObjectFilterAdapter(this.optionLists, context);
        this.mRecyclerView.setAdapter(adapter);

        this.close.setOnClickListener(v -> dialog.dismiss());

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 60;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);
        y = converter.dipToPixels(context, dpHeight);

        addClickListner();

        dialog.setContentView(view);
        dialog.show();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout((int) x, (int) y);
    }

    private void addClickListner() {
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener((recyclerView, position, v) -> {
            if (optionLists.size() != 0) {
                callback.onSubObjectFilterChange(optionLists.get(position).getValue(), optionLists.get(position).getDisplayText());
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });
    }

    public interface SubObjectFilterSelectionCallback {
        void onSubObjectFilterChange(String value, String displayText);
    }
}
