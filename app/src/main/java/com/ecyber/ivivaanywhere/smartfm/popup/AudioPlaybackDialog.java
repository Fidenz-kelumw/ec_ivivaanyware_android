package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;

import java.io.IOException;

/**
 * Created by diharaw on 3/9/17.
 */

public class AudioPlaybackDialog {

    private Activity mActivity;
    private Context mContext;
    private String mUserId;
    private String mFilePath;
    private ImageButton mBtnPlayPause;
    private ImageButton mBtnStop;
    private MediaPlayer mPlayer = null;
    private Chronometer mPlaybackTime;
    private boolean mIsPlaying = false;
    private long mTimeWhenStopped = 0;
    private ProgressDialog mProgressDialog;
    private int mContinuePos = 0;

    public AudioPlaybackDialog(Context context, Activity activity, String filePath, boolean fullPath) {
        mActivity = activity;
        mContext = context;
        mUserId = new AccountPermission().getAccountInfo().get(2);

        if(fullPath) {
            mFilePath = filePath;
        } else {
            mFilePath = Utility.buildUrl(filePath);
        }

        // sample file
        // mFilePath = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3";
    }

    public void show() {
        final Dialog mAudioDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(mActivity.getApplicationContext());
        View view = inflater.inflate(R.layout.view_audio_playback_dialog, null);

        mBtnPlayPause  = (ImageButton) view.findViewById(R.id.btn_play_pause);
        mBtnStop  = (ImageButton) view.findViewById(R.id.btn_stop);
        mPlaybackTime = (Chronometer) view.findViewById(R.id.lbl_audio_time);

        ImageButton btnClose = (ImageButton) view.findViewById(R.id.audio_dialog_close_btn);

        btnClose.setOnClickListener(view1 -> {
            stopPlayback();
            mAudioDialog.dismiss();
        });

        mPlaybackTime.setText("00:00:00");

        mPlaybackTime.setOnChronometerTickListener(cArg -> {
            long time = SystemClock.elapsedRealtime() - cArg.getBase();
            int h   = (int)(time /3600000);
            int m = (int)(time - h*3600000)/60000;
            int s= (int)(time - h*3600000- m*60000)/1000 ;
            String hh = h < 10 ? "0"+h: h+"";
            String mm = m < 10 ? "0"+m: m+"";
            String ss = s < 10 ? "0"+s: s+"";
            cArg.setText(hh+":"+mm+":"+ss);
        });

        mBtnPlayPause.setOnClickListener(view12 -> {
            if(mIsPlaying) {
                pausePlayback();
            } else {
                startPlayback();
            }
        });

        mBtnStop.setOnClickListener(view13 -> {
            if(mIsPlaying) {
                stopPlayback();
            }
        });

        mAudioDialog.setContentView(view);
        mAudioDialog.show();
        mAudioDialog.setCanceledOnTouchOutside(false);
        mAudioDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 57;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;


        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mAudioDialog.getWindow().setLayout((int) x, (int) y);

        mProgressDialog = new ProgressDialog(mActivity);

        mProgressDialog.setMessage("Preparing Stream");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    void startPlayback() {

        if(mPlayer != null) {
            mPlayer.seekTo(mContinuePos);
            mPlayer.start();
            mPlaybackTime.setBase(SystemClock.elapsedRealtime() + mTimeWhenStopped);
            mPlaybackTime.start();

            mBtnPlayPause.setImageResource(R.drawable.pause);
        } else {
            mIsPlaying = true;

            mPlayer = new MediaPlayer();
            try {
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setDataSource(mFilePath);

                mPlayer.setOnCompletionListener(mp -> {
                    stopPlayback();
                    mBtnPlayPause.setImageResource(R.drawable.play);
                });

                mPlayer.setOnPreparedListener(mp -> {
                    mProgressDialog.hide();
                    mPlayer.start();
                    mPlaybackTime.setBase(SystemClock.elapsedRealtime() + mTimeWhenStopped);
                    mPlaybackTime.start();
                    mBtnPlayPause.setImageResource(R.drawable.pause);
                });

                mPlayer.prepareAsync();
                mProgressDialog.show();

            } catch (IOException e) {

            }
        }
    }

    void stopPlayback() {
        try {
            mPlayer.pause();
            mPlayer.release();
            mPlayer = null;
            mTimeWhenStopped = 0;
            mPlaybackTime.setBase(SystemClock.elapsedRealtime());
            mPlaybackTime.stop();
            mPlaybackTime.setText("00:00:00");
            mBtnPlayPause.setImageResource(R.drawable.play);
            mIsPlaying = false;
        } catch (Exception e){

        }
    }

    void pausePlayback() {
        mPlayer.pause();
        mContinuePos = mPlayer.getCurrentPosition();
        mTimeWhenStopped = mPlaybackTime.getBase() - SystemClock.elapsedRealtime();
        mPlaybackTime.stop();
        mBtnPlayPause.setImageResource(R.drawable.play);
        mIsPlaying = false;
    }
}
