package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;

import java.util.List;

/**
 * Created by Lakmal on 5/2/2016.
 */
public class SingleListAdapter extends RecyclerView.Adapter<SingleListAdapter.Holder> {

    private Context mContext;
    private List<DataLookUp> mDataLookUpsDataSet;
    private SingleItemClickListener mSingleItemClickListener;

    public SingleListAdapter(Context context, List<DataLookUp> dataLookUps, SingleItemClickListener singleItemClickListener) {
        mContext = context;
        mDataLookUpsDataSet = dataLookUps;
        mSingleItemClickListener = singleItemClickListener;
    }

    public void addItem(DataLookUp item) {
        mDataLookUpsDataSet.add(item);
        notifyDataSetChanged();
    }

    public void clearDataSet() {
        mDataLookUpsDataSet.clear();
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.widget_single_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.tvItemValue.setText(mDataLookUpsDataSet.get(position).getDisplayText());
        holder.tvItemValue.setTag(mDataLookUpsDataSet.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return null != mDataLookUpsDataSet ? mDataLookUpsDataSet.size() : 0;
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView tvItemValue;

        public Holder(View itemView) {
            super(itemView);
            tvItemValue = (CustomTextView) itemView.findViewById(R.id.tv_single_item_value);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            mSingleItemClickListener.onClickSingleItem(mDataLookUpsDataSet.get(getAdapterPosition()));
        }
    }

    public interface SingleItemClickListener {
        void onClickSingleItem(DataLookUp optionItem);
    }


}
