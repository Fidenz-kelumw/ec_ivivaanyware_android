package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.services.sync.CheckListSync;

/**
 * Created by Choota on 4/25/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ChecklistInfoDialog implements CheckListSync.ChecklistInfoCallback {
    private Context context;
    private CheckListSync checkListSync;

    private CustomTextView msg;
    private ImageButton btnCancel;
    private ProgressBar pb;

    private String url, apiKey, userKey, objType, objKey, infoCode, itemKey;

    public ChecklistInfoDialog(Context context) {
        this.context = context;
        this.checkListSync = new CheckListSync();
    }

    public void showInfoDialog(String url, String apiKey, String userKey, String objType, String objKey, String infoCode, String itemKey){
        initShow(url, apiKey, userKey, objType, objKey, infoCode, itemKey);

        final Dialog confirmationDialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(context);
        View msgView = inflater.inflate(R.layout.dialog_checklist_info, null);

        msg = (CustomTextView) msgView.findViewById(R.id.tvInfo);
        btnCancel = (ImageButton) msgView.findViewById(R.id.img_btn_close);
        pb = (ProgressBar) msgView.findViewById(R.id.pb);

        msg.setVisibility(View.GONE);
        btnCancel.setOnClickListener(view -> confirmationDialog.dismiss());

        checkBeforeApiCall();

        confirmationDialog.setContentView(msgView);
        confirmationDialog.show();
        confirmationDialog.setCanceledOnTouchOutside(false);
    }

    private void initShow(String url, String apiKey, String userKey, String objType, String objKey, String infoCode, String itemKey) {
        this.url = url;
        this.apiKey = apiKey;
        this.userKey = userKey;
        this.objType = objType;
        this.objKey = objKey;
        this.infoCode = infoCode;
        this.itemKey = itemKey;
    }

    private void checkBeforeApiCall(){
        CheckList checkList = new CheckList().getCheckItem(itemKey);
        if(checkList != null){
            if(checkList.getInfoText() != null && !checkList.getInfoText().isEmpty()){
                pb.setVisibility(View.GONE);
                msg.setVisibility(View.VISIBLE);
                msg.setText(checkList.getInfoText());
            } else {
                checkListSync.getChecklistInfo(this,url,apiKey,userKey,objType,objKey,infoCode);
            }
        } else {
            pb.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
            msg.setText("No valid data found.");
        }
    }

    @Override
    public void onChecklistInfoSuccess(String message) {
        new CheckList().updateCheckItemInfo(message, itemKey);
        pb.setVisibility(View.GONE);
        msg.setVisibility(View.VISIBLE);
        msg.setText(message);
    }

    @Override
    public void onChecklistInfoFail() {
        pb.setVisibility(View.GONE);
        msg.setVisibility(View.VISIBLE);
        msg.setText("Some thing went wrong. Try again.");
    }
}
