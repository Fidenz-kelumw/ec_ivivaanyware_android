package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.FieldTypeButtonsAdapter;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;

/**
 * Created by Choota on 3/14/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class FieldTypeButtons implements FieldTypeButtonsAdapter.UpdateButtonStatusCallback {
    private Context context;
    private ObjectInfoModel model;
    private FieldTypeButtonCallback callback;
    private FieldTypeButtonsAdapter adapter;

    private View view;
    private RecyclerView recyclerView;
    private CustomTextView title;

    private String defaultDelection;
    private int objectPosition;

    public FieldTypeButtons(Context context, int postion, ObjectInfoModel objectInfoModel, FieldTypeButtonCallback callback) {
        this.context = context;
        this.model = objectInfoModel;
        this.callback = callback;
        this.defaultDelection = (objectInfoModel.getValueModels() != null ? (objectInfoModel.getValueModels().get(0).getValue()) : "");
        this.objectPosition = postion;
    }

    public View generateButtons() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        this.view = layoutInflater.inflate(R.layout.view_buttons, null);

        this.recyclerView = (RecyclerView) view.findViewById(R.id.recyclerviewButtons);
        this.title = (CustomTextView) view.findViewById(R.id.tv_mlt_title);


        this.title.setText(((model.getFieldname() != null && !model.getFieldname().equals("")) ? model.getFieldname() : "Work Priority"));
        this.adapter = new FieldTypeButtonsAdapter(context,
                model.getOptionListModels(),
                ((model.getValueModels() != null && model.getValueModels().size() > 0) ? model.getValueModels().get(0).getValue() : "0"),
                this);

        this.recyclerView.hasFixedSize();
        this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        this.recyclerView.setAdapter(this.adapter);

        return view;
    }

    @Override
    public void onButtonStatusUpdateSuccess(String data, String name) {
        callback.onFieldTypeButtonStateChange(objectPosition, data, name);
    }

    public interface FieldTypeButtonCallback {
        void onFieldTypeButtonStateChange(int objectPosition, String value, String displayText);
    }
}
