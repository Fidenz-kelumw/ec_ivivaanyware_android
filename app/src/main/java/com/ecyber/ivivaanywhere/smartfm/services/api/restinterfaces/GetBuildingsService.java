package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.BuildingsResponce;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public interface GetBuildingsService {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetBuildings")
    Call<BuildingsResponce> getBuildingList(@Field("UserKey") String UserKey,
                                            @Field("apikey") String apikey);
}
