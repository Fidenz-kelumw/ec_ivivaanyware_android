package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class ObjectTypeResponce {
    @SerializedName("ObjectTypes")
    @Expose
    public List<ObjectType> ObjectTypes;

    public ObjectTypeResponce(List<ObjectType> objectTypes) {
        ObjectTypes = objectTypes;
    }

    public ObjectTypeResponce() {
    }

    public List<ObjectType> getObjectTypes() {
        return ObjectTypes;
    }

    public void setObjectTypes(List<ObjectType> objectTypes) {
        ObjectTypes = objectTypes;
    }
}
