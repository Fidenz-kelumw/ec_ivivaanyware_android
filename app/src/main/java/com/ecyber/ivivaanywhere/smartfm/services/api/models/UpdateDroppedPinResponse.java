package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/15/2016.
 */
public class UpdateDroppedPinResponse {
    @SerializedName("Success")
    @Expose
    private int mSuccess;

    @SerializedName("Message")
    @Expose
    private String mMessage;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
