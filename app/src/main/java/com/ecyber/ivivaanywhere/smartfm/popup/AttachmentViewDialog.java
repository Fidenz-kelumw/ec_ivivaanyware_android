package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DipPixConverter;

/**
 * Created by Lakmal on 6/1/2016.
 */
public class AttachmentViewDialog {

    private Activity mActivity;

    public AttachmentViewDialog(Activity activity) {
        mActivity = activity;
    }

    public void show(View v) {
        CustomTextView tvDownloadUrl = (CustomTextView) v.findViewById(R.id.textView_image_url_to_download);

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density));
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density));

        final float x, y;
        DipPixConverter converter = new DipPixConverter();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        final String titleImage = (((TextView) v.findViewById(R.id.textView_attachment_title)).getText()).toString();

        final Dialog dialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater1 = LayoutInflater.from(mActivity);
        View view = inflater1.inflate(R.layout.dialog_attachment_image_view, null);
        dialog.setContentView(view);

        final SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) view.findViewById(R.id.imageView_att_zoom_image);
        final TextView title = (TextView) view.findViewById(R.id.textView_att_zoom_title);
        ImageButton cls = (ImageButton) view.findViewById(R.id.imageButton_att_zoom_close);
        final ProgressBar pb_zoom_pan = (ProgressBar) view.findViewById(R.id.progressBar_zoom_pan);

        Bitmap tempBitMap = new CImageLoader(mActivity).getFullImage(tvDownloadUrl.getText().toString());

        if (tempBitMap != null) {
            imageView.setImage(ImageSource.bitmap(tempBitMap));
            title.setText(titleImage);
            if (dialog.isShowing() == false) {
                dialog.show();
                pb_zoom_pan.setVisibility(View.GONE);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout((int) x, (int) y);
            }
        } else {
            Toast.makeText(mActivity, "Please wait. Image is downloading.", Toast.LENGTH_LONG).show();
        }
        cls.setOnClickListener(v1 -> dialog.dismiss());
    }
}
