package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/6/2016.
 */
public interface UpdateObjectItem {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "UpdateObjectItem")
    Call<SuccessResponse> updateObjectItem(@Field("ObjectKey") String objectKey,
                                           @Field("ObjectType") String objectType,
                                           @Field("UserKey") String userKey,
                                           @Field("TabName") String tabName,
                                           @Field("ItemKey") String itemKey,
                                           @Field("ItemData") String itemData,
                                           @Field("apikey") String apikey);
}
