package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 4/27/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "SubObjectOptionList")
public class SubObjectOptionList extends Model {

    @SerializedName("DisplayText")
    @Expose
    @Column(name = "DisplayText")
    String displayText;

    @SerializedName("Value")
    @Expose
    @Column(name = "Value")
    String value;

    @Column(name = "ObjectKey")
    String objectKey;

    public SubObjectOptionList() {
    }

    public SubObjectOptionList(String displayText, String value, String objectKey) {
        this.displayText = displayText;
        this.value = value;
        this.objectKey = objectKey;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public List<SubObjectOptionList> getSubObjectOptionLists(String objectKey){
        return new Select()
                .from(SubObjectOptionList.class)
                .where("ObjectKey = ?", objectKey)
                .execute();

    }
}
