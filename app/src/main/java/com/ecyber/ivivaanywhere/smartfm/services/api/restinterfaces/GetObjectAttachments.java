package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CheckListResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/11/2016.
 */
public interface GetObjectAttachments {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectAttachments")
    Call<Attachments> getObjectAttachments(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName);

}
