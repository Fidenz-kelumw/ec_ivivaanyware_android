package com.ecyber.ivivaanywhere.smartfm.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends AppCompatActivity {

    private String url;
    private final String TAG = "Main";
    private ColoredSnackbar coloredSnackbar;

    @BindView(R.id.btnClose)
    ImageButton btnClose;

    @BindView(R.id.webview)
    WebView webview;

    @BindView(R.id.progressBar4)
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        coloredSnackbar = new ColoredSnackbar(this);

        pb.setVisibility(View.VISIBLE);
        url = getIntent().getStringExtra("weburl");

        webview.setBackgroundColor(getResources().getColor(R.color.main_background_color));

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setUserAgentString("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0");

        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " +url);
                pb.setVisibility(View.INVISIBLE);

                if(!url.endsWith("pdf")) {
                    try {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            webview.evaluateJavascript("document.querySelector('meta[name=viewport]').setAttribute('content', 'user-scalable = 1;', false);", null);
                        } else {
                            webview.loadUrl("javascript: document.querySelector('meta[name=viewport]').setAttribute('content', 'user-scalable = 1;', false);");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "JS Execution Error");
                    }
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                pb.setVisibility(View.INVISIBLE);
                coloredSnackbar.showSnackBar("Something went wrong, try again later",ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            }
        });

        if(url.endsWith("pdf")){
            String checkSsl = DomainData.getSsl();
            if(checkSsl != null && !checkSsl.equals("")){
                if(checkSsl.equals("1")){
                    webview.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
                }
                else {
                    webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
                }
            }

        } else {
            webview.loadUrl(url);
        }
    }

    @OnClick(R.id.btnClose)
    public void onCloseClick(View vew){
        finish();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_display_bottom);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_display_bottom);
    }
}
