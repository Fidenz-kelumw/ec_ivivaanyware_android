package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/23/2016.
 */
public class ForwardObjectResponse {
    @SerializedName("Success")
    @Expose
    public String mSuccess;

    @SerializedName("Message")
    @Expose
    public String mMessage;

    public String getSuccess() {
        return mSuccess;
    }

    public void setSuccess(String success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
