package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Choota on 1/9/18.
 */

public class SetDateFilterDialog implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private Context context;
    private SetDateFilterCallback callback;
    private String startDate, endDate;
    private FragmentManager fragmentManager;

    private Dialog dialog;
    private ImageButton btnClearStart, btnClearEnd, imageButtonSelectTypeClose;
    private CustomButton btnView, btnEndDate, btnStartDate;

    private int selectedButton;
    private int minDate, minMonth, minYear;
    private boolean isStartDateSet, isEndDateSet;

    public SetDateFilterDialog(Context context, SetDateFilterCallback callback, FragmentManager fragmentManager) {
        this.context = context;
        this.callback = callback;
        this.fragmentManager = fragmentManager;

        this.dialog = new Dialog(context, R.style.CustomDialog);
        this.isStartDateSet = false;
        this.isEndDateSet = false;
    }

    public void show(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;

        this.dialog.setContentView(R.layout.dialog_set_date_filter);
        this.initView();

        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void initView() {
        imageButtonSelectTypeClose = (ImageButton) dialog.findViewById(R.id.imageButton_select_type_close);
        btnStartDate = (CustomButton) dialog.findViewById(R.id.btnStartDate);
        btnClearStart = (ImageButton) dialog.findViewById(R.id.btnClearStart);
        btnEndDate = (CustomButton) dialog.findViewById(R.id.btnEndDate);
        btnClearEnd = (ImageButton) dialog.findViewById(R.id.btnClearEnd);
        btnView = (CustomButton) dialog.findViewById(R.id.btn_view);

        imageButtonSelectTypeClose.setOnClickListener(this);
        btnStartDate.setOnClickListener(this);
        btnClearStart.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);
        btnClearEnd.setOnClickListener(this);
        btnView.setOnClickListener(this);

        setDatesToView();
    }

    private void setDatesToView(){
        if(!startDate.equals("")){
            btnStartDate.setText(startDate);
            btnEndDate.setText(endDate);

            btnClearEnd.setEnabled(true);
            btnClearStart.setEnabled(true);
            btnView.setEnabled(true);
        } else {
            btnClearEnd.setEnabled(false);
            btnClearStart.setEnabled(false);
            btnView.setEnabled(false);
        }
    }

    private void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        datePickerDialog.setMaxDate(now);
        if (selectedButton == 2 && minYear != 0) {
            Calendar min = new GregorianCalendar(minYear, minMonth, minDate);
            datePickerDialog.setMinDate(min);
        }

        datePickerDialog.show(fragmentManager, "Datepickerdialog");
        datePickerDialog.setOnCancelListener(dialogInterface -> {
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton_select_type_close:
                if(dialog != null && dialog.isShowing())
                    dialog.dismiss();
                break;
            case R.id.btnClearStart:
                minYear = 0;
                btnStartDate.setText("Start Date");
                btnClearStart.setEnabled(false);
                isStartDateSet = false;

                btnView.setEnabled(false);
                break;
            case R.id.btnClearEnd:
                btnEndDate.setText("End Date");
                btnClearEnd.setEnabled(false);
                isEndDateSet = false;

                btnView.setEnabled(false);
                break;
            case R.id.btnStartDate:
                selectedButton = 1;
                showDatePicker();
                break;
            case R.id.btnEndDate:
                if(isStartDateSet) {
                    selectedButton = 2;
                    showDatePicker();
                } else {
                    Toast.makeText(context, "Please set \"Start Date\" first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_view:
                callback.onFilterDateSet(btnStartDate.getText().toString(), btnEndDate.getText().toString());
                if(dialog != null && dialog.isShowing())
                    dialog.dismiss();
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String month = ((monthOfYear + 1) >= 10 ? String.valueOf((monthOfYear + 1)) : ("0" + (monthOfYear + 1)));

        if (selectedButton == 1) {
            minDate = dayOfMonth;
            minMonth = monthOfYear;
            minYear = year;

            btnStartDate.setText("" + year + "-" + (month) + "-" + dayOfMonth);
            btnClearStart.setEnabled(true);
            isStartDateSet = true;

        } else if (selectedButton == 2) {
            btnEndDate.setText("" + year + "-" + (month) + "-" + dayOfMonth);
            btnClearEnd.setEnabled(true);
            isEndDateSet = true;
        }

        if (isEndDateSet && isStartDateSet)
            btnView.setEnabled(true);
    }

    public interface SetDateFilterCallback {
        void onFilterDateSet(String startDate, String endDate);
    }
}
