package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

/**
 * Created by diharaw on 3/14/17.
 */

public class FacesView {


    private final View mView;
    private CustomTextView mTitle;
    private Context mContext;
    private String mFieldId;
    private int mPosition;
    private ObjectInfoModel mObjectInfo;
    private ImageButton mFace1;
    private ImageButton mFace2;
    private ImageButton mFace3;
    private ImageButton mFace4;
    private ImageButton mFace5;
    private ImageButton mReset;
    private int mValue;
    private FacesCallback mCallback;
    private boolean mIsEditable;

    public FacesView(Context context, FacesCallback callback, @ColorInt int color) {
        mContext = context;
        mCallback = callback;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_faces, null);

        mTitle = (CustomTextView) mView.findViewById(R.id.tv_faces_title);
        mTitle.setTextColor(color);

        mFace1 = (ImageButton) mView.findViewById(R.id.btn_face_1);
        mFace2 = (ImageButton) mView.findViewById(R.id.btn_face_2);
        mFace3 = (ImageButton) mView.findViewById(R.id.btn_face_3);
        mFace4 = (ImageButton) mView.findViewById(R.id.btn_face_4);
        mFace5 = (ImageButton) mView.findViewById(R.id.btn_face_5);

        mReset = (ImageButton) mView.findViewById(R.id.btn_reset_faces);

        registerCallbacks();
    }

    public View getView(ObjectInfoModel info, int position) {
        boolean isEditable = info.getEditable().equals("1") ? true : false;
        mFieldId = info.getFieldid();
        mPosition = position;
        mObjectInfo = info;

        setFieldName(info.getFieldname());
        String defVal = "";
        if (info.getValueModels() != null) {
            List<ValueModel> valueModels = info.getValueModels();
            defVal = valueModels.get(0).getValue();
        }

        setDefaultData();
        mIsEditable = info.getEditable().equals("1") ? true : false;
        setEditable();

        return mView;
    }

    private void setFieldName(String fieldName) {
        mTitle.setText(fieldName);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setValue(int value) {
            mValue = value;

            if(mValue == 1) {
                mFace1.setImageResource(R.drawable.face_1);
                mFace2.setImageResource(R.drawable.face_2);
                mFace3.setImageResource(R.drawable.face_3);
                mFace4.setImageResource(R.drawable.face_4);
                mFace5.setImageResource(R.drawable.face_5_selected);
            } else if (mValue == 2) {
                mFace1.setImageResource(R.drawable.face_1);
                mFace2.setImageResource(R.drawable.face_2);
                mFace3.setImageResource(R.drawable.face_3);
                mFace4.setImageResource(R.drawable.face_4_selected);
                mFace5.setImageResource(R.drawable.face_5);
            } else if (mValue == 3) {
                mFace1.setImageResource(R.drawable.face_1);
                mFace2.setImageResource(R.drawable.face_2);
                mFace3.setImageResource(R.drawable.face_3_selected);
                mFace4.setImageResource(R.drawable.face_4);
                mFace5.setImageResource(R.drawable.face_5);
            } else if (mValue == 4) {
                mFace1.setImageResource(R.drawable.face_1_selected);
                mFace2.setImageResource(R.drawable.face_2);
                mFace3.setImageResource(R.drawable.face_3);
                mFace4.setImageResource(R.drawable.face_4);
                mFace5.setImageResource(R.drawable.face_5);
            } else if (mValue == 5) {
                mFace1.setImageResource(R.drawable.face_1);
                mFace2.setImageResource(R.drawable.face_2_selected);
                mFace3.setImageResource(R.drawable.face_3);
                mFace4.setImageResource(R.drawable.face_4);
                mFace5.setImageResource(R.drawable.face_5);
            } else {
                mFace1.setImageResource(R.drawable.face_1);
                mFace2.setImageResource(R.drawable.face_2);
                mFace3.setImageResource(R.drawable.face_3);
                mFace4.setImageResource(R.drawable.face_4);
                mFace5.setImageResource(R.drawable.face_5);
            }
    }

    private void registerCallbacks() {

        mFace1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(4);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });

        mFace2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(5);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });

        mFace3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(3);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });

        mFace4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(2);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });

        mFace5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(1);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(0);
                mCallback.onFacesValueChanged(mPosition, "", mValue);
            }
        });
    }

    private void setEditable() {
        mFace1.setEnabled(mIsEditable);
        mFace2.setEnabled(mIsEditable);
        mFace3.setEnabled(mIsEditable);
        mFace4.setEnabled(mIsEditable);
        mFace5.setEnabled(mIsEditable);
        mReset.setEnabled(mIsEditable);
    }

    private void setDefaultData() {
        List<ValueModel> defaultValue = mObjectInfo.getValueModels();

        if(defaultValue.size() > 0) {

            try {
                mValue =  Integer.parseInt(defaultValue.get(0).getValue());
            } catch(NumberFormatException e) {
                mValue = 0;
            }

            setValue(mValue);
        }
    }

    public interface FacesCallback {
        void onFacesValueChanged(int position, String fieldId, int value);
    }
}
