package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ChathuraHettiarachchi on 4/5/16.
 */
public class RegistrationResponce {

    @SerializedName("User")
    @Expose
    User user;

    @SerializedName("AccountInfo")
    @Expose
    AccountInfo accountInfo;

    @SerializedName("Defaults")
    @Expose
    Defaults defaults;

    @SerializedName("HiddenSettings")
    @Expose
    HiddenSettings hiddenSettings;

    @SerializedName("Redirect")
    @Expose
    String Redirect;

    ErrorLog error;


    public ErrorLog getError() {
        return error;
    }

    public void setError(ErrorLog error) {
        this.error = error;
    }

    public RegistrationResponce() {
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public Defaults getDefaults() {
        return defaults;
    }

    public void setDefaults(Defaults defaults) {
        this.defaults = defaults;
    }

    public HiddenSettings getHiddenSettings() {
        return hiddenSettings;
    }

    public void setHiddenSettings(HiddenSettings hiddenSettings) {
        this.hiddenSettings = hiddenSettings;
    }

    public String getRedirect() {
        return Redirect;
    }

    public void setRedirect(String redirect) {
        Redirect = redirect;
    }

    public static class User {

        @SerializedName("UserKey")
        @Expose
        String UserKey;

        @SerializedName("UserName")
        @Expose
        String UserName;

        @SerializedName("ImagePath")
        @Expose
        String ImagePath;

        public User() {
        }

        public String getUserKey() {
            return UserKey;
        }

        public void setUserKey(String userKey) {
            UserKey = userKey;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String imagePath) {
            ImagePath = imagePath;
        }
    }

    public static class AccountInfo {

        @SerializedName("Account")
        @Expose
        String Account;

        @SerializedName("ApiKey")
        @Expose
        String ApiKey;

        public AccountInfo() {
        }

        public String getAccount() {
            return Account;
        }

        public void setAccount(String account) {
            Account = account;
        }

        public String getApiKey() {
            return ApiKey;
        }

        public void setApiKey(String apiKey) {
            ApiKey = apiKey;
        }
    }

    public static class Defaults {

        @SerializedName("MasterDataExpiryHours")
        @Expose
        public String MasterDataExpiryHours;

        @SerializedName("TransactionExpirySeconds")
        @Expose
        public String TransactionExpirySeconds;

        @SerializedName("InboxSize")
        @Expose
        public String InboxSize;

        @SerializedName("UserName")
        @Expose
        public String UserName;

        @SerializedName("ImagePath")
        @Expose
        public String ImagePath;

        public String getInboxSize() {
            return InboxSize;
        }

        public void setInboxSize(String inboxSize) {
            InboxSize = inboxSize;
        }

        @SerializedName("LocationKey")
        @Expose
        public String LocationKey;

        @SerializedName("GetObjectForQR")
        @Expose
        public String GetObjectForQR;

        @SerializedName("AppName")
        @Expose
        public String AppName;

        public Defaults() {
        }

        public String getMasterDataExpiryHours() {
            return MasterDataExpiryHours;
        }

        public void setMasterDataExpiryHours(String masterDataExpiryHours) {
            MasterDataExpiryHours = masterDataExpiryHours;
        }

        public String getTransactionExpirySeconds() {
            return TransactionExpirySeconds;
        }

        public void setTransactionExpirySeconds(String transactionExpirySeconds) {
            TransactionExpirySeconds = transactionExpirySeconds;
        }

        public String getLocationKey() {
            return LocationKey;
        }

        public void setLocationKey(String locationKey) {
            LocationKey = locationKey;
        }
    }

    public static class HiddenSettings {

        @SerializedName("GetObjectForQR")
        @Expose
        String GetObjectForQR;

        public HiddenSettings() {
        }

        public String getGetObjectForQR() {
            return GetObjectForQR;
        }

        public void setGetObjectForQR(String getObjectForQR) {
            GetObjectForQR = getObjectForQR;
        }
    }
}
