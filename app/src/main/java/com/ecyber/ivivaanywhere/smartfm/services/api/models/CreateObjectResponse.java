package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 5/9/2016.
 */
public class CreateObjectResponse {
    @SerializedName("Success")
    @Expose
    private int mSuccess;

    @SerializedName("Message")
    @Expose
    private String mMessage;

    @SerializedName("ObjectKey")
    @Expose
    private String mObjectKey;

    @SerializedName("DetailsPage")
    @Expose
    private String mDetailsPage;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public String getDetailsPage() {
        return mDetailsPage;
    }

    public void setDetailsPage(String detailsPage) {
        mDetailsPage = detailsPage;
    }
}
