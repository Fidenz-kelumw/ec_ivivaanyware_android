package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Actions;
import com.ecyber.ivivaanywhere.smartfm.models.PageLinks;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class ActionButton implements View.OnClickListener {

    private Activity mActivity;
    private View mView;
    private CustomButton mBtnAction;
    private ActionButtonListener mActionButtonListener;
    private boolean isActionButton;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    public ActionButton(Activity activity, ActionButtonListener actionButtonListener) {
        mActivity = activity;
        mActionButtonListener = actionButtonListener;
        mNetwork = new NetworkCheck();
        mColoredSnackBar = new ColoredSnackbar(mActivity);
        initView();

    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.view_action_button, null);
        mBtnAction = (CustomButton) mView.findViewById(R.id.btn_action);
        mBtnAction.setOnClickListener(this);
    }

    public View setPageLinkButton(PageLinks pageLinks, int position, boolean isActionButton) {
        this.isActionButton = isActionButton;
        mBtnAction.setText(pageLinks.getLinkName());
        mBtnAction.setTag(pageLinks.getLinkType());
        mBtnAction.setId(position);
        return mView;
    }


    @Override
    public void onClick(View v) {

        boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            if (isActionButton) {
                mActionButtonListener.OnActionButtonClickListener(v);
            } else {
                mActionButtonListener.OnLinkButtonClickListener(v);
            }
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }

    }

    public View setActionButton(Actions action, int position, boolean isActionButton) {
        this.isActionButton = isActionButton;
        mBtnAction.setText(action.getActionName());
        mBtnAction.setTag(action.getActionID());
        mBtnAction.setId(position);

        boolean isEnable = (action.getEnabled().equals("1") ? true : false);
        setEnableButton(isEnable);

        return mView;
    }

    public void setEnableButton(boolean disable) {
        mBtnAction.setEnabled(disable);
        if(disable){
            mBtnAction.setTextColor(mActivity.getResources().getColor(R.color.pure_white));
        } else {
            mBtnAction.setTextColor(mActivity.getResources().getColor(R.color.settings_titles_color));
        }
    }


    public interface ActionButtonListener {
        void OnLinkButtonClickListener(View view);

        void OnActionButtonClickListener(View view);
    }


}
