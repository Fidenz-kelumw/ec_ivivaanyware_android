package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.BuildingsAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.FilterBuildings;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.StringEmptyCheck;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.services.sync.BuildingsSync;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class BuildingDialog implements BuildingsSync.GetBuildingCallBack {
    private Context context;
    private Dialog dialog;
    private BuildingsSync buildingsSync;
    private BuildingsAdapter buildingsAdapter;
    private List<Buildings> mBuildings = new ArrayList<>();

    RecyclerView mRecyclerView;
    ProgressBar progressBar;
    ImageButton close;
    String mRequestFrom;
    String locationKey;
    private LocationCallback mLocationCallback;
    private ProgressBar mProgressBar;
    private CustomTextView mTvMessage;
    private boolean isNoDataNeeded;


    public BuildingDialog(Context context, boolean isNoDataNeeded) {
        this.context = context;
        buildingsSync = new BuildingsSync(this);
        locationKey = "";
        this.isNoDataNeeded = isNoDataNeeded;
    }

    public void showBuildingListDialog(String requestFrom, LocationCallback locationCallback) {
        mLocationCallback = locationCallback;
        mRequestFrom = requestFrom;
        final Dialog bDialog = new Dialog(context, R.style.CustomDialog);
        LayoutInflater i = LayoutInflater.from(context);
        View view = i.inflate(R.layout.dialog_select_building, null);
        dialog = bDialog;
        mBuildings = new ArrayList<>();

        AccountPermission accountPermission = new AccountPermission();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_buildings);
        CustomEditTextView edtSearch = (CustomEditTextView) view.findViewById(R.id.et_searchView);
        mTvMessage = (CustomTextView) view.findViewById(R.id.tv_message);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        close = (ImageButton) view.findViewById(R.id.imageButton_select_type_close);

        bDialog.setContentView(view);
        this.progressBar.setVisibility(View.VISIBLE);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        buildingsAdapter = new BuildingsAdapter(mBuildings, context);
        mRecyclerView.setAdapter(buildingsAdapter);


        edtSearch.addTextChangedListener(mTextWatcher);
        addClickListner();

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 57;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);
        y = converter.dipToPixels(context, dpHeight);

        close.setOnClickListener(v -> bDialog.dismiss());
        bDialog.show();
        bDialog.setCancelable(false);
        bDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bDialog.getWindow().setLayout((int) x, (int) y);

        dataDownloadManager(buildingsSync, accountPermission);
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String search = s.toString().trim();
            StringEmptyCheck check = new StringEmptyCheck();
            boolean isEmpty = check.isNotNullNotEmptyNotWhiteSpaceOnly(search);

            mTvMessage.setVisibility(View.GONE);

            if (!isEmpty) {
                if (mBuildings != null) {
                    buildingsAdapter.clear();
                    for (Buildings building : mBuildings) {
                        buildingsAdapter.addBuilding(building);
                    }
                }
            } else {
                if (mBuildings != null) {
                    List<Buildings> buildList = new FilterBuildings().filterBuildings(mBuildings, search);
                    if (buildList.size() > 0) {
                        buildingsAdapter.clear();
                        for (Buildings build : buildList) {
                            buildingsAdapter.addBuilding(build);
                        }
                    } else {
                        buildingsAdapter.clear();
                        mTvMessage.setText("No Result for \"" + search + "\"");
                        mTvMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    mTvMessage.setText("Poor Internet Connection.");
                    mTvMessage.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void dataDownloadManager(BuildingsSync buildingsSync, AccountPermission accountPermission) {

        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
        List<Buildings> buildingsList = new Buildings().getAllBuildings();
        mBuildings = buildingsList;

        if (dataDownloadOptimization.isDataAvailableInTable(Buildings.class)) {
            if (dataDownloadOptimization.isTableDataOld(SmartConstants.BUILDINGS, true)) {
                mBuildings.clear();
                buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), isNoDataNeeded);
            } else {
                if(isNoDataNeeded){
                    buildingsList.add(0,new Buildings("Any Location", ""));
                }
                for (Buildings buildings : buildingsList) {
                    buildingsAdapter.addBuilding(buildings);
                }
                progressBar.setVisibility(View.GONE);
            }
        } else {
            buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), isNoDataNeeded);
        }
    }

    @Override
    public void onBuildingsDownload(Buildings buildings) {
        mTvMessage.setVisibility(View.GONE);
        buildingsAdapter.addBuilding(buildings);
        mBuildings.add(buildings);
    }

    @Override
    public void onBuildingsDownloadComplete() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBuildingsDownloadError(String error) {
        Log.d("Error loading buildings", error);
        dismissDialog();
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    public String getLocationKey() {
        return locationKey;
    }

    public void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    private void addClickListner() {
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener((recyclerView, position, v) -> {
            if (mBuildings.size() != 0) {
                locationKey = mBuildings.get(position).LocationKey;
                String locationName = mBuildings.get(position).LocationName;
                if (mRequestFrom.equals(SmartConstants.FROM_FILTER_ACTIVITY)) {
                    mLocationCallback.onSelectItem(locationKey, locationName);
                } else if (mRequestFrom.equals(SmartConstants.FROM_ORDER_ACTIVITY)) {
                    mLocationCallback.onSelectItem(locationKey, locationName);
                }
                dismissDialog();
            }
        });
    }

    public interface LocationCallback {
        void onSelectItem(String locationKey, String locationName);
    }
}
