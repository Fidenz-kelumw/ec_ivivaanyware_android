package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.AttachmentAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentMaster;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.popup.AttachmentSendDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.AudioPlaybackDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.NewAttachmentViewDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.AddNewAttachmentResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.AddNewAttachmentSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetObjectAttachmentsSync;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lakmal on 4/20/2016.
 */
public class AttachmentsFragment extends Fragment implements AttachmentSendDialog.AttachmentSendCallback, AttachmentAdapter.AttachmentItemClickListener, GetObjectAttachmentsSync.GetObjectAttachmentsCallBack, AddNewAttachmentSync.AddNewAttachmentCallBack {

    @BindView(R.id.rv_attachment_list)
    RecyclerView rvAttachmentList;

    @BindView(R.id.swiperefresh_attachment_list)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.btn_add_attachment)
    CustomButton btnAddAttachment;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.progressBar3)
    ProgressBar mProgressBar;

    private Context mContext;
    private String mObjectType;
    private String mObjectKey;
    private String mTabName;

    private boolean isTabForceLoad;

    private NetworkCheck mNetwork;
    private AccountPermission mAccountPermission;
    private GetObjectAttachmentsSync mObjectAttachmentsSync;
    private AttachmentAdapter mAttachmentAdapter;

    private List<AttachmentMaster> mAttachmentList = new ArrayList<>();
    private ColoredSnackbar mColoredSnackBar;
    private String mPicturePath;
    private ProgressDialog mProgressDialog;

    private String mFileName = "c.jpeg";
    private static String imagePath = null;

    private static AttachmentSendDialog mAttachmentSendDialog;
    private Dialog sendDialog;

    public AttachmentsFragment() {
    }

    @SuppressLint("ValidFragment")
    public AttachmentsFragment(ProgressBar progressBar, Context context) {
        mContext = context;
        mNetwork = new NetworkCheck();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_attachments, container, false);
        ButterKnife.bind(this, rootView);
        mColoredSnackBar = new ColoredSnackbar(getActivity());
        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        btnAddAttachment.setEnabled(false);

        initViews();
        dataLoad(false);
        return rootView;
    }

    private void openAttachmentSendDialog() {
        imagePath = null;
        mAttachmentSendDialog = null;
        mAttachmentSendDialog = new AttachmentSendDialog(getActivity(), this, this, mObjectKey);
        sendDialog = mAttachmentSendDialog.show();
        mAttachmentSendDialog.setDialogInstance(sendDialog);
    }

    /**
     * Add attachment button click event
     * Open add attachment selection dialog
     *
     * @param v
     */
    @OnClick(R.id.btn_add_attachment)
    public void OnClickBtnAddAttachment(View v) {
        openAttachmentSendDialog();
    }


    /**
     * Get result selected after a image or wrote a signature image
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SmartConstants.RESULT_IMAGE_CAMERA) {
            if (SmartConstants.RESULT_OK == resultCode) {
                if (imagePath != null) {
                    mPicturePath = imagePath;
                    mAttachmentSendDialog.imageSetToView(mPicturePath, false, null);
                }

            }
        } else if (requestCode == SmartConstants.RESULT_IMAGE_GALLERY) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mPicturePath = cursor.getString(columnIndex);
                cursor.close();

                mAttachmentSendDialog.imageSetToView(mPicturePath, false, selectedImage);
            }
        } else if (requestCode == SmartConstants.RESULT_VIDEO_CAPTURE) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Uri videoUri = data.getData();
                mAttachmentSendDialog.setVideoToView(videoUri);
            }
        } else if (requestCode == SmartConstants.RESULT_SKETCH) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Bundle b = data.getExtras();

                String filePath = b.getString(SmartConstants.SKETCH_IMAGE);
                mAttachmentSendDialog.imageSetToView(filePath, false, null);
            }
        }
    }

    @Override
    public void onCapturePath(String picturePath) {
        imagePath = picturePath;
    }

    /**
     * add a new attachment to API calling method
     *
     * @param attachmentType attachment type signature or other etc..
     * @param title          name
     * @param b64code        image (b64code)
     * @param sourceFileName file name
     */
    private void addAttachment(String attachmentType, String title, String b64code, String sourceFileName) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mProgressDialog = ProgressDialog.show(getActivity(), "",
                    "Uploading attachment...", true);

            new AddNewAttachmentSync(this, mContext).addNewAttachment(
                    mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName, attachmentType, title, b64code, sourceFileName);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * Initializing of all views in this Activity
     */
    private void initViews() {
        mAccountPermission = new AccountPermission();
        mObjectAttachmentsSync = new GetObjectAttachmentsSync(this);

        rvAttachmentList.setHasFixedSize(true);
        rvAttachmentList.setLayoutManager(new LinearLayoutManager(mContext));

        mAttachmentAdapter = new AttachmentAdapter(mContext,
                mAttachmentList,
                mAccountPermission.getAccountInfo().get(0), this);
        rvAttachmentList.setAdapter(mAttachmentAdapter);

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        rvAttachmentList.addItemDecoration(new RecyclerDecorator(mContext, spanCount, spacing, includeEdge, false));

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mAttachmentAdapter.clear();
            dataLoad(true);
            mSwipeRefreshLayout.setRefreshing(false);
        });
    }

    /**
     * attachment data loading
     * check time is expired also?
     *
     * @param isUpdate
     */
    private void dataLoad(boolean isUpdate) {

        if (isTabForceLoad){
            invokeToAttachmentsSync();
        } else {
            DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
            mProgressBar.setVisibility(View.VISIBLE);
            mTvMessage.setVisibility(View.GONE);
            // An Attachment uploaded after
            if (isUpdate) {
                invokeToAttachmentsSync();
                return;
            }

            // Check data is already and trans time and update list
            if (dataDownloadOptimization.isDataAvailableInTable(Attachments.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToAttachmentsSync();
                } else {
                    boolean isAvailable = mObjectAttachmentsSync.getFromDb(mObjectKey);
                    if (!isAvailable) {
                        invokeToAttachmentsSync();
                    }
                }
            } else {
                invokeToAttachmentsSync();
            }
        }
    }

    /**
     * call to cloud (API) method for get all attachments in ObjectAttachmentSync class
     */
    private void invokeToAttachmentsSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mObjectAttachmentsSync.GetObjectAttachments(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            mProgressBar.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * getObjectAttachments() methods successful call back event
     *
     * @param attachments
     */
    @Override
    public void OnGetObjectAttachmentsSuccess(Attachments attachments) {
        mTvMessage.setVisibility(View.GONE);
        btnAddAttachment.setEnabled(1 == attachments.getAddNewAttachment());

        mAttachmentAdapter.clear();
        List<AttachmentTypesMaster> attachmentTypes = attachments.getAttachmentTypes();

        List<AttachmentMaster> attachmentMasterList = attachments.getAttachmentMasterList();
        for (AttachmentMaster attachmentMaster : attachmentMasterList) {
            mAttachmentAdapter.addItem(attachmentMaster);
        }

        if (attachmentMasterList.size() == 0) {
            mTvMessage.setText(SmartConstants.ITEM_NOT_FOUND);
            mTvMessage.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
        rvAttachmentList.scrollToPosition(0/*mAttachmentAdapter.getItemCount() - 1*/);
    }

    /**
     * getObjectAttachments() methods unsuccessful call back event
     *
     * @param message
     */
    @Override
    public void OnOnGetObjectAttachmentsError(String message) {
        mAttachmentAdapter.clear();
        mTvMessage.setText(message);
        mTvMessage.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * addNewAttachment() method successful call back event
     *
     * @param addNewAttachmentResponse
     */
    @Override
    public void OnAddNewAttachmentSuccess(AddNewAttachmentResponse addNewAttachmentResponse) {

        String message = "Successfully uploaded";

        if (addNewAttachmentResponse != null && addNewAttachmentResponse.getMessage() != null && !addNewAttachmentResponse.getMessage().isEmpty())
            message = addNewAttachmentResponse.getMessage();

        if (addNewAttachmentResponse != null) {
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
            dataLoad(true);
            mProgressBar.setVisibility(View.VISIBLE);
            rvAttachmentList.scrollToPosition(0);
        }

        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_LONG);
    }

    /**
     * addNewAttachment() method unsuccessful call back event
     *
     * @param error
     */
    @Override
    public void OnAddNewAttachmentError(String error) {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
        Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();

        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
    }

    /**
     * Attachment list item click event
     *
     * @param v
     * @param position
     */
    @Override
    public void OnAttachmentItemClick(View v, int position, AttachmentMaster attachment) {

        String ATTType = attachment.getmATTType();

        if (ATTType.equals("IMG")) {
            new NewAttachmentViewDialog(getActivity()).show(v);
        } else if (ATTType.equals("AUD")) {
            new AudioPlaybackDialog(getActivity(), getActivity(), attachment.getFilePath(), false).show();
        } else if (ATTType.equals("VID")) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(Utility.buildUrl(attachment.getFilePath())), "video/*");
            this.startActivity(intent);
        }
    }

    @Override
    public void onAttachmentSend(String attachmentType, String title, String base64String, Bitmap bitmap) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            addAttachment(attachmentType, title, base64String, mFileName);
            if (bitmap != null) {
                bitmap.recycle();
            }
        } else {
            if (getActivity() != null)
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onAttachmentSend(String attachmentCode, String attachmentType, String title, String filePath, String fileName) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            new AddNewAttachmentSync(this, mContext).addNewAttachmentEX(mObjectType, mObjectKey, mTabName, attachmentType, title, fileName, attachmentCode, filePath);
        } else {
            if (getActivity() != null)
                mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }
}
