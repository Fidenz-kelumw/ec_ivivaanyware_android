package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.zxing.qrcode.decoder.Mode;

/**
 * Created by Lakmal on 6/8/2016.
 */
@Table(name = "Domain")
public class DomainData extends Model {
    @Column(name = "ssl")
    @SerializedName("ssl")
    @Expose
    public String mSsl;

    @Column(name = "account")
    @SerializedName("account")
    @Expose
    public String mAccount;

    @Column(name = "apikey")
    @SerializedName("apikey")
    @Expose
    public String mApikey;

    @Column(name = "iviva_status")
    @SerializedName("iviva-status")
    @Expose
    public int mIvivaStatus;

    public DomainData() {
        super();
    }

    public DomainData(String ssl, String account, String apikey, int ivivaStatus) {

        mSsl = ssl;
        mAccount = account;
        mApikey = apikey;
        mIvivaStatus = ivivaStatus;
    }

    public static String getSsl() {
        String value = null;
        try {
            DomainData domainData = new Select().from(DomainData.class).executeSingle();
            value = domainData.mSsl;
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    public static void clearTable() {
        new Delete().from(DomainData.class).execute();
    }

    public static void saveData(String ssl, String account, String apiKey, int ivivaStatus) {
        clearTable();
        DomainData domainData = new DomainData(ssl, account, apiKey, ivivaStatus);
        domainData.save();
    }
}
