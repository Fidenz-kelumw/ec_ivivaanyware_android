package com.ecyber.ivivaanywhere.smartfm.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/5/16.
 */
@Table(name = "Registration")
public class Registration extends Model {

    @Column(name = "Domain")
    public String Domain;

    @Column(name = "UserKey")
    public String UserKey;

    @Column(name = "UserName")
    public String UserName;

    @Column(name = "ImagePath")
    public String ImagePath;

    @Column(name = "Account")
    public String Account;

    @Column(name = "ApiKey")
    public String ApiKey;

    //from Default data
    @Column(name = "MasterDataExpiryHours")
    public String MasterDataExpiryHours;

    @Column(name = "TransactionExpirySeconds")
    public String TransactionExpirySeconds;

    @Column(name = "InboxSize")
    public String InboxSize;

    @Column(name = "LocationKey")
    public String LocationKey;

    @Column(name = "GetObjectForQR")
    public String GetObjectForQR;

    public Registration() {
        super();
    }

    public Registration(String domain, String userKey, String userName, String imagePath, String account, String apiKey, String masterDataExpiryHours, String transactionExpirySeconds, String inboxSize, String locationKey, String getObjectForQR) {
        Domain = domain;
        UserKey = userKey;
        UserName = userName;
        ImagePath = imagePath;
        Account = account;
        ApiKey = apiKey;
        MasterDataExpiryHours = masterDataExpiryHours;
        TransactionExpirySeconds = transactionExpirySeconds;
        InboxSize = inboxSize;
        LocationKey = locationKey;
        GetObjectForQR = getObjectForQR;
    }

    public static void updateInboxSize(int inboxSize) {
        new Update(Registration.class)
                .set("InboxSize = ?", inboxSize)
                .execute();

        // update Inbox messages
        Notifications.deleteOldMessages(inboxSize);
    }

    public static void updateDefaultData(DefaultResponse defaults) {
        String locationKey = defaults.mDefaults.getLocationKey();
        String userName = defaults.mDefaults.UserName;
        String imagePath = defaults.mDefaults.ImagePath;

        String masterDataExp = defaults.mDefaults.MasterDataExpiryHours;
        String transDataExp = defaults.mDefaults.TransactionExpirySeconds;
        String inboxSize = defaults.mDefaults.InboxSize;
        String getObjectForQR = defaults.mDefaults.GetObjectForQR;

        //Get existing data from Registration DB
        Registration user = new Registration().select();
        masterDataExp = Utility.IsEmpty(masterDataExp) ? user.MasterDataExpiryHours : masterDataExp;
        transDataExp = Utility.IsEmpty(transDataExp) ? user.TransactionExpirySeconds : transDataExp;
        inboxSize = Utility.IsEmpty(inboxSize) ? user.InboxSize : inboxSize;
        locationKey = Utility.IsEmpty(locationKey) ? user.LocationKey : locationKey;
        getObjectForQR = Utility.IsEmpty(getObjectForQR) ? user.GetObjectForQR : getObjectForQR;

        String updateString = "MasterDataExpiryHours=?, TransactionExpirySeconds=?, InboxSize=?, GetObjectForQR=?";

        new Update(Registration.class)
                .set(updateString, masterDataExp, transDataExp, inboxSize, getObjectForQR)
                .execute();

        if (locationKey != null) {
            if ((!locationKey.isEmpty())) {
                new Update(Registration.class)
                        .set("LocationKey = ?", locationKey)
                        .execute();
            }
        }

        if (userName != null) {
            if ((!userName.isEmpty())) {
                new Update(Registration.class)
                        .set("UserName = ?", userName)
                        .execute();
            }
        }

        if (imagePath != null) {
            if ((!imagePath.isEmpty())) {
                new Update(Registration.class)
                        .set("ImagePath = ?", imagePath)
                        .execute();
            }
        }

    }

    public static Buildings getDefaultLocation() {
        Registration model = new Select().from(Registration.class).executeSingle();
        return Buildings.getBuilding(model.LocationKey);
    }

    public static Registration getInboxSize() {
        return new Select().from(Registration.class).executeSingle();
    }

    public List<Registration> getAllRegistrations() {
        return new Select()
                .from(Registration.class)
                .execute();
    }

    public Registration select() {
        return new Select()
                .from(Registration.class)
                .executeSingle();
    }

    public static void clearTable() {
        new Delete().from(Registration.class).execute();
    }

    public static void saveData(String domain, String userKey, String userName, String imagePath, String account, String apiKey, String mDataExp, String tDataExp, String inboxSize, String locationKey, String getObjectForQR) {
        clearTable();
        new Registration(domain,userKey,userName,imagePath,account,apiKey,mDataExp,tDataExp,inboxSize,locationKey,getObjectForQR).save();
    }
}
