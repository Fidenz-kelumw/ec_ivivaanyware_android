package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.SubObjectAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SubObjectsAdapter extends RecyclerView.Adapter<SubObjectsAdapter.Holder> {

    private Context context;
    private List<SubObject> items;
    private List<SubObject> itemsFiltered;
    private List<Stages> stages;
    private SubObjectActionCallback callback;

    private List<Stages> stagesForSO;

    public SubObjectsAdapter(Context context, List<SubObject> subObjects, String objectType, SubObjectActionCallback callback) {
        this.context = context;
        this.stages = new Stages().selectByObjectType(objectType);
        this.callback = callback;

        this.items = subObjects;
        this.itemsFiltered = subObjects;

        if (items != null && items.size()>0)
            this.stagesForSO = new Stages().selectByObjectType(items.get(0).getObjectType());
        else
            this.stagesForSO = new ArrayList<>();
    }

    public void addItem(SubObject objects) {
        items.add(objects);
        itemsFiltered.add(objects);
        notifyDataSetChanged();
    }

    public void clear() {
        //items.clear();
        //notifyDataSetChanged();
    }

    public void updateActionPerformedObject(SubObject object, SubObjectAction action){

        try {
            for (int i = 0; i < itemsFiltered.size(); i++) {
                if (itemsFiltered.get(i).getObjectKey().equals(object.getObjectKey())) {
                    for (int i1 = 0; i1 < itemsFiltered.get(i).getAction().size(); i1++) {
                        if (itemsFiltered.get(i).getAction().get(i1).getActionId().equals(action.getActionId()))
                            itemsFiltered.get(i).getAction().get(i1).setIsChecked("1");
                        else
                            itemsFiltered.get(i).getAction().get(i1).setIsChecked("0");
                        itemsFiltered.get(i).getAction().get(i1).save();
                    }
                    break;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_sub_object_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final SubObject object = itemsFiltered.get(position);
        holder.name.setText(object.getObjectID());
        holder.description.setText(object.getDescription());

        boolean isColorSet = false;

        for (int i = 0; i < stagesForSO.size(); i++) {
            if (stagesForSO.get(i).Stage != null) {
                if (items.get(position).getStage().equals(stagesForSO.get(i).Stage)) {
                    GradientDrawable gd = new GradientDrawable();
                    gd.setColor(Color.parseColor(stagesForSO.get(i).Color));
                    gd.setCornerRadii(new float[]{5, 5, 0, 0, 0, 0, 5, 5});
                    holder.layLeft.setBackground(gd);
                    isColorSet = true;
                    break;
                }
            }
        }

        if (!isColorSet) {
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(Color.parseColor("#FFFFFF"));
            gd.setCornerRadii(new float[]{5, 5, 0, 0, 0, 0, 5, 5});
            holder.layLeft.setBackground(gd);
        }

        if (object.getAction() != null && object.getAction().size() > 0) {
            holder.description.setVisibility(View.GONE);
            holder.layAction.setVisibility(View.VISIBLE);

            CustomButton[] buttons = {holder.actionButton1, holder.actionButton2, holder.actionButton3};
            resetButtons(buttons);

            for (int i = 0; i < object.getAction().size(); i++) {
                SubObjectAction action = object.getAction().get(i);
                switch (i) {
                    case 0:
                        if (action.getIsChecked() != null && action.getIsChecked().equals("1"))
                            setButtonMarked(holder.actionButton1);

                        holder.actionButton1.setVisibility(View.VISIBLE);
                        holder.actionButton1.setText(action.getButtonText() != null ? action.getButtonText():"");
                        holder.actionButton1.setOnClickListener(view -> callback.onSubObjectActionPerformed(object, action));
                        break;
                    case 1:
                        if (action.getIsChecked() != null && action.getIsChecked().equals("1"))
                            setButtonMarked(holder.actionButton2);

                        holder.actionButton2.setVisibility(View.VISIBLE);
                        holder.actionButton2.setText(action.getButtonText() != null ? action.getButtonText():"");
                        holder.actionButton2.setOnClickListener(view -> callback.onSubObjectActionPerformed(object, action));
                        break;
                    case 2:
                        if (action.getIsChecked() != null && action.getIsChecked().equals("1"))
                            setButtonMarked(holder.actionButton3);

                        holder.actionButton3.setVisibility(View.VISIBLE);
                        holder.actionButton3.setText(action.getButtonText() != null ? action.getButtonText():"");
                        holder.actionButton3.setOnClickListener(view -> callback.onSubObjectActionPerformed(object, action));
                        break;
                }
            }
        } else {
            holder.description.setVisibility(View.VISIBLE);
            holder.layAction.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsFiltered != null ? itemsFiltered.size() : 0;
    }

    private void setButtonMarked(CustomButton button){
        button.setBackgroundResource(R.drawable.button_rounded_disable);
        button.setEnabled(false);
    }

    private void resetButtons(CustomButton[] buttons){
        for (CustomButton button : buttons) {
            button.setVisibility(View.INVISIBLE);
            button.setBackgroundResource(R.drawable.button_rounded_red);
            button.setEnabled(true);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemsFiltered = items;
                } else {
                    List<SubObject> filteredList = new ArrayList<>();
                    for (SubObject row : items) {
                        if (row.getObjectID().toLowerCase().contains(charString.toLowerCase()) || row.getDescription().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    itemsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemsFiltered = (ArrayList<SubObject>) filterResults.values;
                notifyDataSetChanged();
                callback.onSubObjectsSearchPerform(itemsFiltered, charSequence.toString());
            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        CustomTextView name, description;
        CustomButton actionButton1, actionButton2, actionButton3;
        RelativeLayout layLeft;
        LinearLayout layAction;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tv_filter_name);
            description = (CustomTextView) itemView.findViewById(R.id.tv_filter_description);
            layLeft = (RelativeLayout) itemView.findViewById(R.id.lay_indicator);
            layAction = (LinearLayout) itemView.findViewById(R.id.lay_actions);

            actionButton1 = (CustomButton) itemView.findViewById(R.id.btnAction1);
            actionButton2 = (CustomButton) itemView.findViewById(R.id.btnAction2);
            actionButton3 = (CustomButton) itemView.findViewById(R.id.btnAction3);
        }
    }

    public interface SubObjectActionCallback {
        void onSubObjectActionPerformed(SubObject subObject, SubObjectAction action);
        void onSubObjectsSearchPerform(List<SubObject> objects, String query);
    }
}
