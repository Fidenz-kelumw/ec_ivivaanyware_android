package com.ecyber.ivivaanywhere.smartfm.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/29/2016.
 */
@Table(name = "PageLinks")
public class PageLinks extends Model{

    @Column(name = "InfoID")
    @SerializedName("InfoID")
    @Expose
    public long mInfoID;

    @Column(name = "LinkType")
    @SerializedName("LinkType")
    @Expose
    public String mLinkType;

    @Column(name = "LinkName")
    @SerializedName("LinkName")
    @Expose
    public String mLinkName;

    @Column(name = "ObjectType")
    @SerializedName("ObjectType")
    @Expose
    public String mObjectType;

    @Column(name = "ObjectKey")
    @SerializedName("ObjectKey")
    @Expose
    public String mObjectKey;

    @SerializedName("FieldData")
    public List<FieldData> mFieldData;

    public PageLinks() {
        super();
    }

    public PageLinks(long infoID, String linkType, String linkName, String objectType, String objectKey) {
        mInfoID = infoID;
        mLinkType = linkType;
        mLinkName = linkName;
        mObjectType = objectType;
        mObjectKey = objectKey;
    }

    public long getInfoID() {
        return mInfoID;
    }

    public void setInfoID(long infoID) {
        mInfoID = infoID;
    }

    public List<PageLinks> getAll() {
        return new Select().from(PageLinks.class).execute();
    }

    public String getLinkType() {
        return mLinkType;
    }

    public void setLinkType(String linkType) {
        mLinkType = linkType;
    }

    public String getLinkName() {
        return mLinkName;
    }

    public void setLinkName(String linkName) {
        mLinkName = linkName;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public List<FieldData> getFieldData() {
        return mFieldData;
    }

    public void setFieldData(List<FieldData> fieldData) {
        mFieldData = fieldData;
    }
}
