package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.CreateObjectActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.MainActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.WorkOrderListActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.FieldTypeSAARAdapter;
import com.ecyber.ivivaanywhere.smartfm.customviews.LabelView;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.ActionButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.AddAttachmentView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.DateTimeSelectView;
import com.ecyber.ivivaanywhere.smartfm.customviews.DropDownSelectView;
import com.ecyber.ivivaanywhere.smartfm.customviews.FacesView;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeButtons;
import com.ecyber.ivivaanywhere.smartfm.customviews.FieldTypeSAAR;
import com.ecyber.ivivaanywhere.smartfm.customviews.MultiLineEditTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.customviews.SingleLineTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.customviews.SmileyView;
import com.ecyber.ivivaanywhere.smartfm.customviews.StarsView;
import com.ecyber.ivivaanywhere.smartfm.customviews.SubmitButtonVew;
import com.ecyber.ivivaanywhere.smartfm.customviews.ThumbsView;
import com.ecyber.ivivaanywhere.smartfm.customviews.TopicView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Type;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Actions;
import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.PageLinks;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueType;
import com.ecyber.ivivaanywhere.smartfm.popup.ConfirmationDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;
import com.ecyber.ivivaanywhere.smartfm.services.sync.InfoSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectsSync;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Lakmal on 4/20/2016.
 */
public class InfoFragment extends Fragment implements SingleLineTextView.SingleLineTextListener, InfoSync.InfoCallback, DropDownSelectView.DropDownSelectTextListener, SubmitButtonVew.SubmitActionListener, ConfirmationDialog.ConfirmationCallback, DateTimeSelectView.DateTimeSelectListener, MultiLineEditTextView.MultiLineEditTexListener, AddAttachmentView.AddAttachmentCallback, ActionButton.ActionButtonListener, FieldTypeButtons.FieldTypeButtonCallback, FieldTypeSAAR.FieldTypeSAARCallback, ThumbsView.ThumbsCallback, FacesView.FacesCallback, SmileyView.SmileyCallback, StarsView.StarCallback {

    @BindView(R.id.lin_lay_info_container)
    LinearLayout linLayInfoContainer;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private static final String TAG = "InfoFragment";
    private InfoSync infoSync;
    private AccountPermission mAccountPermission;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    private Activity mActivity;
    private ProgressBar mProgressBar;
    private InfoObject mObjectInfoRes;

    private SubmitButtonVew mSubmitButtonVew;
    private MultiLineEditTextView mMultiLineEditTextView;
    private AddAttachmentView mAddAttachVew;
    private static AddAttachmentView mAttchInstance;
    private FieldTypeSAARAdapter saarAdapter;

    private int color;
    private int uploadingCount = 0;
    private String imagePath = null;
    private String mPreviousActivity = "";
    private String mFilterID;
    private String mFromActivity = "";
    private String mObjectKey, mObjectType, mTabName, mLocationKey;
    private boolean hasSubmitConfirmation = false;
    private boolean isTabForceLoad;
    private List<ValueModel> mTempAttach;

    public InfoFragment() {
    }

    @SuppressLint("ValidFragment")
    public InfoFragment(Activity activity, ProgressBar progressBar) {
        mActivity = activity;
        mProgressBar = progressBar;
        mNetwork = new NetworkCheck();
        color = mActivity.getResources().getColor(R.color.info_title_color);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_info, container, false);

        ButterKnife.bind(this, rootView);
        initLayoutData();
        dataLoad();
        swipeRefresh();

        return rootView;
    }

    private void swipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (linLayInfoContainer != null && linLayInfoContainer.getChildCount() > 0)
                linLayInfoContainer.removeAllViews();
            invokeToObjectInfoSync(true);
        });
    }

    private void initLayoutData() {
        mColoredSnackBar = new ColoredSnackbar(getActivity());

        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        mLocationKey = getArguments().getString(SmartConstants.LOCATION_KEY, "");
        mPreviousActivity = getArguments().getString(SmartConstants.ACTIVITY_NAME, "");
        mFilterID = getArguments().getString(SmartConstants.FILTER_ID, "");
        mFromActivity = getArguments().getString(SmartConstants.FROM_WHERE_ACTIVITY, "");
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        mAccountPermission = new AccountPermission();
        infoSync = new InfoSync(getActivity(), this);
        mTempAttach = new ArrayList<>();

        SmartConstants.selectedObjectInfo = new ArrayList<>();
        SideNavigationActivity.isUpdateAvailable = false;
    }

    private void dataLoad() {
        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        mTvMessage.setText("");
        mTvMessage.setVisibility(View.GONE);

        if (isTabForceLoad){
            new InfoObject().clearTable();
            invokeToObjectInfoSync(false);
        } else {
            if (dataDownloadOptimization.isDataAvailableInTable(InfoObject.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    if (!mPreviousActivity.equals("WorkOrderListActivity")) {
                        invokeToObjectInfoSync(false);
                    } else {
                        new InfoObject().clearTable();
                        invokeToObjectInfoSync(false);
                    }

                } else {
                    if (!mPreviousActivity.equals("WorkOrderListActivity")) {
                        invokeToObjectInfoSync(false);
                    } else {
                        createInfoPgByLocalDb(mObjectKey);
                        if (mProgressBar != null) {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                if (!mPreviousActivity.equals("WorkOrderListActivity")) {
                    invokeToObjectInfoSync(false);
                } else {
                    new InfoObject().clearTable();
                    invokeToObjectInfoSync(false);
                }
            }
        }
    }

    private void invokeToObjectInfoSync(boolean isSwipeRefresh) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            if (mProgressBar != null && !isSwipeRefresh) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
            infoSync.getInfoObject(
                    mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType,
                    mObjectKey,
                    mTabName);

        } else {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);

        }
    }

    private void createInfoPgByLocalDb(String objectKey) {
        InfoObject info = new Select().from(InfoObject.class).where("ObjectKey = ?", objectKey).executeSingle();
        if (info == null) {
            invokeToObjectInfoSync(false);
        } else {
            createInfoPage(infoSync.getInfoFromDB(info));
        }
    }

    private void createInfoPage(final InfoObject infoResponse) {
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mObjectInfoRes = infoResponse;

                        if (mObjectInfoRes.getInfoObject().size() != 0)
                            initObjectInfo(mObjectInfoRes.getmObjectInfo());

                        if (mObjectInfoRes.getActions().size() != 0)
                            initActions(mObjectInfoRes.getActions());

                        if (mObjectInfoRes.getPageLinkses().size() != 0)
                            initPageLink(mObjectInfoRes.getPageLinkses());

                        if (mProgressBar != null) {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    } catch (NullPointerException e) {
                        android.util.Log.e(TAG, "run: " + e.getStackTrace().toString());
                    } catch (Exception e) {

                    }
                }
            });
        }
    }

    private void initObjectInfo(List<ObjectInfoModel> objectInfo) {
        int position = 0;
        SmartConstants.selectedObjectInfo = objectInfo;

        for (ObjectInfoModel info : objectInfo) {

            String valueType = info.getValuetype();
            switch (valueType) {
                case SmartConstants.VALUE_TYPE_SLT:
                    createSLTView(info, position);
                    break;
                case SmartConstants.VALUE_TYPE_DS:
                    createDropDownSelect(info, position);
                    break;
                case SmartConstants.VALUE_TYPE_DT:
                    createDateTimeView(info, false, position);
                    break;
                case SmartConstants.VALUE_TYPE_DAT:
                    createDateTimeView(info, true, position);
                    break;
                case SmartConstants.VALUE_TYPE_MLT:
                    createMultiLineView(info, position);
                    break;
                case SmartConstants.VALUE_TYPE_SAS:
                    createSearchAndSelectView(info, position);
                    break;
                case SmartConstants.VALUE_TYPE_ATT:
                    createAttachmentsView(position, info);
                    break;
                case SmartConstants.VALUE_TYPE_BUTTONS:
                    createButtonsView(position, info);
                    break;
                case SmartConstants.VALUE_TYPE_SAAR:
                    createSAARView(position, info);
                    break;
                case SmartConstants.VALUE_TYPE_LABEL:
                    createLabelView(info, position);
                    break;
                case SmartConstants.VALUE_TYPE_FEEDBACK: {
                    String style = info.getStyle();

                    switch (style) {
                        case SmartConstants.FEEDBACK_STYLE_FACES: {
                            createFacesView(info, position);
                            break;
                        }
                        case SmartConstants.FEEDBACK_STYLE_SMILEY: {
                            createSmileyView(info, position);
                            break;
                        }
                        case SmartConstants.FEEDBACK_STYLE_STARS: {
                            createStarsView(info, position);
                            break;
                        }
                        case SmartConstants.FEEDBACK_STYLE_THUMBS: {
                            createThumbsView(info, position);
                            break;
                        }
                    }

                    break;
                }
            }
            position++;
        }
        setSubmitButton();
        checkSubmitIsAvailable();
    }

    private void initActions(List<Actions> actions) {
        View linkTopic = new TopicView(mActivity).setTopic("");
        linLayInfoContainer.addView(linkTopic);
        int position = 0;
        for (Actions action : actions) {
            View actionDetail = new ActionButton(mActivity, InfoFragment.this).setActionButton(action, position, true);
            //linLayInfoContainer.addView(actionDetail);
            TopicView.getButtonContainer().addView(actionDetail);
            position++;
        }
    }

    private void initPageLink(List<PageLinks> pageLinks) {
        int position = 0;

        if (pageLinks != null && pageLinks.size() > 0) {
            View linkTopic = new TopicView(mActivity).setTopic("Links:");
            linLayInfoContainer.addView(linkTopic);

            for (PageLinks pageLink : pageLinks) {

                String linkType = pageLink.getLinkType();
                switch (linkType) {
                    case SmartConstants.LINK_TYPE_DETAILS:
                        View actionDetail = new ActionButton(mActivity, InfoFragment.this).setPageLinkButton(pageLink, position, false);
                        //linLayInfoContainer.addView(actionDetail);
                        TopicView.getButtonContainer().addView(actionDetail);
                        break;
                    case SmartConstants.LINK_TYPE_LIST:
                        View actionList = new ActionButton(mActivity, InfoFragment.this).setPageLinkButton(pageLink, position, false);
                        //linLayInfoContainer.addView(actionList);
                        TopicView.getButtonContainer().addView(actionList);
                        break;
                    case SmartConstants.LINK_TYPE_CREATE:
                        View actionCreate = new ActionButton(mActivity, InfoFragment.this).setPageLinkButton(pageLink, position, false);
                        //linLayInfoContainer.addView(actionCreate);
                        TopicView.getButtonContainer().addView(actionCreate);
                        break;
                }
                position++;
            }
        }
    }

    private void setSubmitButton() {
        mSubmitButtonVew = new SubmitButtonVew(mActivity, InfoFragment.this, mObjectInfoRes.getUpdateButtonText());
        View view = mSubmitButtonVew.getSubmitButtonView();

        boolean isPageReadOnly = true;
        for (ObjectInfoModel model : mObjectInfoRes.getmObjectInfo()) {
            if(model.getEditable().equals("1")){
                isPageReadOnly = false;
                break;
            }
        }
        if(isPageReadOnly) {
            if (mObjectInfoRes.getHideUpdateButton() != null && mObjectInfoRes.getHideUpdateButton().equals("0"))
                linLayInfoContainer.addView(view);
        } else {
            linLayInfoContainer.addView(view);
        }
    }

    private void createSLTView(ObjectInfoModel info, int position) {
        android.util.Log.e("FM", mActivity + "");
        try {
            View view = new SingleLineTextView(mActivity, this, color).getSingleLineTextViewObjectInfo(info, position);
            linLayInfoContainer.addView(view);
        } catch (Exception e) {
            Log.e("FM", e.getMessage());
        }
    }

    private void createDropDownSelect(ObjectInfoModel info, int position) {
        View selectDialog = new DropDownSelectView(mActivity, this, false, false, color).getSelectDialogObjectInfo(info, position, " Select " + info.getFieldname());
        linLayInfoContainer.addView(selectDialog);
    }

    private void createDateTimeView(ObjectInfoModel info, boolean isEnableTime, int position) {
        View view = new DateTimeSelectView(mActivity.getApplicationContext(), getFragmentManager(), this).getDateTimeView(info, isEnableTime, position, color);
        linLayInfoContainer.addView(view);
    }

    private void createMultiLineView(ObjectInfoModel info, int position) {
        mMultiLineEditTextView = new MultiLineEditTextView(mActivity.getApplicationContext(), this, color);
        View view = mMultiLineEditTextView.getMultiLineEditView(info, position);
        linLayInfoContainer.addView(view);
    }

    private void createSearchAndSelectView(ObjectInfoModel info, int position) {
        View selectDialog = new DropDownSelectView(mActivity, this, true, false, color).getSearchAndSelectDialog(mObjectType, mObjectKey, info, position, "Search");
        linLayInfoContainer.addView(selectDialog);
    }

    private void createAttachmentsView(int position, ObjectInfoModel info) {
        boolean isDisableDelete = (info.getDisableDelete() != null && info.getDisableDelete().equals("1")) ? true:false;
        mAddAttachVew = new AddAttachmentView(mActivity.getBaseContext(), getActivity(), InfoFragment.this, this, mAccountPermission.getAccountInfo().get(0), mAccountPermission.getAccountInfo().get(2), mAccountPermission.getAccountInfo().get(1), isDisableDelete);
        View viewAddAttach = mAddAttachVew.getAddAttachView(info, position);
        //set values for temp attachment list
        mTempAttach = info.getValueModels();
        mAddAttachVew.setInstance(mAddAttachVew);

        linLayInfoContainer.addView(viewAddAttach);
    }

    private void createButtonsView(int position, ObjectInfoModel info) {
        android.util.Log.e("FM", mActivity + "");
        try {
            View view = new FieldTypeButtons(getActivity(), position, info, this).generateButtons();
            linLayInfoContainer.addView(view);
        } catch (Exception e) {
            Log.e("FM", e.getMessage());
        }
    }

    private void createSAARView(int position, ObjectInfoModel info) {
        try {
            View view = new FieldTypeSAAR(getActivity(),
                    position,
                    info,
                    this).generateView();
            linLayInfoContainer.addView(view);
        } catch (Exception e) {
            Log.e("FM", e.getMessage());
        }
        return;
    }

    private void setObjectInfoDisplayText(int position, String displayText, String value) {
        Log.e("FM", mObjectInfoRes.getmObjectInfo().get(position).getFieldid() + "/" + displayText + "/" + value);
        mObjectInfoRes.getmObjectInfo().get(position).setDisplaytext(displayText);

        List<ValueModel> valueModels = mObjectInfoRes.getmObjectInfo().get(position).getValueModels();
        if (valueModels != null && valueModels.size() > 0) {
            valueModels.get(0).setValue(value);
            mObjectInfoRes.getmObjectInfo().get(position).setValueModels(valueModels);
        } else {
            List<ValueModel> vList = new ArrayList<>();
            ValueModel model = new ValueModel(mObjectInfoRes.getmObjectInfo().get(position).getFieldid(),
                    ValueType.SINGLE,
                    value,
                    null, null);

            vList.add(model);
            mObjectInfoRes.getmObjectInfo().get(position).setValueModels(vList);
        }

        checkSubmitIsAvailable();
        SideNavigationActivity.isUpdateAvailable = true;
    }

    private void createThumbsView(ObjectInfoModel info, int position) {
        ThumbsView thumbs = new ThumbsView(mActivity, this, color);
        View view = thumbs.getView(info, position);
        linLayInfoContainer.addView(view);
    }

    private void createFacesView(ObjectInfoModel info, int position) {
        FacesView thumbs = new FacesView(mActivity, this, color);
        View view = thumbs.getView(info, position);
        linLayInfoContainer.addView(view);
    }

    private void createSmileyView(ObjectInfoModel info, int position) {
        SmileyView thumbs = new SmileyView(mActivity, this, color);
        View view = thumbs.getView(info, position);
        linLayInfoContainer.addView(view);
    }

    private void createStarsView(ObjectInfoModel info, int position) {
        StarsView thumbs = new StarsView(mActivity, this, color);
        View view = thumbs.getView(info, position);
        linLayInfoContainer.addView(view);
    }

    private void createLabelView(ObjectInfoModel info, int position){
        LabelView view = new LabelView(mActivity);
        linLayInfoContainer.addView(view.getLabelView(info, position));
    }

    private void refreshInfoPage() {
        new InfoObject().clearTable();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    private void checkSubmitIsAvailable() {
        // TODO: 3/10/17 need some logic change 
        boolean disableSubmit = mObjectInfoRes.getmUpdateObjectInfo().equals("1") ? true : false;
        hasSubmitConfirmation = mObjectInfoRes.getmUpdateConfirmation().equals("1") ? true : false;

        if (disableSubmit) {
            List<ObjectInfoModel> objList = mObjectInfoRes.getmObjectInfo();
            SmartConstants.selectedObjectInfo = objList;

            for (ObjectInfoModel objectInfo : objList) {
                String displayTxt = "";

                if (objectInfo.getValueModels() != null && objectInfo.getValueModels().size() > 0) {
                    List<ValueModel> valueModels = objectInfo.getValueModels();
                    displayTxt = objectInfo.getDisplaytext().isEmpty() ? valueModels.get(0).getValue() : objectInfo.getDisplaytext();
                }

                boolean isMandatory = objectInfo.getMandatory().equals("1") ? true : false;
                if (isMandatory && displayTxt.length() <= 0) {
                    mSubmitButtonVew.setEnableButton(false);
                    return;
                } else {
                    mSubmitButtonVew.setEnableButton(true);
                }
            }
        } else {
            mSubmitButtonVew.setEnableButton(false);
        }
    }

    private String getDateISOFormat(String selectedDate) {
        String[] splitDate = selectedDate.split("/");

        String[] yer = splitDate[2].split(" ");
        String selected;
        if (yer.length > 1) {
            selected = yer[0] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + yer[1];
        } else {
            selected = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0] + "T" + "00:00" + ":00";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        String d = null;
        try {
            d = dateFormat.format(dateFormat.parse(selected));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    @Override
    public void onInfoCallSuccess(InfoObject infoObject) {
        swipeRefreshLayout.setRefreshing(false);
        createInfoPage(infoObject);
    }

    @Override
    public void onInfoCallFail(String error) {
        swipeRefreshLayout.setRefreshing(false);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        if (error != null) {
            if (error.equals(SmartConstants.ITEM_NOT_FOUND)) {
                mTvMessage.setVisibility(View.VISIBLE);
                mTvMessage.setText(SmartConstants.ITEM_NOT_FOUND);
                return;
            }
        }

        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(error == null ? SmartConstants.TRY_AGAIN_EXCEPTION : error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
    }

    @Override
    public void onGetWOCategoriesSuccess(List<DataLookUp> dataResponse) {

    }

    @Override
    public void onGetUpdateObjectSuccess(DataUpdateStatus updateRes, String objectKey, JSONArray fieldData) {
        mColoredSnackBar.dismissSnacBar();
        String message = updateRes.getMessage();
        if (updateRes.getSuccess() == 1) {
            infoSync.updateInfoObjectInfoDetails(objectKey, fieldData);
            mColoredSnackBar.showSnackBar(Utility.IsEmpty(message) ? SmartConstants.SUBMIT_SUCCESS : message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
            refreshInfoPage();
        } else {
            mColoredSnackBar.showSnackBar(Utility.IsEmpty(message) ? SmartConstants.SUBMIT_FAILED : message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onGetUpdateObjectError(String error) {
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(error, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onExecuteActionError(DataUpdateStatus response) {
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(SmartConstants.EXECUTE_FAILED, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onExecuteActionSuccess(DataUpdateStatus response) {
        if (response.getSuccess() == 1) {
            if (!response.getPageAction().isEmpty()) {
                mColoredSnackBar.dismissSnacBar();
                Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_LONG).show();
                switch (response.getPageAction()) {
                    case SmartConstants.PG_ACTION_REFRESH:
                        refreshInfoPage();
                        break;
                    case SmartConstants.PG_ACTION_OBJECT_LIST:
                        if (mPreviousActivity.equals("WorkOrderListActivity")) {
                            ObjectsSync.foreExpireObjectList();
                            getActivity().onBackPressed();
                        } else {

                            if (!mFromActivity.equals(SmartConstants.FROM_INVALID_ACTIVITY)) {
                                ObjectsSync.foreExpireObjectList();
                                Intent intent = new Intent(mActivity, WorkOrderListActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                intent.putExtra(SmartConstants.OBJECT_TYPE, mObjectType);
                                intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
                                intent.putExtra(SmartConstants.LOCATION_NAME, "");
                                intent.putExtra(SmartConstants.FILTER_ID, mFilterID);
                                intent.putExtra(SmartConstants.CUSTOM_FILTER, "");
                                intent.putExtra(SmartConstants.MORE, "");
                                intent.putExtra(SmartConstants.PAGE, "filter");
                                getActivity().overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                                startActivity(intent);
                            } else {
                                FragmentTransaction ftn = getFragmentManager().beginTransaction();
                                ftn.detach(this).attach(this).commit();
                                break;
                            }

                        }
                        break;
                    case SmartConstants.PG_ACTION_HOME:
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActivity().startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
                        break;
                }

            } else {
                mColoredSnackBar.dismissSnacBar();
                String message = response.getMessage();
                mColoredSnackBar.showSnackBar(Utility.IsEmpty(message) ? SmartConstants.EXECUTE_FAILED : message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
            }
        } else {
            mColoredSnackBar.dismissSnacBar();
            String message = response.getMessage();
            mColoredSnackBar.showSnackBar(Utility.IsEmpty(message) ? SmartConstants.EXECUTE_FAILED : message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onSingleLineTextFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }


    @Override
    public void onDropDownSelectFocusOut(int position, String displayText, String value) {
        setObjectInfoDisplayText(position, displayText, value);
    }

    @Override
    public void onDateTimeSelectTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, getDateISOFormat(displayText));
    }

    @Override
    public void OnSubmitClick(View v) {
        android.util.Log.e("fm", uploadingCount + "");
        if (uploadingCount > 0) {
            SnackbarManager.show(
                    com.nispok.snackbar.Snackbar.with(mActivity)
                            .text("Please wait.Images are being uploaded.")
                            .actionLabel("Cancel")
                            .actionColor(mActivity.getResources().getColor(R.color.mdtp_red))
                            .actionListener(snackbar -> snackbar.dismiss())
                    , mActivity);
        } else {
            if (hasSubmitConfirmation) {
                String message = "Are you sure you want to submit?";
                new ConfirmationDialog(mActivity, message, SmartConstants.SUBMIT_BUTTON, this).show();
            } else {
                // TODO: 3/10/17 invokeSubmitButtonAction
                invokeSubmitButtonAction(null);
            }
        }
    }

    @Override
    public void onClickConfirmation(String message, int position, Dialog dialogInstance) {
        if (position == SmartConstants.SUBMIT_BUTTON) {
            invokeSubmitButtonAction(dialogInstance);
        } else {
            invokeActionButton(dialogInstance, position);
        }
    }

    private void invokeSubmitButtonAction(Dialog confirmationDialog) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            if (confirmationDialog != null)
                confirmationDialog.dismiss();

            mColoredSnackBar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);

            JSONArray fieldData = new JSONArray();
            for (ObjectInfoModel objectInfo : mObjectInfoRes.getmObjectInfo()) {
                JSONObject field = new JSONObject();
                try {
                    field.put("FieldID", objectInfo.getFieldid());

                    if (objectInfo.getValuetype().equals(Type.ATT) || objectInfo.getValuetype().equals(Type.SAAR)) {
                        JSONArray arrayData = new JSONArray();

                        List<ValueModel> valueModels = objectInfo.getValueModels();
                        if (objectInfo.getValuetype().equals(Type.ATT)) {
                            if (valueModels != null && valueModels.size() > 0) {
                                for (ValueModel model : valueModels) {
                                    JSONObject attObject = new JSONObject();

                                    attObject.put("ATTName", model.getATTName());
                                    attObject.put("ATTType", model.getATTType());

                                    arrayData.put(attObject);
                                }
                            }
                        } else {
                            if (valueModels != null && valueModels.size() > 0) {
                                for (ValueModel model : valueModels) {
                                    arrayData.put(model.getValue());
                                }
                            }
                        }

                        field.put("Value", arrayData);
                    } else {
                        List<ValueModel> valueModels = objectInfo.getValueModels();
                        if (valueModels != null && valueModels.size() > 0) {
                            ValueModel model = valueModels.get(0);
                            field.put("Value", (model.getValue() != null ? model.getValue() : ""));
                        } else {
                            field.put("Value", "");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                fieldData.put(field);
            }


            String dataValues = fieldData.toString();
            System.out.println(dataValues);

            infoSync.updateObjectInfo(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType, mObjectKey, dataValues, fieldData);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onMultiLineEditTextFocusOut(int position, String displayText) {
        setObjectInfoDisplayText(position, displayText, displayText);
    }

    @Override
    public void onCapturePath(String path, AddAttachmentView view) {
        if (path != null) {
            imagePath = path;
        }
        mAttchInstance = view;
    }

    @Override
    public void addFileToValue(String fileName, int position) {
        String ext = fileName.split(Pattern.quote("."))[1];
        String type = "";

        switch (ext) {
            case "jpg": {
                type = "IMG";
                break;
            }

            case "mp4": {
                type = "AUD";
                break;
            }

            case "mov": {
                type = "VID";
                break;
            }
        }

        ValueModel att = new ValueModel(mObjectInfoRes.getmObjectInfo().get(position).getFieldid(), ValueType.OBJECTARRAY, null, fileName, type);
        List<ValueModel> values = mObjectInfoRes.getmObjectInfo().get(position).getValueModels();

        if (values == null) {
            values = new ArrayList<>();
        }

        values.add(att);
        mObjectInfoRes.getmObjectInfo().get(position).setValueModels(values);

        SideNavigationActivity.isUpdateAvailable = true;
    }

    @Override
    public void onAttachedItemRemove(int position, int removeItemPosition, String guiId) {

        List<ObjectInfoModel> models = mObjectInfoRes.getmObjectInfo();
        ObjectInfoModel model = models.get(position);
        List<ValueModel> values = model.getValueModels();

        if (values != null) {
            for (int i = 0; i < values.size(); i++) {

                if (values.get(i).getATTName().equals(guiId)) {
                    values.remove(i);
                    model.setValueModels(values);
                    models.add(position, model);
                    mObjectInfoRes.setmObjectInfo(models);

                    break;
                }

            }
        }
    }

    @Override
    public void onUploadingStatus(int count) {
        uploadingCount = uploadingCount + (count);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SmartConstants.RESULT_IMAGE_CAMERA) {
            if (getActivity().RESULT_OK == resultCode) {
                if (imagePath != null) {
                    Bitmap TempViewBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imagePath), 1024, 768, false);
                    addAttachToList(imagePath, null, SmartConstants.ATTACHMENT_IMG, false);
                }
            }

        } else if (requestCode == SmartConstants.RESULT_IMAGE_GALLERY) {
            if (getActivity().RESULT_OK == resultCode) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                addAttachToList(picturePath, selectedImage, SmartConstants.ATTACHMENT_IMG, false);
            }
        } else if (requestCode == SmartConstants.RESULT_VIDEO_CAPTURE) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Uri videoUri = data.getData();
                mAttchInstance.addVideo(videoUri);
            }
        } else if (requestCode == SmartConstants.RESULT_SKETCH) {
            if (SmartConstants.RESULT_OK == resultCode) {
                Bundle b = data.getExtras();

                String filePath = b.getString(SmartConstants.SKETCH_IMAGE);
                addAttachToList(filePath, null, SmartConstants.ATTACHMENT_IMG, true);
            }
        }
    }


    private void addAttachToList(String picturePath, Uri uri, int type, boolean isFromSketch) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mAttchInstance.addAttachment(picturePath, uri, type, isFromSketch);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void OnLinkButtonClickListener(View v) {
        String tag = v.getTag().toString();
        switch (tag) {
            case SmartConstants.LINK_TYPE_CREATE:
                invokeCreateView(v);
                break;

            case SmartConstants.LINK_TYPE_LIST:
                invokeToObjectList(v);
                break;

            case SmartConstants.LINK_TYPE_DETAILS:
                int position = v.getId();
                PageLinks pageLinks = mObjectInfoRes.getPageLinkses().get(position);
                Intent intent = new Intent(mActivity.getApplicationContext(), SideNavigationActivity.class);
                intent.putExtra(SmartConstants.OBJECT_TYPE, pageLinks.getObjectType());
                intent.putExtra(SmartConstants.OBJECT_KEY, pageLinks.getObjectKey());
                intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
                intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, mFromActivity);
                startActivity(intent);
                mActivity.overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
                break;

        }
    }

    private void invokeCreateView(View v) {
        int position = v.getId();
        PageLinks pageLinks = mObjectInfoRes.getPageLinkses().get(position);

        ArrayList<FieldDataSerializable> fieldDataList = new ArrayList<FieldDataSerializable>();
        for (FieldData fieldData : pageLinks.getFieldData()) {
            FieldDataSerializable fieldDataSerializable = new FieldDataSerializable(fieldData.getFieldId(), fieldData.getValue(), fieldData.getDisplayText());
            fieldDataList.add(fieldDataSerializable);
        }

        Intent intent = new Intent(mActivity.getApplicationContext(), CreateObjectActivity.class);
        intent.putExtra(SmartConstants.OBJECT_TYPE, pageLinks.getObjectType().isEmpty() || pageLinks.getObjectType() == null ? "" : pageLinks.getObjectType());
        intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
        intent.putExtra(SmartConstants.OBJECT_KEY, pageLinks.getObjectKey().isEmpty() || pageLinks.getObjectKey() == null ? "" : pageLinks.getObjectKey());
        intent.putExtra(SmartConstants.CUSTOM_FILTER, fieldDataList);
        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, mFromActivity);
        startActivity(intent);
        mActivity.overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    private void invokeToObjectList(View v) {
        int position = v.getId();
        PageLinks pageLinks = mObjectInfoRes.getPageLinkses().get(position);
        ObjectsSync.foreExpireObjectList();
        Intent intent = new Intent(mActivity.getApplicationContext(), WorkOrderListActivity.class);

        intent.putExtra(SmartConstants.OBJECT_TYPE, pageLinks.getObjectType());
        intent.putExtra(SmartConstants.LOCATION_KEY, "");
        intent.putExtra(SmartConstants.LOCATION_NAME, "");
        intent.putExtra(SmartConstants.FILTER_ID, "");
        intent.putExtra(SmartConstants.RELATED_WORK_ORDER, true);
        intent.putExtra(SmartConstants.SHOW_HOME, false);

        ArrayList<FieldDataSerializable> serializables = new ArrayList<FieldDataSerializable>();
        for (FieldData fieldData : pageLinks.getFieldData()) {
            FieldDataSerializable fieldDataSerializable = new FieldDataSerializable(fieldData.getFieldId(), fieldData.getValue(), fieldData.getDisplayText());
            serializables.add(fieldDataSerializable);
        }

        intent.putExtra(SmartConstants.CUSTOM_FILTER, serializables);
        intent.putExtra(SmartConstants.MORE, "");
        intent.putExtra(SmartConstants.PAGE, "info");
        startActivity(intent);
        mActivity.overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    @Override
    public void OnActionButtonClickListener(View view) {
        int position = view.getId();

        boolean isConfirm = mObjectInfoRes.getActions().get(position).getConfirmation().equals("1") ? true : false;
        if (isConfirm) {
            new ConfirmationDialog(mActivity, "Are you sure you want to proceed?", position, this).show();
        } else {
            invokeExecuteAction(position);
        }
    }

    private void invokeActionButton(Dialog confirmationDialog, int position) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            confirmationDialog.dismiss();
            mColoredSnackBar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            infoSync.executeAction(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType, mObjectKey, mObjectInfoRes.getActions().get(position).getActionID(), mObjectInfoRes.getActions().get(position).getCargo());

        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    private void invokeExecuteAction(int position) {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            mColoredSnackBar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            infoSync.executeAction(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType, mObjectKey, mObjectInfoRes.getActions().get(position).getActionID(), mObjectInfoRes.getActions().get(position).getCargo());
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onFieldTypeButtonStateChange(int objectPosition, String value, String displayText) {
        setObjectInfoDisplayText(objectPosition, displayText, value);
    }

    @Override
    public void onSAARAdapterGenerated(FieldTypeSAARAdapter adapter) {
        saarAdapter = adapter;
    }

    @Override
    public void onNewSAARAdded(String value, String name, int position) {
        mObjectInfoRes.getmObjectInfo().get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.getmObjectInfo().get(position).getValueModels();
        ValueModel valueModel = new ValueModel(mObjectInfoRes.getmObjectInfo().get(position).getFieldid(),
                ValueType.ARRAY, value, null, null);

        if (valueModels != null) {
            valueModels.add(valueModel);
        } else {
            valueModels = new ArrayList<>();
            valueModels.add(valueModel);
        }

        mObjectInfoRes.getmObjectInfo().get(position).setValueModels(valueModels);
        checkSubmitIsAvailable();
    }

    @Override
    public void onSAARRemoved(String value, int position) {
        mObjectInfoRes.getmObjectInfo().get(position).setDisplaytext("");

        List<ValueModel> valueModels = mObjectInfoRes.getmObjectInfo().get(position).getValueModels();
        for (int i = 0; i < valueModels.size(); i++) {
            if (valueModels.get(i).getValue().equals(value)) {
                valueModels.remove(i);
                break;
            }
        }

        mObjectInfoRes.getmObjectInfo().get(position).setValueModels(valueModels);
        checkSubmitIsAvailable();
    }

    @Override
    public void onSAARDialogItemAdded(String value, String name) {
        saarAdapter.addItem(value);
    }

    @Override
    public void onSAARDialogItemRemoved(String value, String name) {
        saarAdapter.removeItem(value);
    }

    public void onThumbsValueChanged(int position, String fieldId, int value) {
        setObjectInfoDisplayText(position, fieldId, Integer.toString(value));
    }

    @Override
    public void onFacesValueChanged(int position, String fieldId, int value) {
        setObjectInfoDisplayText(position, fieldId, Integer.toString(value));
    }

    @Override
    public void onSmileyValueChanged(int position, String fieldId, int value) {
        setObjectInfoDisplayText(position, fieldId, Integer.toString(value));
    }

    @Override
    public void onStarValueChanged(int position, String fieldId, int value) {
        setObjectInfoDisplayText(position, fieldId, Integer.toString(value));
    }
}
