package com.ecyber.ivivaanywhere.smartfm.models;

import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Choota on 11/29/17.
 */

public class MarkerModel {
    public MarkerOptions markerOptions;
    public SpotsItem spotsItem;
    public String markerID;

    public MarkerModel(MarkerOptions markerOptions, SpotsItem spotsItem, String markerID) {
        this.markerOptions = markerOptions;
        this.spotsItem = spotsItem;
        this.markerID = markerID;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public void setMarkerOptions(MarkerOptions markerOptions) {
        this.markerOptions = markerOptions;
    }

    public String getMarkerID() {
        return markerID;
    }

    public void setMarkerID(String markerID) {
        this.markerID = markerID;
    }

    public SpotsItem getSpotsItem() {
        return spotsItem;
    }

    public void setSpotsItem(SpotsItem spotsItem) {
        this.spotsItem = spotsItem;
    }
}
