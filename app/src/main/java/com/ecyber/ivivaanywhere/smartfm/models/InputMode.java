package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diharaw on 2/22/17.
 */

@Table(name = "InputMode")
public class InputMode extends Model {

    @Column(name="ObjectKey")
    private String mObjectKey;

    @Column(name="Mode")
    private String mMode;

    public InputMode() {
        super();
    }

    public InputMode(String objectKey, String mode) {
        mObjectKey = objectKey;
        mMode = mode;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        this.mObjectKey = objectKey;
    }

    public String getMode() {
        return mMode;
    }

    public void setMode(String mode) {
        this.mMode = mode;
    }

    public static List<InputMode> getInputModesForObject(String objectKey) {
        return new Select().from(InputMode.class).where("ObjectKey =?",objectKey).execute();
    }

    public static void setInputModesForObject(List<String> inputModes, String objectKey) {
        for(String im : inputModes) {
            InputMode inputMode = new InputMode(objectKey, im);
            inputMode.save();
        }
    }
}
