package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lakmal on 7/22/16.
 */
public class StageAdapter extends RecyclerView.Adapter<StageAdapter.Holder> {

    private List<Stages> mStagesList = new ArrayList<>();

    public StageAdapter() {

    }

    public void addItem(Stages item) {
        mStagesList.add(item);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_object_statge_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Stages item = mStagesList.get(position);
        holder.txtName.setText(item.getStage());
        holder.vStageColor.setBackgroundColor(Color.parseColor(item.getColor()));

    }

    @Override
    public int getItemCount() {
        return null != mStagesList ? mStagesList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        CustomTextView txtName;
        View vStageColor;

        public Holder(View itemView) {
            super(itemView);
            vStageColor = (View) itemView.findViewById(R.id.view_stage_color);
            txtName = (CustomTextView) itemView.findViewById(R.id.txt_stage_name);
        }
    }

}
