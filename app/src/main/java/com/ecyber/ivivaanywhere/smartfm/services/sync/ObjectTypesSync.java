package com.ecyber.ivivaanywhere.smartfm.services.sync;

import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.CreateFromMenu;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.Roles;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;
import com.ecyber.ivivaanywhere.smartfm.models.Tabs;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ObjectTypeResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectTypes;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class ObjectTypesSync {

    private GetObjectCallBack delegate;

    public ObjectTypesSync(GetObjectCallBack delegate) {
        this.delegate = delegate;
    }

    public void getObjectTypes(String account, String apikey, String userkey) {
        ServiceGenerator.CreateService(GetObjectTypes.class, account)
                .getObjectTypes(userkey, apikey)
                .enqueue(new Callback<ObjectTypeResponce>() {
                    @Override
                    public void onResponse(Call<ObjectTypeResponce> call, Response<ObjectTypeResponce> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getObjectTypes() != null) {
                                    if (response.body().getObjectTypes().size() != 0) {
                                        clearTable();
                                        for (int i = 0; i < response.body().getObjectTypes().size(); i++) {
                                            saveToDb(response.body().getObjectTypes().get(i));
                                            delegate.onObjectTypesDownload(response.body().getObjectTypes().get(i));
                                        }
                                        new TimeCap().updateTableTime("ObjectTypes");
                                        delegate.onObjectTypesDownloadComplete();
                                    } else {
                                        delegate.onObjectTypesDownloadError(SmartConstants.DOWNLOAD_FAILED);
                                    }
                                } else {
                                    delegate.onObjectTypesDownloadError(SmartConstants.DOWNLOAD_FAILED);
                                }
                            } else {
                                delegate.onObjectTypesDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                android.util.Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Object Types", message);
                                delegate.onObjectTypesDownloadError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onObjectTypesDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ObjectTypeResponce> call, Throwable t) {
                        Log.d("Error downloading objectTupes", t.getMessage());
                        delegate.onObjectTypesDownloadError(SmartConstants.DOWNLOAD_FAILED);
                    }
                });
    }


    public interface GetObjectCallBack {
        void onObjectTypesDownload(ObjectType objectType);

        void onObjectTypesDownloadComplete();

        void onObjectTypesDownloadError(String error);
    }

    private void clearTable() {
        new ObjectType().clearTable();
        new Stages().clearTable();
        new Roles().clearTable();
        new Tabs().clearTable();
        new CreateFromMenu().clearTable();
    }

    private void saveToDb(ObjectType objectType) {
        objectType.save();

        if (objectType.getStages() != null) {
            for (int i = 0; i < objectType.getStages().size(); i++) {
                Stages stages = new Stages();
                stages.Stage = objectType.getStages().get(i).Stage;
                stages.Color = objectType.getStages().get(i).Color;
                stages.ObjectType = objectType.ObjectType;
                stages.save();
            }
        }

        /*Tabs defTab = new Tabs();
        defTab.TabType = "Info";
        defTab.TabName = "Info";
        defTab.ObjectType = objectType.ObjectType;
        defTab.save();*/

        if (objectType.getTabs() != null) {
            for (int i = 0; i < objectType.getTabs().size(); i++) {
                Tabs tabs = new Tabs();
                tabs.TabType = objectType.getTabs().get(i).TabType;
                tabs.TabName = objectType.getTabs().get(i).TabName;
                tabs.TabHide = objectType.getTabs().get(i).TabHide;
                tabs.ObjectType = objectType.ObjectType;
                tabs.save();
            }
        }

        if (objectType.getRoles() != null) {
            for (int i = 0; i < objectType.getRoles().size(); i++) {
                Roles roles = new Roles();
                roles.RoleID = objectType.getRoles().get(i).RoleID;
                roles.Color = objectType.getRoles().get(i).Color;
                roles.ObjectType = objectType.ObjectType;
                roles.save();
            }
        }

        List<CreateFromMenu> createFromMenuList = objectType.getCreateFromMenuList();
        if (createFromMenuList != null) {
            if (createFromMenuList.size() > 0) {
                for (CreateFromMenu createFromMenu : createFromMenuList) {
                    createFromMenu.mObjectType = objectType.getObjectType();
                    createFromMenu.save();
                }
            }
        }
    }
}
