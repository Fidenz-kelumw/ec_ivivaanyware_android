package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomCheckedTextView;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Choota on 3/14/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class SAARAdapter extends RecyclerView.Adapter<SAARAdapter.Holder>{
    private Context context;
    private List<OptionListModel> items;
    private List<OptionListModel> orig;
    private List<String> selectedItems;
    private SAARAdapterCallback callback;
    boolean isClicked;

    public SAARAdapter(Context context, List<OptionListModel> items, List<String> selectedItems, SAARAdapterCallback callback) {
        this.context = context;
        this.items = items;
        this.selectedItems = Collections.unmodifiableList(selectedItems);
        this.callback = callback;
    }

    public void addItem(OptionListModel optionListModel){
        items.add(optionListModel);
        notifyDataSetChanged();
    }

    public void clear(){
        items.clear();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<OptionListModel> results = new ArrayList<>();
                if (orig == null)
                    orig = items;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final OptionListModel g : orig) {
                            if (g.getDisplayText().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                items = (List<OptionListModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_saar_single, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        final OptionListModel m = items.get(position);

        holder.name.setText(m.getDisplayText());
        isClicked = false;

        System.out.println(Arrays.toString(selectedItems.toArray()));

        if(selectedItems !=null && selectedItems.contains(m.getValue()))
            holder.name.setChecked(true);
        else
            holder.name.setChecked(false);

        holder.name.setOnClickListener(view -> {
            if(holder.name.isChecked()){
                holder.name.setChecked(false);
                callback.onSAARAdapterItemCheckRemoved(m.getValue(), m.getDisplayText());
            } else {
                holder.name.setChecked(true);
                callback.onSAARAdapterItemChecked(m.getValue(), m.getDisplayText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size():0;
    }

    public interface SAARAdapterCallback{
        void onSAARAdapterItemChecked(String value, String data);
        void onSAARAdapterItemCheckRemoved(String value, String data);
    }

    public class Holder extends RecyclerView.ViewHolder{

        CheckBox box;
        CustomCheckedTextView name;

        public Holder(View itemView) {
            super(itemView);
            box = (CheckBox) itemView.findViewById(R.id.checkBox);
            name = (CustomCheckedTextView) itemView.findViewById(R.id.name);
        }
    }
}
