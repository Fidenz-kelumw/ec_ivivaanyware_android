package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name = "SubObject")
public class SubObject extends Model {

    @Column(name = "ObjectType")
    @SerializedName("ObjectType")
    @Expose
    public String ObjectType;

    @Column(name = "ObjectKey")
    @SerializedName("ObjectKey")
    @Expose
    public String ObjectKey;

    @Column(name = "ObjectID")
    @SerializedName("ObjectID")
    @Expose
    public String ObjectID;

    @Column(name = "Description")
    @SerializedName("Description")
    @Expose
    public String Description;

    @Column(name = "Stage")
    @SerializedName("Stage")
    @Expose
    public String Stage;

    @SerializedName("Action")
    @Expose
    public List<SubObjectAction> actions;

    @Column(name = "ObjectKeyFromSelection")
    public String ObjectKeyFromSelection;

    public SubObject() {
    }

    public SubObject(String objectType, String objectKey, String objectID, String description, String stage, String objectKeyFromSelection, String buttonText, String actionID, String userConfirmation) {
        ObjectType = objectType;
        ObjectKey = objectKey;
        ObjectID = objectID;
        Description = description;
        Stage = stage;
        ObjectKeyFromSelection = objectKeyFromSelection;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public String getObjectKey() {
        return ObjectKey;
    }

    public void setObjectKey(String objectKey) {
        ObjectKey = objectKey;
    }

    public String getObjectID() {
        return ObjectID;
    }

    public void setObjectID(String objectID) {
        ObjectID = objectID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getStage() {
        return Stage;
    }

    public void setStage(String stage) {
        Stage = stage;
    }

    public String getObjectKeyFromSelection() {
        return ObjectKeyFromSelection;
    }

    public void setObjectKeyFromSelection(String objectKeyFromSelection) {
        ObjectKeyFromSelection = objectKeyFromSelection;
    }

    public List<SubObjectAction> getAction() {
        return actions;
    }

    public void setAction(List<SubObjectAction>  actions) {
        this.actions = actions;
    }

    public List<SubObject> getSelectedObjects(String ObjectKeyFromSelection){

        List<SubObject> selectedObjects = new Select().from(SubObject.class).where("ObjectKeyFromSelection =?",ObjectKeyFromSelection).execute();

        if (selectedObjects != null){

            for (int i = 0; i < selectedObjects.size(); i++) {
                selectedObjects.get(i).setAction(new SubObjectAction().actionsForObjectKey(selectedObjects.get(i).getObjectKey()));
            }

        } else {
            return null;
        }

        return selectedObjects;
    }

    public void clearTable(){
        new Delete().from(SubObject.class).execute();
        new Delete().from(SubObjectDeafultFilter.class).execute();
        new Delete().from(SubObjectOptionList.class).execute();
        new Delete().from(SubObjectAction.class).execute();
    }
}
