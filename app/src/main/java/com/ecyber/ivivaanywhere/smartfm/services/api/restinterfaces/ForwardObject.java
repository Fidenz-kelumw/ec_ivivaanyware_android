package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CreateObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ForwardObjectResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 5/23/2016.
 */
public interface ForwardObject {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "ForwardObject")
    Call<ForwardObjectResponse> forwardObject(@Field("apikey") String apiKey,
                                              @Field("UserKey") String userKey,
                                              @Field("ObjectType") String objectType,
                                              @Field("ObjectKey") String objectKey,
                                              @Field("MemberKey") String memberKey,
                                              @Field("MessageText") String messageText);
}
