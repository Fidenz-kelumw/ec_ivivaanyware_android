package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 5/11/2016.
 */
@Table(name = "AttachmentMaster")
public class AttachmentMaster extends Model {

    @Column(name = "ObjectKey")
    public String mObjectKey;

    @Column(name = "AttachmentID")
    @SerializedName("AttachmentID")
    @Expose
    public long mAttachmentID;

    @Column(name = "AttachmentKey")
    @SerializedName("AttachmentKey")
    @Expose
    public int mAttachmentKey;

    @Column(name = "AttachmentType")
    @SerializedName("AttachmentType")
    @Expose
    public String mAttachmentType = "";

    @Column(name = "AttachmentName")
    @SerializedName("AttachmentName")
    @Expose
    public String mAttachmentName = "";

    @Column(name = "FilePath")
    @SerializedName("FilePath")
    @Expose
    public String mFilePath = "";

    @Column(name = "UploadedAt")
    @SerializedName("UploadedAt")
    @Expose
    public String mUploadedAt = "";

    @Column(name = "UserKey")
    @SerializedName("UserKey")
    @Expose
    public String mUserKey;

    @Column(name = "UserName")
    @SerializedName("UserName")
    @Expose
    public String mUserName = "";

    @Column(name = "ImagePath")
    @SerializedName("ImagePath")
    @Expose
    public String mImagePath = "";

    @SerializedName("UploadedUser")
    @Expose
    public User mUploadedUser;

    @Column(name = "ATTType")
    @SerializedName("ATTType")
    @Expose
    public String mATTType;


    public AttachmentMaster() {
        super();
    }

    public AttachmentMaster(String objectKey, long attachmentID, int attachmentKey, String attachmentType, String attachmentName, String filePath, String uploadedAt, String userKey, String userName, String imagePath, String ATTType) {
        mObjectKey = objectKey;
        mAttachmentID = attachmentID;
        mAttachmentKey = attachmentKey;
        mAttachmentType = attachmentType;
        mAttachmentName = attachmentName;
        mFilePath = filePath;
        mUploadedAt = uploadedAt;
        mUserKey = userKey;
        mUserName = userName;
        mImagePath = imagePath;
        mATTType = ATTType;
    }

    public String getmATTType() {
        return mATTType;
    }

    public void setmATTType(String mATTType) {
        this.mATTType = mATTType;
    }

    public long getAttachmentID() {
        return mAttachmentID;
    }

    public void setAttachmentID(long attachmentID) {
        mAttachmentID = attachmentID;
    }

    public int getAttachmentKey() {
        return mAttachmentKey;
    }

    public void setAttachmentKey(int attachmentKey) {
        mAttachmentKey = attachmentKey;
    }

    public String getAttachmentType() {
        return mAttachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        mAttachmentType = attachmentType;
    }

    public String getAttachmentName() {
        return mAttachmentName == null ? "" : mAttachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        mAttachmentName = attachmentName;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getUploadedAt() {
        return mUploadedAt;
    }

    public void setUploadedAt(String uploadedAt) {
        mUploadedAt = uploadedAt;
    }

    public String getUserKey() {
        return mUserKey;
    }

    public void setUserKey(String userKey) {
        mUserKey = userKey;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public User getUploadedUser() {
        return mUploadedUser;
    }

    public void setUploadedUser(User uploadedUser) {
        mUploadedUser = uploadedUser;
    }

    public List<AttachmentMaster> select(String objectKey) {
        return new Select().from(AttachmentMaster.class).where("ObjectKey =?",objectKey).execute();
    }
}
