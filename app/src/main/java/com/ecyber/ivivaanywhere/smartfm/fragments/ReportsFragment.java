package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.WebViewActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.ObjectURLsAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ItemClickSupport;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;
import com.ecyber.ivivaanywhere.smartfm.popup.SetDateFilterDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectURLsSync;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ReportsFragment extends Fragment implements ObjectURLsSync.ObjectURLsCallback, SetDateFilterDialog.SetDateFilterCallback {

    private AccountPermission mAccountPermission;
    private NetworkCheck mNetwork;
    private ColoredSnackbar coloredSnackbar;

    private ObjectURLsAdapter objectURLsAdapter;
    private ObjectURLsSync objectURLsSync;
    private SetDateFilterDialog setDateFilterDialog;

    private List<ObjectURL> items;
    private List<ObjectURL> itemsOriginal;
    private String mObjectType, mObjectKey, mLocationKey, mFilterID, mPreviousActivityName, mFromActivity, mTabName;

    private int selectedButton;
    private int minDate, minMonth, minYear;
    private boolean isStartDateSet, isEndDateSet;
    private boolean isTabForceLoad;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.tv_message)
    CustomTextView message;

    @BindView(R.id.progressbar)
    ProgressBar pb;

    @BindView(R.id.btnDateFilter)
    CustomButton btnDateFilter;

    @BindView(R.id.btnDateFilterClear)
    ImageButton btnDateFilterClear;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_url_items, container, false);
        ButterKnife.bind(this, rootView);

        init();
        loadData();

        initListners();

        return rootView;
    }

    private void init() {
        items = new ArrayList<>();
        itemsOriginal = new ArrayList<>();
        mNetwork = new NetworkCheck();
        mAccountPermission = new AccountPermission();
        coloredSnackbar = new ColoredSnackbar(getActivity());
        objectURLsSync = new ObjectURLsSync(getActivity(), this);
        setDateFilterDialog = new SetDateFilterDialog(getActivity(), this, getFragmentManager());

        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mLocationKey = getArguments().getString(SmartConstants.LOCATION_KEY);
        mFilterID = getArguments().getString(SmartConstants.FILTER_ID);
        mPreviousActivityName = getArguments().getString(SmartConstants.ACTIVITY_NAME);
        mFromActivity = getArguments().getString(SmartConstants.FROM_WHERE_ACTIVITY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;
        recyclerview.addItemDecoration(new RecyclerDecorator(this.getActivity().getApplicationContext(), spanCount, spacing, includeEdge, false));

        btnDateFilterClear.setEnabled(false);
    }

    private void initListners() {
        swipeRefresh.setOnRefreshListener(() -> {
            btnDateFilter.setText("Set Date Filter");
            btnDateFilterClear.setEnabled(false);

            filterDataWithFilterValue(null, null);

            invokeToObjectItemSync();
        });

        ItemClickSupport.addTo(recyclerview).setOnItemClickListener((recyclerView, position, v) -> {
            ObjectURL objectURL = items.get(position);

            if (objectURL != null && objectURL.getURLPath() != null && !objectURL.getURLPath().equals("")) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("weburl", objectURL.getURLPath());
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.slide_bottomlayer_display, R.anim.keep_active);
            }
        });
    }

    private void loadData() {
        pb.setVisibility(View.VISIBLE);

        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();

        if (isTabForceLoad) {
            invokeToObjectItemSync();
        } else {
            if (dataDownloadOptimization.isDataAvailableInTable(ObjectURL.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToObjectItemSync();
                } else {
                    boolean isAvailable = objectURLsSync.isDataAvailable();
                    if (!isAvailable) {
                        invokeToObjectItemSync();
                    }
                }
            } else {
                invokeToObjectItemSync();
            }
        }
    }

    private void invokeToObjectItemSync() {
        boolean state = mNetwork.NetworkConnectionCheck(getActivity());
        if (state) {
            objectURLsSync.getObjectURLs(mAccountPermission.getAccountInfo().get(0),
                    mObjectKey,
                    mObjectType,
                    mAccountPermission.getAccountInfo().get(2),
                    mTabName,
                    mAccountPermission.getAccountInfo().get(1));
        } else {
            coloredSnackbar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
            pb.setVisibility(View.INVISIBLE);
            message.setVisibility(View.VISIBLE);
            message.setText("Error in network connection");
        }
    }

    private void filterDataWithFilterValue(String start, String end) {

        List<ObjectURL> filteredData = new ArrayList<>();

        if (start == null) {
            filteredData = new ArrayList<>(itemsOriginal);
            items = new ArrayList<>(itemsOriginal);
        } else {
            Date stDate = getDate(start);
            Date edDate = getDate(end);

            for (ObjectURL item : itemsOriginal) {

                Date d = getDate(item.getUpdatedAt());
                if (d.compareTo(stDate) >= 0 && d.compareTo(edDate) <= 0) {
                    filteredData.add(item);
                }
            }
        }

        if (filteredData.size() == 0) {
            message.setVisibility(View.VISIBLE);
            message.setText("No items found");
        } else {
            message.setVisibility(View.INVISIBLE);
        }

        swipeRefresh.setRefreshing(false);
        objectURLsAdapter = new ObjectURLsAdapter(getActivity(), filteredData);
        recyclerview.setAdapter(objectURLsAdapter);
    }

    private Date getDate(String date) {

        String dateX = date;
        if (dateX.length() > 10)
            dateX = date.substring(0, 10);

        String[] data = dateX.split("-");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.valueOf(data[0]));
        cal.set(Calendar.MONTH, Integer.valueOf(data[1]) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.valueOf(data[2]));

        return cal.getTime();
    }

    @OnClick(R.id.btnDateFilter)
    public void onSetFilterPressed() {

        String[] data = btnDateFilter.getText().toString().split(" to ");
        String sd, ed;

        if (data.length == 2) {
            sd = data[0];
            ed = data[1];
        } else {
            sd = "";
            ed = "";
        }
        setDateFilterDialog.show(sd, ed);
    }

    @OnClick(R.id.btnDateFilterClear)
    public void onCLearFilterPressed() {
        btnDateFilter.setText("Set Date Filter");
        btnDateFilterClear.setEnabled(false);

        filterDataWithFilterValue(null, null);
        message.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ///null.unbind();
    }

    @Override
    public void onObjectURLsSuccess(List<ObjectURL> objectURL) {
        try {
            pb.setVisibility(View.INVISIBLE);
            message.setVisibility(View.INVISIBLE);

            itemsOriginal.clear();
            items.clear();

            itemsOriginal = objectURL;
            items = new ArrayList<>(itemsOriginal);

            swipeRefresh.setRefreshing(false);
            objectURLsAdapter = new ObjectURLsAdapter(getActivity(), objectURL);
            recyclerview.setAdapter(objectURLsAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onObjectURLsFail(String error) {
        message.setVisibility(View.VISIBLE);
        pb.setVisibility(View.INVISIBLE);
        message.setText("No items found");
    }

    @Override
    public void onObjectURLsFoundItems(List<ObjectURL> objectURL) {
        pb.setVisibility(View.INVISIBLE);
        message.setVisibility(View.INVISIBLE);

        itemsOriginal.clear();
        items.clear();

        itemsOriginal = objectURL;
        items = new ArrayList<>(itemsOriginal);

        swipeRefresh.setRefreshing(false);
        objectURLsAdapter = new ObjectURLsAdapter(getActivity(), objectURL);
        recyclerview.setAdapter(objectURLsAdapter);
    }

    @Override
    public void onFilterDateSet(String startDate, String endDate) {
        this.btnDateFilter.setText(startDate + " to " + endDate);
        btnDateFilterClear.setEnabled(true);

        filterDataWithFilterValue(startDate, endDate);
    }
}
