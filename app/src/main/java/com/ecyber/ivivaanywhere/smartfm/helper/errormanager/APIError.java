package com.ecyber.ivivaanywhere.smartfm.helper.errormanager;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class APIError {
    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }
}
