package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SuccessResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.UpdateUserGPSCoordinates;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class UpdateUserGPSCoordinatesSync {

    private Context context;
    private UpdateUserGPSCoordinateCallback callback;
    private AccountPermission account;

    public UpdateUserGPSCoordinatesSync(Context context) {
        this.context = context;
        this.account = new AccountPermission();
    }

    public void update(UpdateUserGPSCoordinateCallback callback, String lat, String lan){
        this.callback = callback;

        ServiceGenerator.CreateService(UpdateUserGPSCoordinates.class,account.getAccountInfo().get(0))
                .updateUserGPSCoordinates(account.getAccountInfo().get(2),lat,lan,account.getAccountInfo().get(1))
                .enqueue(new Callback<SuccessResponse>() {
                    @Override
                    public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {

                        if (response.body() != null) {
                            if (response.body().getSuccess().equals("1"))
                                callback.onUpdateUserGPSCoordinateSuccess();
                            else
                                callback.onUpdateUserGPSCoordinateFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                        } else
                            callback.onUpdateUserGPSCoordinateFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }

                    @Override
                    public void onFailure(Call<SuccessResponse> call, Throwable t) {
                        Log.e("UpdateUserGPSCoordinate", t.getLocalizedMessage());
                        callback.onUpdateUserGPSCoordinateFail(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                });
    }

    public interface UpdateUserGPSCoordinateCallback {
        void onUpdateUserGPSCoordinateSuccess();
        void onUpdateUserGPSCoordinateFail(String message);
    }
}
