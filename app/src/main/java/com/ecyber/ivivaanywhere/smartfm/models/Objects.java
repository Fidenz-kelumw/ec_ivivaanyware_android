package com.ecyber.ivivaanywhere.smartfm.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class Objects {

    @SerializedName("ObjectType")
    @Expose
    public String mObjectType;

    @SerializedName("ObjectKey")
    @Expose
    public String mObjectKey;

    @SerializedName("ObjectID")
    @Expose
    public String mObjectID;

    @SerializedName("Description")
    @Expose
    public String mDescription;

    @SerializedName("Stage")
    @Expose
    public String mStage;

    public Objects(String objectType, String objectKey, String objectID, String descrition, String stage) {
        mObjectType = objectType;
        mObjectKey = objectKey;
        mObjectID = objectID;
        mDescription = descrition;
        mStage = stage;
    }

    public Objects() {
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    public String getObjectKey() {
        return mObjectKey;
    }

    public void setObjectKey(String objectKey) {
        mObjectKey = objectKey;
    }

    public String getObjectID() {
        return mObjectID;
    }

    public void setObjectID(String objectID) {
        mObjectID = objectID;
    }

    public String getDescrition() {
        return mDescription;
    }

    public void setDescrition(String descrition) {
        mDescription = descrition;
    }

    public String getStage() {
        return mStage;
    }

    public void setStage(String stage) {
        mStage = stage;
    }

}
