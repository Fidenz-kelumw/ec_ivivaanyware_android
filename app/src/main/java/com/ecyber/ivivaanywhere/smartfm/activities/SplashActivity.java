package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.activeandroid.util.Log;
import com.crashlytics.android.Crashlytics;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    private final String ALERT = "alert";
    private final String TITLE = "title";
    private final String TYPE = "type";
    private final String FROM = "F";
    private final String O_TYPE = "OT";
    private final String O_KEY = "OK";
    private final String LOCATION_KEY = "LK";
    private SmartSharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        preferences = new SmartSharedPreferences(this);
        getNotificationData();
        Log.e(SplashActivity.class.getSimpleName(), "Stating splash");
    }

    private void getNotificationData() {
        Bundle data = getIntent().getExtras();

        if (data == null) {
            preferences.setNotifyID("");
        }

        if (data != null) {
            String notifyID = preferences.getNotifyID();
            String googleMsgID = data.getString("google.message_id");
            if (googleMsgID != null) {
                if (!googleMsgID.equals(notifyID)) {
                    notifySaveAndInvoke(data, googleMsgID);
                } else {
                    defaultActivity("", "", 0, "0");
                }
            } else {
                defaultActivity("", "", 0, "0");
            }
        } else {
            defaultActivity("", "", 0, "0");
        }
    }

    private void notifySaveAndInvoke(Bundle data, String googleId) {
        String oType = "";
        String oKey = "";
        String locReqKey = "0";

        long msgId = 0;
        JSONObject dataAsJson = getDataAsJson(data);
        if (dataAsJson != null) {
            try {
                String alert = dataAsJson.getString(ALERT);
                String from = dataAsJson.getString(FROM);

                oType = dataAsJson.getString(O_TYPE);
                oKey = dataAsJson.getString(O_KEY);
                locReqKey = dataAsJson.getString(LOCATION_KEY);

                oType = Utility.IsEmpty(oType) ? "" : oType;
                oKey = Utility.IsEmpty(oKey) ? "" : oKey;
                if (!googleId.isEmpty()) {
                    preferences.setNotifyID(googleId);
                    msgId = Notifications.saveToDB(alert, from, oType, oKey);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        defaultActivity(oType, oKey, msgId, locReqKey);
    }

    private void defaultActivity(String oType, String oKey, long msgId, String locationKey) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.putExtra(SmartConstants.OBJECT_TYPE, oType);
        intent.putExtra(SmartConstants.OBJECT_KEY, oKey);
        intent.putExtra(SmartConstants.MESSAGE_ID, msgId);
        intent.putExtra(SmartConstants.LOCATION_REQ_KEY, locationKey);
        startActivity(intent);
        finish();
    }

    private JSONObject getDataAsJson(Bundle data) {
        JSONObject result = new JSONObject();
        try {
            result.put(ALERT, data.get(ALERT) != null ? data.get(ALERT).toString() : "");
            result.put(TITLE, data.get(TITLE) != null ? data.get(TITLE).toString() : "");
            result.put(TYPE, data.get(TYPE) != null ? data.get(TYPE).toString() : "");
            result.put(FROM, data.get(FROM) != null ? data.get(FROM).toString() : "");
            result.put(O_TYPE, data.get(O_TYPE) != null ? data.get(O_TYPE).toString() : "");
            result.put(O_KEY, data.get(O_KEY) != null ? data.get(O_KEY).toString() : "");
            result.put(LOCATION_KEY, data.get(LOCATION_KEY) != null ? data.get(LOCATION_KEY).toString() : "0");
        } catch (Exception e) {

        }
        return result;
    }

}
