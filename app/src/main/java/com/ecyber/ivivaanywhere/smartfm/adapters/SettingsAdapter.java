package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.models.Settings;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/8/16.
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.Holder>{

    List<Settings> items;
    Context context;

    public SettingsAdapter(List<Settings> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public void addItems(Settings settings) {
        items.add(settings);
        notifyDataSetChanged();
    }


    @Override
    public SettingsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_settings_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(SettingsAdapter.Holder holder, int position) {
        holder.name.setText(items.get(position).getName());
        holder.date.setText(items.get(position).getDate());
        holder.count.setText(items.get(position).getCount());
    }

    @Override
    public int getItemCount() {
        return null != items ? items.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomTextView name, count, date;

        public Holder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.tv_name);
            count = (CustomTextView) itemView.findViewById(R.id.tv_item_count);
            date = (CustomTextView) itemView.findViewById(R.id.tv_lastdate);
        }
    }
}
