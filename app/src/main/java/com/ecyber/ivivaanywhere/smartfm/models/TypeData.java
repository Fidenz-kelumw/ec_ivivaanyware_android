package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/7/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name="ObjectItemsButtons")
public class TypeData extends Model {

    @Column(name = "Name")
    public String Name;

    @Column(name = "Value")
    public String Value;

    @Column(name = "ItemKey")
    public String ItemKey;

    public TypeData() {
    }

    public TypeData(String name, String value) {
        Name = name;
        Value = value;
    }

    public TypeData(String name, String value, String itemKey) {
        Name = name;
        Value = value;
        ItemKey = itemKey;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getItemKey() {
        return ItemKey;
    }

    public void setItemKey(String itemKey) {
        ItemKey = itemKey;
    }

    public List<TypeData> getAllButtons(String itemKey) {
        return new Select().from(TypeData.class).where("ItemKey =?",itemKey).execute();
    }

    public void clearTable() {
        new Delete().from(TypeData.class).execute();
    }

    @Override
    public String toString() {
        return "TypeData{" +
                "Name='" + Name + '\'' +
                ", Value='" + Value + '\'' +
                '}';
    }
}
