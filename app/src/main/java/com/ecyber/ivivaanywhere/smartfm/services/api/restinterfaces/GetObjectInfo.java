package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Info;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Lakmal on 4/29/2016.
 */
public interface GetObjectInfo  {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectInfo")
    Call<ResponseBody> getObjectInfoV2(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectInfo")
    Call<Info> getObjectInfo(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey);


    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "{path}")
    Call<DataResponse> getWOCategories(@Path("path") String path, @Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey,
                                       @Field("FieldInfo") String fieldData, @Field("LocationKey") String locationKey);

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "UpdateObjectInfo")
    Call<DataUpdateStatus> getUpdateObjectInfo(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey,
                                       @Field("FieldData") String fieldData);



    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "ExecuteAction")
    Call<DataUpdateStatus> getExcecuteAction(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("ActionID")String actionId,@Field("Cargo") String cargo);
}
