package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

/**
 * Created by Choota on 3/13/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class Type {
    public static final String SLT = "SLT";
    public static final String DS = "DS";
    public static final String BUTTONS = "BUTTONS";
    public static final String SAS = "SAS";
    public static final String SAAR = "SAAR";
    public static final String MLT = "MLT";
    public static final String DT = "DT";
    public static final String DAT = "DAT";
    public static final String FEEDBACK = "FEEDBACK";
    public static final String ATT = "ATT";
    public static final String LABEL = "LABEL";
}
