package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.LocationNotificationGenerator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.MarkerModel;
import com.ecyber.ivivaanywhere.smartfm.models.SpotResolveModel;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.ecyber.ivivaanywhere.smartfm.popup.MarkerDetailAlert;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SpotDetailsResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetSpotsSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateNearestSpotSync;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NearbyAssetsFragment extends Fragment implements OnMapReadyCallback, GetSpotsSync.GetSpotsCallback, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, MarkerDetailAlert.MarkerDetailCallback, LocationNotificationGenerator.NearLocationCallback, UpdateNearestSpotSync.UpdateNearestSpotCallback {

    @BindView(R.id.fabMyLocation)
    FloatingActionButton fabMyLocation;

    private Context context;
    private GoogleMap map;
    private LocationManager locationManager;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Marker currLocationMarker;
    private LocationNotificationGenerator notificationGenerator;

    private LatLng myCurrentLocation, myOldLocation;

    private GetSpotsSync getSpotsSync;
    private ColoredSnackbar snackbar;

    private List<String> insideSpots;
    private List<MarkerModel> spotMarkers;
    private List<LatLng> spotMarkersLocation;
    private List<SpotsItem> spotsItems;

    private MarkerDetailAlert markerDetailAlert;
    private String objectType, objectKey;

    private boolean isTabForceLoad;
    private boolean isMarkerRotating, isFirstTimeLocation, isFromDetailPopup;
    private FragmentActivity myContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nearby_assets, container, false);
        ButterKnife.bind(this, rootView);

        objectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        objectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        String[] nearSpots = getArguments().getStringArray(SmartConstants.NEAR_LOCATION_SPOTS);
        if (nearSpots != null && nearSpots.length > 0)
            insideSpots = Arrays.asList(nearSpots);
        else
            insideSpots = null;

        isFirstTimeLocation = false;
        setUpUI();

        return rootView;
    }

    private void setUpUI() {
        notificationGenerator = new LocationNotificationGenerator(getActivity(), this, objectType, objectKey);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        getSpotsSync = new GetSpotsSync(getActivity(), this);
        snackbar = new ColoredSnackbar(getActivity());
        markerDetailAlert = new MarkerDetailAlert(getActivity(), this);

        isMarkerRotating = false;
        spotMarkers = new ArrayList<>();
        spotMarkersLocation = new ArrayList<>();

        //SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        getMapFragment().getMapAsync(this);
    }

    private MapFragment getMapFragment() {
        android.app.FragmentManager fm = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fm = getFragmentManager();
        } else {
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.map);
    }

    private void getSpotsToDrawOnMap() {
        try {
            snackbar.showSnackBar(fabMyLocation, "Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            getSpotsSync.requestSpotsAPI();
        } catch (Exception e) {
            e.printStackTrace();
            snackbar.dismissSnacBar();
            snackbar.showSnackBar(fabMyLocation, "Something went wrong", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
        }
    }

    private void addMarkersOnMap(List<SpotsItem> spots) {

        if (insideSpots != null) {
            try {
                if (spots != null && spots.size() > 0) {
                    for (SpotsItem spot : spots) {
                        for (String insideSpot : insideSpots) {
                            if (spot.getSpotID().equals(insideSpot))
                                spot.setInside(true);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            for (SpotsItem spot : spots) {
                spot.setInside(false);
            }
        }

        try {
            for (SpotsItem spot : spots) {
                if (spot.getLan() != null && spot.getLat() != null) {
                    try {
                        LatLng location = new LatLng(Double.parseDouble(spot.getLat()), Double.parseDouble(spot.getLan()));
                        myCurrentLocation = location;
//                        MarkerOptions markerOption = new MarkerOptions().position(location);
//
//                        Marker marker = map.addMarker(markerOption);
//
//                        spotMarkers.add(new MarkerModel(markerOption, spot, marker.getId()));
//                        spotMarkersLocation.add(location);

                        drawMarkerWithCircle(location, spot);
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                }
            }

            map.getUiSettings().setMapToolbarEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setCompassEnabled(true);

            map.setBuildingsEnabled(true);
            map.setMyLocationEnabled(true);

            map.setOnMarkerClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawMarkerWithCircle(LatLng location, SpotsItem item) {
        double radiusInMeters = (item.getDistance() != null ? Double.parseDouble(item.getDistance()) : 10);
        int strokeColor;
        int shadeColor;

        BitmapDescriptor descriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        if (item.isInside()) {
            descriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
            strokeColor = 0xff006bff;   //blue outline
            shadeColor = 0x44006bff;    //opaque blue fill
        } else {
            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
            strokeColor = 0xffff0000;   //red outline
            shadeColor = 0x44ff0000;    //opaque red fill
        }

        CircleOptions circleOptions = new CircleOptions().center(location).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(4);
        Circle mCircle = map.addCircle(circleOptions);

        MarkerOptions markerOptions = new MarkerOptions().position(location).icon(descriptor);
        Marker mMarker = map.addMarker(markerOptions);

//        MarkerOptions markerOption = new MarkerOptions().position(location);
//        Marker marker = map.addMarker(markerOption);

        spotMarkers.add(new MarkerModel(markerOptions, item, mMarker.getId()));
        spotMarkersLocation.add(location);
    }

    public void updateMarkerImage(List<String> insideSpots) {
        try {
            List<SpotsItem> regeneratedSpots = new ArrayList<>();
            for (MarkerModel marker : spotMarkers) {
                marker.getSpotsItem().setInside(false);
                regeneratedSpots.add(marker.spotsItem);
            }

            for (String spot : insideSpots) {
                for (MarkerModel marker : spotMarkers) {
                    if (marker.getSpotsItem().getSpotID().equals(spot))
                        marker.getSpotsItem().setInside(true);
                }
            }

            map.clear();
            spotMarkers = new ArrayList<>();
            spotMarkersLocation = new ArrayList<>();
            addMarkersOnMap(regeneratedSpots);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void focusMyLocation() {
        if (myCurrentLocation != null) {
            CameraPosition newCamPos = new CameraPosition(myCurrentLocation,
                    17.0f,
                    40,
                    map.getCameraPosition().bearing); //use old bearing
            map.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 1500, null);
        }
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your \"Locations Settings\" is set to 'Off'.\nPlease enable \"Locations Settings\" to " +
                        "use this feature")
                .setPositiveButton("Location Settings", (paramDialogInterface, paramInt) -> {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                })
                .setNegativeButton("Cancel", (paramDialogInterface, paramInt) -> {
                });
        dialog.show();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @OnClick(R.id.fabMyLocation)
    public void onMyLocationViewClicked() {
        focusMyLocation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null)
            getFragmentManager().beginTransaction().remove(mapFragment).commit();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();

        try {
            notificationGenerator.stopRepeatingTask();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("OnResume.....");

        try {
            if (mGoogleApiClient != null)
                mGoogleApiClient.connect();

            notificationGenerator.startRepeatingTask();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        this.getSpotsToDrawOnMap();

        if (checkLocation()) {
            this.buildGoogleApiClient();
            this.mGoogleApiClient.connect();
        }
    }

    @Override
    public void onSpotsFound(boolean status, List<SpotsItem> spots) {
        if (status && spots.size() > 0) {
            this.spotsItems = spots;
            addMarkersOnMap(spots);
            snackbar.dismissSnacBar();
        } else {
            snackbar.dismissSnacBar();
            snackbar.showSnackBar(fabMyLocation, "Something went wrong", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String markerId = marker.getId();
        for (MarkerModel model : spotMarkers) {
            if (markerId.equals(model.markerID)) {
                markerDetailAlert.show(model.spotsItem);
            }
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            myCurrentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

//            MarkerOptions markerOptions = new MarkerOptions();
//            markerOptions.position(myCurrentLocation);
//            markerOptions.flat(true);
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_navigation_36dp));
//
//            currLocationMarker = map.addMarker(markerOptions);
            focusMyLocation();
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000); //5 seconds
        mLocationRequest.setFastestInterval(1000); //3 seconds
        mLocationRequest.setSmallestDisplacement(2);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setMaxWaitTime(100);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        myOldLocation = new LatLng(myCurrentLocation.latitude, myCurrentLocation.longitude);
        myCurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());

        notificationGenerator.setPriorityLocationData(spotsItems, location);
        notificationGenerator.setNotifiedSpots(insideSpots);
        if (!isFirstTimeLocation) {
            isFirstTimeLocation = true;
            notificationGenerator.stopRepeatingTask();
            notificationGenerator.startRepeatingTask();
        }

// To add custom marker as current location
//        if(currLocationMarker != null){
//            currLocationMarker.remove();
//        }
//
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(myCurrentLocation);
//        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_navigation_36dp));
//        currLocationMarker = map.addMarker(markerOptions);

//        if (currLocationMarker == null) {
//            MarkerOptions markerOptions = new MarkerOptions();
//            markerOptions.position(myCurrentLocation);
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_navigation_36dp));
//            currLocationMarker = map.addMarker(markerOptions);
//            GoogleMapMarkerAnimation.animateMarkerToGB(currLocationMarker, myCurrentLocation, new GoogleMapMarkerAnimation.LatLngInterpolator.Linear());
//        } else {
//            currLocationMarker.setPosition(myOldLocation);
//            GoogleMapMarkerAnimation.animateMarkerToGB(currLocationMarker, myCurrentLocation, new GoogleMapMarkerAnimation.LatLngInterpolator.Linear());
//        }

//        if (location.hasBearing()) {
//            CameraPosition cameraPosition = new CameraPosition.Builder()
//                    .target(myCurrentLocation)             // Sets the center of the map to current location
//                    .zoom(17)                   // Sets the zoom
//                    .bearing(location.getBearing()) // Sets the orientation of the camera to east
//                    .tilt(40)                   // Sets the tilt of the camera to 0 degrees
//                    .build();                   // Creates a CameraPosition from the builder
//            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        }
    }

    @Override
    public void onMarkerDetailViewPressed(SpotDetailsResponse item) {

        isFromDetailPopup = true;

        Log.e("Location alert", "popup clicked");
        Intent intent = new Intent(getActivity(), SideNavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SmartConstants.OBJECT_TYPE, item.getNextpageObjectType());
        intent.putExtra(SmartConstants.OBJECT_KEY, item.getNextPageObjectKey());
        intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_GPS_ACTIVITY);
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);

        try {
            if (mGoogleApiClient != null)
                mGoogleApiClient.disconnect();
            notificationGenerator.stopRepeatingTask();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMarkerDetailError() {
        snackbar.showSnackBar(fabMyLocation, "Location details not found", ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_LONG);
    }

    @Override
    public void onUserWalksToLocation(List<SpotResolveModel> models) {
        for (SpotResolveModel model : models) {
            new UpdateNearestSpotSync().updateNearestSpot(this, model.getSpotItem().getSpotID(), String.valueOf(model.getDistenceToLocation()), "Near");
        }
    }

    @Override
    public void onNotifiedSpotsChange(List<SpotResolveModel> notifiedSpots) {
        List<String> spots = new ArrayList<>();
        for (SpotResolveModel spot : notifiedSpots) {
            spots.add(spot.getSpotItem().getSpotID());
        }

        insideSpots = new ArrayList<>(spots);
    }

    @Override
    public void onNearestSpotUpdated(boolean status) {
        Log.i("Near spot updated", "" + status);
    }
}
