package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

/**
 * Created by diharaw on 3/14/17.
 */

public class ThumbsView {

    private final View mView;
    private CustomTextView mTitle;
    private Context mContext;
    private String mFieldId;
    private int mPosition;
    private ObjectInfoModel mObjectInfo;
    private ImageButton mUp;
    private ImageButton mDown;
    private ImageButton mReset;
    private int mValue;
    private ThumbsCallback mCallback;
    private boolean mIsEditable;

    public ThumbsView(Context context, ThumbsCallback callback,  @ColorInt int color) {
        mContext = context;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_thumbs, null);

        mTitle = (CustomTextView) mView.findViewById(R.id.tv_thumbs_title);
        mTitle.setTextColor(color);

        mCallback = callback;

        mUp = (ImageButton) mView.findViewById(R.id.btn_thumbs_up);
        mDown = (ImageButton) mView.findViewById(R.id.btn_thumbs_down);
        mReset = (ImageButton) mView.findViewById(R.id.btn_reset_thumbs);

        registerCallbacks();
    }

    public View getView(ObjectInfoModel info, int position) {
        mFieldId = info.getFieldid();
        mPosition = position;
        mObjectInfo = info;

        setFieldName(info.getFieldname());
        String defVal = "";
        if (info.getValueModels() != null) {
            List <ValueModel> valueModels = info.getValueModels();
            defVal = valueModels.get(0).getValue();
        }

        setDefaultData();
        mIsEditable = info.getEditable().equals("1") ? true : false;
        setEditable();

        return mView;
    }

    private void setFieldName(String fieldName) {
        mTitle.setText(fieldName);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setValue(int value) {
            mValue = value;

            if(mValue == 1) {
                mDown.setImageResource(R.drawable.img_feedback_thumbsdown_selected);
                mUp.setImageResource(R.drawable.img_feedback_thumbsup_unselected);
            } else if (mValue == 2) {
                mDown.setImageResource(R.drawable.img_feedback_thumbsdown_unselected);
                mUp.setImageResource(R.drawable.img_feedback_thumbsup_selected);
            } else {
                mDown.setImageResource(R.drawable.img_feedback_thumbsdown_unselected);
                mUp.setImageResource(R.drawable.img_feedback_thumbsup_unselected);
            }
    }

    private void setEditable() {
        mDown.setEnabled(mIsEditable);
        mUp.setEnabled(mIsEditable);
        mReset.setEnabled(mIsEditable);
    }

    private void registerCallbacks() {

        mUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(2);
                mCallback.onThumbsValueChanged(mPosition, "", mValue);
            }
        });

        mDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(1);
                mCallback.onThumbsValueChanged(mPosition, "", mValue);
            }
        });

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(0);
                mCallback.onThumbsValueChanged(mPosition, "", mValue);
            }
        });
    }

    private void setDefaultData() {
        List<ValueModel> defaultValue = mObjectInfo.getValueModels();

        if(defaultValue.size() > 0) {

            try {
                mValue =  Integer.parseInt(defaultValue.get(0).getValue());
            } catch(NumberFormatException e) {
                mValue = 0;
            }

            setValue(mValue);
        }
    }

    public interface ThumbsCallback {
        void onThumbsValueChanged(int position, String fieldId, int value);
    }

}
