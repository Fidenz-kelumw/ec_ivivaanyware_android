package com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public enum ValueType {
    SINGLE,
    ARRAY,
    OBJECTARRAY
}
