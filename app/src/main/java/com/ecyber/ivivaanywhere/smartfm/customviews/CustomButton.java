package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.ecyber.ivivaanywhere.smartfm.R;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class CustomButton extends Button {

    Context context;

    public CustomButton(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        //if (attrs!=null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
            //String fontName = a.getString(R.styleable.CustomButton_btnFontName);
            String fontName = "MelbourneRegularBasic.otf";
            if (fontName!=null) {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        //}
    }

}
