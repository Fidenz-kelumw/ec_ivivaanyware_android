package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public interface GetObjectItems {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectItems")
    Call<ResponseBody> getObjectItems(@Field("ObjectKey") String objectKey,
                                      @Field("ObjectType") String objectType,
                                      @Field("UserKey") String userKey,
                                      @Field("TabName") String tabName,
                                      @Field("apikey") String apikey);
}
