package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetSpotsResponse;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetSpots;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 11/28/17.
 */

public class GetSpotsSync {

    private AccountPermission account;
    private GetSpotsCallback callback;
    private ProgressDialog dialog;

    public GetSpotsSync(Activity activity, GetSpotsCallback callback) {
        this.callback = callback;
        this.account = new AccountPermission();
    }

    public void requestSpotsAPI() {
        ServiceGenerator.CreateService(GetSpots.class, account.getAccountInfo().get(0))
                .getSpots(account.getAccountInfo().get(2), account.getAccountInfo().get(1))
                .enqueue(new Callback<GetSpotsResponse>() {
                    @Override
                    public void onResponse(Call<GetSpotsResponse> call, Response<GetSpotsResponse> response) {
                        if (response != null && response.isSuccessful() && response.body() != null) {
                            if (response.body().getSpots() != null && response.body().getSpots().size() > 0)
                                saveToDB(response.body().getSpots());
                            else
                                callback.onSpotsFound(false, null);
                        } else {
                            try {
                                String message = response.errorBody().string();
                                ErrorLog.saveErrorLog("Get Spots", message);
                                callback.onSpotsFound(false, null);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onSpotsFound(false, null);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSpotsResponse> call, Throwable t) {
                        try {
                            String message = t.getMessage();
                            ErrorLog.saveErrorLog("Get Spots", message);
                            callback.onSpotsFound(false, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.onSpotsFound(false, null);
                        }
                    }
                });
    }

    private void saveToDB(List<SpotsItem> spots) {
        try {
            new SpotsItem().clearTable();

            for (SpotsItem spot : spots) {
                spot.save();
            }

            new TimeCap().updateTableTime(SmartConstants.SPOTSITEM);
            callback.onSpotsFound(true, spots);
        } catch (Exception e) {
            e.printStackTrace();
            callback.onSpotsFound(false, null);
        }
    }

    public interface GetSpotsCallback {
        void onSpotsFound(boolean status, List<SpotsItem> spots);
    }
}
