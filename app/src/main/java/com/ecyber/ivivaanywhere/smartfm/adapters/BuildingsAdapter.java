package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class BuildingsAdapter extends RecyclerView.Adapter<BuildingsAdapter.Holder>{

    private List<Buildings> items;
    private Context context;

    public BuildingsAdapter(List<Buildings> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public void addBuilding(Buildings building) {
        items.add(building);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_single_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Buildings buildings = items.get(position);
        holder.name.setText(buildings.LocationName);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_single_item_value);
        }
    }
}
