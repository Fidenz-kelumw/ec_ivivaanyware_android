package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 6/6/2016.
 */
public class GetObjectForQrResponse {
    
    @SerializedName("ObjectKey")
    @Expose
    public String mObjectKey;

    @SerializedName("ObjectType")
    @Expose
    public String mObjectType;

    @SerializedName("ObjectID")
    @Expose
    public String mObjectID;

    @SerializedName("Message")
    @Expose
    public String mMessage;

    public GetObjectForQrResponse() {
    }

    public GetObjectForQrResponse(String mObjectKey, String mObjectType, String mObjectID, String mMessage) {
        this.mObjectKey = mObjectKey;
        this.mObjectType = mObjectType;
        this.mObjectID = mObjectID;
        this.mMessage = mMessage;
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    public String getmObjectType() {
        return mObjectType;
    }

    public void setmObjectType(String mObjectType) {
        this.mObjectType = mObjectType;
    }

    public String getmObjectID() {
        return mObjectID;
    }

    public void setmObjectID(String mObjectID) {
        this.mObjectID = mObjectID;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    @Override
    public String toString() {
        return "GetObjectForQrResponse{" +
                "mObjectKey='" + mObjectKey + '\'' +
                ", mObjectType='" + mObjectType + '\'' +
                ", mObjectID='" + mObjectID + '\'' +
                ", mMessage='" + mMessage + '\'' +
                '}';
    }
}
