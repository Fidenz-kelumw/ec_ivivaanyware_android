package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.text.Editable;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.BaseKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.popup.SLTEditDialog;

import java.util.List;

/**
 * Created by Lakmal on 5/2/2016.
 */
public class SingleLineTextView implements SLTEditDialog.SLTCallback {

    private final View mView;
    private CustomEditTextView mEtSlt, mEtSltNew;
    private CustomTextView mTvSlt;
    private ImageView arrow;
    private Context mContext;
    private String mFieldId, defVal;
    private int mPosition;
    private SingleLineTextListener mSingleLineTextListener;
    private ObjectInfoModel mObjectInfo;
    private boolean isTextIsEllipsed;
    private SLTEditDialog sltEditDialog;


    public SingleLineTextView(Context context, SingleLineTextListener singleLineTextListener, @ColorInt int color) {
        mContext = context;
        mSingleLineTextListener = singleLineTextListener;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_single_line_text, null);

        mEtSlt = (CustomEditTextView) mView.findViewById(R.id.et_slt_edit_area);
        mEtSltNew = (CustomEditTextView) mView.findViewById(R.id.et_slt_edit_area_editable);
        mTvSlt = (CustomTextView) mView.findViewById(R.id.tv_slt_title);
        arrow = (ImageView) mView.findViewById(R.id.imageView5);

        mTvSlt.setTextColor(color);

    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
        try {
            if (mObjectInfo != null) {
                List<ValueModel> valueModels = mObjectInfo.getValueModels();
                mSingleLineTextListener.onSingleLineTextFocusOut(mPosition, mEtSltNew.getText().toString(), valueModels.get(0).getValue());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
         }
    };

    public View getSingleLineTextViewObjectInfo(final ObjectInfoModel info, int position) {
        boolean isEditable = info.getEditable().equals("1") ? true : false;
        sltEditDialog = new SLTEditDialog(mContext, this);

        mFieldId = info.getFieldid();
        mPosition = position;
        mObjectInfo = info;
        mEtSlt.setTag(mPosition + "");
        mEtSltNew.setTag(mPosition + "");

        defVal = "";
        setFieldName(info.getFieldname());
        if (info.getValueModels() != null) {
           List <ValueModel> valueModels = info.getValueModels();
            defVal = valueModels.get(0).getValue();
        }

        mEtSlt.setText(defVal);
        mEtSltNew.setText(defVal);

        mEtSlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sltEditDialog.show(info.fieldname, mEtSlt.getText().toString(), info.getEditable());
            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sltEditDialog.show(info.fieldname, mEtSlt.getText().toString(), info.getEditable());
            }
        });

        if(info.getExtended() !=null && info.getExtended().equals("0")){
            mEtSltNew.setVisibility(View.VISIBLE);
            mEtSlt.setVisibility(View.GONE);
            arrow.setVisibility(View.GONE);
        } else {
            mEtSltNew.setVisibility(View.GONE);
            mEtSlt.setVisibility(View.VISIBLE);
            arrow.setVisibility(View.VISIBLE);
        }

        isTextViewEllipsized(mEtSlt);
        setEnableEditText(isEditable);
        mEtSltNew.addTextChangedListener(mTextWatcher);

        mEtSlt.setSingleLine(true);
        mEtSlt.setKeyListener(null);
        mEtSlt.setEllipsize(TextUtils.TruncateAt.END);
        mEtSlt.setTextColor(Color.BLACK);

        mEtSltNew.setSingleLine(true);
        mEtSltNew.setKeyListener(null);
        mEtSltNew.setEllipsize(TextUtils.TruncateAt.END);
        mEtSltNew.setTextColor(Color.BLACK);

        mEtSltNew.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mEtSltNew.setKeyListener(null);
                    mEtSltNew.setEllipsize(TextUtils.TruncateAt.END);
                } else {
                    mEtSltNew.setEllipsize(null);
                    mEtSltNew.setKeyListener(new BaseKeyListener() {
                        @Override
                        public int getInputType() {
                            return 1;
                        }
                    });
                }
            }
        });

        mEtSlt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mEtSlt.setKeyListener(null);
                    mEtSlt.setEllipsize(TextUtils.TruncateAt.END);
                } else {
                    mEtSlt.setEllipsize(null);
                    mEtSlt.setKeyListener(new BaseKeyListener() {
                        @Override
                        public int getInputType() {
                            return 1;
                        }
                    });
                }
            }
        });

        return mView;
    }

    private void setEnableEditText(boolean isEditable) {
        mEtSlt.setEnabled(isEditable);
        mEtSltNew.setEnabled(isEditable);

    }

    private void setFieldName(String fieldName) {
        mTvSlt.setText(fieldName);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void isTextViewEllipsized(final CustomEditTextView textView) {
        textView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Layout layout = textView.getLayout();
                if(layout != null) {
                    int lines = layout.getLineCount();
                    if(lines > 0) {
                        int ellipsisCount = layout.getEllipsisCount(lines-1);
                        if ( ellipsisCount > 0) {
                            isTextIsEllipsed = true;
                        }else {
                            isTextIsEllipsed = false;
                        }
                    } else
                        isTextIsEllipsed = false;
                } else {
                    isTextIsEllipsed = false;
                }
                return true;
            }
        });
    }

    @Override
    public void onSLTTextUpdate(String text) {
        List <ValueModel> valueModels = mObjectInfo.getValueModels();
        mSingleLineTextListener.onSingleLineTextFocusOut(mPosition, text, valueModels.get(0).getValue());
        mEtSlt.setText(text);
        mEtSltNew.setText(text);
    }

    public interface SingleLineTextListener {
        void onSingleLineTextFocusOut(int position, String displayText, String value);
    }
}
