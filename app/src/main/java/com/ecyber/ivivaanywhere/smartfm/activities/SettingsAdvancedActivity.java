package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.SingleListAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.popup.AppResetConfirmation;
import com.ecyber.ivivaanywhere.smartfm.services.system.BackgroundLocationService;
import com.ecyber.ivivaanywhere.smartfm.services.system.LocationAlertGenerator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsAdvancedActivity extends SmartFMBaseActivity implements SingleListAdapter.SingleItemClickListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.btn_action_left)
    ImageButton mBack;

    @BindView(R.id.tv_done)
    CustomTextView mDone;
    @BindView(R.id.tv_lastdate)
    CustomTextView mDate;
    @BindView(R.id.tv_name)
    CustomTextView mReset;
    @BindView(R.id.tv_item_count)
    CustomTextView mCount;
    @BindView(R.id.tv_account)
    TextView mAccount;
    @BindView(R.id.tv_domain)
    TextView mDomain;
    @BindView(R.id.tv_image_upload_size)
    CustomTextView mTvImageUploadSize;

    @BindView(R.id.edt_masterdata)
    EditText mMasterData;
    @BindView(R.id.edt_transaction)
    EditText mTransaction;
    @BindView(R.id.edt_nearestSpot)
    EditText mNearestSportDuration;
    @BindView(R.id.edt_inbox_size)
    EditText mEdtInboxSize;
    @BindView(R.id.edt_locationTracking)
    CustomEditTextView mLocationTracking;
    @BindView(R.id.switchLocation)
    Switch mSwitchLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_advanced);

        ButterKnife.bind(this);
        loadAdvancedSettings();
    }

    //region OnClickEvents
    @OnClick(R.id.btn_action_left)
    public void onClickBack(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        updateSettings();
    }

    private void updateSettings() {
        String sizeInbox = mEdtInboxSize.getText().toString().trim();
        if (sizeInbox.isEmpty()) {
            Toast.makeText(this, "Please enter (Inbox Size) value.", Toast.LENGTH_SHORT).show();
            return;
        }

        String sizeTransaction = mTransaction.getText().toString().trim();
        if (sizeTransaction.isEmpty()) {
            Toast.makeText(this, "Please enter (Transaction Data Exp Time(s)) value.", Toast.LENGTH_SHORT).show();
            return;
        }

        String sizeMasterData = mMasterData.getText().toString().trim();
        if (sizeMasterData.isEmpty()) {
            Toast.makeText(this, "Please enter (Master Data Exp Time(hrs)) value.", Toast.LENGTH_SHORT).show();
            return;
        }
        saveData();
        super.onBackPressed();
    }

    @OnClick(R.id.tv_done)
    public void onClickDone(View view) {
    }

    @OnClick(R.id.rel_error_log)
    public void onClickErrorLog(View v) {
        Intent intent = new Intent(this, ErrorLogListActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    @OnClick(R.id.lay_reset_app)
    public void onClickResetApp(View view) {
        new AppResetConfirmation(this).showConfirmation();
    }

    @OnClick(R.id.rel_img_upload_size)
    public void onClickImageUploadSize(View view) {
        openDialogImageResize();
    }
    //endregion

    private View mDialogView;
    private Dialog mDialogList;
    private RecyclerView mRvSingleList;
    private EditText mSearchView;
    private TextView mHeaderTitle, mTvMessage;
    private ProgressBar mProgressBar;
    private RelativeLayout mRelSearchContainer;

    private void openDialogImageResize() {
        mDialogList = new Dialog(this, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(this);
        mDialogView = inflater.inflate(R.layout.dialog_list, null);

        ImageButton btnClose = (ImageButton) mDialogView.findViewById(R.id.img_btn_close);
        ImageButton btnQRBack = (ImageButton) mDialogView.findViewById(R.id.btn_qr_back);
        btnQRBack.setVisibility(View.GONE);
        ImageButton QRText = (ImageButton) mDialogView.findViewById(R.id.btnQr);
        QRText.setVisibility(View.GONE);

        mRvSingleList = (RecyclerView) mDialogView.findViewById(R.id.rv_single_list);
        mSearchView = (EditText) mDialogView.findViewById(R.id.et_searchView);
        mHeaderTitle = (TextView) mDialogView.findViewById(R.id.tv_dialog_header_title);
        mProgressBar = (ProgressBar) mDialogView.findViewById(R.id.progressBar);
        mTvMessage = (TextView) mDialogView.findViewById(R.id.tv_message);
        mRelSearchContainer = (RelativeLayout) mDialogView.findViewById(R.id.rel_lay_search_container);
        mRelSearchContainer.setVisibility(View.GONE);
        mHeaderTitle.setText("Image Upload Size");

        List<DataLookUp> data = new ArrayList<>();
        DataLookUp small = new DataLookUp();
        small.mDisplayText = SmartConstants.SMALL;
        data.add(small);
        DataLookUp medium = new DataLookUp();
        medium.mDisplayText = SmartConstants.MEDIUM;
        data.add(medium);
        DataLookUp large = new DataLookUp();
        large.mDisplayText = SmartConstants.LARGE;
        data.add(large);
        DataLookUp original = new DataLookUp();
        original.mDisplayText = SmartConstants.ORIGINAL;
        data.add(original);

        mRvSingleList.setLayoutManager(new LinearLayoutManager(this));
        mRvSingleList.setHasFixedSize(true);
        SingleListAdapter adapter = new SingleListAdapter(this.getApplicationContext(), data, this);
        mRvSingleList.setAdapter(adapter);
        btnClose.setOnClickListener(v -> mDialogList.dismiss());

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 40;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;

        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(this, dpWidth);
        y = converter.dipToPixels(this, dpHeight);

        mDialogList.setContentView(mDialogView);
        mDialogList.show();
        mDialogList.setCanceledOnTouchOutside(false);
        mDialogList.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialogList.getWindow().setLayout((int) x, (int) y);
    }

    private void loadAdvancedSettings() {
        AccountPermission accountPermission = new AccountPermission();
        mMasterData.setText(accountPermission.getMasterDataRelodTime());
        mTransaction.setText(accountPermission.getTransactionDataRelodTime());
        mNearestSportDuration.setText(new SmartSharedPreferences(SettingsAdvancedActivity.this).getNearestSpotUpdateDurarion());
        mLocationTracking.setText(new SmartSharedPreferences(SettingsAdvancedActivity.this).getLocationSyncDurarion());

        mSwitchLocation.setChecked(new SmartSharedPreferences(SettingsAdvancedActivity.this).isLocationSyncEnabled());

        mReset.setText("Reset App");
        mCount.setText("");
        mDate.setText("");

        mAccount.setText(accountPermission.getAccount());
        mDomain.setText(accountPermission.getDomain());
        mTvImageUploadSize.setText(new SmartSharedPreferences(this).getResizedImage());
        mEdtInboxSize.setText("");
        mEdtInboxSize.setText(Registration.getInboxSize().InboxSize + "");

        mSwitchLocation.setOnCheckedChangeListener(this);
    }

    private void saveData() {
        List<Registration> registrations = new Registration().getAllRegistrations();

        Registration registration = Registration.load(Registration.class, registrations.get(0).getId());
        registration.MasterDataExpiryHours = mMasterData.getText().toString();
        registration.TransactionExpirySeconds = mTransaction.getText().toString();
        registration.save();

        int size = Integer.parseInt(mEdtInboxSize.getText().toString());
        Registration.updateInboxSize(size);

        new SmartSharedPreferences(SettingsAdvancedActivity.this).setNearestSpotUpdateDurarion(mNearestSportDuration.getText() != null ? mNearestSportDuration.getText().toString() : null);
        new SmartSharedPreferences(SettingsAdvancedActivity.this).setLocationSyncDurarion(mLocationTracking.getText() != null ? mLocationTracking.getText().toString() : null);
        new SmartSharedPreferences(SettingsAdvancedActivity.this).setIsLocationSyncEnabled(mSwitchLocation.isChecked());
    }

    private void resetApplication() {
        DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();
        dataDownloadOptimization.resetApplication();
    }

    @Override
    public void onClickSingleItem(DataLookUp optionItem) {
        String size = optionItem.mDisplayText;
        setResizedImage(size);
        mDialogList.dismiss();
        mTvImageUploadSize.setText(size);
    }

    private void setResizedImage(String size) {
        new SmartSharedPreferences(this).setResizedImage(size);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked && !LocationAlertGenerator.checkLocation(SettingsAdvancedActivity.this)){
            mSwitchLocation.setChecked(false);
            return;
        }

        if (isChecked && LocationAlertGenerator.checkLocation(SettingsAdvancedActivity.this)){
            // start service
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(getApplicationContext(), BackgroundLocationService.class);
                startService(intent);
            }, 500);

        } else {
            // end service
            stopService(new Intent(getApplicationContext(), BackgroundLocationService.class));
        }
    }
}
