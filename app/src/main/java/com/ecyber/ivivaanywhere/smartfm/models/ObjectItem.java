package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name="ObjectItems")
public class ObjectItem extends Model {

    @Column(name = "ItemName")
    public String ItemName;

    @Column(name = "ItemKey")
    public String ItemKey;

    @Column(name = "ItemData")
    public String ItemData;

    @Column(name = "DataColor")
    public String DataColor;

    @Column(name = "ItemEdit")
    public String ItemEdit;

    @Column(name = "TypeID")
    public String TypeID;

    @Column(name = "MIN")
    public float MIN;

    @Column(name = "MAX")
    public float MAX;

    @Column(name = "INC")
    public float INC;

    public String tempData;

    public List<TypeData> TypeData;

    public ObjectItem() {
    }

    public ObjectItem(String itemName, String itemKey, String itemData, String dataColor, String itemEdit, String typeID) {
        ItemName = itemName;
        ItemKey = itemKey;
        ItemData = itemData;
        DataColor = dataColor;
        ItemEdit = itemEdit;
        TypeID = typeID;
    }

    public ObjectItem(String itemName, String itemKey, String itemData, String dataColor, String itemEdit, String typeID, float MIN, float MAX, float INC) {
        ItemName = itemName;
        ItemKey = itemKey;
        ItemData = itemData;
        DataColor = dataColor;
        ItemEdit = itemEdit;
        TypeID = typeID;
        this.MIN = MIN;
        this.MAX = MAX;
        this.INC = INC;
    }

    public ObjectItem(String itemName, String itemKey, String itemData, String dataColor, String itemEdit, String typeID, int MIN, int MAX, int INC, List<TypeData> typeData) {
        ItemName = itemName;
        ItemKey = itemKey;
        ItemData = itemData;
        DataColor = dataColor;
        ItemEdit = itemEdit;
        TypeID = typeID;
        this.MIN = MIN;
        this.MAX = MAX;
        this.INC = INC;
        TypeData = typeData;
    }

    public List<TypeData> getTypeData() {
        return TypeData;
    }

    public void setTypeData(List<TypeData> typeData) {
        TypeData = typeData;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemKey() {
        return ItemKey;
    }

    public void setItemKey(String itemKey) {
        ItemKey = itemKey;
    }

    public String getItemData() {
        return ItemData;
    }

    public void setItemData(String itemData) {
        ItemData = itemData;
    }

    public String getDataColor() {
        return DataColor;
    }

    public void setDataColor(String dataColor) {
        DataColor = dataColor;
    }

    public String getItemEdit() {
        return ItemEdit;
    }

    public void setItemEdit(String itemEdit) {
        ItemEdit = itemEdit;
    }

    public String getTypeID() {
        return TypeID;
    }

    public void setTypeID(String typeID) {
        TypeID = typeID;
    }

    public float getMIN() {
        return MIN;
    }

    public void setMIN(float MIN) {
        this.MIN = MIN;
    }

    public float getMAX() {
        return MAX;
    }

    public void setMAX(float MAX) {
        this.MAX = MAX;
    }

    public float getINC() {
        return INC;
    }

    public void setINC(float INC) {
        this.INC = INC;
    }

    public List<ObjectItem> getAllObjectItems() {
        return new Select().from(ObjectItem.class).execute();
    }

    public void clearTable() {
        new Delete().from(ObjectItem.class).execute();
    }

    public String getTempData() {
        return tempData;
    }

    public void setTempData(String tempData) {
        this.tempData = tempData;
    }

    @Override
    public String toString() {
        return "ObjectItem{" +
                "ItemName='" + ItemName + '\'' +
                ", ItemKey='" + ItemKey + '\'' +
                ", ItemData='" + ItemData + '\'' +
                ", DataColor='" + DataColor + '\'' +
                ", ItemEdit='" + ItemEdit + '\'' +
                ", TypeID='" + TypeID + '\'' +
                ", MIN=" + MIN +
                ", MAX=" + MAX +
                ", INC=" + INC +
                ", TypeData=" + TypeData +
                '}';
    }
}
