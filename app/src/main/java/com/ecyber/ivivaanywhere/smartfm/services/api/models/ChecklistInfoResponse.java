package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Choota on 4/25/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class ChecklistInfoResponse {

    @SerializedName("InfoText")
    @Expose
    String infoText;

    public ChecklistInfoResponse(String infoText) {
        this.infoText = infoText;
    }

    public ChecklistInfoResponse() {
    }

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }
}
