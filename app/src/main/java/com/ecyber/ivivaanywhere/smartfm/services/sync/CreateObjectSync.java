package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.CreateObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.CreateObject;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectInfo;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/6/2016.
 */
public class CreateObjectSync {

    private CreateObjectCallBackListener mCreateObjectCallBackListener;

    public CreateObjectSync(CreateObjectCallBackListener createObjectCallBackListener) {
        mCreateObjectCallBackListener = createObjectCallBackListener;
    }

    public void CreateObject(String url, String apiKey, String userKey, String objType, String fieldData) {
        ServiceGenerator.CreateService(CreateObject.class, url).getCreateObject(apiKey, userKey, objType, fieldData)
                .enqueue(new Callback<CreateObjectResponse>() {
                    @Override
                    public void onResponse(Call<CreateObjectResponse> call, Response<CreateObjectResponse> response) {
                        if (response.isSuccessful()) {

                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    ObjectsSync.foreExpireObjectList();
                                    mCreateObjectCallBackListener.OnCreateObjectSuccess(response.body());
                                }
                            } else {
                                mCreateObjectCallBackListener.OnCreateObjectError(SmartConstants.OBJ_CREATE_UN_SUCCESS);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Create Object",message);
                                mCreateObjectCallBackListener.OnCreateObjectError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mCreateObjectCallBackListener.OnCreateObjectError(SmartConstants.OBJ_CREATE_UN_SUCCESS);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateObjectResponse> call, Throwable t) {
                        mCreateObjectCallBackListener.OnCreateObjectError(SmartConstants.OBJ_CREATE_UN_SUCCESS);
                    }
                });

    }

    public interface CreateObjectCallBackListener {
        void OnCreateObjectSuccess(CreateObjectResponse response);
        void OnCreateObjectError(String message);
    }
}
