package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class ObjectsResponce {

    @SerializedName("Objects")
    public List<Objects> mObjectList;

    public ObjectsResponce(List<com.ecyber.ivivaanywhere.smartfm.models.Objects> objects) {
        mObjectList = objects;
    }

    public ObjectsResponce() {
    }

    public List<com.ecyber.ivivaanywhere.smartfm.models.Objects> getObjects() {
        return mObjectList;
    }

    public void setObjects(List<com.ecyber.ivivaanywhere.smartfm.models.Objects> objects) {
        mObjectList = objects;
    }

}
