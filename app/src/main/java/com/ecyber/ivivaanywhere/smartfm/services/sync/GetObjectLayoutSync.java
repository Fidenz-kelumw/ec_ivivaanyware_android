package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.util.Log;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.DroppedPin;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.models.Pin;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LayoutResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.UpdateDroppedPinResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectLayout;
import com.google.android.gms.maps.model.Marker;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 5/15/2016.
 */
public class GetObjectLayoutSync {

    /*private Activity mActivity;*/
    private GetObjectLayoutCallBack mGetObjectLayoutCallBack;

    public GetObjectLayoutSync(GetObjectLayoutCallBack mGetObjectLayoutCallBack) {
        this.mGetObjectLayoutCallBack = mGetObjectLayoutCallBack;
    }


    public void updateDropPin(String url, String apiKey, String userKey, String objType, final String objKey, String tabName, final double x, final double y, final Marker newMarker, final double oldX, final double oldY) {
        ServiceGenerator.CreateService(GetObjectLayout.class, url).getUpdateDroppedPin(apiKey, userKey, objType, objKey, tabName, x, y)
                .enqueue(new Callback<UpdateDroppedPinResponse>() {
                    @Override
                    public void onResponse(Call<UpdateDroppedPinResponse> call, Response<UpdateDroppedPinResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String message = response.body().getMessage();
                                if (response.body().getSuccess() == 1) {
                                    ObjectLayout.updateDropPin(objKey, newMarker);//update existing data
                                    forceObjectLayoutTimeExpire();
                                    mGetObjectLayoutCallBack.OnUpdateDropPinSuccess(Utility.IsEmpty(message) ? "Updated successfully" : message, newMarker);
                                } else {

                                    mGetObjectLayoutCallBack.OnUpdateDropPinError(Utility.IsEmpty(message) ? SmartConstants.DROP_PIN_UPDATE_FAILED : message, oldX, oldY, newMarker);
                                }

                            } else {
                                mGetObjectLayoutCallBack.OnUpdateDropPinError(SmartConstants.DROP_PIN_UPDATE_FAILED, oldX, oldY, newMarker);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Update DropPin", message);
                                mGetObjectLayoutCallBack.OnUpdateDropPinError(SmartConstants.DROP_PIN_UPDATE_FAILED, oldX, oldY, newMarker);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetObjectLayoutCallBack.OnUpdateDropPinError(SmartConstants.DROP_PIN_UPDATE_FAILED, oldX, oldY, newMarker);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateDroppedPinResponse> call, Throwable t) {
                        mGetObjectLayoutCallBack.OnUpdateDropPinError(SmartConstants.DROP_PIN_UPDATE_FAILED, oldX, oldY, newMarker);
                    }
                });
    }

    private void forceObjectLayoutTimeExpire() {
        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        Date daysAgo = new DateTime(date).minusYears(1).toDate();
        String dateToDBD = simpleDateD.format(daysAgo);

        new Update(TimeCap.class)
                .set("UpdatedTime = ?", dateToDBD)
                .where("TableName = ?", SmartConstants.OBJECT_LAYOUT)
                .execute();
    }

    public void getObjectLayout(String url, String apiKey, String userKey, final String objType, final String objKey, String tabName) {
        //Get online
        ServiceGenerator.CreateService(GetObjectLayout.class, url).getObjectLayout(apiKey, userKey, objType, objKey, tabName)
                .enqueue(new Callback<LayoutResponse>() {
                    @Override
                    public void onResponse(Call<LayoutResponse> call, Response<LayoutResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getLayout() != null) {
                                if (response.body().getLayout().getmPath() != null) {
                                    new ObjectLayout().clearTable();
                                    saveToDB(response.body(), objKey);
                                    Log.e("FM", "Layout updated.");
                                    mGetObjectLayoutCallBack.OnGetObjectLayoutSuccess(response.body());
                                    new TimeCap().updateTableTime(tabName);

                                } else {
                                    mGetObjectLayoutCallBack.OnGetObjectLayoutError(SmartConstants.LAYOUT_NOT_FOUND);
                                }
                            } else {
                                mGetObjectLayoutCallBack.OnGetObjectLayoutError(SmartConstants.LAYOUT_NOT_FOUND);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Object Layout", message);
                                mGetObjectLayoutCallBack.OnGetObjectLayoutError(SmartConstants.LAYOUT_NOT_FOUND);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mGetObjectLayoutCallBack.OnGetObjectLayoutError(SmartConstants.LAYOUT_NOT_FOUND);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LayoutResponse> call, Throwable t) {
                        mGetObjectLayoutCallBack.OnGetObjectLayoutError(SmartConstants.LAYOUT_NOT_FOUND);
                    }
                });
    }

    private void saveToDB(LayoutResponse response, String objectKey) {
        ObjectLayout layout = new ObjectLayout();
        layout.mObjectKey = objectKey;
        layout.mPath = response.getLayout().getmPath();
        layout.mZoomLevels = response.getLayout().getmZoomLevels();
        layout.mX = response.getDroppedPin().getmX();
        layout.mY = response.getDroppedPin().getmY();
        layout.mEditable = response.getDroppedPin().getmEditable();
        if(response.getLayout().getPinLabel() != null) {
            layout.pinName = response.getLayout().getPinLabel().getName();
            layout.pinDescription = response.getLayout().getPinLabel().getDescription();
        }
        long layoutId = layout.save();

        List<LayoutResponse.Pins> pinList = response.getPinList();
        for (LayoutResponse.Pins pinObj : pinList) {
            Pin pin = new Pin();
            pin.setLayoutId(layoutId);
            pin.setName(pinObj.getName());
            pin.setDescription(pinObj.getDescription());
            pin.setX(pinObj.getX());
            pin.setY(pinObj.getY());
            pin.setColor(pinObj.getColor());
            pin.setObjectType(pinObj.getObjectType());
            pin.setObjectKey(pinObj.getObjectKey());
            pin.save();
        }
    }

    public LayoutResponse getFromDb(String objectKey) {
        LayoutResponse response = new LayoutResponse();
        ObjectLayout layoutDB = new Select().from(ObjectLayout.class).where("ObjectKey=?", objectKey).executeSingle();
        Long layoutId = layoutDB.getId();

        // -- Layout
        LayoutResponse.Layout layout = new LayoutResponse().new Layout();
        layout.setmPath(layoutDB.getPath());
        layout.setmZoomLevels(layoutDB.getZoomLevels());

        // Pins
        List<Pin> allPin = new Pin().getAllPin(layoutId);
        List<LayoutResponse.Pins> pinList = new ArrayList<>();
        for (Pin pin : allPin) {
            LayoutResponse.Pins pins = new LayoutResponse().new Pins();
            pins.setName(pin.getName());
            pins.setDescription(pin.getDescription());
            pins.setX(pin.getX());
            pins.setY(pin.getY());
            pins.setColor(pin.getColor());
            pins.setObjectType(pin.getObjectType());
            pins.setObjectKey(pin.getObjectKey());
            pinList.add(pins);
        }

        // DroppedPin
        DroppedPin droppedPin = new DroppedPin();
        droppedPin.setmX(layoutDB.getX());
        droppedPin.setmY(layoutDB.getY());
        droppedPin.setmEditable(layoutDB.getEditable());

        response.setLayout(layout);
        response.setPinList(pinList);
        response.setDroppedPin(droppedPin);

        return response;
    }

    public boolean getLayoutFromDb(String objectKey) {
        ObjectLayout layoutDB = new Select().from(ObjectLayout.class).where("ObjectKey=?", objectKey).executeSingle();
        if (layoutDB != null) {
            mGetObjectLayoutCallBack.OnGetObjectLayoutSuccess(getFromDb(objectKey));
            return true;
        } else {
            return false;
        }
    }


    public interface GetObjectLayoutCallBack {
        void OnGetObjectLayoutSuccess(LayoutResponse response);

        void OnGetObjectLayoutError(String message);

        void OnUpdateDropPinSuccess(String message, Marker newMarker);

        void OnUpdateDropPinError(String message, double oldX, double oldY, Marker newMarker);
    }
}

