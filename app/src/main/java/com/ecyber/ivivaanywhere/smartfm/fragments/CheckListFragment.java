package com.ecyber.ivivaanywhere.smartfm.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.CheckListAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.popup.ChecklistInfoDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.RemarkDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.CheckListSync;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lakmal on 4/20/2016.
 */
public class CheckListFragment extends Fragment implements RemarkDialog.RemarkDialogCallback, CheckListSync.UpdateDescriptionCallback, CheckListSync.GetCheckListCallback, CheckListAdapter.CheckListActionListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, DialogInterface.OnCancelListener {

    private ProgressBar mProgressBar;
    @BindView(R.id.rv_checklist)
    RecyclerView rvCheckList;

    @BindView(R.id.swiperefresh_checklist)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    private String mObjectType;
    private String mObjectKey;
    private String mTabName;

    private boolean isTabForceLoad;

    private AccountPermission mAccountPermission;
    private Context mContext;
    private CheckListSync mCheckListSync;

    private Activity mActivity;
    private List<CheckList> mCheckList = new ArrayList<>();
    private CheckListAdapter mCheckListAdapter;
    private NetworkCheck network;
    private Button btnDate, btnOk;
    private String mSelectedDeadLineDate;
    int sday, smonth, syear, shour, smin;
    String month, day, hour, min;
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;

    public CheckListFragment() {

    }

    @SuppressLint("ValidFragment")
    public CheckListFragment(Activity activity, ProgressBar progressBar) {
        mActivity = activity;
        mProgressBar = progressBar;
        mNetwork = new NetworkCheck();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_check_list, container, false);

        ButterKnife.bind(this, rootView);
        mColoredSnackBar = new ColoredSnackbar(mActivity);
        mContext = mActivity.getApplicationContext();

        mObjectType = getArguments().getString(SmartConstants.OBJECT_TYPE);
        mObjectKey = getArguments().getString(SmartConstants.OBJECT_KEY);
        mTabName = getArguments().getString(SmartConstants.TAB_NAME);
        isTabForceLoad = getArguments().getBoolean(SmartConstants.FORCE_RELOAD);

        network = new NetworkCheck();
        initViews();
        dataLoad(false);
        return rootView;
    }

    private void configCheckListViews() {
        rvCheckList.setHasFixedSize(true);
        rvCheckList.setLayoutManager(new LinearLayoutManager(mContext));

        mCheckListAdapter = new CheckListAdapter(mContext, this, mCheckList);
        rvCheckList.setAdapter(mCheckListAdapter);

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing1);
        boolean includeEdge = true;
        rvCheckList.addItemDecoration(new RecyclerDecorator(mContext, spanCount, spacing, includeEdge, false));
    }

    private void initViews() {
        mAccountPermission = new AccountPermission();
        mCheckListSync = new CheckListSync(this);

        configCheckListViews();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mProgressBar.setVisibility(View.INVISIBLE);
            mCheckListAdapter.clear();
            dataLoad(true);
        });
    }

    /**
     * Check list data loading cloud and local database
     */
    private void dataLoad(boolean isPullDown) {

        if (!isPullDown)
            mProgressBar.setVisibility(View.VISIBLE);

        if (isTabForceLoad){
            invokeToCheckListSync();
        } else {
            DataDownloadOptimization dataDownloadOptimization = new DataDownloadOptimization();

            if (dataDownloadOptimization.isDataAvailableInTable(CheckList.class)) {
                if (dataDownloadOptimization.isTableDataOld(mTabName, false)) {
                    invokeToCheckListSync();
                } else {
                    mTvMessage.setVisibility(View.GONE);
                    mCheckListSync.getFromLocalDb(mObjectKey);

                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            } else {
                invokeToCheckListSync();
            }
        }
    }

    private void invokeToCheckListSync() {
        boolean state = mNetwork.NetworkConnectionCheck(mActivity);
        if (state) {
            mTvMessage.setVisibility(View.GONE);
            mCheckListSync.getCheckList(mAccountPermission.getAccountInfo().get(0),
                    mAccountPermission.getAccountInfo().get(1),
                    mAccountPermission.getAccountInfo().get(2),
                    mObjectType, mObjectKey, mTabName);
        } else {
            mTvMessage.setText("Error in network connection");
            mTvMessage.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void onUpdateCheckDescriptionSuccess(int status, String message, String checkItemKey, int itemPosition, String description) {
        if (status == 1) {
            mCheckListAdapter.updateCheckDescription(description, itemPosition);
            mColoredSnackBar.dismissSnacBar();
            message = Utility.IsEmpty(message) ? "Description successfully set" : message;
            mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onUpdateCheckDescriptionError(String message) {
        mColoredSnackBar.dismissSnacBar();
        message = Utility.IsEmpty(message) ? SmartConstants.DESCRIPTION_UPDATE_FAILED : message;
        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }


    @Override
    public void onClickRemarkUpdate(View v, CheckList checkList, String description, int position) {
        boolean state = NetworkCheck.IsAvailableNetwork(mActivity);
        if (state) {
            mColoredSnackBar.showSnackBar("Please wait...", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            new CheckListSync().updateCheckListDescription(this, mAccountPermission.getAccountInfo().get(0), mAccountPermission.getAccountInfo().get(1), mAccountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, mTabName, checkList.getmItemKey(), description, position);
        }
    }

    @Override
    public void onGetCheckListSuccess(CheckList checkList, String setItemStatus, String setItemDeadline, String setItemDescription) {
        mCheckListAdapter.addItem(checkList);
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onGetCheckListError(String error) {
        mTvMessage.setText(error);
        mTvMessage.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateCheckStatusSuccess(String message, int status, String itemKey, int position, String checkBoxStatus) {
        if (status == 1) {
            mCheckListAdapter.updateStatus(checkBoxStatus, position);
            message = Utility.IsEmpty(message) ? "Status successfully set" : message;
        } else {
            message = Utility.IsEmpty(message) ? SmartConstants.STATUS_UPDATE_FAILED : message;
        }
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateCheckStatusError(String error, int position) {
        Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
        mCheckListAdapter.updateStatus("0", position);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateCheckDeadLineSuccess(int status, String message, String itemKey, int position) {
        if (status == 1) {
            mCheckListAdapter.updateCheckDeadLine(mSelectedDeadLineDate, position);
            message = Utility.IsEmpty(message) ? "Deadline successfully set" : message;
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateCheckDeadLineError(String error) {
        Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnCheckChanged(CompoundButton checkButton, String itemKey, int position) {
        showCheckConfirmationDialog(checkButton, itemKey, position);
    }

    @Override
    public void OnClickRemark(int position, CheckList checkList) {
        if (checkList.getmSetItemDescription() != null && !checkList.getmSetItemDescription().equals("")) {
            if (checkList.getmSetItemDescription().equals("1"))
                new RemarkDialog(mActivity, checkList, position).show(this);
        }
    }

    @Override
    public void OnClickInfo(int position, CheckList checkList) {
        new ChecklistInfoDialog(getActivity())
                .showInfoDialog(mAccountPermission.getAccountInfo().get(0),
                        mAccountPermission.getAccountInfo().get(1),
                        mAccountPermission.getAccountInfo().get(2),
                        mObjectType,
                        mObjectKey,
                        checkList.getInfoCode(),
                        checkList.getmItemKey());
    }

    @Override
    public void OnClickDate(final int position, final CheckList checkList) {
        String itemNumber = checkList.getmItemKey().toString();

        if (checkList.getmStatus().equals("0")) {
            if (mContext != null) {
                final Dialog confirmDialog = new Dialog(mActivity, R.style.CustomDialog);
                LayoutInflater i = LayoutInflater.from(mContext);
                View v = i.inflate(R.layout.dialog_data_picker, null);

                final Button btnDeadLine = (Button) v.findViewById(R.id.btn_set_dead_line); // used for set time button
                Button btnCancel = (Button) v.findViewById(R.id.btn_cancel);
                final Button ok = (Button) v.findViewById(R.id.btn_ok);

                ok.setEnabled(false);
                btnOk = ok;


                ImageButton cls = (ImageButton) v.findViewById(R.id.img_btn_close);
                btnDate = btnDeadLine;

                confirmDialog.setContentView(v);
                btnDeadLine.setOnClickListener(view -> {

                    Calendar now = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                            CheckListFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );

                    datePickerDialog.show(getFragmentManager(), "Datepickerdialog");

                    datePickerDialog.setOnCancelListener(dialogInterface -> {
                        ok.setEnabled(false);
                        btnDeadLine.setText("Set deadline");
                    });
                });

                cls.setOnClickListener(view -> confirmDialog.dismiss());

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        confirmDialog.dismiss();
                    }
                });

                ok.setOnClickListener(view -> {
                    boolean state = network.NetworkConnectionCheck(mContext);
                    if (state) {
                        confirmDialog.dismiss();
                        smonth += 1;
                        String selectedDate1 = syear + "-" + smonth + "-" + sday + "T" + shour + ":" + smin + ":00";

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                        Date dateTime = new Date();
                        try {
                            dateTime = dateFormat.parse(selectedDate1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        mSelectedDeadLineDate = dateFormat.format(dateTime);
                        mTvMessage.setVisibility(View.GONE);
                        mCheckListSync.updateCheckListDeadLine(mAccountPermission.getAccountInfo().get(0), mAccountPermission.getAccountInfo().get(1), mAccountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, mTabName, checkList.getmItemKey(), mSelectedDeadLineDate, position);

                    } else {
                        mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                    }
                });

                confirmDialog.show();
                confirmDialog.setCanceledOnTouchOutside(false);
                confirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                confirmDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            }

        } else {
            if (mContext != null) {
                Toast toast = Toast.makeText(mContext, "Item already set as completed", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }


    private void showCheckConfirmationDialog(final CompoundButton checkBox, final String itemKey, final int position) {
        final Dialog checkBosConfirmDialog = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View msgView = inflater.inflate(R.layout.dialog_confirmation, null);

        checkBosConfirmDialog.setContentView(msgView);

        CustomTextView msg = (CustomTextView) msgView.findViewById(R.id.tv_message);
        CustomButton btnCancel = (CustomButton) msgView.findViewById(R.id.btn_cancel);
        CustomButton btnUpdate = (CustomButton) msgView.findViewById(R.id.btn_ok);

        btnUpdate.setText("Yes");
        btnCancel.setText("No");


        if (checkBox.isChecked()) {
            msg.setText("Do you want to check this item ?\n\n");
        } else {
            msg.setText("Do you want to uncheck this item ?\n\n");
        }

        btnCancel.setOnClickListener(view -> {
            if (checkBox.isChecked()) {
                checkBox.setChecked(false);
            } else {
                checkBox.setChecked(true);
            }
            checkBosConfirmDialog.dismiss();
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            private String status;

            @Override
            public void onClick(View view) {
                boolean state = mNetwork.NetworkConnectionCheck(mContext);
                if (state) {
                    checkBosConfirmDialog.dismiss();
                    if (checkBox.isChecked()) {
                        status = "1";
                    } else {
                        status = "0";
                    }
                    mProgressBar.setVisibility(View.VISIBLE);
                    mTvMessage.setVisibility(View.GONE);
                    mCheckListSync.updateCheckListStatus(mAccountPermission.getAccountInfo().get(0), mAccountPermission.getAccountInfo().get(1), mAccountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, mTabName, itemKey, status, position);

                } else {
                    mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
                }

            }
        });

        checkBosConfirmDialog.show();

        checkBosConfirmDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        sday = dayOfMonth;
        smonth = monthOfYear + 1;
        syear = year;

        if (smonth < 10)
            month = "0" + smonth;
        else
            month = "" + smonth;

        if (sday < 10)
            day = "0" + sday;
        else
            day = "" + sday;

        btnDate.setText("Date: " + day + "-" + month + "-" + syear);

        Calendar now = Calendar.getInstance();
        TimePickerDialog dlg = TimePickerDialog.newInstance(CheckListFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true);
        dlg.setStartTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE));
        dlg.show(getFragmentManager(), "Timepickerdialog");
        dlg.setOnCancelListener(this);
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        btnOk.setEnabled(false);
        btnOk.setText("Set deadline");
        btnDate.setText("Set deadline");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String s = btnDate.getText().toString();

        shour = hourOfDay;
        smin = minute;

        if (shour < 10)
            hour = "0" + shour;
        else
            hour = "" + shour;

        if (smin < 10)
            min = "0" + smin;
        else
            min = "" + smin;

        smonth -= 1;

        btnDate.setText(s + "  Time: " + hour + ":" + min);
        btnOk.setEnabled(true);
    }


}
