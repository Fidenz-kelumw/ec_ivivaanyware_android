package com.ecyber.ivivaanywhere.smartfm.customviews;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;

import java.util.List;

/**
 * Created by diharaw on 3/14/17.
 */

public class SmileyView {

    private final View mView;
    private CustomTextView mTitle;
    private Context mContext;
    private String mFieldId;
    private int mPosition;
    private ObjectInfoModel mObjectInfo;
    private ImageButton mSmiley1;
    private ImageButton mSmiley2;
    private ImageButton mSmiley3;
    private ImageButton mSmiley4;
    private ImageButton mSmiley5;
    private ImageButton mReset;
    private int mValue;
    private SmileyCallback mCallback;
    private boolean mIsEditable;

    public SmileyView(Context context, SmileyCallback callback, @ColorInt int color) {
        mContext = context;
        mCallback = callback;

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.view_smiley, null);

        mTitle = (CustomTextView) mView.findViewById(R.id.tv_smiley_title);
        mTitle.setTextColor(color);

        mSmiley1 = (ImageButton) mView.findViewById(R.id.btn_smiley_1);
        mSmiley2 = (ImageButton) mView.findViewById(R.id.btn_smiley_2);
        mSmiley3 = (ImageButton) mView.findViewById(R.id.btn_smiley_3);
        mSmiley4 = (ImageButton) mView.findViewById(R.id.btn_smiley_4);
        mSmiley5 = (ImageButton) mView.findViewById(R.id.btn_smiley_5);

        mReset = (ImageButton) mView.findViewById(R.id.btn_reset_smiley);

        registerCallbacks();
    }

    public View getView(ObjectInfoModel info, int position) {
        mFieldId = info.getFieldid();
        mPosition = position;
        mObjectInfo = info;

        setFieldName(info.getFieldname());
        String defVal = "";
        if (info.getValueModels() != null) {
            List<ValueModel> valueModels = info.getValueModels();
            defVal = valueModels.get(0).getValue();
        }

        setDefaultData();
        mIsEditable = info.getEditable().equals("1") ? true : false;
        setEditable();

        return mView;
    }

    private void setFieldName(String fieldName) {
        mTitle.setText(fieldName);
    }

    public String getFieldId() {
        return mFieldId;
    }

    public void setFieldId(String fieldId) {
        mFieldId = fieldId;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setValue(int value) {
            mValue = value;

            if(mValue == 1) {
                mSmiley1.setImageResource(R.drawable.img_feedback_face);
                mSmiley2.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley3.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley4.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley5.setImageResource(R.drawable.img_feedback_face_unselected);
            } else if (mValue == 2) {
                mSmiley1.setImageResource(R.drawable.img_feedback_face);
                mSmiley2.setImageResource(R.drawable.img_feedback_face);
                mSmiley3.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley4.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley5.setImageResource(R.drawable.img_feedback_face_unselected);
            } else if (mValue == 3) {
                mSmiley1.setImageResource(R.drawable.img_feedback_face);
                mSmiley2.setImageResource(R.drawable.img_feedback_face);
                mSmiley3.setImageResource(R.drawable.img_feedback_face);
                mSmiley4.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley5.setImageResource(R.drawable.img_feedback_face_unselected);
            } else if (mValue == 4) {
                mSmiley1.setImageResource(R.drawable.img_feedback_face);
                mSmiley2.setImageResource(R.drawable.img_feedback_face);
                mSmiley3.setImageResource(R.drawable.img_feedback_face);
                mSmiley4.setImageResource(R.drawable.img_feedback_face);
                mSmiley5.setImageResource(R.drawable.img_feedback_face_unselected);
            } else if (mValue == 5) {
                mSmiley1.setImageResource(R.drawable.img_feedback_face);
                mSmiley2.setImageResource(R.drawable.img_feedback_face);
                mSmiley3.setImageResource(R.drawable.img_feedback_face);
                mSmiley4.setImageResource(R.drawable.img_feedback_face);
                mSmiley5.setImageResource(R.drawable.img_feedback_face);
            } else {
                mSmiley1.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley2.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley3.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley4.setImageResource(R.drawable.img_feedback_face_unselected);
                mSmiley5.setImageResource(R.drawable.img_feedback_face_unselected);
            }
    }

    private void setEditable() {
        mSmiley1.setEnabled(mIsEditable);
        mSmiley2.setEnabled(mIsEditable);
        mSmiley3.setEnabled(mIsEditable);
        mSmiley4.setEnabled(mIsEditable);
        mSmiley5.setEnabled(mIsEditable);
        mReset.setEnabled(mIsEditable);
    }

    private void registerCallbacks() {

        mSmiley1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(1);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });

        mSmiley2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(2);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });

        mSmiley3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(3);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });

        mSmiley4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(4);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });

        mSmiley5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(5);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValue(0);
                mCallback.onSmileyValueChanged(mPosition, "", mValue);
            }
        });
    }

    private void setDefaultData() {
        List<ValueModel> defaultValue = mObjectInfo.getValueModels();

        if(defaultValue.size() > 0) {

            try {
                mValue =  Integer.parseInt(defaultValue.get(0).getValue());
            } catch(NumberFormatException e) {
                mValue = 0;
            }

            setValue(mValue);
        }
    }

    public interface SmileyCallback {
        void onSmileyValueChanged(int position, String fieldId, int value);
    }
}
