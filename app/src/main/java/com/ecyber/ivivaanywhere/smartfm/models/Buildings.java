package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */

@Table(name = "Buildings")
public class Buildings extends Model {

    @SerializedName("LocationName")
    @Expose
    @Column(name = "LocationName")
    public String LocationName;

    @SerializedName("LocationKey")
    @Expose
    @Column(name = "LocationKey")
    public String LocationKey;

    public Buildings() {
        super();
    }

    public Buildings(String locationName, String locationKey) {
        super();
        LocationName = locationName;
        LocationKey = locationKey;
    }

    public List<Buildings> getAllBuildings() {
        return new Select()
                .from(Buildings.class)
                .execute();
    }

    public static Buildings getBuilding(String locationKey) {
        return new Select().from(Buildings.class).where("LocationKey = ?", locationKey).executeSingle();
    }

    public void clearTable() {
        new Delete().from(Buildings.class).execute();
    }
}
