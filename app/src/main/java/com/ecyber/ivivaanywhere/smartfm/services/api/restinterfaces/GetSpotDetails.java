package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SpotDetailsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Choota on 11/28/17.
 */

public interface GetSpotDetails {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetSpotDetails")
    Call<SpotDetailsResponse> getSpotDetails(@Field("UserKey") String userKey,
                                       @Field("apikey") String apikey,
                                       @Field("ObjectType") String objectType,
                                       @Field("ObjectKey") String objectKey,
                                       @Field("LocationKey") String locationKey,
                                       @Field("Distance") String distance);
}
