package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 4/27/2016.
 */
public class CheckListDeadLineUpdateResponse {

    @SerializedName("Success")
    @Expose
    private int mSuccess;

    @SerializedName("Message")
    @Expose
    private String mMessage;

    public int getSuccess() {
        return mSuccess;
    }

    public void setSuccess(int success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
