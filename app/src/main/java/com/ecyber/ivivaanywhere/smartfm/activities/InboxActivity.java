package com.ecyber.ivivaanywhere.smartfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.NotificationAdapter;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lakmal on 5/17/2016.
 */
public class InboxActivity extends SmartFMBaseActivity implements NotificationAdapter.NotificationItemClickListener {

    @BindView(R.id.rv_notification_list)
    RecyclerView mRvNotificationList;

    @BindView(R.id.btn_action_right)
    ImageButton mBtnRight;

    @BindView(R.id.imgv_left_home)
    ImageView mBtnHome;

    @BindView(R.id.btn_unread)
    CustomButton mBtnUnread;

    @BindView(R.id.btn_all)
    CustomButton mBtnAll;

    @BindView(R.id.img_left_down)
    ImageView imgViewLeftDown;

    @BindView(R.id.img_right_down)
    ImageView imgViewRightDown;

    @BindView(R.id.tv_title)
    CustomTextView mTvTitle;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.progressBar2)
    ProgressBar mProgressBar;

    @BindView(R.id.swiperefresh_notification_list)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private NotificationAdapter mAdapter;
    private List<Notifications> mAllNotification = new ArrayList<>();

    private int spanCount = 1;
    private int spacing;
    private boolean includeEdge = true;
    private int mMessageStatus = Notifications.UNREAD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        ButterKnife.bind(this);
        mBtnRight.setVisibility(View.INVISIBLE);
        mBtnHome.setVisibility(View.INVISIBLE);
        spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        mTvTitle.setText("Inbox");
        mProgressBar.setVisibility(View.VISIBLE);

        mAdapter = new NotificationAdapter(mAllNotification, this, this);

        mRvNotificationList.setAdapter(mAdapter);
        mRvNotificationList.addItemDecoration(new RecyclerDecorator(this, spanCount, spacing, includeEdge, true));
        initViews();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mAdapter.Clear();
            initViews();
            mSwipeRefreshLayout.setRefreshing(false);
        });

    }

    /**
     * All views are initialing to this activity
     */
    private void initViews() {
        mRvNotificationList.setHasFixedSize(true);
        mRvNotificationList.setLayoutManager(new LinearLayoutManager(this));
        mRvNotificationList.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        List<Notifications> list = new ArrayList<>();

        if (mMessageStatus == Notifications.UNREAD)
            list = new Notifications().getAllNotification(Notifications.UNREAD);
        else
            list = new Notifications().getAllNotification(Notifications.ALL);


        setToAdapter(list);
    }

    /**
     * All notifications load to adapter
     * @param allNotification
     */
    private void setToAdapter(List<Notifications> allNotification) {
        mTvMessage.setVisibility(View.GONE);
        mAdapter.clear();
        if (allNotification.size() == 0) {
            mTvMessage.setText(SmartConstants.MESSAGE_NOT_FOUND);
            mTvMessage.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
            return;
        }

        for (Notifications notifications : allNotification) {
            notifications.setExpand(false);
            if (!notifications.getObjectKey().isEmpty()) {
                notifications.setHasIcon(true);
            }
            mAdapter.addItem(notifications);
        }
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Back button click event
     * @param v
     */
    @OnClick(R.id.btn_action_left)
    public void onClickBackButton(View v) {
        onBackPressed();
    }

    /**
     * Unread button click event
     * @param v
     */
    @OnClick(R.id.btn_unread)
    public void onClickUnread(View v) {
        setBtnUnRead();
        mMessageStatus = Notifications.UNREAD;

        List<Notifications> allNotification = new Notifications().getAllNotification(Notifications.UNREAD);
        setToAdapter(allNotification);
    }

    private void setBtnUnRead() {
        mBtnUnread.setBackgroundResource(R.drawable.button_left_rounded_red);
        mBtnAll.setBackgroundResource(R.drawable.button_right_border_red);

        mBtnAll.setTextColor(getResources().getColor(R.color.my_conversation_color_red));
        mBtnUnread.setTextColor(getResources().getColor(R.color.pure_black));

        imgViewLeftDown.setVisibility(View.VISIBLE);
        imgViewRightDown.setVisibility(View.GONE);
    }

    private void setBtnAll() {
        mBtnUnread.setBackgroundResource(R.drawable.button_left_border_red);
        mBtnAll.setBackgroundResource(R.drawable.button_right_rounded_red);

        mBtnAll.setTextColor(getResources().getColor(R.color.pure_black));
        mBtnUnread.setTextColor(getResources().getColor(R.color.my_conversation_color_red));

        imgViewLeftDown.setVisibility(View.GONE);
        imgViewRightDown.setVisibility(View.VISIBLE);
    }


    /**
     * All button click event
     * @param v
     */
    @OnClick(R.id.btn_all)
    public void onClickAll(View v) {
        setBtnAll();
        mMessageStatus = Notifications.ALL;

        List<Notifications> allNotification = new Notifications().getAllNotification(Notifications.ALL);
        setToAdapter(allNotification);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    /**
     * Notification item click event callback function
     * show message details view dialog
     * update read status of notification
     * @param message need to show message in dialog
     */
    @Override
    public void onClickItem(Notifications message) {
        Intent intent = new Intent(this, SideNavigationActivity.class);
        intent.putExtra(SmartConstants.OBJECT_TYPE, message.getObjectType());
        intent.putExtra(SmartConstants.OBJECT_KEY, message.getObjectKey());
        intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_INVALID_ACTIVITY);
        startActivity(intent);

        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }
}
