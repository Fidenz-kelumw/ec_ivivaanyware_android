package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.models.Tabs;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/12/16.
 */
public class SideNavigationAdapter extends RecyclerView.Adapter<SideNavigationAdapter.Holder> {
    private static final String TAG = "SideNavigationAdapter";
    private List<Tabs> mTabList;
    private SideNavTabClickListener mSideNavTabClickListener;

    public SideNavigationAdapter(List<Tabs> mTabList, SideNavTabClickListener sideNavTabClickListener) {
        this.mTabList = mTabList;
        this.mSideNavTabClickListener = sideNavTabClickListener;
    }


    public void addItem(Tabs tabs) {
        mTabList.add(tabs);
        notifyDataSetChanged();
    }

    public void updateItem(int position) {
        try {
            for (Tabs tab : mTabList) {
                tab.isSelected = false;
            }
            if (position != -1) {
                mTabList.get(position).isSelected = true;
                notifyDataSetChanged();
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG, "updateItem: " + e.getStackTrace().toString());
        } catch (Exception e) {
            Log.e(TAG, "updateItem: " + e.getStackTrace().toString());
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_side_navigation, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        String type = mTabList.get(position).TabType;
        String name = mTabList.get(position).getTabName();
        boolean isSelected = mTabList.get(position).isSelected;
        switch (type) {
            case "Info":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_info_normal : R.drawable.ico_info_selected);
                break;
            case "Messages":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_messages_normal : R.drawable.ico_messages_selected);
                break;
            case "Members":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_members_normal : R.drawable.ico_members_selected);
                break;
            case "Attachments":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_attachments_normal : R.drawable.ico_attachments_selected);
                break;
            case "Layout":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_layout_normal : R.drawable.ico_layout_selected);
                break;
            case "Checklist":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_checklist_normal : R.drawable.ico_checklist_selected);
                break;
            case "SubObjects":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_subobjects_unselected : R.drawable.ico_subobjects_selected);
                break;
            case "ItemList":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_itemlist_unselected : R.drawable.ico_itemlist_selected);
                break;
            case "URLs":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_urls_unselected : R.drawable.ico_urls_selected);
                break;
            case "gps":
                holder.button.setBackgroundResource(false == isSelected ? R.drawable.ico_gps_normal : R.drawable.ico_gps_selected);
                break;
            default:
                holder.button.setBackgroundResource(R.color.pure_black);
                break;
        }
        holder.tvTabName.setText(name);
        holder.button.setTag(type);
    }

    @Override
    public int getItemCount() {
        return null != mTabList ? mTabList.size() : 0;
    }

    public interface SideNavTabClickListener {
        void OnClickTabButton(View v, int position, String tabName);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageButton button;
        TextView tvTabName;

        public Holder(View itemView) {
            super(itemView);
            button = (ImageButton) itemView.findViewById(R.id.btn_side_navigation);
            tvTabName = (TextView) itemView.findViewById(R.id.tv_tab_name);
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final TextView tvTabName = (TextView) itemView.findViewById(R.id.tv_tab_name);
            String name = tvTabName.getText().toString();
            mSideNavTabClickListener.OnClickTabButton(v, this.getAdapterPosition(), name);
        }
    }
}
