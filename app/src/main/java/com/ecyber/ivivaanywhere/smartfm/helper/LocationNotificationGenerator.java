package com.ecyber.ivivaanywhere.smartfm.helper;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.activities.MainActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.SideNavigationActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.models.SpotResolveModel;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Choota on 12/6/17.
 */

public class LocationNotificationGenerator implements Runnable {

    // main variables
    private Handler handler;
    private Activity activity;
    private NearLocationCallback callback;

    // data list objects spots
    private List<SpotsItem> spotsItems;
    private List<SpotResolveModel> resolvedLocationList;
    private List<SpotResolveModel> userInsideSpots;
    private List<SpotResolveModel> notifiedSpots;

    // data objects spots
    private SpotsItem notifiedSpot;
    private SpotResolveModel notifiedSpotModel;

    // current location data
    private Location currentLocation;

    // other
    private int updateFrequency;
    private String NOTIFICATION_CHANNEL_ID = "SmartFM_Channel_ID";
    private String objectType, objectKey;

    public LocationNotificationGenerator(Activity activity, NearLocationCallback callback, String objectType, String objectKey) {
        this.activity = activity;
        this.handler = new Handler();
        this.callback = callback;

        this.objectKey = objectKey;
        this.objectType = objectType;

        this.updateFrequency = (Integer.parseInt(new SmartSharedPreferences(activity).getNearestSpotUpdateDurarion()) * 1000);
        this.notifiedSpots = new ArrayList<>();
    }

    public void setPriorityLocationData(List<SpotsItem> spots, Location currentLocation) {
        this.spotsItems = spots;
        this.currentLocation = currentLocation;
    }

    public void setNotifiedSpots(List<String> spots) {
        if (spots != null && spots.size() > 0) {
            try {
                List<SpotResolveModel> resolveModels = doResolveLocations();
                List<SpotResolveModel> notifiedItems = new ArrayList<>();
                for (String spot : spots) {
                    for (SpotResolveModel model : resolveModels) {
                        if (model.getSpotItem().getSpotID().equals(spot))
                            notifiedItems.add(model);
                    }
                }

                this.notifiedSpots = new ArrayList<>(notifiedItems);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void startRepeatingTask() {
        this.run();
    }

    public void stopRepeatingTask() {
        handler.removeCallbacks(this);
    }

    private List<SpotResolveModel> doResolveLocations() {
        resolvedLocationList = new ArrayList<>();

        try {
            for (SpotsItem item : spotsItems) {
                if(item.getNotifyNearestSpot() != null && item.getNotifyNearestSpot().equals("1")) {
                    try {
                        Location centerLocation = new Location((item.getSpotID() != null ? item.getSpotID() : ""));

                        centerLocation.setLatitude(Double.parseDouble(item.getLat()));
                        centerLocation.setLongitude(Double.parseDouble(item.getLan()));

                        float distenceBetween = centerLocation.distanceTo(currentLocation);
                        boolean isInside = (distenceBetween <= Float.parseFloat(item.getDistance()));

                        resolvedLocationList.add(new SpotResolveModel(item, isInside, false, distenceBetween));

                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                }
            }

            Collections.sort(resolvedLocationList, (a1, a2) -> Double.compare(a1.getDistenceToLocation(), a2.getDistenceToLocation()));
            return resolvedLocationList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void createNotification() {
        try {
            String insideSpots = "";
            String[] spotList = new String[userInsideSpots.size()];

            boolean compareNowAndOld = isAlreadyNotified();

            if (userInsideSpots != null && userInsideSpots.size() > 0 && !(compareNowAndOld)) {

                this.clearIfAvailable();

                for (int i = 0; i < userInsideSpots.size(); i++) {
                    if (userInsideSpots.size() == 1) {
                        insideSpots += userInsideSpots.get(i).getSpotItem().getSpotID() + ".";
                    } else {
                        if (userInsideSpots.size() == 2) {
                            if (i == 0) {
                                insideSpots += userInsideSpots.get(i).getSpotItem().getSpotID() + " and ";
                            } else {
                                insideSpots += userInsideSpots.get(i).getSpotItem().getSpotID() + ".";
                            }
                        } else {
                            if (i == userInsideSpots.size() - 1) {
                                insideSpots += " and " + userInsideSpots.get(i).getSpotItem().getSpotID();
                            } else {
                                if (i == 0) {
                                    insideSpots += userInsideSpots.get(i).getSpotItem().getSpotID();
                                } else {
                                    insideSpots += ", " + userInsideSpots.get(i).getSpotItem().getSpotID();
                                }
                            }
                        }
                    }

                    spotList[i] = userInsideSpots.get(i).getSpotItem().getSpotID();
                }

                // add already notified spots
                this.notifiedSpots = new ArrayList<>(userInsideSpots);
                this.executeAPIUpdateWithNearLocations(userInsideSpots);
                this.callback.onNotifiedSpotsChange(notifiedSpots);

                String messageData = "You are near " + insideSpots;

                int x = new Random().nextInt(9999);
                long[] pattern = new long[0];
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                Intent launchActivity = new Intent(activity, MainActivity.class);
                launchActivity.putExtra(SmartConstants.NEAR_LOCATION_SPOTS, spotList);
                launchActivity.putExtra(SmartConstants.OBJECT_KEY, objectKey);
                launchActivity.putExtra(SmartConstants.OBJECT_TYPE, objectType);
                launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                PendingIntent pi = PendingIntent.getActivity(activity, x, launchActivity, 0);

                NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_DEFAULT;
                    NotificationChannel notificationChannel = new NotificationChannel("ID", "Name", importance);
                    notificationManager.createNotificationChannel(notificationChannel);
                    builder = new NotificationCompat.Builder(activity, notificationChannel.getId());
                } else {
                    builder = new NotificationCompat.Builder(activity);
                }

                builder = builder
                        .setSmallIcon(R.drawable.ic_fm_notify)
                        .setColor(activity.getResources().getColor(R.color.mdtp_button_color))
                        .setContentTitle("Smart FM")
                        .setContentText(messageData)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pi)
                        .setVibrate(pattern)
                        .setSound(alarmSound)
                        .setAutoCancel(true);
                notificationManager.notify(x, builder.build());
                Log.i("Location update", messageData);
            } else {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isAlreadyNotified() {
        boolean result = false;

        try {
            String[] notified = new String[notifiedSpots.size()];
            String[] userInside = new String[userInsideSpots.size()];

            for (int i = 0; i < notifiedSpots.size(); i++) {
                notified[i] = notifiedSpots.get(i).getSpotItem().getSpotID();
            }

            for (int i = 0; i < userInsideSpots.size(); i++) {
                userInside[i] = userInsideSpots.get(i).getSpotItem().getSpotID();
            }

            result = Arrays.equals(userInside, notified);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private void clearIfAvailable() {
        try {
            NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeAPIUpdateWithNearLocations(List<SpotResolveModel> models){
        if(models != null && models.size()>0)
            callback.onUserWalksToLocation(models);
    }

    @Override
    public void run() {
        try {
            // resolve location and do the math
            List<SpotResolveModel> resolveModels = doResolveLocations();

            if (resolveModels != null) {
                userInsideSpots = new ArrayList<>();
                for (SpotResolveModel model : resolveModels) {
                    if (model.isInside())
                        userInsideSpots.add(model);
                }

                createNotification();
            }

        } finally {
            handler.postDelayed(this, updateFrequency);
        }
    }

    public interface NearLocationCallback {
        void onUserWalksToLocation(List<SpotResolveModel> models);
        void onNotifiedSpotsChange(List<SpotResolveModel> notifiedSpots);
    }
}
