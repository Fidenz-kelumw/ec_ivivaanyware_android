package com.ecyber.ivivaanywhere.smartfm.helper.errormanager;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lasitha on 2/14/16.
 */
@Table(name = "ErrorLog")
public class ErrorLog extends Model {

    @Column(name = "date")
    public String mDate;

    @Column(name = "error")
    public String mError;

    @Column(name = "title")
    public String mTitle;

    public ErrorLog() {
        super();
    }

    public ErrorLog(String date, String title, String error) {
        mDate = date;
        mError = error;
        mTitle = title;
    }

    public List<ErrorLog> getAllErrors() {
        return new Select().from(ErrorLog.class).orderBy("Id DESC").execute();
    }

    public static void saveErrorLog(String title, String errorMessage) {
        int size = new Select().from(ErrorLog.class).execute().size();
        int count = new Select().from(ErrorLog.class).count();
        if (count>=20) {
            ErrorLog model = new Select().from(ErrorLog.class).orderBy("Id ASC").executeSingle();
            new Delete().from(ErrorLog.class).where("Id = ?",model.getId()).execute();
        }

        new ErrorLog(getDateTimeNow(), title, errorMessage).save();
    }

    public static void cleatTable() {
        new Delete().from(ErrorLog.class).execute();
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getError() {
        return mError;
    }

    public void setError(String error) {
        mError = error;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public static String getDateTimeNow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date dateNow = new Date();
        String nDate = simpleDateFormat.format(dateNow);

        return nDate;
    }
}
