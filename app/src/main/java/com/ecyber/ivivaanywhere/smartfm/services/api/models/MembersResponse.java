package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/22/2016.
 */
public class MembersResponse {

    @SerializedName("Members")
    @Expose
    private List<Members> mMembers;

    public MembersResponse(List<Members> members) {
        mMembers = members;
    }

    public List<Members> getMembers() {
        return mMembers;
    }

    public void setMembers(List<Members> members) {
        mMembers = members;
    }
}
