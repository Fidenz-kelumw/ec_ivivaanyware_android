package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class BuildingsResponce{
    @SerializedName("Buildings")
    @Expose
    List<com.ecyber.ivivaanywhere.smartfm.models.Buildings> Buildings;

    public BuildingsResponce(List<com.ecyber.ivivaanywhere.smartfm.models.Buildings> buildings) {
        Buildings = buildings;
    }

    public List<com.ecyber.ivivaanywhere.smartfm.models.Buildings> getBuildings() {
        return Buildings;
    }

    public void setBuildings(List<com.ecyber.ivivaanywhere.smartfm.models.Buildings> buildings) {
        Buildings = buildings;
    }
}
