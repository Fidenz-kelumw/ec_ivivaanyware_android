package com.ecyber.ivivaanywhere.smartfm.services.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lakmal on 6/3/2016.
 */
public class ValidateRegistrationResponse {

    @SerializedName("Success")
    @Expose
    public String mSuccess;

    public String getSuccess() {
        return mSuccess;
    }

    public void setSuccess(String success) {
        mSuccess = success;
    }
}
