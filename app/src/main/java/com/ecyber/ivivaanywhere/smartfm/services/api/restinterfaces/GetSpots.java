package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetSpotsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Choota on 11/28/17.
 */

public interface GetSpots {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetSpots")
    Call<GetSpotsResponse> getSpots(@Field("UserKey") String userKey,
                                    @Field("apikey") String apikey);
}
