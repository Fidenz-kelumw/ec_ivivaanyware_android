package com.ecyber.ivivaanywhere.smartfm.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayerActivity extends AppCompatActivity {

    @BindView(R.id.viewVideo)
    VideoView videoView;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    private String videoUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);

        MediaController vidControl = new MediaController(this);
        videoUrl = getIntent().getStringExtra(SmartConstants.VIDEO_URL);
        //videoUrl = "http://techslides.com/demos/sample-videos/small.mp4";

        if(videoUrl != null && !videoUrl.isEmpty()){
            Uri vidUri = Uri.parse(videoUrl);
            videoView.setVideoURI(vidUri);
            videoView.start();

            progressBar.setVisibility(View.VISIBLE);
            videoView.setOnPreparedListener(mp -> {
                mp.start();
                mp.setOnVideoSizeChangedListener((mp1, arg1, arg2) -> {
                    progressBar.setVisibility(View.GONE);
                    mp1.start();
                });
            });

            vidControl.setAnchorView(videoView);
            videoView.setMediaController(vidControl);

        } else {
            Toast.makeText(VideoPlayerActivity.this, "Invalid video URL", Toast.LENGTH_LONG).show();
        }
    }
}
