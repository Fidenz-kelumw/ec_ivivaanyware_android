package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.MembersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 4/22/2016.
 */
public interface GetMembers {

    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectMembers")
    Call<MembersResponse> getMembers(@Field("apikey") String apiKey, @Field("UserKey") String userKey, @Field("ObjectType") String objectType, @Field("ObjectKey") String objectKey, @Field("TabName") String tabName);
}
