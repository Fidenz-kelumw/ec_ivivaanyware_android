package com.ecyber.ivivaanywhere.smartfm.services.sync;

import com.activeandroid.query.Update;
import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.SmartFMApplication;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ObjectsResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjects;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class ObjectsSync {

    private GetObjectsCallBack delegate;

    public ObjectsSync(GetObjectsCallBack delegate) {
        this.delegate = delegate;
    }

    public void getObjects(String account, String apikey, String userkey,
                           String objectType, String locationKey, String filterId, String customFilter, String more, String title) {

        String errorMessage = "No "+title+" found.";

        ServiceGenerator.CreateServiceWithTimeout(GetObjects.class, account)
                .getObjects(objectType, userkey, locationKey, filterId, customFilter, more, apikey)
                .enqueue(new Callback<ObjectsResponce>() {
                    @Override
                    public void onResponse(Call<ObjectsResponce> call, Response<ObjectsResponce> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getObjects() != null) {
                                    if (response.body().getObjects().size() != 0) {
                                        SmartFMApplication.mObjectList = response.body().getObjects();
                                        android.util.Log.e("FM", "Object List from - Cloud..");
                                        new TimeCap().updateTableTime(SmartConstants.OBJECT_LIST);
                                        delegate.onObjecsDownloadComplete(SmartFMApplication.mObjectList);
                                    } else {
                                        delegate.onObjectsDownloadError(errorMessage);
                                    }
                                } else {
                                    delegate.onObjectsDownloadError(errorMessage);
                                }
                            } else {
                                delegate.onObjectsDownloadError(errorMessage);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                android.util.Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Objects", message);
                                delegate.onObjectsDownloadError(errorMessage);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onObjectsDownloadError(errorMessage);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<ObjectsResponce> call, Throwable t) {
                        Log.d("Error loading objects", t.toString());
                        delegate.onObjectsDownloadError(errorMessage);
                    }
                });
    }

    public boolean getFromMemory() {
        try {
            List<Objects> objectList = SmartFMApplication.mObjectList;
            if (objectList == null) {
                return false;
            } else {
                android.util.Log.e("FM", "Object List from - Memory..");
                delegate.onObjecsDownloadComplete(objectList);
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public static void foreExpireObjectList() {
        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        Date daysAgo = new DateTime(date).minusYears(1).toDate();
        String dateToDBD = simpleDateD.format(daysAgo);

        new Update(TimeCap.class)
                .set("UpdatedTime = ?", dateToDBD)
                .where("TableName = ?", SmartConstants.OBJECT_LIST)
                .execute();
    }



    public interface GetObjectsCallBack {

        void onObjecsDownloadComplete(List<Objects> objectsList);

        void onObjectsDownloadError(String message);

    }

}
