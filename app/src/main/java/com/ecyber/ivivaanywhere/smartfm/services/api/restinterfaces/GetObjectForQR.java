package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ForwardObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.GetObjectForQrResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lakmal on 6/6/2016.
 */
public interface GetObjectForQR {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "GetObjectForQR")
    Call<GetObjectForQrResponse> getObjectForQR(@Field("apikey") String apiKey,
                                                @Field("UserKey") String userKey,
                                                @Field("OS") String os,
                                                @Field("QRCode") String qrCode,
                                                @Field("LookUpService") String lookupService);
}
