package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.SpotDetailsResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.GetSpotDetailsSync;
import com.squareup.picasso.Picasso;

/**
 * Created by Choota on 11/29/17.
 */

public class MarkerDetailAlert implements GetSpotDetailsSync.GetSpotDetailsCallback {

    private Context context;
    private MarkerDetailCallback callback;
    private SpotsItem item;
    private SpotDetailsResponse detailsResponse;
    private AccountPermission permission;

    private ProgressDialog alert;
    private Dialog dialog;
    private CustomTextView tvTitle;
    private ImageButton btnClose;
    private ImageView imgBanner;
    private CustomTextView tvDescription;
    private CustomButton btnView;

    private boolean isErrorOccured;

    public MarkerDetailAlert(Context context, MarkerDetailCallback callback) {
        this.context = context;
        this.callback = callback;

        this.alert = new ProgressDialog(context);
        this.dialog = new Dialog(context, R.style.CustomDialog);
        this.permission = new AccountPermission();
        this.isErrorOccured = false;
    }

    public void show(SpotsItem item) {
        this.item = item;

        this.alert.setMessage("Please wait...");
        this.alert.setCancelable(false);
        this.alert.show();

        new GetSpotDetailsSync().requestSpots(this, item.getObjectType(), item.getObjectKey(), item.getLocationKey(), item.getDistance());
    }

    private void populateAndShowAlert() {
        this.dialog.setContentView(R.layout.dialog_marker_details);
        this.initView();

        if (detailsResponse != null) {
            try {
                this.tvTitle.setText((detailsResponse.getSpotPopupHeading() != null ? detailsResponse.getSpotPopupHeading() : ""));
                this.tvDescription.setText((detailsResponse.getSpotPopupDescription() != null ? detailsResponse.getSpotPopupDescription() : ""));
                this.tvDescription.setVisibility((tvDescription.getText().toString().isEmpty() ? View.GONE : View.VISIBLE));
                this.btnView.setText(detailsResponse.getButtonText() != null ? detailsResponse.getButtonText() : "View");
                this.btnView.setEnabled(detailsResponse.getEnableActionButton() != null && (detailsResponse.getEnableActionButton().equals("1")));

                String ssl = (DomainData.getSsl() != null && !DomainData.getSsl().equals("0")) ? "https://" : "http://";
                String account = permission.getAccountInfo().get(0);
                String apiKey = permission.getAccountInfo().get(1);

                if(detailsResponse.getSpotPopupImageURL() != null && !detailsResponse.getSpotPopupImageURL().toString().isEmpty()) {
                    String imageURL = ssl + account + detailsResponse.getSpotPopupImageURL() + "?apikey=" + apiKey;
                    Picasso.with(context)
                            .load(imageURL)
                            .placeholder(R.drawable.placeholder1)
                            .into(imgBanner);
                }

                this.btnView.setOnClickListener(v -> {
                    callback.onMarkerDetailViewPressed(detailsResponse);
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                });
            } catch (Exception e) {
                e.printStackTrace();
                isErrorOccured = true;
            }
        }

        this.btnClose.setOnClickListener(v -> {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        });

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 80;

        final float x;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(context, dpWidth);

        if (!isErrorOccured)
            dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout((int) x, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void initView() {
        tvTitle = (CustomTextView) dialog.findViewById(R.id.tvTitle);
        btnClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        imgBanner = (ImageView) dialog.findViewById(R.id.imgBanner);
        tvDescription = (CustomTextView) dialog.findViewById(R.id.tvDescription);
        btnView = (CustomButton) dialog.findViewById(R.id.btnView);
    }

    @Override
    public void onSpotDetailsFound(boolean status, SpotDetailsResponse response) {
        if (alert != null && alert.isShowing())
            alert.dismiss();

        if (status) {
            detailsResponse = response;
            populateAndShowAlert();
        } else {
            callback.onMarkerDetailError();
        }
    }

    public interface MarkerDetailCallback {
        void onMarkerDetailViewPressed(SpotDetailsResponse item);

        void onMarkerDetailError();
    }
}
