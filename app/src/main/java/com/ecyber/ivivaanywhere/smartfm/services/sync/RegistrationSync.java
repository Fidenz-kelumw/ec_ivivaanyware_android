package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.ecyber.ivivaanywhere.smartfm.helper.CImageCache.CImageLoader;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Actions;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentMaster;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.models.CreateFromMenu;
import com.ecyber.ivivaanywhere.smartfm.models.DefaultData;
import com.ecyber.ivivaanywhere.smartfm.models.DomainData;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.Info;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;
import com.ecyber.ivivaanywhere.smartfm.models.MessagesStatus;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectInfo;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.OptionList;
import com.ecyber.ivivaanywhere.smartfm.models.PageLinks;
import com.ecyber.ivivaanywhere.smartfm.models.Pin;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.Roles;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;
import com.ecyber.ivivaanywhere.smartfm.models.Tabs;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.User;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.RegistrationResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ValidateRegistrationResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.RegistrationService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ChathuraHettiarachchi on 4/5/16.
 */
public class RegistrationSync {

    private RegistrationEvents delegate;
    private ValidateRegistrationCallback mValidateRegistrationCallback;
    private String domaindID;

    public static final String REG_SUCCESS = "1";
    public static final String REG_UN_SUCCESS = "0";
    public Context mContext;

    public RegistrationSync(RegistrationEvents delegate, Context context) {
        this.delegate = delegate;
        this.domaindID = "";
        this.mContext = context;
    }

    public RegistrationSync(ValidateRegistrationCallback delegate) {
        mValidateRegistrationCallback = delegate;
    }

    public static void userUnregister(Context context) {
        clearAllTable(context);
        new CImageLoader(context).clearCache();
    }

    public static void clearAllTable(Context context) {
        new Registration().clearTable();
        new TimeCap().clearTable();
        new Filter().clearTable();
        new Buildings().clearTable();
        DomainData.clearTable();
        new SmartSharedPreferences(context).clearSharefPref();

        new Delete().from(Actions.class).execute();
        new Delete().from(AttachmentMaster.class).execute();
        new Delete().from(Attachments.class).execute();
        new Delete().from(AttachmentTypesMaster.class).execute();
        new Delete().from(CheckList.class).execute();
        new Delete().from(DefaultData.class).execute();
        new Delete().from(FieldData.class).execute();
        new Delete().from(Filter.class).execute();
        new Delete().from(Info.class).execute();
        new Delete().from(Members.class).execute();
        new Delete().from(Messages.class).execute();
        new Delete().from(MessagesStatus.class).execute();
        new Delete().from(Notifications.class).execute();
        new Delete().from(ObjectInfo.class).execute();
        new Delete().from(ObjectLayout.class).execute();
        new Delete().from(ObjectType.class).execute();
        new Delete().from(OptionList.class).execute();
        new Delete().from(PageLinks.class).execute();
        new Delete().from(Pin.class).execute();
        new Delete().from(Registration.class).execute();
        new Delete().from(Roles.class).execute();
        new Delete().from(Stages.class).execute();
        new Delete().from(Tabs.class).execute();
        new Delete().from(TimeCap.class).execute();
        new Delete().from(User.class).execute();
        new Delete().from(ErrorLog.class).execute();
        new Delete().from(DomainData.class).execute();
        new Delete().from(CreateFromMenu.class).execute();
    }

    public void GetValidateRegistration(String url, String apiKey, String userKey) {
        ServiceGenerator.CreateService(RegistrationService.class, url).getValidateRegistration(apiKey, userKey)
                .enqueue(new Callback<ValidateRegistrationResponse>() {
                    @Override
                    public void onResponse(Call<ValidateRegistrationResponse> call, Response<ValidateRegistrationResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mValidateRegistrationCallback.onValidateUserSuccess(response.body().getSuccess().equals(REG_SUCCESS) ? REG_SUCCESS : REG_UN_SUCCESS);
                            } else {
                                mValidateRegistrationCallback.onValidateUserError(SmartConstants.TRY_AGAIN);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Validate Registration", message);
                                mValidateRegistrationCallback.onValidateUserError(SmartConstants.TRY_AGAIN);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mValidateRegistrationCallback.onValidateUserError(SmartConstants.TRY_AGAIN_EXCEPTION);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidateRegistrationResponse> call, Throwable t) {
                        mValidateRegistrationCallback.onValidateUserError(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                });

    }

    private void saveDomainData(String ssl, String account, String apiKey, int ivivaStatus) {
        saveDomainDataToSharedPref(ssl, account, apiKey, ivivaStatus);
        DomainData.saveData(ssl, account, apiKey, ivivaStatus);
    }

    public void getDomainData(final GetDomainDataCallback delegate, String domainID, String account, String apikey, String ssl) {

        if (!Utility.IsEmpty(account) && !Utility.IsEmpty(apikey)) {

            if(!Utility.IsEmpty(ssl)) {

                saveDomainData(ssl, account, apikey, 0);
                delegate.onDomainDataSuccess(new DomainData(ssl, account, apikey, 0));
                return;

            } else {

                saveDomainData("0", account, apikey, 0);
                delegate.onDomainDataSuccess(new DomainData("0", account, apikey, 0));
                return;

            }
        }

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("domain", domainID);
        ServiceGenerator.CreateServiceHTTPS(RegistrationService.class, SmartConstants.GET_DOMAIN_URL)
                .getDomainData(requestBody)
                .enqueue(new Callback<DomainData>() {
                    @Override
                    public void onResponse(Call<DomainData> call, Response<DomainData> response) {
                        if (response.isSuccessful()) {
                            if (response.code() == 200) {
                                if (response.body().mAccount != null) {
                                    String ssl = response.body().mSsl;
                                    String account = response.body().mAccount;
                                    String apiKey = response.body().mApikey;
                                    int ivivaStatus = response.body().mIvivaStatus;

                                    /*saveDomainDataToSharedPref(ssl, account, apiKey, ivivaStatus);
                                    DomainData.saveData(ssl, account, apiKey, ivivaStatus);*/
                                    saveDomainData(ssl, account, apiKey, ivivaStatus);
                                    delegate.onDomainDataSuccess(response.body());
                                } else {
                                    delegate.onDomainDataError(SmartConstants.DOMAIN_ID_FAILED);
                                }
                            } else {
                                if (response.code() == 404) {
                                    delegate.onDomainDataError(/*"Incorrect Domain ID"*/SmartConstants.DOMAIN_ID_FAILED);
                                } else if (response.code() == 500) {
                                    delegate.onDomainDataError(/*"Domain identification failed"*/SmartConstants.DOMAIN_ID_FAILED);
                                } else {
                                    delegate.onDomainDataError(/*"Domain identification failed"*/SmartConstants.DOMAIN_ID_FAILED);
                                }
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                JSONObject error = new JSONObject(message);
                                String errorMsg = error.has("errorMessage") ? error.getString("errorMessage") : message;
                                ErrorLog.saveErrorLog("Get Domain Data", errorMsg);
                                delegate.onDomainDataError(SmartConstants.DOMAIN_ID_FAILED);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onDomainDataError(SmartConstants.DOMAIN_ID_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DomainData> call, Throwable t) {
                        delegate.onDomainDataError(SmartConstants.TRY_AGAIN_EXCEPTION);
                    }
                });

    }

    public void signIn(String domainId, String userId, final String registrationId, String url, String regKey, String fireBaseToken) {

        saveUserId(userId);
        domaindID = domainId;

        ServiceGenerator.CreateService(RegistrationService.class, url)
                .getRegistrationData(domainId, userId, registrationId, regKey, fireBaseToken)
                .enqueue(new Callback<RegistrationResponce>() {
                    @Override
                    public void onResponse(Call<RegistrationResponce> call, Response<RegistrationResponce> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (!response.body().getUser().getUserKey().isEmpty()) {
                                    saveRegistrationData(response.body());
                                    delegate.onLoginSuccess(response.body());
                                } else {
                                    delegate.onLoginError(SmartConstants.REGISTRATION_FAILED);
                                }
                            } else {
                                delegate.onLoginError(SmartConstants.REGISTRATION_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Registration Data", message);
                                delegate.onLoginError(SmartConstants.REGISTRATION_FAILED);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onLoginError(SmartConstants.REGISTRATION_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegistrationResponce> call, Throwable t) {
                        Log.d("RegistrationAPI", "SignInError " + t.getMessage());
                        delegate.onLoginError(SmartConstants.TRY_AGAIN);
                    }
                });
    }

    public interface RegistrationEvents {
        void onLoginSuccess(RegistrationResponce userRegResponse);

        void onLoginError(String message);


    }

    public interface ValidateRegistrationCallback {

        void onValidateUserSuccess(String status);

        void onValidateUserError(String message);
    }

    public interface GetDomainDataCallback {
        void onDomainDataSuccess(DomainData domainData);

        void onDomainDataError(String message);

    }

    private void saveUserId(String userId) {
        new SmartSharedPreferences(mContext).setUserId(userId);
    }

    private void saveRegistrationData(RegistrationResponce registrationModel) {
        String domain = domaindID;
        String userKey = registrationModel.getUser().getUserKey();
        String userName = registrationModel.getUser().getUserName();
        String imagePath = registrationModel.getUser().getImagePath();
        String account = "";
        String apiKey = "";

        RegistrationResponce.AccountInfo accInfo = registrationModel.getAccountInfo();

        if(accInfo != null)
        {
            String tmpAccount = registrationModel.getAccountInfo().getAccount();
            account = (tmpAccount != null) ? tmpAccount : "";

            String tmpApiKey = registrationModel.getAccountInfo().getApiKey();
            apiKey = (tmpApiKey != null) ? tmpApiKey : "";
        } else {
            SmartSharedPreferences pref = new SmartSharedPreferences(mContext);
            account = pref.getAccount();
            apiKey = pref.getApiKey();
        }

        //Default
        String mDataExp = "0";
        String tDataExp = "0";
        String locationKey = "0";
        String inboxSize = "0";
        //Hidden settings
        String getObjectForQR = "0";

        if (registrationModel.getDefaults() != null) {
            mDataExp = registrationModel.getDefaults().getMasterDataExpiryHours();
            tDataExp = registrationModel.getDefaults().getTransactionExpirySeconds();
            inboxSize = registrationModel.getDefaults().getInboxSize();
            locationKey = registrationModel.getDefaults().getLocationKey();

            mDataExp = Utility.IsEmpty(mDataExp) ? "0" : mDataExp;
            tDataExp = Utility.IsEmpty(tDataExp) ? "0" : tDataExp;
            inboxSize = Utility.IsEmpty(inboxSize) ? "0" : inboxSize;
            locationKey = Utility.IsEmpty(locationKey) ? "0" : locationKey;
        }

        if (registrationModel.getHiddenSettings() != null) {
            getObjectForQR = registrationModel.getHiddenSettings().getGetObjectForQR();
            getObjectForQR = Utility.IsEmpty(getObjectForQR) ? "0" : getObjectForQR;
        }

        new Registration(domain, userKey, userName, imagePath, account, apiKey, mDataExp, tDataExp, inboxSize, locationKey, getObjectForQR).save();

        SmartSharedPreferences pref = new SmartSharedPreferences(mContext);
        pref.setUserKey(userKey);
        pref.setUserName(userName);
        pref.setUserAccountImage(imagePath);
        pref.setUserDomain(domain);

        if(!Utility.IsEmpty(account))
            pref.setAccount(account);

        if(!Utility.IsEmpty(apiKey))
            pref.setApiKey(apiKey);
    }

    private void saveDomainDataToSharedPref(String ssl, String account, String apiKey, int ivivaStatus) {
        SmartSharedPreferences pref = new SmartSharedPreferences(mContext);
        pref.setSSLKey(ssl);
        pref.setAccount(account);
        pref.setApiKey(apiKey);
        pref.setIvivaStatus(ivivaStatus);
    }

    public void updateUserData() {
        SmartSharedPreferences pref = new SmartSharedPreferences(mContext);

        //domain data
        String account = pref.getAccount();
        String sslKey = pref.getSSLKey();
        String apiKey = pref.getApiKey();
        int ivIvaStatus = pref.getIvivaStatus();
        //user data
        String userDomain = pref.getUserDomain();
        String userKey = pref.getUserKey();
        String userName = pref.getUserName();
        String userAccountImage = pref.getUserAccountImage();

        DomainData.saveData(sslKey, account, apiKey, ivIvaStatus);

        //Get existing data from Registration DB
        Registration user = new Registration().select();
        String mDataExp = user.MasterDataExpiryHours;
        String tDataExp = user.TransactionExpirySeconds;
        String inboxSize = user.InboxSize;
        String locationKey = user.LocationKey;
        String getObjectForQR = user.GetObjectForQR;

        mDataExp = Utility.IsEmpty(mDataExp) ? "0" : mDataExp;
        tDataExp = Utility.IsEmpty(tDataExp) ? "0" : tDataExp;
        inboxSize = Utility.IsEmpty(inboxSize) ? "0" : inboxSize;
        locationKey = Utility.IsEmpty(locationKey) ? "0" : locationKey;
        getObjectForQR = Utility.IsEmpty(getObjectForQR) ? "0" : getObjectForQR;

        Registration.saveData(userDomain, userKey, userName, userAccountImage, account, apiKey, mDataExp, tDataExp, inboxSize, locationKey, getObjectForQR);
    }
}
