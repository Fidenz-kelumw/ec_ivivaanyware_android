package com.ecyber.ivivaanywhere.smartfm.services.sync;

import com.activeandroid.util.Log;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.FiltersResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetFilters;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
public class FiltersSync {

    private GetFiltersCallBack delegate;

    public FiltersSync(GetFiltersCallBack delegate) {
        this.delegate = delegate;
    }

    public void getFilters(String account, String apikey, String userkey){
        ServiceGenerator.CreateService(GetFilters.class, account)
                .getFilters(userkey,apikey)
                .enqueue(new Callback<FiltersResponce>() {
                    @Override
                    public void onResponse(Call<FiltersResponce> call, Response<FiltersResponce> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getFilters() != null) {
                                    if (response.body().getFilters().size() != 0) {
                                        new Filter().clearTable();
                                        for (int i = 0; i < response.body().getFilters().size(); i++) {
                                            saveToDb(response.body().getFilters().get(i));
                                            delegate.onFiltersDownload(response.body().getFilters().get(i));
                                        }
                                        new TimeCap().updateTableTime(SmartConstants.FILTERS);
                                        delegate.onFiltersDownloadComplete();
                                    } else {
                                        delegate.onFiltersDownloadError(SmartConstants.DOWNLOAD_FAILED);
                                    }
                                } else {
                                    delegate.onFiltersDownloadError(SmartConstants.DOWNLOAD_FAILED);
                                }
                            } else {
                                delegate.onFiltersDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                android.util.Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Filters",message);
                                delegate.onFiltersDownloadError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                delegate.onFiltersDownloadError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<FiltersResponce> call, Throwable t) {
                        Log.d("Error loading filters", t.toString());
                        delegate.onFiltersDownloadError(SmartConstants.DOWNLOAD_FAILED);
                    }
                });
    }

    private void saveToDb(Filter filter) {
        filter.save();
    }

    public interface GetFiltersCallBack{
        void onFiltersDownload(Filter filter);
        void onFiltersDownloadComplete();
        void onFiltersDownloadError(String error);
    }
}
