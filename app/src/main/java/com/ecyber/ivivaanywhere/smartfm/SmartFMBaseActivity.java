package com.ecyber.ivivaanywhere.smartfm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.activities.MainActivity;
import com.ecyber.ivivaanywhere.smartfm.activities.SplashActivity;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateFireBaseSync;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Lakmal on 6/6/2016.
 */
public class SmartFMBaseActivity extends Activity {
    private static boolean isAppWentToBg = false;
    private static boolean isWindowFocused = false;
    private static boolean isBackPressed = false;

    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    public boolean wasInBackground;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 1000*60*2;
    private String Tag = "Tag";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        isStop = false;
        comeForeground();
        super.onStart();

    }

    private boolean isStop = false;
    @Override
    protected void onStop() {
        isStop = true;
        super.onStop();
        goBackground();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        isWindowFocused = hasFocus;

        if (isBackPressed && !hasFocus) {//false|false
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
        Log.w(Tag, "win:"+ isStop);
        if (isStop) {
            goBackground();
        }
        isStop = false;

    }

    @Override
    public void onBackPressed() {

        if (!(this instanceof MainActivity /*||this instanceof RegistrationActivity*/)) {
            isBackPressed = true;
            isWindowFocused = true;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    public void goBackground() {
        if (!isWindowFocused && !isBackPressed) {
            isAppWentToBg = true;
            startActivityTransitionTimer();
            return;
        }
    }

    private void comeForeground() {

        if (isAppWentToBg) {
            isAppWentToBg = false;
            updateFirebaseToken();

            if (wasInBackground)
            {
                if (this instanceof MainActivity /*|| this instanceof RegistrationActivity*/) {
                    return;
                }

                finish();
                Intent intent = new Intent(this, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }

            stopActivityTransitionTimer();
        }
    }

    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                wasInBackground = true;
                Log.w(Tag, "wasInBackground = true");
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }
        wasInBackground = false;
    }

    private void updateFirebaseToken() {
        SmartSharedPreferences pref = new SmartSharedPreferences(this);
        boolean tokenStatus = pref.getFirebaseTokenStatus();
        String userKey = pref.getUserKey();

        if(!tokenStatus && !userKey.equals(SmartSharedPreferences.INVALID_STRING)) {

            String token = FirebaseInstanceId.getInstance().getToken();
            Log.d("FIREBASE TOKEN", "Refreshed token: " + token);

            try {
                UpdateFireBaseSync fireBaseSync = new UpdateFireBaseSync();
                AccountPermission permission = new AccountPermission();

                fireBaseSync.UpdateFirebaseToken(permission.getAccountInfo().get(0),
                                                 permission.getAccountInfo().get(1),
                                                 permission.getAccountInfo().get(2),
                                                 token,
                                                 this);
            } catch (Exception e) {

            }
        }
    }
}
