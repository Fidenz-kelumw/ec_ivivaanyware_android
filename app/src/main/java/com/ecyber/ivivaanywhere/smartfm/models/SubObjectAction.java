package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Choota on 1/17/18.
 */

@Table(name = "SubObjectAction")
public class SubObjectAction extends Model {

    @Column(name = "ButtonText")
    @SerializedName("ButtonText")
    @Expose
    private String buttonText;

    @Column(name = "ActionID")
    @SerializedName("ActionID")
    @Expose
    private String actionId;

    @Column(name = "IsChecked")
    @SerializedName("IsChecked")
    @Expose
    private String isChecked;

    @Column(name = "UserConfirmation")
    @SerializedName("UserConfirmation")
    @Expose
    private String userConfirmation;

    @Column(name = "ObjectKey")
    public String ObjectKey;

    public SubObjectAction() {
    }

    public SubObjectAction(String buttonText, String actionId, String isChecked, String userConfirmation, String objectKey) {
        this.buttonText = buttonText;
        this.actionId = actionId;
        this.isChecked = isChecked;
        this.userConfirmation = userConfirmation;
        ObjectKey = objectKey;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getUserConfirmation() {
        return userConfirmation;
    }

    public void setUserConfirmation(String userConfirmation) {
        this.userConfirmation = userConfirmation;
    }

    public String getObjectKey() {
        return ObjectKey;
    }

    public void setObjectKey(String objectKey) {
        ObjectKey = objectKey;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public List<SubObjectAction> actionsForObjectKey (String key){
        return new Select().from(SubObjectAction.class).where("ObjectKey =?",key).execute();
    }
}
