package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.RegistrationResponce;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetDefaultData;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lakmal on 6/2/2016.
 */
public class DefaultSync {

    private DefaultSyncCallback mDefaultSyncCallback;
    private Context context;

    public DefaultSync(DefaultSyncCallback defaultSyncCallback, Context context) {
        this.mDefaultSyncCallback = defaultSyncCallback;
        this.context = context;
    }

    public void getDefaultData(String url, String apiKey, String userKey) {
        ServiceGenerator.CreateService(GetDefaultData.class,url).getDefaultData(apiKey,userKey)
                .enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                saveToDb(response.body());
                                mDefaultSyncCallback.onDefaultDataSuccess(response.body());
                            } else {
                                mDefaultSyncCallback.onDefaultDataError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get Default Data",message);
                                mDefaultSyncCallback.onDefaultDataError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mDefaultSyncCallback.onDefaultDataError(SmartConstants.DOWNLOAD_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        mDefaultSyncCallback.onDefaultDataError(SmartConstants.DOWNLOAD_FAILED);
                    }
                });
    }

    private void saveToDb(DefaultResponse defaults) {
        Registration.updateDefaultData(defaults);
        new SmartSharedPreferences(context).setApplicationName(defaults.mDefaults.AppName);
        new TimeCap().updateTableTime(SmartConstants.DEFAULT);
    }

    public interface DefaultSyncCallback {

        void onDefaultDataSuccess(DefaultResponse defaultResponse);

        void onDefaultDataError(String message);
    }
}

