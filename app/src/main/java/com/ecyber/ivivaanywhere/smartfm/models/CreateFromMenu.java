package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lakmal on 7/29/16.
 */
@Table(name = "CreateFromMenu")
public class CreateFromMenu extends Model{

    @Column(name = "ObjectType")
    public String mObjectType;

    @Column(name = "FieldID")
    @SerializedName("FieldID")
    @Expose
    public String mFieldID;

    @Column(name = "Value")
    @SerializedName("Value")
    @Expose
    public String mValue;

    @Column(name = "DisplayText")
    @SerializedName("DisplayText")
    @Expose
    public String mDisplayText;

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getDisplayText() {
        return mDisplayText;
    }

    public void setDisplayText(String displayText) {
        mDisplayText = displayText;
    }

    public String getFieldID() {

        return mFieldID;
    }

    public void setFieldID(String fieldID) {
        mFieldID = fieldID;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    public void clearTable(){
        new Delete().from(CreateFromMenu.class).execute();
    }

    public List<CreateFromMenu> selectByObjectType(String objectType){
        return new Select()
                .from(CreateFromMenu.class)
                .where("ObjectType = ?",objectType)
                .execute();
    }
}
