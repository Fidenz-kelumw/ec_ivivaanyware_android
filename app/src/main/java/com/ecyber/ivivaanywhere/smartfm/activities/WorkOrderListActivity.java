package com.ecyber.ivivaanywhere.smartfm.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMApplication;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.ObjectsAdapter;
import com.ecyber.ivivaanywhere.smartfm.adapters.StageAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomEditTextView;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DipPixConverter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.FilterObjects;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.StringEmptyCheck;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.FieldDataSerializable;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.Objects;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.Stages;
import com.ecyber.ivivaanywhere.smartfm.popup.BuildingDialog;
import com.ecyber.ivivaanywhere.smartfm.popup.CustomFilterDialog;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ObjectsSync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkOrderListActivity extends SmartFMBaseActivity implements CustomFilterDialog.CustomFilterCallback, ObjectsSync.GetObjectsCallBack, ObjectsAdapter.ObjectClickListner, BuildingDialog.LocationCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.recyclerview_objects)
    RecyclerView mRecyclerView;

    @BindView(R.id.rv_stage_list)
    RecyclerView mRvStageList;

    @BindView(R.id.rel_stage_container)
    RelativeLayout mRelStageContainer;

    @BindView(R.id.progressBar2)
    ProgressBar mProgressBar;

    @BindView(R.id.btn_right)
    RelativeLayout layBtnRight;

    @BindView(R.id.img_btn_right)
    ImageButton imgBtnRight;

    @BindView(R.id.btn_home)
    RelativeLayout layBtnHome;

    @BindView(R.id.imgv_left_back)
    ImageView imgvLeftBtnBack;

    @BindView(R.id.imgv_left_home)
    ImageView imgvLeftBtnHome;

    @BindView(R.id.btn_select_location)
    CustomButton btnSelectLocation;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.editText_search_bar)
    CustomEditTextView edtSearchBar;

    @BindView(R.id.swipe_refresh_object_list)
    SwipeRefreshLayout mSwipeRefreshObjectList;

    ObjectsSync objectsSync;
    ObjectsAdapter adapter;
    List<Objects> objectses = new ArrayList<>();

    String objecttType, filterId, more;
    String mLocationName = "";
    String mLocationKey = "";
    String mMenuName = "";
    String mFilterName = "";
    boolean isShowHome = false;
    boolean isRelatedWorkOrder = false;
    private AccountPermission accountPermission;
    private ArrayList<FieldDataSerializable> mCustomFilter = new ArrayList<>();
    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;
    BuildingDialog dialog;
    private List<Objects> mObjectsList = new ArrayList<>();

    public static final int PICK_FILTER_REQUEST = 1;
    public static final int PICK_BACK_PRESS = 2;
    private StageAdapter mStageAdapter;
    private android.os.Handler mHandler = new android.os.Handler();
    private DataDownloadOptimization dataDownloadOptimization;
    private ArrayList<FieldDataSerializable> fieldDataList;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SmartFMApplication.mObjectList.size() > 0) {
            if (dataDownloadOptimization.isTableDataOld(SmartConstants.OBJECT_LIST, false)) {
                loadObjectList();
            } else {
                boolean isAvailable = objectsSync.getFromMemory();
                if (!isAvailable) {
                    loadObjectList();
                }
            }
        } else {
            loadObjectList();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_order_list);

        ButterKnife.bind(this);
        mColoredSnackBar = new ColoredSnackbar(this);
        mNetwork = new NetworkCheck();
        imgBtnRight.setBackgroundResource(R.drawable.ic_nv_filter);

        dialog = new BuildingDialog(this, true);
        edtSearchBar.setEnabled(false);
        edtSearchBar.addTextChangedListener(mTextWatcher);
        dataDownloadOptimization = new DataDownloadOptimization();
        loadIntentValues();
        configViews();
        loadStageList();

        mHandler.postDelayed(mRunnable, 10 * 1000);

        mSwipeRefreshObjectList.setOnRefreshListener(() -> {
            loadObjectList();
            mSwipeRefreshObjectList.setRefreshing(false);
        });
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mRelStageContainer.animate().translationY(0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mRelStageContainer.setVisibility(View.GONE);
                        }
                    });
        }
    };

    private int getRowCount() {
        List<Stages> stages = new Stages().selectByObjectType(objecttType != null ? objecttType : "");
        int size = stages.size();
        int row = 4;
        if (size <= 2) {
            row = 2;
        } else if (size == 3) {
            row = 3;
        } else if (size >= 4) {
            row = 4;
        }
        return row;
    }

    private void configViews() {

        mRvStageList.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getBaseContext(), getRowCount());
        gridLayoutManager.setAutoMeasureEnabled(true);
        mRvStageList.setLayoutManager(gridLayoutManager);
        mRvStageList.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        mStageAdapter = new StageAdapter();
        mRvStageList.setAdapter(mStageAdapter);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        adapter = new ObjectsAdapter(objectses, this, this, objecttType);

        mRecyclerView.setAdapter(adapter);

        int spanCount = 1;
        int spacing = getResources().getDimensionPixelSize(R.dimen.spacing);
        boolean includeEdge = true;

        mRecyclerView.addItemDecoration(new RecyclerDecorator(this, spanCount, spacing, includeEdge, false));

        objectsSync = new ObjectsSync(this);
    }

    private void loadStageList() {
        List<Stages> stages = new Stages().selectByObjectType(objecttType != null ? objecttType : "");
        if (!stages.isEmpty()) {
            for (Stages stage : stages) {
                mStageAdapter.addItem(stage);
            }
            mRelStageContainer.setVisibility(View.VISIBLE);

        } else {
            mRelStageContainer.setVisibility(View.GONE);
        }

    }

    /**
     * Set name to action bar title
     */
    private void loadIntentValues() {

        try {
            Intent intent = getIntent();
            objecttType = intent.getStringExtra(SmartConstants.OBJECT_TYPE);
            mMenuName = intent.getStringExtra(SmartConstants.MENU_NAME);
            mFilterName = intent.getStringExtra(SmartConstants.FILTER_NAME);

            ObjectType objectType = new Select().from(ObjectType.class).where("ObjectType=?", objecttType).executeSingle();
            if ((mMenuName == null || mMenuName.equals("")) || (mFilterName == null && mFilterName.equals("")))
                setTitle(objectType.getMenuName());
            else
                setTitle(null);

            String createPage = objectType.getCustomFilterPage();
            if (createPage == null | createPage.equals("") |createPage.equals("0")) {
                imgBtnRight.setVisibility(View.GONE);
                makeFilterButtonGone();
            }

            isRelatedWorkOrder = intent.getBooleanExtra(SmartConstants.RELATED_WORK_ORDER, false);
            isShowHome = intent.getBooleanExtra(SmartConstants.SHOW_HOME, false);

            if (isShowHome) {
                imgvLeftBtnBack.setVisibility(View.GONE);
                imgvLeftBtnHome.setVisibility(View.VISIBLE);
                imgBtnRight.setVisibility(View.GONE);//temp
                makeFilterButtonGone();
            }

            if (isRelatedWorkOrder) {
                imgvLeftBtnHome.setVisibility(View.GONE);
            }

            mLocationKey = intent.getStringExtra(SmartConstants.LOCATION_KEY);
            filterId = intent.getStringExtra(SmartConstants.FILTER_ID);
            mLocationName = intent.getStringExtra(SmartConstants.LOCATION_NAME);
            String page = intent.getStringExtra(SmartConstants.PAGE);

            accountPermission = new AccountPermission();

            if (mLocationKey.isEmpty() && !mLocationName.equals("Any Location")) {
                Buildings defaultLocation = Registration.getDefaultLocation();
                if (defaultLocation != null) {
                    mLocationKey = defaultLocation.LocationKey.isEmpty() ? "" : defaultLocation.LocationKey;
                    mLocationName = defaultLocation.LocationName.isEmpty() ? "" : defaultLocation.LocationName;
                } else {
                    mLocationKey = "";
                    mLocationName = "Any Location";
                }
            }

            btnSelectLocation.setText(mLocationName.isEmpty() ? "Select Location" : mLocationName);
            if (page != null) {
                if (page.equals("info")) {
                    mCustomFilter = (ArrayList<FieldDataSerializable>) intent.getSerializableExtra(SmartConstants.CUSTOM_FILTER);
                }
            }

            more = intent.getStringExtra(SmartConstants.MORE);

            if (intent.getBooleanExtra(SmartConstants.SHOW_ASSETS_HOME, false)) {
                layBtnRight.setVisibility(View.GONE);
                makeFilterButtonGone();
                layBtnHome.setVisibility(View.VISIBLE);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layBtnHome.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params.setMargins(0, 0, (int) (new DipPixConverter().dipToPixels(getApplicationContext(), 16)), 0);

                layBtnHome.setLayoutParams(params);
            }

        } catch (Exception e) {
        }
    }

    private void makeFilterButtonGone () {
        layBtnRight.getLayoutParams().width = 0;
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String search = s.toString();
            filterObjects(search.trim());
            //adapter.getFilter().filter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Custom search icon click event
     * Open search Activity(FilterFieldsActivity)
     *
     * @param v
     */
    @OnClick(R.id.img_btn_right)
    public void onClickRightBtn(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            if (fieldDataList == null)
                fieldDataList = new ArrayList<>();

            new CustomFilterDialog(this, this, objecttType, mLocationKey, fieldDataList).show();
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    /**
     * Select location button click event
     *
     * @param v
     */
    @OnClick(R.id.btn_select_location)
    public void onClickSelectLocation(View v) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            dialog.showBuildingListDialog(SmartConstants.FROM_FILTER_ACTIVITY, this);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.imgv_left_home)
    public void onHomeClick(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    private void filterObjects(String search) {
        StringEmptyCheck check = new StringEmptyCheck();
        boolean isEmpty = check.isNotNullNotEmptyNotWhiteSpaceOnly(search);

        mTvMessage.setVisibility(View.GONE);

        if (!isEmpty) {
            if (mObjectsList != null) {
                adapter.clear();
                for (Objects objects : mObjectsList) {
                    adapter.addItem(objects);
                }
            }
        } else {
            if (mObjectsList != null) {
                List<Objects> objList = new FilterObjects().filterObjects(mObjectsList, search);
                if (objList.size() > 0) {
                    adapter.clear();
                    for (Objects obj : objList) {
                        adapter.addItem(obj);
                    }
                } else {
                    adapter.clear();
                    mTvMessage.setText("No Result for \"" + search + "\"");
                    mTvMessage.setVisibility(View.VISIBLE);
                }
            } else {
                mTvMessage.setText("Poor Internet Connection.");
                mTvMessage.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Set title
     *
     * @param objectType name
     */
    private void setTitle(String forceName) {

        String topic = mMenuName + " - " + mFilterName;
//        switch (objectType) {
//            case "WorkOrder":
//                topic = "WorkOrders";
//                break;
//            case "Asset":
//                topic = "Assets";
//                break;
//            case "Incident":
//                topic = "Incidents";
//                break;
//            case "Inventory":
//                topic = "Inventories";
//                break;
//        }

        if (forceName != null)
            title.setText(forceName);
        else
            title.setText(topic);
    }

    /**
     * Initials views of Activity and object data loading
     * call to getObject() methods in ObjectSync class
     */
    private void loadObjectList() {

        boolean state = mNetwork.NetworkConnectionCheck(this);
        if (state) {
            mObjectsList.clear();
            adapter.clear();
            edtSearchBar.setText("");
            mTvMessage.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            String customFilter = getCustomFilterStringArray(mCustomFilter);

            String titleOfView = (mMenuName != null ? mMenuName : title.getText().toString());

            try {
                objectsSync.getObjects(accountPermission.getAccountInfo().get(0),
                        accountPermission.getAccountInfo().get(1),
                        accountPermission.getAccountInfo().get(2), objecttType, mLocationKey,
                        filterId, customFilter, more, titleOfView);
            } catch (Exception e) {

            }
        } else {
            mProgressBar.setVisibility(View.GONE);
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.btn_action_left)
    public void onClickBack(View view) {
        if (isShowHome) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            onBackPressed();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }

    /**
     * get object successful and completed method call  back function
     *
     * @param objectsList
     */
    @Override
    public void onObjecsDownloadComplete(List<Objects> objectsList) {
        adapter.clear();
        mObjectsList.clear();
        for (Objects objects : objectsList) {
            adapter.addItem(objects);
            mObjectsList.add(objects);
        }

        mProgressBar.setVisibility(View.GONE);
        edtSearchBar.setEnabled(true);
        mTvMessage.setText("");

    }

    /**
     * get object unsuccessful method call  back function
     *
     * @param message
     */
    @Override
    public void onObjectsDownloadError(String message) {
        mProgressBar.setVisibility(View.GONE);
        mTvMessage.setVisibility(View.VISIBLE);
        mTvMessage.setText(message);

    }

    /**
     * Object list item click event
     *
     * @param objects
     */
    @Override
    public void ObjectClick(Objects objects) {
        Intent intent = new Intent(getApplicationContext(), SideNavigationActivity.class);
        intent.putExtra(SmartConstants.OBJECT_TYPE, objecttType);
        intent.putExtra(SmartConstants.LOCATION_KEY, mLocationKey);
        intent.putExtra(SmartConstants.OBJECT_KEY, objects.getObjectKey());
        intent.putExtra(SmartConstants.ACTIVITY_NAME, getClass().getSimpleName());
        intent.putExtra(SmartConstants.FILTER_ID, filterId);
        intent.putExtra(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_ORDER_ACTIVITY);

        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.keep_active);
    }

    /**
     * Location list item click event in location dialog
     *
     * @param locationKey
     * @param locationName
     */
    @Override
    public void onSelectItem(String locationKey, String locationName) {
        mLocationName = locationName;
        mLocationKey = locationKey;
        invokeToGetObject(locationKey, locationName, mCustomFilter);
    }

    /**
     * call to getObjects() method for get all objects from cloud(API)
     *
     * @param locationKey
     * @param locationName
     * @param customFilterList
     */
    private void invokeToGetObject(String locationKey, String locationName, ArrayList<FieldDataSerializable> customFilterList) {
        boolean state = mNetwork.NetworkConnectionCheck(this);
        mTvMessage.setText("");
        edtSearchBar.setText("");
        mTvMessage.setVisibility(View.GONE);
        if (state) {
            mLocationKey = locationKey;
            btnSelectLocation.setText(0 < mLocationName.length() ? locationName : "Select Location");
            adapter.clear();
            mObjectsList.clear();
            mProgressBar.setVisibility(View.VISIBLE);
            //-------------
            String customFilter = getCustomFilterStringArray(customFilterList);
            Log.e("Custom filter : ", customFilter);
            String titleOfView = (mMenuName != null ? mMenuName : title.getText().toString());
            //-------------
            objectsSync.getObjects(accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), objecttType, mLocationKey,
                    filterId, customFilter, more, titleOfView);
        } else {
            mColoredSnackBar.showSnackBar(SmartConstants.NO_INTERNET_CONNECTION, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
        }
    }

    private String getCustomFilterStringArray(List<FieldDataSerializable> fieldDataSerializables) {
        JSONArray fieldData = new JSONArray();
        String dataValues = "";

        for (FieldDataSerializable fieldDataSerializable : fieldDataSerializables) {
            JSONObject field = new JSONObject();
            try {
                String value = fieldDataSerializable.Value.isEmpty() || fieldDataSerializable.Value == null ? "" : fieldDataSerializable.Value;
                field.put("FieldID", fieldDataSerializable.FieldID);
                if (fieldDataSerializable.Type != null) {
                    if (fieldDataSerializable.Type.equals("ATT") || fieldDataSerializable.Type.equals("SAAR")) {
                        JSONArray jsonArray = new JSONArray();
                        if (fieldDataSerializable.SAARValues != null) {
                            for (String s : fieldDataSerializable.SAARValues) {
                                jsonArray.put(s);
                            }
                        }
                        field.put("Value", jsonArray);
                    } else {
                        field.put("Value", value);
                    }
                } else {
                    field.put("Value", value);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fieldData.put(field);
        }

        dataValues = fieldData.toString();

        return dataValues;
    }

    @Override
    public void onClickSubmitFilter(ArrayList<FieldDataSerializable> fieldDataList) {
        //this.fieldDataList = fieldDataList;   // remove the comment to populate popup with old data
        invokeToGetObject(mLocationKey, mLocationName, fieldDataList);
    }
}
