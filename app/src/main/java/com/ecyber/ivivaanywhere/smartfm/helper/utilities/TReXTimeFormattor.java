package com.ecyber.ivivaanywhere.smartfm.helper.utilities;


import org.joda.time.Interval;
import org.joda.time.Period;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Lakmal on 5/10/2016.
 */

public class TReXTimeFormattor {

    SimpleDateFormat simpleDateFormat;
    Date dateTimeNow;
    String sDateTimeNow;

    public TReXTimeFormattor() {

        simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date now = new Date();
        sDateTimeNow = simpleDateFormat.format(now);

        try {
            dateTimeNow = simpleDateFormat.parse(sDateTimeNow);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String printDifference(Date startDate) {
        Date endDate = dateTimeNow;
        Interval interval = new Interval(startDate.getTime(), endDate.getTime());
        Period period = interval.toPeriod();

        if (period.getYears() != 0) {
            int yr = period.getYears();
            if (yr == 1) {
                return yr + " year ago";
            } else {
                return yr + " years ago";
            }
        }else if (period.getMonths() != 0) {
            int months = period.getMonths();
            if (months == 1) {
                return months + " month ago";
            } else {
                return months + " months ago";
            }
        }else if (period.getWeeks() != 0) {
            int weeks = period.getWeeks();
            if (weeks == 1) {
                return weeks + " week ago";
            } else {
                return weeks + " weeks ago";
            }
        }else if (period.getDays() != 0) {
            int days = period.getDays();
            if (days == 1) {
                return "Yesterday";
            } else {
                return days + " days ago";
            }
        }else if (period.getHours() != 0) {
            int hours = period.getHours();
            if (hours == 1) {
                return hours + " hour ago";
            } else {
                return hours + " hours ago";
            }
        }else if (period.getMinutes() != 0) {
            int minutes = period.getMinutes();
            if (minutes == 1) {
                return minutes + " minute ago";
            } else {
                return minutes + " minutes ago";
            }
        } else if (period.getSeconds() != 0) {
            if (period.getSeconds()<=59) {
                return period.getSeconds()+" seconds ago";
            }
        } else {
            return "Now";
        }

        return "";

    }


}
