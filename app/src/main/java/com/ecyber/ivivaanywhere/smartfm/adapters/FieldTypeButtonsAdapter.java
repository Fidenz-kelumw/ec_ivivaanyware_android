package com.ecyber.ivivaanywhere.smartfm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;

import java.util.List;

/**
 * Created by Choota on 3/6/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class FieldTypeButtonsAdapter extends RecyclerView.Adapter<FieldTypeButtonsAdapter.Holder> {

    private List<OptionListModel> items;
    private Context context;
    private UpdateButtonStatusCallback callback;

    private String currentBtn;

    public FieldTypeButtonsAdapter(Context context, List<OptionListModel> items, String currentBtn, UpdateButtonStatusCallback callback) {
        this.items = items;
        this.context = context;
        this.currentBtn = currentBtn;
        this.callback = callback;
    }

    private void updateCurrent(String selection, String data) {
        this.currentBtn = selection;
        callback.onButtonStatusUpdateSuccess(selection, data);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_list_item_button, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        final OptionListModel item = items.get(position);

        if (item != null) {
            holder.button.setText(item.getDisplayText());
            if (currentBtn.equals(item.getValue())) {
                holder.button.setBackgroundResource(R.drawable.drawable_button);
            } else {
                holder.button.setBackgroundResource(R.drawable.drawable_button_black);
            }
        }

        holder.button.setOnClickListener(view -> {
            if (item.getValue() != currentBtn) {
                updateCurrent(item.getValue(), item.getDisplayText());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {

        CustomButton button;

        public Holder(View itemView) {
            super(itemView);
            button = (CustomButton) itemView.findViewById(R.id.btnData);
        }
    }

    public interface UpdateButtonStatusCallback {
        void onButtonStatusUpdateSuccess(String data, String name);
    }
}
