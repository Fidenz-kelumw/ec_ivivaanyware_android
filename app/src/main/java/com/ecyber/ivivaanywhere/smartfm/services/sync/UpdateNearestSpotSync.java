package com.ecyber.ivivaanywhere.smartfm.services.sync;

import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.StatusResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.UpdateNearestSpot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 11/28/17.
 */

public class UpdateNearestSpotSync {

    private AccountPermission account;

    public void updateNearestSpot(UpdateNearestSpotCallback callback, String spotId, String distence, String proximity) {
        this.account = new AccountPermission();

        ServiceGenerator.CreateService(UpdateNearestSpot.class, account.getAccountInfo().get(0))
                .updateNearestSpot(account.getAccountInfo().get(2), account.getAccountInfo().get(1), spotId, proximity, distence)
                .enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        if (response != null && response.isSuccessful() && response.body() != null) {
                            if (response.body().getStatus() != null) {
                                callback.onNearestSpotUpdated(response.body().getStatus().equals("1"));
                            } else {
                                callback.onNearestSpotUpdated(false);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                ErrorLog.saveErrorLog("NearestSpot", message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            callback.onNearestSpotUpdated(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        try {
                            String message = t.getMessage();
                            ErrorLog.saveErrorLog("NearestSpot", message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        callback.onNearestSpotUpdated(false);
                    }
                });
    }

    public interface UpdateNearestSpotCallback {
        void onNearestSpotUpdated(boolean status);
    }
}
