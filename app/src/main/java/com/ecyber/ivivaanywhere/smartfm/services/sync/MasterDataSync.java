package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;

import com.ecyber.ivivaanywhere.smartfm.helper.dataoptimizer.DataDownloadOptimization;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.models.Buildings;
import com.ecyber.ivivaanywhere.smartfm.models.Filter;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectType;
import com.ecyber.ivivaanywhere.smartfm.models.SpotsItem;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DefaultResponse;

import java.util.List;

/**
 * Created by Dihara on 2/16/17.
 */

public class MasterDataSync implements ObjectTypesSync.GetObjectCallBack, BuildingsSync.GetBuildingCallBack, DefaultSync.DefaultSyncCallback, FiltersSync.GetFiltersCallBack{

    private MasterDataSyncCallback delegate;
    private Context context;

    private ObjectTypesSync objectTypesSync = new ObjectTypesSync(this);
    private BuildingsSync buildingsSync = new BuildingsSync(this);
    private DefaultSync defaultSync;
    private FiltersSync filtersSync = new FiltersSync(this);

    private DataDownloadOptimization mDataDownloadOptimization;
    private AccountPermission accountPermission;
    private boolean isSuccess;
    private String lastError;

    public interface MasterDataSyncCallback {

        void onMasterDataSyncComplete();

        void onMasterDataSyncFailed(String error);
    }

    public MasterDataSync(MasterDataSyncCallback delegate, Context context) {
        this.delegate = delegate;
        this.defaultSync = new DefaultSync(this, context);
        this.context = context;

        accountPermission = new AccountPermission();
        mDataDownloadOptimization = new DataDownloadOptimization();
    }

    public boolean getMasterData(boolean isForceUpdate) {
        if(!isForceUpdate) {
            if (mDataDownloadOptimization.isTableDataOld(SmartConstants.MASTER_SYNC, true)) {
                isSuccess = true;
                syncDefaultData();
                return true;
            }
        } else {
            syncDefaultData();
        }
        return false;
    }

    // SYNCS

    // Default Data
    private void syncDefaultData() {
        defaultSync.getDefaultData(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
    }

    // Object Types
    private void syncObjectTypes() {
        objectTypesSync.getObjectTypes(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
    }

    // Buildings
    private void syncBuildings() {
        buildingsSync.getBuildingList(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2), false);
    }

    // Filters
    private void syncFilters() {
        filtersSync.getFilters(accountPermission.getAccountInfo().get(0), accountPermission.getAccountInfo().get(1), accountPermission.getAccountInfo().get(2));
    }

    // Default Sync Callbacks
    @Override
    public void onDefaultDataSuccess(DefaultResponse defaultResponse) {
        syncObjectTypes();
    }

    @Override
    public void onDefaultDataError(String message) {
        syncObjectTypes();
        isSuccess = false;
        lastError = message;
    }

    // Object Type Sync Callbacks
    @Override
    public void onObjectTypesDownload(ObjectType objectType) {
        // IGNORE
    }

    @Override
    public void onObjectTypesDownloadComplete() {
        syncBuildings();
    }

    @Override
    public void onObjectTypesDownloadError(String error) {
        syncBuildings();
        isSuccess = false;
        lastError = error;
    }

    // Building Sync Callbacks
    @Override
    public void onBuildingsDownload(Buildings buildings) {
        // IGNORE
    }

    @Override
    public void onBuildingsDownloadComplete() {
        syncFilters();
    }

    @Override
    public void onBuildingsDownloadError(String error) {
        syncFilters();
        isSuccess = false;
        lastError = error;
    }

    // Filters Callbacks
    @Override
    public void onFiltersDownload(Filter filter) {
        // IGNORE
    }

    @Override
    public void onFiltersDownloadComplete() {
        isSuccess = true;
        lastError = SmartConstants.DOWNLOAD_FAILED;

        if(isSuccess) {
            new TimeCap().updateTableTime(SmartConstants.MASTER_SYNC);
            delegate.onMasterDataSyncComplete();
        }
        else
            delegate.onMasterDataSyncFailed(lastError);
    }

    @Override
    public void onFiltersDownloadError(String error) {
        delegate.onMasterDataSyncFailed(error);
    }

}
