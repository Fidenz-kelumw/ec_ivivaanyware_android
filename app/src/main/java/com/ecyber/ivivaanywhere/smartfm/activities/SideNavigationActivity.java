package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.SideNavigationAdapter;
import com.ecyber.ivivaanywhere.smartfm.adapters.SideNavigationTypeNameAdapter;
import com.ecyber.ivivaanywhere.smartfm.fragments.AttachmentsFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.CheckListFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.InfoFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.ItemListFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.MembersFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.MessageFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.NearbyAssetsFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.ObjectLayoutFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.RelatedWOFragment;
import com.ecyber.ivivaanywhere.smartfm.fragments.ReportsFragment;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.ColoredSnackbar;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.NetworkCheck;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.OnSwipeTouchListener;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.Attachments;
import com.ecyber.ivivaanywhere.smartfm.models.CheckList;
import com.ecyber.ivivaanywhere.smartfm.models.Members;
import com.ecyber.ivivaanywhere.smartfm.models.Messages;
import com.ecyber.ivivaanywhere.smartfm.models.Notifications;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectItem;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectLayout;
import com.ecyber.ivivaanywhere.smartfm.models.ObjectURL;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.Tabs;
import com.ecyber.ivivaanywhere.smartfm.models.TypeData;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.popup.ObjectForwardDialog;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.ForwardObjectResponse;
import com.ecyber.ivivaanywhere.smartfm.services.sync.ForwardSync;
import com.ecyber.ivivaanywhere.smartfm.services.sync.UpdateUserGPSCoordinatesSync;
import com.ecyber.ivivaanywhere.smartfm.services.system.LocationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SideNavigationActivity extends SmartFMBaseActivity implements ForwardSync.ForwardObjectCallback/*, DropDownSelectView.DropDownSelectTextListener*/, ObjectForwardDialog.ForwardObjectDialogCallback, SideNavigationAdapter.SideNavTabClickListener, SideNavigationTypeNameAdapter.SideNavTabNameClickListener, LocationHelper.LocationHelperCallback {

    @BindView(R.id.recyclerview_side_navigation)
    RecyclerView mSideNavigation;

    @BindView(R.id.recycler_view_side_nav_type_name)
    RecyclerView mSideNavTypeName;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.drawer_left)
    RelativeLayout leftDrawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.btn_action_right)
    ImageButton mBtnRight;

    @BindView(R.id.progressBar2)
    ProgressBar mProgressBar;

    String mObjectType;
    SideNavigationAdapter adapter;

    private NetworkCheck mNetwork;
    private ColoredSnackbar mColoredSnackBar;
    private ActionBarDrawerToggle drawerToggle;
    private SideNavigationTypeNameAdapter mTypeNameAdapter;

    private List<Tabs> mTabList;
    private long mMsgID;
    private String mObjectKey;
    private String mLocationKey;
    private String currentTab = "";
    private String mFilterID;
    private String mPreviousActivityName;
    private String mMemberKey = "";
    private String mFromActivity = "";
    private String[] insideSpots;
    private boolean isTabsNeedToHide;
    public static boolean isUpdateAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side_navigation);
        ButterKnife.bind(this);

        isTabsNeedToHide = false;
        mNetwork = new NetworkCheck();
        mColoredSnackBar = new ColoredSnackbar(this);
        mObjectType = getIntent().getStringExtra(SmartConstants.OBJECT_TYPE);
        mObjectKey = getIntent().getStringExtra(SmartConstants.OBJECT_KEY);
        mMsgID = getIntent().getLongExtra(SmartConstants.MESSAGE_ID, 0);

        mLocationKey = getIntent().getStringExtra(SmartConstants.LOCATION_KEY);
        mFilterID = getIntent().getStringExtra(SmartConstants.FILTER_ID);
        mPreviousActivityName = getIntent().getStringExtra(SmartConstants.ACTIVITY_NAME);
        mFromActivity = getIntent().getStringExtra(SmartConstants.FROM_WHERE_ACTIVITY);
        mFromActivity = getIntent().getStringExtra(SmartConstants.FROM_WHERE_ACTIVITY);
        insideSpots = getIntent().getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS);

        if (mPreviousActivityName != null && mPreviousActivityName.equals("SmarFmNotificationReceiver")) {
            final Registration registration = new Registration();
            final List<Registration> registrationList = registration.getAllRegistrations();
            if (registrationList != null) {
                if (registrationList.size() == 0) {
                    startActivity(new Intent(SideNavigationActivity.this, SplashActivity.class));
                    SideNavigationActivity.this.finish();
                    return;
                }
            }
            long messageID = getIntent().getLongExtra(SmartConstants.MESSAGE_ID, 0);
            Notifications.updateStatus(messageID);
        } else {
            System.out.println("from notificaiton");
        }

        loadSideNavigationTabs();
        loadSideNavigationTabsDetails();
        initDrawer();
        List<Tabs> filteredTabs = new Tabs().getFilteredTabs(mObjectType);
        if (mTabList != null) {
            if (mTabList.size() > 0) {
                try {
                    mTabList.get(0).isSelected = true;
                    isTabsNeedToHide = (mTabList.get(0).getTabHide() != null && mTabList.get(0).getTabHide().equals("1"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        try {
            if (filteredTabs != null) {
                if (filteredTabs.size() != 0) {
                    msgReadAvailable();

                    Tabs tab;
                    int position, drawable;

                    if (insideSpots != null && insideSpots.length > 0){
                        position = getForceGPSTab(filteredTabs);
                        tab = filteredTabs.get(position);
                        drawable = R.drawable.ico_gps_normal;
                    }else {
                        position = 0;
                        tab = filteredTabs.get(position);
                        drawable = R.drawable.ico_info_normal;
                    }

                    selectTabItem(tab.getTabType(), drawable, tab.getTabName(), position);
                }
            }
        } catch (NullPointerException e) {
        } catch (IndexOutOfBoundsException e) {
        } catch (IllegalArgumentException e) {
        }

        // Slide navigation detail view swipe right event
        mSideNavigation.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeDown() {

            }

            @Override
            public void onSwipeLeft() {


            }

            @Override
            public void onSwipeUp() {

            }

            @Override
            public void onSwipeRight() {
                if (!isTabsNeedToHide)
                    drawerLayout.openDrawer(leftDrawer);
            }
        });
        forceHideTabWithAPITabHideParam();
    }

    private void msgReadAvailable() {
        boolean state = mNetwork.NetworkConnectionCheck(getBaseContext());
        if (state) {
            if (mMsgID != 0)
                Notifications.updateStatus(mMsgID);
        }
    }

    private int getForceGPSTab(List<Tabs> filteredTabs) {
        try {
            for (int i = 0; i < filteredTabs.size(); i++) {
                if (filteredTabs.get(i).getObjectType() != null && filteredTabs.get(i).getTabType().equals("gps"))
                    return i;
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Forward button click event
     * open forward dialog
     *
     * @param v
     */
    @OnClick(R.id.btn_action_right)
    public void onClickButtonRight(View v) {
        new ObjectForwardDialog(this, this).show(mObjectType, mObjectKey);
    }

    /**
     * initial drawer
     */

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    /**
     * set data to slide navigation bar icons
     * data getting from local database
     */
    private void loadSideNavigationTabs() {

        mTabList = new Tabs().getFilteredTabs(mObjectType);

        // start - custom logic to reload data when having same tabType
        if (mTabList != null && mTabList.size() > 0){
            mTabList = setUpManualForceLoad(new ArrayList<>(mTabList));
        }
        // end - custom logic to reload data when having same tabType

        for (Tabs tabs : mTabList) {
            tabs.isSelected = false;
        }

        adapter = new SideNavigationAdapter(mTabList, this);

        mSideNavigation.setHasFixedSize(true);
        mSideNavigation.setLayoutManager(new LinearLayoutManager(this));
        mSideNavigation.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        mSideNavigation.setAdapter(adapter);
    }

    private List<Tabs> setUpManualForceLoad(List<Tabs> tabs){

        // final tabList
        List<Tabs> finalData = new ArrayList<>();

        // iterate tabs
        for (int i = 0; i < tabs.size(); i++) {

            // create list of same tabType
            List<Tabs> sameTypeList = new ArrayList<>();
            for (Tabs tab : tabs) {
                if (tab.TabType.equals(tabs.get(i).TabType))
                    sameTypeList.add(tab);
            }

            if (sameTypeList.size() > 1)
                tabs.get(i).isNeedForceReload = true;
            else
                tabs.get(i).isNeedForceReload = false;

            finalData.add(tabs.get(i));
        }

        return finalData;
    }

    /**
     * set data to slide navigation list name
     * get data from local database
     */
    private void loadSideNavigationTabsDetails() {
        List<Tabs> tabsList = new Tabs().getFilteredTabs(mObjectType);
        mTypeNameAdapter = new SideNavigationTypeNameAdapter(tabsList, this, this);

        mSideNavTypeName.setHasFixedSize(true);
        mSideNavTypeName.setLayoutManager(new LinearLayoutManager(this));
        mSideNavTypeName.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        mSideNavTypeName.setAdapter(mTypeNameAdapter);
    }

    /**
     * invoking method of navigation Tab Icon item click event
     *
     * @param tagName
     * @param tabIcon
     * @param tabName
     */
    private void selectTabItem(String tagName, int tabIcon, String tabName, int position) {

        if (currentTab.equals(tabName)) {
            return;
        }

        // start - custom logic to reload data when having same tabType
        boolean tabForceReloadStatus = false;
        try{
            tabForceReloadStatus = mTabList.get(position).isNeedForceReload;
        } catch (Exception e){
            e.printStackTrace();
        }
        // end - custom logic to reload data when having same tabType

        adapter.updateItem(position);
        currentTab = tabName;

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putString(SmartConstants.OBJECT_TYPE, mObjectType);
        bundle.putString(SmartConstants.TAB_NAME, tabName);
        bundle.putString(SmartConstants.OBJECT_KEY, mObjectKey);
        bundle.putString(SmartConstants.LOCATION_KEY, mLocationKey);
        bundle.putString(SmartConstants.FILTER_ID, mFilterID);
        bundle.putString(SmartConstants.ACTIVITY_NAME, mPreviousActivityName);
        bundle.putString(SmartConstants.LOCATION_KEY, mLocationKey);
        bundle.putString(SmartConstants.FROM_WHERE_ACTIVITY, SmartConstants.FROM_ORDER_ACTIVITY);
        bundle.putBoolean(SmartConstants.FORCE_RELOAD, tabForceReloadStatus);   // custom logic to reload data when having same tabType
        bundle.putStringArray(SmartConstants.NEAR_LOCATION_SPOTS, insideSpots);

        switch (tagName) {
            case "Info":
                bundle.putString(SmartConstants.FROM_WHERE_ACTIVITY, mFromActivity);
                mProgressBar.setVisibility(View.VISIBLE);
                InfoFragment infoFragment = new InfoFragment(this, mProgressBar);
                infoFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, infoFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "Messages":
                MessageFragment messageFragment = new MessageFragment(this, mProgressBar);
                messageFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, messageFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "Members":
                MembersFragment memberFragment = new MembersFragment(this, mProgressBar);
                memberFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, memberFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "Attachments":
                AttachmentsFragment attachmentFragment = new AttachmentsFragment(mProgressBar, this);
                attachmentFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, attachmentFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "Layout":
                ObjectLayoutFragment layoutFragment = new ObjectLayoutFragment(this, mProgressBar);
                layoutFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, layoutFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "Checklist":
                CheckListFragment checkListFragment = new CheckListFragment(this, mProgressBar);
                checkListFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, checkListFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "ItemList":
                ItemListFragment itemListFragment = new ItemListFragment();
                itemListFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, itemListFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "SubObjects":
                RelatedWOFragment relatedWOFragment = new RelatedWOFragment();
                relatedWOFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, relatedWOFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "URLs":
                ReportsFragment reportsFragment = new ReportsFragment();
                reportsFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container, reportsFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case "gps":
                NearbyAssetsFragment assetsFragment = new NearbyAssetsFragment();
                assetsFragment.setArguments(bundle);
                mProgressBar.setVisibility(View.INVISIBLE);

                fragmentTransaction.replace(R.id.container, assetsFragment, tagName);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

        }

        insideSpots = new String[0];
        title.setText(tabName);
    }

    private void issUpdateAvailable(final View v, final int position, final String tabName) {
        if (isUpdateAvailable) {
            new MaterialDialog.Builder(this)
                    .title("Warning")
                    .content("Unsaved data will be lost. Do you want to proceed?")
                    .positiveText("Ok")
                    .negativeText("Cancel")
                    .onPositive((dialog, which) -> {
                        dialog.dismiss();
                        isUpdateAvailable = false;
                        selectTabItem(v.getTag().toString(), R.drawable.ico_members_normal, tabName, position);
                        drawerLayout.closeDrawer(Gravity.LEFT);
                    })
                    .onNegative((dialog, which) -> {
                        dialog.dismiss();
                        drawerLayout.closeDrawer(Gravity.LEFT);
                    })
                    .show();
        } else {
            selectTabItem(v.getTag().toString(), R.drawable.ico_members_normal, tabName, position);
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    // this is a new change given by Pasan and this crazy idea break whole application logic
    // application suppose to be work as a dynamic app. but with this changes, it will break
    // already mention but no use, --by Choota--
    private void forceHideTabWithAPITabHideParam() {
        RelativeLayout sidePannel = (RelativeLayout) findViewById(R.id.side_tab);
        RelativeLayout sideDrawer = (RelativeLayout) findViewById(R.id.drawer_left);

        if (isTabsNeedToHide) {
            sideDrawer.setVisibility(View.GONE);
            sidePannel.setVisibility(View.GONE);

            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            sideDrawer.setVisibility(View.VISIBLE);
            sidePannel.setVisibility(View.VISIBLE);

            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    private void forceUserLocationToServer(){
        LocationHelper.init(SideNavigationActivity.this, this);
        if(LocationHelper.isLocationEnabled()){
            LocationHelper.requestlocation(true);
        }
    }

    /**
     * Navigation Tab Icon list item click event
     *
     * @param v
     * @param tabName
     */
    @Override
    public void OnClickTabButton(final View v, final int position, final String tabName) {
        issUpdateAvailable(v, position, tabName);
    }

    /**
     * Navigation Tab Name list item click event
     *
     * @param v
     * @param tabName
     */
    @Override
    public void OnClickTabName(final View v, final int position, final String tabName) {
        issUpdateAvailable(v, position, tabName);
    }

    /**
     * click back button
     * all tab area data (Info, messages, members, layout, checklist, photos etc..)table clear
     *
     * @param view
     */
    @OnClick(R.id.btn_action_left)
    public void onClickBack(View view) {
        onBackPressed();
    }

    @OnClick(R.id.imgv_left_home)
    public void onHomeClick(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
        pressBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pressBack();
    }

    private void pressBack() {
        for (Tabs tabs : mTabList) {
            if (tabs.isSelected) {
                tabs.isSelected = false;
            }
        }
        //new Info().clearTable();
        new InfoObject().clearTable();
        new Messages().clearTable();
        new Members().clearTable();
        new CheckList().clearTable();
        new Attachments().clearTable();
        new ObjectLayout().clearTable();
        new ObjectItem().clearTable();
        new TypeData().clearTable();
        new SubObject().clearTable();
        new ObjectURL().clearTable();

        finish();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }
    /*@Override
    public void onDropDownSelectFocusOut(int position, String displayText, String value) {
        Log.d("SmartFM", displayText + "|" + value);
        mMemberKey = value;
    }*/

    /**
     * object forward is Successful call back function
     *
     * @param object
     */
    @Override
    public void forwardObjectSuccess(ForwardObjectResponse object) {
        mColoredSnackBar.dismissSnacBar();
        String message = object.getMessage();
        message = Utility.IsEmpty(message) ? "Successfully sent" : message;
        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_OK, Snackbar.LENGTH_SHORT);
    }

    /**
     * object forward unsuccessful call back function
     *
     * @param message
     */
    @Override
    public void forwardObjectError(String message) {
        mColoredSnackBar.dismissSnacBar();
        mColoredSnackBar.showSnackBar(message, ColoredSnackbar.TYPE_ERROR, Snackbar.LENGTH_SHORT);
    }


    @Override
    public void onForwardObjectSend(String memberKey, String message, Dialog dialogInstance, String objectKEy, String objectType) {
        boolean state = mNetwork.NetworkConnectionCheck(getBaseContext());
        if (state) {
            ForwardSync forwardSync = new ForwardSync(this, getApplicationContext());
            AccountPermission accountPermission = new AccountPermission();

            mColoredSnackBar.showSnackBar("Message is sending.", ColoredSnackbar.TYPE_UPDATE, Snackbar.LENGTH_INDEFINITE);
            forwardSync.forwardObject(accountPermission.getAccountInfo().get(0),
                    accountPermission.getAccountInfo().get(1),
                    accountPermission.getAccountInfo().get(2), mObjectType, mObjectKey, memberKey, message);
            dialogInstance.dismiss();
        } else {
            Toast.makeText(getApplicationContext(), SmartConstants.NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            String locReqKey = getIntent().getStringExtra(SmartConstants.LOCATION_REQ_KEY);
            if (locReqKey != null && locReqKey.equals("1")) {
                getIntent().removeExtra(SmartConstants.OBJECT_TYPE);
                this.forceUserLocationToServer();
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onLocationFound(boolean status, double lon, double lat, Location location) {
        if (status && location != null) {
            String latX, lanX;

            latX = String.valueOf(lat);
            lanX = String.valueOf(lon);

            new UpdateUserGPSCoordinatesSync(this).update(new UpdateUserGPSCoordinatesSync.UpdateUserGPSCoordinateCallback() {
                @Override
                public void onUpdateUserGPSCoordinateSuccess() {
                    Log.i("UpdateUserGPSCoordinate", "Successfully updated");
                }

                @Override
                public void onUpdateUserGPSCoordinateFail(String message) {
                    Log.i("UpdateUserGPSCoordinate", "Failed to updated");
                }
            }, latX, lanX);
        }
    }


    //    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        Log.e("OnNewIntent", "Fired");
//
//        try {
//            NearbyAssetsFragment fragment = (NearbyAssetsFragment) getFragmentManager().findFragmentByTag("gps");
//            String[] data = intent.getStringArrayExtra(SmartConstants.NEAR_LOCATION_SPOTS);
//
//            if (data != null)
//                fragment.updateMarkerImage(Arrays.asList(data));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
