package com.ecyber.ivivaanywhere.smartfm.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.adapters.AttachmentTypeAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.DisplayPixelCalculator;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentTypesMaster;

import java.util.List;

/**
 * Created by Lakmal on 6/1/2016.
 */
public class AttachmentTypeDialog implements AttachmentTypeAdapter.AttachmentTypeItemClickListener{

    private Activity mActivity;
    private AttachmentTypeListCallback mDelegate;
    private RecyclerView mRvSingleList;
    private RelativeLayout mSearchContainer;
    private AttachmentTypeAdapter mTypeAdapter;
    private Dialog mDialogList;

    public AttachmentTypeDialog(Activity activity, AttachmentTypeListCallback delegate) {
        mActivity = activity;
        mDelegate = delegate;
    }

    public void makeDialog() {
        mDialogList = new Dialog(mActivity, R.style.CustomDialog);
        LayoutInflater inflater = LayoutInflater.from(mActivity.getApplicationContext());
        View dialogView = inflater.inflate(R.layout.dialog_list, null);

        ImageButton tvQR = (ImageButton) dialogView.findViewById(R.id.btnQr);
        ImageButton btnClose = (ImageButton) dialogView.findViewById(R.id.img_btn_close);
        mRvSingleList = (RecyclerView) dialogView.findViewById(R.id.rv_single_list);
        TextView mHeaderTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_header_title);
        mSearchContainer = (RelativeLayout) dialogView.findViewById(R.id.rel_lay_search_container);

        mSearchContainer.setVisibility(View.GONE);
        tvQR.setVisibility(View.GONE);
        mHeaderTitle.setText("Attachment Type");

        mRvSingleList.setLayoutManager(new LinearLayoutManager(mActivity.getApplicationContext()));
        mRvSingleList.setHasFixedSize(true);

        List<AttachmentTypesMaster> mTypeList = new Select().from(AttachmentTypesMaster.class).execute();
        mTypeAdapter = new AttachmentTypeAdapter(mActivity, mTypeList, this);
        mRvSingleList.setAdapter(mTypeAdapter);

        btnClose.setOnClickListener(v -> mDialogList.dismiss());

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        float dpHeight = ((displayMetrics.heightPixels / displayMetrics.density) / 100) * 57;
        float dpWidth = ((displayMetrics.widthPixels / displayMetrics.density) / 100) * 90;


        final float x, y;
        DisplayPixelCalculator converter = new DisplayPixelCalculator();
        x = converter.dipToPixels(mActivity, dpWidth);
        y = converter.dipToPixels(mActivity, dpHeight);

        mDialogList.setContentView(dialogView);
        mDialogList.show();
        mDialogList.setCanceledOnTouchOutside(false);
        mDialogList.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialogList.getWindow().setLayout((int) x, (int) y);


    }

    /**
     * Attachment type list item click event
     *
     * @param type
     */
    @Override
    public void onClickTypeItem(AttachmentTypesMaster type) {
        mDelegate.onTypeItemClick(type, mDialogList);
    }

    public interface AttachmentTypeListCallback {
        void onTypeItemClick(AttachmentTypesMaster type, Dialog dialogInstance);
    }

}
