package com.ecyber.ivivaanywhere.smartfm.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.SmartFMBaseActivity;
import com.ecyber.ivivaanywhere.smartfm.adapters.ErrorLogAdapter;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomButton;
import com.ecyber.ivivaanywhere.smartfm.customviews.CustomTextView;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.RecyclerDecorator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lakmal on 5/16/2016.
 */
public class ErrorLogListActivity extends SmartFMBaseActivity {

    @BindView(R.id.tv_title)
    CustomTextView mTvTitle;

    @BindView(R.id.rv_error_log)
    RecyclerView mRvErrorLog;

    @BindView(R.id.tv_message)
    CustomTextView mTvMessage;

    @BindView(R.id.swiperefresh_list)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private ErrorLogAdapter mAdapter;
    private List<ErrorLog> allErrors;
    private int spacing = 8;
    private int spanCount = 1;
    private boolean includeEdge = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_log);

        ButterKnife.bind(this);
        mTvTitle.setText("Error Log");
        initView();
        dataLoad();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            if (mAdapter != null) {
                mAdapter.clear();
            }
            dataLoad();
            mSwipeRefreshLayout.setRefreshing(false);
        });
    }

    /**
     * click event of Clear Button
     * @param v
     */
    @OnClick(R.id.btn_clear)
    public void onClickButtonClear(View v) {
        showConfirmationDialogBox("Are you sure you want to clear the log?");
    }

    /**
     * show confirmation message when clicking ClearButton for Clear error logs list
     * @param message display message on confirmation dialog
     */
    private void showConfirmationDialogBox(String message) {
        final Dialog confirmationDialog = new Dialog(this, R.style.CustomDialog);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(getBaseContext().LAYOUT_INFLATER_SERVICE);
        View msgView = inflater.inflate(R.layout.dialog_confirmation, null);

        confirmationDialog.setContentView(msgView);

        CustomTextView msg = (CustomTextView) msgView.findViewById(R.id.tv_message);
        CustomButton btnCancel = (CustomButton) msgView.findViewById(R.id.btn_cancel);
        CustomButton btnConfirm = (CustomButton) msgView.findViewById(R.id.btn_ok);

        msg.setText(message);
        btnConfirm.setText("Confirm");
        btnCancel.setText("Cancel");

        btnConfirm.setOnClickListener(v -> {
            ErrorLog.cleatTable();
            if (mAdapter != null) {
                mAdapter.clear();
            }
            confirmationDialog.dismiss();
        });

        btnCancel.setOnClickListener(v -> confirmationDialog.dismiss());

        confirmationDialog.show();
        confirmationDialog.setCanceledOnTouchOutside(true);
    }

    /**
     * initial all views in this activity
     */
    private void initView() {
        mRvErrorLog.setHasFixedSize(true);
        mRvErrorLog.setLayoutManager(new LinearLayoutManager(this));
        mRvErrorLog.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        allErrors = new ArrayList<>();

        mAdapter = new ErrorLogAdapter(allErrors, this);
        mRvErrorLog.setAdapter(mAdapter);
        mRvErrorLog.addItemDecoration(new RecyclerDecorator(this, spanCount, spacing, includeEdge, true));

    }

    /**
     * data loading to error list get from local data base
     */
    private void dataLoad() {

        mTvMessage.setVisibility(View.GONE);
        allErrors = new ErrorLog().getAllErrors();

        if (allErrors.size() == 0) {
            mTvMessage.setText("Errors not found");
            mTvMessage.setVisibility(View.VISIBLE);
            return;
        }

        for (ErrorLog allError : allErrors) {
            mAdapter.addItem(allError);
        }
    }


    @OnClick(R.id.btn_action_left)
    public void onClickBackButton(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();//finish();
        overridePendingTransition(R.anim.keep_active, R.anim.slide_to_right);
    }
}
