package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Chathura Jayanath on 8/5/2015.
 */
@Table(name = "TimeCap")
public class TimeCap extends Model {

    @Column(name = "TableName")
    public String tableName;

    @Column(name = "UpdatedTime")
    public String updatedTime;

    public TimeCap(String tableName, String updatedTime) {
        super();
        this.tableName = tableName;
        this.updatedTime = updatedTime;
    }

    public TimeCap() {
        super();
    }

    public static String getTableUpdatedTime(String tableName) {
        TimeCap cap = new Select()
                .from(TimeCap.class)
                .where("TableName = ?", tableName)
                .executeSingle();

        return cap.updatedTime;
    }

    public List<TimeCap> getAllTimeUpdates() {
        return new Select()
                .from(TimeCap.class)
                .execute();
    }

    public List<TimeCap> getTableLastUpdateTime(String tableName) {
        return new Select()
                .from(TimeCap.class)
                .where("TableName = ?", tableName)
                .execute();
    }

    public void updateTableTime(String tableName) {

        SimpleDateFormat simpleDateD = new SimpleDateFormat("yyyy/M/dd HH:mm:ss");
        Date date = new Date();
        String dateToDBD = simpleDateD.format(date);

        try {

            List<TimeCap> data = getTableLastUpdateTime(tableName);

            if (data != null && data.size() > 0) {
                new Update(TimeCap.class)
                        .set("UpdatedTime = ?", dateToDBD)
                        .where("TableName = ?", tableName)
                        .execute();
            } else {
                TimeCap timeCap = new TimeCap();
                timeCap.tableName = tableName;
                timeCap.updatedTime = dateToDBD;
                timeCap.save();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTable() {
        new Delete().from(TimeCap.class).execute();
    }


}
