package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.activeandroid.query.Select;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Type;
import com.ecyber.ivivaanywhere.smartfm.models.Actions;
import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;
import com.ecyber.ivivaanywhere.smartfm.models.FieldData;
import com.ecyber.ivivaanywhere.smartfm.models.PageLinks;
import com.ecyber.ivivaanywhere.smartfm.models.Registration;
import com.ecyber.ivivaanywhere.smartfm.models.SubObject;
import com.ecyber.ivivaanywhere.smartfm.models.TimeCap;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InfoObject;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.InputModesModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ObjectInfoModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.OptionListModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueModel;
import com.ecyber.ivivaanywhere.smartfm.models.objectinfomodel.ValueType;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataResponse;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.DataUpdateStatus;
import com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.GetObjectInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Choota on 3/9/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

public class InfoSync {
    private Context context;
    private InfoCallback callback;

    public InfoSync(Context context, InfoCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void getInfoObject(String url, final String apiKey, String userKey, String objType, final String objKey, String mTabName) {
        ServiceGenerator.CreateService(GetObjectInfo.class, url).getObjectInfoV2(apiKey, userKey, objType, objKey).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        new InfoObject().clearTable();
                        new SubObject().clearTable();
                        convertResponceAndSave(response.body(), objKey);
                        new TimeCap().updateTableTime(mTabName);
                        Log.e("FM", "ObjectInfo Updated.");
                    } else {
                        callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
                    }
                } else {
                    try {
                        String message = response.errorBody().string();
                        Log.d("SmartFm - ErrorLog", message);
                        ErrorLog.saveErrorLog("Get Object Info", message);
                        callback.onInfoCallFail(message);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onInfoCallFail(SmartConstants.TRY_AGAIN);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
            }
        });
    }

    public void getLookUpData(String url, String apiKey, String userKey, String objType, final String path, String objKey, String fieldData) {

        String locationKey = new Registration().select().LocationKey;

        ServiceGenerator.CreateService(GetObjectInfo.class, url).getWOCategories(path, apiKey, userKey, objType, objKey, fieldData, locationKey)
                .enqueue(new Callback<DataResponse>() {
                    @Override
                    public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getDataLookUps() != null) {
                                    if (response.body().getDataLookUps().size() != 0) {
                                        if(response.body().getDataLookUps().size() == 1 && response.body().getDataLookUps().get(0).getValue().equals("0")){
                                            callback.onInfoCallFail(response.body().getDataLookUps().get(0).getDisplayText());
                                        } else {
                                            callback.onGetWOCategoriesSuccess(response.body().getDataLookUps());
                                        }
                                    } else {
                                        callback.onInfoCallFail(SmartConstants.ITEM_NOT_AVAILABLE);
                                    }
                                } else {
                                    callback.onInfoCallFail(SmartConstants.ITEM_NOT_AVAILABLE);
                                }
                            } else {
                                callback.onInfoCallFail(SmartConstants.ITEM_NOT_AVAILABLE);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Get " + path, message);
                                callback.onInfoCallFail(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onInfoCallFail(SmartConstants.ITEM_NOT_AVAILABLE);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<DataResponse> call, Throwable t) {
                        callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
                    }
                });
    }

    public void updateObjectInfo(String url, String apiKey, String userKey, String objType, final String objKey, String fieldData, final JSONArray jsonfieldData) {
        ServiceGenerator.CreateService(GetObjectInfo.class, url).getUpdateObjectInfo(apiKey, userKey, objType, objKey, fieldData)
                .enqueue(new Callback<DataUpdateStatus>() {
                    @Override
                    public void onResponse(Call<DataUpdateStatus> call, Response<DataUpdateStatus> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    ObjectsSync.foreExpireObjectList();
                                    callback.onGetUpdateObjectSuccess(response.body(), objKey, jsonfieldData);
                                } else {
                                    callback.onGetUpdateObjectSuccess(response.body(), objKey, jsonfieldData);
                                }
                            } else {
                                callback.onGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Update Object Info", message);
                                callback.onGetUpdateObjectError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onGetUpdateObjectError(SmartConstants.SUBMIT_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DataUpdateStatus> call, Throwable t) {
                        callback.onInfoCallFail(SmartConstants.SUBMIT_FAILED);
                    }
                });

    }

    public void executeAction(String url, String apiKey, String userKey, String objType, String objKey, String actionId, String cargo) {
        ServiceGenerator.CreateService(GetObjectInfo.class, url).getExcecuteAction(apiKey, userKey, objType, objKey, actionId, cargo)
                .enqueue(new Callback<DataUpdateStatus>() {
                    @Override
                    public void onResponse(Call<DataUpdateStatus> call, Response<DataUpdateStatus> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Log.e("FM", response.body().toString());
                                callback.onExecuteActionSuccess(response.body());
                            } else {
                                callback.onExecuteActionError(response.body());
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Execute Action", message);
                                callback.onInfoCallFail(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.onInfoCallFail(SmartConstants.EXECUTE_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DataUpdateStatus> call, Throwable t) {
                        callback.onInfoCallFail(SmartConstants.EXECUTE_FAILED);
                    }
                });
    }

    public void convertResponceAndSave(ResponseBody body, String objectKey) {

        InfoObject infoObject = new InfoObject();
        infoObject.setmObjectKey(objectKey);

        try {
            String jsonData = body.string();
            JSONObject objectResponce = new JSONObject(jsonData);

            if (objectResponce.has("UpdateConfirmation"))
                infoObject.setmUpdateConfirmation(objectResponce.getString("UpdateConfirmation"));
            else
                infoObject.setmUpdateConfirmation("0");

            if (objectResponce.has("UpdateObjectInfo"))
                infoObject.setmUpdateObjectInfo(objectResponce.getString("UpdateObjectInfo"));
            else
                infoObject.setmUpdateObjectInfo("0");

            if (objectResponce.has("HideUpdateButton"))
                infoObject.setHideUpdateButton(objectResponce.getString("HideUpdateButton"));
            else
                infoObject.setHideUpdateButton("0");

            if (objectResponce.has("UpdateButtonText"))
                infoObject.setUpdateButtonText(objectResponce.getString("UpdateButtonText"));
            else
                infoObject.setUpdateButtonText("Submit");

            long infoId = infoObject.save();

            JSONArray objectInfo = objectResponce.getJSONArray("ObjectInfo");
            JSONArray pageLinks = objectResponce.getJSONArray("PageLinks");
            JSONArray actions = objectResponce.getJSONArray("Actions");

            //region objectInfo map section
            List<ObjectInfoModel> objectInfoModels = new ArrayList<>();
            if (objectInfo != null) {
                for (int i = 0; i < objectInfo.length(); i++) {
                    JSONObject object = objectInfo.getJSONObject(i);
                    ObjectInfoModel oInfoModel = new ObjectInfoModel();

                    oInfoModel.setObjectKey(objectKey);
                    oInfoModel.setFieldid(object.getString("FieldID"));
                    oInfoModel.setFieldname(object.getString("FieldName"));
                    oInfoModel.setValuetype(object.getString("ValueType"));

                    JSONObject defData = object.getJSONObject("DefaultData");
                    oInfoModel.setDisplaytext(defData.getString("DisplayText"));

                    oInfoModel.setDisableDelete((object.has("DisableDelete") ? object.getString("DisableDelete") : "0"));
                    oInfoModel.setExtended((object.has("Extended") ? object.getString("Extended") : "0"));

                    // this to store value models, list will be added due to
                    // some objects are returning multiple objects
                    // sorry :) since this will create list if we have single value too
                    List<ValueModel> valueModels = new ArrayList<>();

                    try {
                        switch (object.getString("ValueType")) {
                            case Type.SLT:
                            case Type.LABEL:
                            case Type.DS:
                            case Type.BUTTONS:
                            case Type.SAS:
                            case Type.MLT:
                            case Type.DT:
                            case Type.DAT:
                            case Type.FEEDBACK:
                                ValueModel valueModel = new ValueModel(object.getString("FieldID"),
                                        ValueType.SINGLE,
                                        defData.getString("Value"),
                                        null,
                                        null);

                                valueModel.save();
                                valueModels.add(valueModel);

                                oInfoModel.setValueModels(valueModels);
                                break;

                            case Type.SAAR:
                                JSONArray valueArray = defData.getJSONArray("Value");
                                if (valueArray != null) {
                                    for (int j = 0; j < valueArray.length(); j++) {
                                        ValueModel valueModelArray = new ValueModel(object.getString("FieldID"),
                                                ValueType.ARRAY,
                                                valueArray.getString(j),
                                                null,
                                                null);

                                        valueModelArray.save();
                                        valueModels.add(valueModelArray);
                                    }

                                    oInfoModel.setValueModels(valueModels);
                                } else {
                                    oInfoModel.setValueModels(null);
                                }
                                break;
                            case Type.ATT:
                                try {
                                    JSONArray valueObjArray = defData.getJSONArray("Value");
                                    if (valueObjArray != null) {
                                        for (int j = 0; j < valueObjArray.length(); j++) {
                                            JSONObject valueObject = valueObjArray.getJSONObject(j);
                                            ValueModel valueModelArray = new ValueModel(object.getString("FieldID"),
                                                    ValueType.OBJECTARRAY,
                                                    null,
                                                    valueObject.getString("ATTName"),
                                                    valueObject.getString("ATTType"));

                                            valueModelArray.save();
                                            valueModels.add(valueModelArray);
                                        }

                                        oInfoModel.setValueModels(valueModels);
                                    } else {
                                        oInfoModel.setValueModels(null);
                                    }
                                } catch (Exception e) {
                                    List<ValueModel> valueModels1 = new ArrayList<>();
                                    oInfoModel.setValueModels(valueModels1);
                                }
                                break;
                        }
                    } catch (Exception e) {
                        continue;
                    }

                    oInfoModel.setEditable(object.getString("Editable"));

                    if (object.has("OptionList")) {
                        JSONArray optionList = object.getJSONArray("OptionList");
                        List<OptionListModel> optionListModels = new ArrayList<>();

                        for (int j = 0; j < optionList.length(); j++) {
                            JSONObject option = optionList.getJSONObject(j);

                            OptionListModel listModel = new OptionListModel(object.getString("FieldID"),
                                    option.getString("DisplayText"),
                                    option.getString("Value"));

                            listModel.save();
                            optionListModels.add(listModel);
                        }

                        oInfoModel.setOptionListModels(optionListModels);
                    }

                    if (object.has("LookUpService")) {
                        oInfoModel.setLookupservice(object.getString("LookUpService"));
                    }

                    if (object.has("Mandatory")) {
                        oInfoModel.setMandatory(object.getString("Mandatory"));
                    }

                    if (object.has("QRCode")) {
                        oInfoModel.setQrcode(object.getString("QRCode"));
                    }

                    if (object.has("Style")) {
                        oInfoModel.setStyle(object.getString("Style"));
                    }

                    if (object.has("DownloadFolder")) {
                        oInfoModel.setDownloadfolder(object.getString("DownloadFolder"));
                    }

                    if (object.has("UploadFolder")) {
                        oInfoModel.setUploadfolder(object.getString("UploadFolder"));
                    }

                    if (object.has("NoDataValue")) {
                        oInfoModel.setNoDataValue(object.getString("NoDataValue"));
                    }

                    if (object.has("InputModes")) {
                        JSONArray inputModes = object.getJSONArray("InputModes");
                        String[] inputModesArray = new String[inputModes.length()];
                        for (int j = 0; j < inputModes.length(); j++) {
                            InputModesModel model = new InputModesModel(object.getString("FieldID"),
                                    inputModes.getString(j));

                            model.save();
                            inputModesArray[j] = inputModes.getString(j);
                        }
                        oInfoModel.setInputModes(inputModesArray);
                    }

                    oInfoModel.save();
                    objectInfoModels.add(oInfoModel);
                }
                infoObject.setmObjectInfo(objectInfoModels);
            } else {
                callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
            }
            //endregion

            //region info object pagelinks
            List<PageLinks> pageLinkses = new ArrayList<>();
            if (pageLinks != null) {
                for (int i = 0; i < pageLinks.length(); i++) {
                    JSONObject pageLink = pageLinks.getJSONObject(i);
                    PageLinks link = new PageLinks();

                    link.setLinkType(pageLink.getString("LinkType"));
                    link.setLinkName(pageLink.getString("LinkName"));
                    link.setObjectType(pageLink.getString("ObjectType"));
                    link.setObjectKey(pageLink.getString("ObjectKey"));
                    link.setInfoID(infoId);

                    Long linkId = link.save();

                    JSONArray fieldData = pageLink.getJSONArray("FieldData");
                    List<FieldData> fieldDatas = new ArrayList<>();
                    for (int j = 0; j < fieldData.length(); j++) {
                        JSONObject object = fieldData.getJSONObject(j);

                        FieldData data = new FieldData(linkId,
                                object.getString("FieldID"),
                                object.getString("Value"),
                                object.getString("DisplayText"));

                        data.save();
                        fieldDatas.add(data);
                    }
                    link.setFieldData(fieldDatas);
                    pageLinkses.add(link);
                }
                infoObject.setPageLinkses(pageLinkses);
            } else {
                callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
            }
            //endregion

            //region info object actions
            if (actions != null) {
                List<Actions> actionsList = new ArrayList<>();
                for (int i = 0; i < actions.length(); i++) {
                    JSONObject object = actions.getJSONObject(i);

                    Actions action = new Actions(infoId,
                            object.getString("ActionID"),
                            object.getString("ActionName"),
                            object.getString("Enabled"),
                            object.getString("Confirmation"),
                            object.getString("Cargo"));

                    action.save();
                    actionsList.add(action);
                }
                infoObject.setActions(actionsList);
            } else {
                callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
            }
            //endregion
            callback.onInfoCallSuccess(infoObject);

        } catch (IOException e) {
            e.printStackTrace();
            callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
        } catch (JSONException e) {
            e.printStackTrace();
            callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            callback.onInfoCallFail(SmartConstants.ITEM_NOT_FOUND);
        }

    }

    public void updateInfoObjectInfoDetails(String objectKey, JSONArray fieldData) {
//        List<ObjectInfo> objectInfoList = new Info().getObjectInfoByObjKey(objectKey);
//
//        for (ObjectInfo objectInfo : objectInfoList) {
//            if (fieldData != null) {
//                for (int i = 0; i < fieldData.length(); i++) {
//                    try {
//                        String fieldID = fieldData.getJSONObject(i).get("FieldID").toString();
//                        if (fieldID.equals(objectInfo.getFieldID())) {
//                            String value = fieldData.getJSONObject(i).get("Value").toString();
//                            String displayText = fieldData.getJSONObject(i).get("DisplayText").toString();
//
//                            Long id = objectInfo.getDefaultDataID();
//
//                            new Update(DefaultData.class)
//                                    .set("Value = ?,DisplayText=?", value, displayText)
//                                    .where("id = ?", id)
//                                    .execute();
//                        }
//
//                    } catch (Exception e) {
//
//                    }
//
//                }
//            }
    }

    public InfoObject getInfoFromDB(InfoObject info) {
        String objKey = info.getmObjectKey();
        InfoObject infoObject = info;

        List<PageLinks> newPageLinksArray = new ArrayList<>();
        List<ObjectInfoModel> loadedObjectModels = new ArrayList<>();
        List<Actions> newActionsArray = new ArrayList<>();

        //region objectmodel
        List<ObjectInfoModel> objectInfoModel = new ObjectInfoModel().getObjectInfo(info.getmObjectKey());
        for (ObjectInfoModel infoModel : objectInfoModel) {
            List<ValueModel> valueModels = new ValueModel().getValuesObjectArray(infoModel.getFieldid());
            infoModel.setValueModels(valueModels);

            List<OptionListModel> optionListModels = new OptionListModel().getOptionList(infoModel.getFieldid());
            infoModel.setOptionListModels(optionListModels);

            String[] inputModesModels = new InputModesModel().getInputModes(infoModel.getFieldid());
            infoModel.setInputModes(inputModesModels);

            loadedObjectModels.add(infoModel);
        }
        //endregion

        //region pagelinks
        List<PageLinks> pageLinks = new Select().from(PageLinks.class).where("InfoID = ?", info.getId()).execute();
        for (PageLinks pageLink : pageLinks) {
            long linkId = pageLink.getId();

            List<FieldData> newFieldDataArray = new ArrayList<>();

            List<FieldData> fieldDataList = new Select().from(FieldData.class).where("PageLinkID = ?", linkId).execute();
            for (FieldData fieldData : fieldDataList) {
                FieldData newField = new FieldData();
                newField.setFieldId(fieldData.getFieldId());
                newField.setValue(fieldData.getValue());
                newField.setDisplayText(fieldData.getDisplayText());
                newFieldDataArray.add(newField);
            }

            PageLinks newPageLinks = new PageLinks();
            newPageLinks.setLinkType(pageLink.getLinkType());
            newPageLinks.setLinkName(pageLink.getLinkName());
            newPageLinks.setObjectKey(pageLink.getObjectKey());
            newPageLinks.setObjectType(pageLink.getObjectType());
            newPageLinks.setFieldData(newFieldDataArray);

            newPageLinksArray.add(newPageLinks);
        }
        //endregion

        //region actions
        List<Actions> actionsList = new Select().from(Actions.class).where("InfoId = ?", info.getId()).execute();
        for (Actions actions : actionsList) {
            Actions newAction = new Actions();
            newAction.setActionID(actions.getActionID());
            newAction.setActionName(actions.getActionName());
            newAction.setEnabled(actions.getEnabled());
            newAction.setConfirmation(actions.getConfirmation());
            newAction.setCargo(actions.getCargo());

            newActionsArray.add(newAction);
        }
        //endregion

        info.setmObjectInfo(loadedObjectModels);
        info.setPageLinkses(newPageLinksArray);
        info.setActions(newActionsArray);

        return infoObject;
    }

    public interface InfoCallback {
        void onInfoCallSuccess(InfoObject infoObject);

        void onInfoCallFail(String error);

        void onGetWOCategoriesSuccess(List<DataLookUp> dataResponse);

        void onGetUpdateObjectSuccess(DataUpdateStatus updateStatus, String objectKey, JSONArray jsonfieldData);

        void onGetUpdateObjectError(String error);

        void onExecuteActionSuccess(DataUpdateStatus response);

        void onExecuteActionError(DataUpdateStatus response);
    }
}
