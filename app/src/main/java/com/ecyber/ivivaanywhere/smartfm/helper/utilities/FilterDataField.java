package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import android.widget.Toast;

import com.ecyber.ivivaanywhere.smartfm.models.DataLookUp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lakmal on 5/4/2016.
 */
public class FilterDataField {



    public FilterDataField() {
    }

    public List<DataLookUp> filterLookupData(List<DataLookUp> lookUpList, String query) {
        List<DataLookUp> filteredList = new ArrayList<>();
        for (DataLookUp dataLookUp : lookUpList) {
            boolean result = false;
            if (!Utility.IsEmpty(dataLookUp.getDisplayText())) {
                 result = StringLikes.like(dataLookUp.getDisplayText().toString(),query);
            }

            if(result){
                filteredList.add(dataLookUp);
            }
        }
        return filteredList;
    }
/*
    private boolean like(String str, String expr) {
        boolean matches = false;
        expr = expr.toLowerCase();
        expr = expr.replace(".", "\\.");
        expr = expr.replace("?", ".");
        expr = expr.replace("%", ".*");
        str = str.toLowerCase();
        try {
            matches = str.matches(".*" + expr + ".*");
        } catch (Exception e) {
            return false;
        }
        return matches;
    }*/
}
