package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/11/16.
 */
@Table(name = "ObjectType")
public class ObjectType extends Model{

    @Column(name = "MenuType")
    @SerializedName("MenuType")
    @Expose
    public String MenuType;

    @Column(name = "MenuLink")
    @SerializedName("MenuLink")
    @Expose
    public String MenuLink;

    @Column(name = "URL")
    @SerializedName("URL")
    @Expose
    public String URL;

    @Column(name = "ObjectType")
    @SerializedName("ObjectType")
    @Expose
    public String ObjectType;

    @Column(name = "MenuName")
    @SerializedName("MenuName")
    public String MenuName;

    @Column(name = "ObjectIconPath")
    @SerializedName("ObjectIconPath")
    public String ObjectIconPath;

    @SerializedName("Stages")
    @Expose
    List<Stages> Stages;

    @SerializedName("Tabs")
    @Expose
    List<Tabs> Tabs;

    @SerializedName("Roles")
    @Expose
    List<Roles> Roles;

    @Column(name = "CustomFilterPage")
    @SerializedName("CustomFilterPage")
    @Expose
    public String CustomFilterPage;

    @Column(name = "CreatePage")
    @SerializedName("CreatePage")
    @Expose
    public String CreatePage;

    @Column(name = "QRFormatiOS")
    @SerializedName("QRFormatiOS")
    @Expose
    public String QRFormatiOS;

    @SerializedName("CreateFromMenu")
    @Expose
    public List<CreateFromMenu> mCreateFromMenuList;

    @Column(name = "QRFormatAndroid")
    @SerializedName("QRFormatAndroid")
    @Expose
    public String QRFormatAndroid;

    @Column(name = "MovetoDetailPage")
    @SerializedName("MovetoDetailPage")
    @Expose
    public String MovetoDetailPage;

    @Column(name = "FilterListPage")
    @SerializedName("FilterListPage")
    @Expose
    public String FilterListPage;

    public ObjectType() {
        super();
    }

    public ObjectType(String menuType, String menuLink, String URL, String objectType, String menuName, String objectIconPath, List<com.ecyber.ivivaanywhere.smartfm.models.Stages> stages, List<com.ecyber.ivivaanywhere.smartfm.models.Tabs> tabs, List<com.ecyber.ivivaanywhere.smartfm.models.Roles> roles, String customFilterPage, String createPage, String QRFormatiOS, List<CreateFromMenu> mCreateFromMenuList, String QRFormatAndroid, String movetoDetailPage) {
        MenuType = menuType;
        MenuLink = menuLink;
        this.URL = URL;
        ObjectType = objectType;
        MenuName = menuName;
        ObjectIconPath = objectIconPath;
        Stages = stages;
        Tabs = tabs;
        Roles = roles;
        CustomFilterPage = customFilterPage;
        CreatePage = createPage;
        this.QRFormatiOS = QRFormatiOS;
        this.mCreateFromMenuList = mCreateFromMenuList;
        this.QRFormatAndroid = QRFormatAndroid;
        MovetoDetailPage = movetoDetailPage;
    }

    public String getMenuType() {
        return MenuType;
    }

    public void setMenuType(String menuType) {
        MenuType = menuType;
    }

    public String getMenuLink() {
        return MenuLink;
    }

    public void setMenuLink(String menuLink) {
        MenuLink = menuLink;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public String getCustomFilterPage() {
        return CustomFilterPage;
    }

    public void setCustomFilterPage(String customFilterPage) {
        CustomFilterPage = customFilterPage;
    }

    public String getCreatePage() {
        return CreatePage;
    }

    public void setCreatePage(String createPage) {
        CreatePage = createPage;
    }

    public String getQRFormatiOS() {
        return QRFormatiOS;
    }

    public void setQRFormatiOS(String QRFormatiOS) {
        this.QRFormatiOS = QRFormatiOS;
    }

    public String getQRFormatAndroid() {
        return QRFormatAndroid;
    }

    public void setQRFormatAndroid(String QRFormatAndroid) {
        this.QRFormatAndroid = QRFormatAndroid;
    }

    public List<com.ecyber.ivivaanywhere.smartfm.models.Stages> getStages() {
        return Stages;
    }

    public void setStages(List<com.ecyber.ivivaanywhere.smartfm.models.Stages> stages) {
        Stages = stages;
    }

    public List<com.ecyber.ivivaanywhere.smartfm.models.Tabs> getTabs() {
        return Tabs;
    }

    public void setTabs(List<com.ecyber.ivivaanywhere.smartfm.models.Tabs> tabs) {
        Tabs = tabs;
    }

    public List<com.ecyber.ivivaanywhere.smartfm.models.Roles> getRoles() {
        return Roles;
    }

    public void setRoles(List<com.ecyber.ivivaanywhere.smartfm.models.Roles> roles) {
        Roles = roles;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getObjectIconPath() {
        return ObjectIconPath;
    }

    public void setObjectIconPath(String objectIconPath) {
        ObjectIconPath = objectIconPath;
    }

    public List<CreateFromMenu> getCreateFromMenuList() {
        return mCreateFromMenuList;
    }

    public void setCreateFromMenuList(List<CreateFromMenu> createFromMenuList) {
        mCreateFromMenuList = createFromMenuList;
    }

    public String getMovetoDetailPage() {
        return MovetoDetailPage;
    }

    public void setMovetoDetailPage(String movetoDetailPage) {
        MovetoDetailPage = movetoDetailPage;
    }

    public String getFilterListPage() {
        return FilterListPage;
    }

    public void setFilterListPage(String filterListPage) {
        FilterListPage = filterListPage;
    }

    public List<ObjectType> getAllObjectTypes(){
        return new Select()
                .from(ObjectType.class)
                .execute();
    }
    
    public void clearTable(){
        new Delete().from(ObjectType.class).execute();
    }
}
