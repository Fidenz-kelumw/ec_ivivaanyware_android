package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Choota on 3/8/17.
 * Email   : chathura93@yahoo.com
 * GitHub  : https://github.com/chathurahettiarachchi
 * Company : Fidenz Technologies
 */

@Table(name="MessageType")
public class MessageType extends Model {

    @Column(name = "TypeID")
    @SerializedName("TypeID")
    @Expose
    public String TypeID;

    @Column(name = "TypeName")
    @SerializedName("TypeName")
    @Expose
    public String TypeName;

    @Column(name = "Object_Key")
    @Expose
    public String mObjectKey;

    public MessageType() {
    }

    public MessageType(String typeID, String typeName, String mObjectKey) {
        TypeID = typeID;
        TypeName = typeName;
        this.mObjectKey = mObjectKey;
    }

    public String getmObjectKey() {
        return mObjectKey;
    }

    public void setmObjectKey(String mObjectKey) {
        this.mObjectKey = mObjectKey;
    }

    public String getTypeID() {
        return TypeID;
    }

    public void setTypeID(String typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }
}
