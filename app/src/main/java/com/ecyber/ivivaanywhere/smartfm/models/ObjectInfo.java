package com.ecyber.ivivaanywhere.smartfm.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lakmal on 4/29/2016.
 */

@Table(name = "ObjectInfo")
public class ObjectInfo extends Model {
    /*          {
      "FieldID": "Type",
      "FieldName": "Work Order Type",
      "ValueType": "DS",
      "DefaultData": {
        "DisplayText": "Corrective",
        "Value": "1"
      },
      "Editable": "1",
      "OptionList": [
        {
          "DisplayText": "Corrective",
          "Value": "1"
        },
        {
          "DisplayText": "Planned",
          "Value": "2"
        }
      ],
      "LookUpService": "",
      "Mandatory": "1",
      "DownloadFolder": "/AccountResources/SmartWP",
      "UploadFolder": "/UploadContent/SmartWP"
    }*/
    @Column(name = "InfoID")
    @SerializedName("InfoID")
    @Expose
    public long mInfoID;

    @Column(name = "FieldID")
    @SerializedName("FieldID")
    @Expose
    public String mFieldID;

    @Column(name = "FieldName")
    @SerializedName("FieldName")
    @Expose
    public String mFieldName;

    @Column(name = "ValueType")
    @SerializedName("ValueType")
    @Expose
    public String mValueType;


    @Column(name = "DefaultDataID")
    @SerializedName("DefaultDataID")
    @Expose
    public long mDefaultDataID;

    @SerializedName("DefaultData")
    @Expose
    public DefaultData mDefaultData;

    @Column(name = "Editable")
    @SerializedName("Editable")
    @Expose
    public int mEditable;

    @SerializedName("OptionList")
    @Expose
    public List<OptionList> mOptionList;

    @Column(name = "LookUpService")
    @SerializedName("LookUpService")
    @Expose
    public String mLookUpService;

    @Column(name = "Mandatory")
    @SerializedName("Mandatory")
    @Expose
    public int mMandatory;

    @Column(name = "DownloadFolder")
    @SerializedName("DownloadFolder")
    @Expose
    public String mDownloadFolder = "";

    @Column(name = "UploadFolder")
    @SerializedName("UploadFolder")
    @Expose
    public String mUploadFolder = "";

    @Column(name = "NoDataValue")
    @SerializedName("NoDataValue")
    @Expose
    public String noDataValue;

    public ObjectInfo() {
        super();
    }

    public ObjectInfo(long infoID, String fieldID, String fieldName, String valueType, long defaultDataID, int editable, String lookUpService, int mandatory, String downloadFolder, String uploadFolder) {
        mInfoID = infoID;
        mFieldID = fieldID;
        mFieldName = fieldName;
        mValueType = valueType;
        mDefaultDataID = defaultDataID;
        mEditable = editable;
        mLookUpService = lookUpService;
        mMandatory = mandatory;
        mDownloadFolder = downloadFolder;
        mUploadFolder = uploadFolder;
    }

    public long getDefaultDataID() {
        return mDefaultDataID;
    }

    public void setDefaultDataID(long defaultDataID) {
        mDefaultDataID = defaultDataID;
    }

    public long getInfoID() {
        return mInfoID;
    }

    public void setInfoID(long infoID) {
        mInfoID = infoID;
    }

    public List<ObjectInfo> getAll() {
        return new Select().from(ObjectInfo.class).execute();
    }

    public String getFieldID() {
        return mFieldID;
    }

    public void setFieldID(String fieldID) {
        mFieldID = fieldID;
    }

    public String getFieldName() {
        return mFieldName;
    }

    public void setFieldName(String fieldName) {
        mFieldName = fieldName;
    }

    public String getValueType() {
        return mValueType;
    }

    public void setValueType(String valueType) {
        mValueType = valueType;
    }

    public DefaultData getDefaultData() {
        return mDefaultData;
    }

    public void setDefaultData(DefaultData defaultData) {
        mDefaultData = defaultData;
    }

    public int getEditable() {
        return mEditable;
    }

    public void setEditable(int editable) {
        mEditable = editable;
    }

    public List<OptionList> getOptionList() {
        return mOptionList;
    }

    public void setOptionList(List<OptionList> optionList) {
        mOptionList = optionList;
    }

    public String getLookUpService() {
        return mLookUpService;
    }

    public void setLookUpService(String lookUpService) {
        mLookUpService = lookUpService;
    }

    public int getMandatory() {
        return mMandatory;
    }

    public void setMandatory(int mandatory) {
        mMandatory = mandatory;
    }

    public String getDownloadFolder() {
        return mDownloadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        mDownloadFolder = downloadFolder;
    }

    public String getUploadFolder() {
        return mUploadFolder;
    }

    public void setUploadFolder(String uploadFolder) {
        mUploadFolder = uploadFolder;
    }
}
