package com.ecyber.ivivaanywhere.smartfm.services.sync;

import android.content.Context;
import android.util.Log;

import com.ecyber.ivivaanywhere.smartfm.R;
import com.ecyber.ivivaanywhere.smartfm.helper.errormanager.ErrorLog;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.AccountPermission;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartSharedPreferences;
import com.ecyber.ivivaanywhere.smartfm.helper.utilities.Utility;
import com.ecyber.ivivaanywhere.smartfm.models.AttachmentMaster;
import com.ecyber.ivivaanywhere.smartfm.services.api.ServiceGenerator;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.AddNewAttachmentResponse;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewAttachmentSync implements MultiPartUploadSync.AttachmentUploadCallback {

    class UploadDetails {
        public String mObjectType;
        public String mObjectKey;
        public String mTabName;
        public String mAttachmentType;
        public String mAttachmentName;
        public String mFileName;
        public String mATTType;
        public String mUrl;
        public String mApiKey;
        public String mUserKey;

        public UploadDetails(String objectType,
                             String objectKey,
                             String tabName,
                             String attachmentType,
                             String attachmentName,
                             String fileName,
                             String ATTType,
                             String url,
                             String apiKey,
                             String userKey) {
            mObjectType = objectType;
            mObjectKey = objectKey;
            mTabName = tabName;
            mAttachmentType = attachmentType;
            mAttachmentName = attachmentName;
            mFileName = fileName;
            mATTType = ATTType;
            mUrl = url;
            mApiKey = apiKey;
            mUserKey = userKey;
        }
    }

    private AddNewAttachmentCallBack mAddNewAttachmentCallBack;
    private Context mContext;
    private UploadDetails mLastUpload = null;

    public AddNewAttachmentSync(AddNewAttachmentCallBack addNewAttachmentCallBack, Context context) {
        mAddNewAttachmentCallBack = addNewAttachmentCallBack;
        mContext = context;
    }

    public void addNewAttachment(String url, String apiKey, String userKey, String objectType, String objectKey, String tabName,
                                 String attachmentType, String attachmentName, String b64code, String sourceFileName) {
        Log.e("FM-A", b64code.length() + "");
        ServiceGenerator.CreateServiceWithTimeout(com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.AddNewAttachment.class, url)
                .addNewAttachments(apiKey, userKey, objectType, objectKey, tabName, attachmentType, attachmentName, b64code, sourceFileName)
                .enqueue(new Callback<AddNewAttachmentResponse>() {
                    @Override
                    public void onResponse(Call<AddNewAttachmentResponse> call, Response<AddNewAttachmentResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String message = response.body().getMessage();
                                if (response.body().getSuccess() == 1) {
                                    mAddNewAttachmentCallBack.OnAddNewAttachmentSuccess(response.body());
                                } else {
                                    mAddNewAttachmentCallBack.OnAddNewAttachmentError(Utility.IsEmpty(message) ? SmartConstants.ADD_ATTACH_FAILED : message);
                                }
                            } else {
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Add New Attachment", message);
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddNewAttachmentResponse> call, Throwable t) {
                        mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                    }
                });

    }

    public void addNewAttachmentEX(String objectType,
                                   String objectKey,
                                   String tabName,
                                   String attachmentType,
                                   String attachmentName,
                                   String fileName,
                                   String ATTType,
                                   String filePath) {

        AccountPermission acc = new AccountPermission();

        String url = acc.getAccountInfo().get(0);
        String apiKey = acc.getAccountInfo().get(1);
        String userKey = acc.getAccountInfo().get(2);

        mLastUpload = new UploadDetails(objectType, objectKey, tabName, attachmentType, attachmentName, fileName, ATTType, url, apiKey, userKey);

        File uploadFile = new File(filePath);
        String uploadPath = "UploadContent/SmartFM";

        new MultiPartUploadSync().uploadFile(this, url, apiKey, uploadPath, uploadFile, fileName);
    }


    public interface AddNewAttachmentCallBack {
        void OnAddNewAttachmentSuccess(AddNewAttachmentResponse addNewAttachmentResponse);

        void OnAddNewAttachmentError(String error);
    }

    @Override
    public void attachmentSuccess(String fileName, File file) {

        ServiceGenerator.CreateServiceWithTimeout(com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces.AddNewAttachment.class, mLastUpload.mUrl)
                .addNewAttachments(mLastUpload.mApiKey, mLastUpload.mUserKey, mLastUpload.mObjectType, mLastUpload.mObjectKey, mLastUpload.mTabName, mLastUpload.mAttachmentType, mLastUpload.mAttachmentName, mLastUpload.mFileName, mLastUpload.mATTType)
                .enqueue(new Callback<AddNewAttachmentResponse>() {
                    @Override
                    public void onResponse(Call<AddNewAttachmentResponse> call, Response<AddNewAttachmentResponse> response) {

                        mLastUpload = null;

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String message = response.body().getMessage();
                                if (response.body().getSuccess() == 1) {
                                    mAddNewAttachmentCallBack.OnAddNewAttachmentSuccess(response.body());
                                } else {
                                    mAddNewAttachmentCallBack.OnAddNewAttachmentError(Utility.IsEmpty(message) ? SmartConstants.ADD_ATTACH_FAILED : message);
                                }
                            } else {
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                            }
                        } else {
                            try {
                                String message = response.errorBody().string();
                                Log.d("SmartFm - ErrorLog", message);
                                ErrorLog.saveErrorLog("Add New Attachment", message);
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddNewAttachmentResponse> call, Throwable t) {

                        mLastUpload = null;

                        mAddNewAttachmentCallBack.OnAddNewAttachmentError(SmartConstants.ADD_ATTACH_FAILED);
                    }
                });
    }

    @Override
    public void attachmentFailed(String fileName, File file) {
        mLastUpload = null;
    }
}
