package com.ecyber.ivivaanywhere.smartfm.helper.utilities;

import com.ecyber.ivivaanywhere.smartfm.models.Registration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChathuraHettiarachchi on 4/7/16.
 */
public class AccountPermission {
    public AccountPermission() {
    }

    public List<String> getAccountInfo(){
        List<String> result = new ArrayList<>();
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result.add(registrationList.get(0).Account);
            result.add(registrationList.get(0).ApiKey);
            result.add(registrationList.get(0).UserKey);
        }
        return result;
    }

    public String getMasterDataRelodTime(){
        String result = "";
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result = registrationList.get(0).MasterDataExpiryHours;
        }
        return result;
    }

    public String getUser(){
        String result = "";
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result = registrationList.get(0).UserName;
        }
        return result;
    }

    public String getTransactionDataRelodTime(){
        String result = "";
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result = registrationList.get(0).TransactionExpirySeconds;
        }
        return result;
    }

    public String getAccount(){
        String result = "";
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result = registrationList.get(0).Account;
        }
        return result;
    }

    public String getDomain(){
        String result = "";
        List<Registration> registrationList = new Registration().getAllRegistrations();
        if(registrationList != null){
            result = registrationList.get(0).Domain;
        }
        return result;
    }
}
