package com.ecyber.ivivaanywhere.smartfm.services.api.restinterfaces;

import com.ecyber.ivivaanywhere.smartfm.helper.utilities.SmartConstants;
import com.ecyber.ivivaanywhere.smartfm.services.api.models.LookupServiceResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Lakmal on 5/6/2016.
 */
public interface GetDataLookupService {
    @FormUrlEncoded
    @POST(SmartConstants.API_PATH + "{path}")
    Call<LookupServiceResponse> getLookupService(@Path("path") String lookupservice,
                                                 @Field("UserKey") String userKey,
                                                 @Field("apikey") String apikey,
                                                 @Field("LocationKey") String locationKey);
}
